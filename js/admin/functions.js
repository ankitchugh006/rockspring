//function for changing the status of a record
function change_status(id,status){
	$.ajax({
			type:'POST',
			url:base_url+'/admin/users/change_status',
			data:{id:id,status:status},
			success:function(response){
				//alert(response);
				window.location.reload();
			}
		
	})
}
function change_complete_status(id,status){
	$.ajax({
			type:'POST',
			url:base_url+'/admin/projects/change_complete_status',
			data:{id:id,status:status},
			success:function(response){
				//alert(response);
				window.location.reload();
			}
		
	})
}

function change_task_status(id,status){
	$.ajax({
			type:'POST',
			url:base_url+'/admin/projects/tasks/change_task_status',
			data:{id:id,status:status},
			success:function(response){
				//alert(response);
				window.location.reload();
			}
		
	})
}

$('.highlight').click(function() {
	$(this).hide();
	$.ajax({
		type: 'post',
		url: base_url+'/admin/notification/unhighlight',
		success: function(res) {

		}
	});
});

$('.sort_select').change(function() {
        $("#sort_form").submit();
});


$('.text_hours').blur(function() {
        var id=$(this).attr('id');
        var index = id.substr(id.length - 1);
	var hours_logged=$(this).val();
	var foreman_id=$("#foreman_id"+index).val();
	var log_id=$("#log_id"+index).val();
	
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/logs/overrideAjax',
		data:{hours_logged:hours_logged,log_id:log_id,foreman_id:foreman_id},
		success: function(res) {
                        $("#text_hours"+index).hide();
                        $('#hours_go'+index).hide();
                        
                        $("#display_hours"+index).html(hours_logged);
                        $("#display_hours"+index).show();
                        
                         swal({
                          title: "Hours Updated!",
                          text: "",
                          timer: 2000,
                          showConfirmButton: false,
                          type: "success"
                        });
		}
	});
});

function override_hours(log_id,index){
        $('.hours_none').hide();
        $('.display_hours').show();
        
        $("#display_hours"+index).hide();
        $("#text_hours"+index).show();
        $("#text_hours"+index).focus();
        $('#hours_go'+index).show();
       
}

$('.text_quantity').blur(function() {
        var id=$(this).attr('id');
        var index = id.substr(id.length - 1);
	var quantity=$(this).val();
	var foreman_id=$("#foreman_id"+index).val();
	var request_id=$("#request_id"+index).val();
	
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/requests/overrideManpowerAjax',
		data:{quantity:quantity,request_id:request_id,foreman_id:foreman_id},
		success: function(res) {
                        $("#text_quantity"+index).hide();
                        $('#quantity_go'+index).hide();
                        
                        $("#display_quantity"+index).html(quantity);
                        $("#display_quantity"+index).show();
                        
                        swal({
                          title: "Manpower Updated!",
                          text: "",
                          timer: 2000,
                          showConfirmButton: false,
                          type: "success"
                        });
		}
	});
});

$('#user_role').change(function(){

        var val=$(this).val();
        var html='';
        if(val=='role_foreman'){
				html +='<input type="hidden" name="job_role" value="job_foreman">';
        }else if(val=='role_administrator'){

				html +='<label>Job Role</label>';
				html +='<select class="form-control" name="job_role" id="job_role">';			
				html +='<option value="job_administrator">Administrator</option>';
				html +='<option value="job_manager">Project Manager</option>';
				html +='<option value="job_assistant">Assistant Project Manger</option>';
				html +='<option value="job_superintendent">Superintendent</option>';
				html +='<option value="job_estimator">Estimator</option>';
				html +='</select>';
				
				
        }
        $("#job_role_container").html(html);
});

function override_manpower(request_id,index){
        $('.quantity_none').hide();
        $('.display_quantity').show();
        
        
        $("#display_quantity"+index).hide();
        $("#text_quantity"+index).show();
        $("#text_quantity"+index).focus();
        $('#quantity_go'+index).show();
}

$(document).keypress(function(e) {
    if(e.which == 13) {
        if($(".text_quantity:focus")){
                $(".text_quantity:focus").trigger('blur');
        }
        
        if($(".text_hours:focus")){
                $(".text_hours:focus").trigger('blur');
        }
    }
});

var customer_count=$('#customer_count').val();
$("#add_customer").on('click',function(){
	customer_count++;
	var html='<tr class="customer_row" id="customer_count-'+customer_count+'">';
	html += '<td>';
	html += ' <input type="hidden" name="c_id[]" value="0" />';
	html += '<input value="" placeholder="name" name="c_name[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<select name="c_role[]" class="form-control" required>';
	html += '<option value="">-Title-</option>';
	html += '<option value="PM">PM</option>';
	html += '<option value="APM">APM</option>';
	html += '<option value="SUPER">Superintendent</option>';
	html += '</select>';
	html += '</td>';
	html += '<td>';
	html += '<input autocomplete="off" type="email" value="" placeholder="email" name="c_email[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<input autocomplete="off" value="" placeholder="phone" name="c_phone[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<input autocomplete="off" value="" placeholder="office phone" name="o_phone[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<a href="javascript:;" onclick="customer_remove('+customer_count+')"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>';
	html += '</td>';
	html += '</tr>';
	
         $('#member_row_inner tr:last').after(html);
        
	//$('#customer_members').append(html);
});


function customer_remove(count){	
	$('#customer_count-'+count).remove();
}



$("#add_contractor").on('click',function(){
	customer_count++;
	var html='<tr class="customer_row" id="customer_count-'+customer_count+'">';
	html += '<td>';
	html += ' <input type="hidden" name="c_id[]" value="0" />';
	html += '<input value="" placeholder="name" name="c_name[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<select name="c_role[]" class="form-control" required>';
	html += '<option value="">-Title-</option>';
	html += '<option value="PM">PM</option>';
	html += '<option value="APM">APM</option>';
	html += '<option value="SUPER">Superintendent</option>';
	html += '</select>';
	html += '</td>';
	html += '<td>';
	html += '<input autocomplete="off" type="email" value="" placeholder="email" name="c_email[]" class="form-control" required>';
	html += '</td>';
	html += '<td>';
	html += '<input autocomplete="off" value="" placeholder="phone" name="c_phone[]" class="form-control" required>';
	html += '</td>';
	
	html += '<td>';
	html += '<a href="javascript:;" onclick="customer_remove('+customer_count+')"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>';
	html += '</td>';
	html += '</tr>';
	
         $('#member_row_inner tr:last').after(html);
        
	//$('#customer_members').append(html);
});





var jh=$("#jh").val();

$("#project_customer").on('change',function(){
	
	jh++;
	
	var customer_id=$(this).val();
	
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/getCustomerMember',
		data:{customer_id:customer_id},
		dataType:'json',
		success: function(data) {
			 var html='';
			 
			 if(data != 0){
			
				
				html += '<table id="member_row_inner"  class="table table-hover table-striped member_table_'+jh+'">';
                                html += '<tr>';
                                html += '<th>Report?</th>';
                                html += '<th>Member</th>';
                                html += '<th>Title</th>';
                                html += '<th>Email</th>';
                                html += '<th>phone</th>';
                                html += '<th>';
                                html += '<div class="add-member" id="add-'+jh+'">';
                                html += '<a href="javascript:;" onclick="member_add('+jh+')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';	
                                html += '</th>';
                                html += '</tr>';
                                
                                html += '<tr id="member_row_'+jh+'" class="member_row">';
                                html += '<td>';
				html += '<input type="checkbox" name="member_customer_check[]" value='+jh+' >';
				html += '</td>';
                              
                                 html += '<td>';
				 html += '<select id="member-'+jh+'" class="form-control choosen_select" name="member_customer['+jh+']" onChange="getMemberData(this.value,'+jh+')">';
				 html += '<option value="">-Members-</option>';
				 $.each(data, function(index, element) {
						/*html +='<div class="row customer_row" id="customer_count-'+jh+'">';
						html += '<div class="col-lg-1">';
						html += '<input type="checkbox" name="member_customer[]" value="'+element.id+'">';
						html += '</div>';
						html += '<div class="col-lg-3">';*/
						
						html += '<option value="'+element.id+'">'+element.name+'</option>';
						
						/*html += '</div>';
		
						html += '<div class="col-lg-2">';
						html += element.role;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.email;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.phone;
						html += '</div>';
						html += '</div>';*/
				 });
				 html += '</select>';
				 html += '</td>';
                                
                                
                                 html += '<td>';
				 html += '<span id="title-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 
				 html += '<td>';
				 html += '<span id="email-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 html += '<td>';
				 html += '<span id="phone-'+jh+'"></span>';
				 html += '</td>';
                                 
				 html += '<td>';
				 html += '';
				 html += '</td>';
                                 
				 
				 html += '</table>';
				
			 }
             $("#project_customer_members").html(html); 
             $("#project_customer_members1").html(html); 
			 $(".choosen_select").chosen();         
		}
	});
	
	
});

function getMemberData(id,index){
	
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/getMemberData',
		data:{member_id:id},
		dataType:'json',
		success: function(data) {
			if(data!=0){
				$("#title-"+index).html(data.role); 
				$("#email-"+index).html(data.email); 
				$("#phone-"+index).html(data.phone); 
				$("#add-"+index).show();
			}
		}
	});
	
}
function member_add(index){
	jh++;
	var customer_id=$("#project_customer").val();
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/getCustomerMember',
		data:{customer_id:customer_id},
		dataType:'json',
		success: function(data) {
			var html='';
			 
			 if(data != 0){
			
				
                                html += '<tr id="member_row_'+jh+'" class="member_row">';
                                html += '<td>';
				html += '<input type="checkbox" name="member_customer_check[]" value='+jh+' >';
				html += '</td>';
                              
                                 html += '<td>';
				 html += '<select id="member-'+jh+'" class="form-control choosen_select" name="member_customer['+jh+']" onChange="getMemberData(this.value,'+jh+')">';
				 html += '<option value="">-Members-</option>';
				 $.each(data, function(index, element) {
						/*html +='<div class="row customer_row" id="customer_count-'+jh+'">';
						html += '<div class="col-lg-1">';
						html += '<input type="checkbox" name="member_customer[]" value="'+element.id+'">';
						html += '</div>';
						html += '<div class="col-lg-3">';*/
						
						html += '<option value="'+element.id+'">'+element.name+'</option>';
						
						/*html += '</div>';
		
						html += '<div class="col-lg-2">';
						html += element.role;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.email;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.phone;
						html += '</div>';
						html += '</div>';*/
				 });
				 html += '</select>';
				 html += '</td>';
                                
                                
                                 html += '<td>';
				 html += '<span id="title-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 
				 html += '<td>';
				 html += '<span id="email-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 html += '<td>';
				 html += '<span id="phone-'+jh+'"></span>';
				 html += '</td>';
                                 
				 html += '<td>';
				 html += '<a href="javascript:;" onclick="member_remove('+jh+')"><span class="glyphicon glyphicon-minus" style="color:red"></span></a>';	
				 html += '</td>';
                                 html += '</tr>';
                                 console.log(html);

			 }
                   // $(".member_row_inner").html(html);
                    $('#member_row_inner tr:last').after(html);
                    $(".choosen_select").chosen();         
		}
	});
        
        
}

function member_remove(index){
	$("#member_row_"+index).remove();
}



//get customer member when page load
$(document).ready(function(){
	jh++;
	var customer_id=$("#project_customer").val();
	$.ajax({
		type: 'post',
		url: base_url+'/admin/projects/getCustomerMember',
		data:{customer_id:customer_id},
		dataType:'json',
		success: function(data) {
			 var html='';
			 
			 if(data != 0){
			
				
				html += '<table id="member_row_inner"  class="table table-hover table-striped member_table_'+jh+'">';
                                html += '<tr>';
                                html += '<th>Report?</th>';
                                html += '<th>Member</th>';
                                html += '<th>Title</th>';
                                html += '<th>Email</th>';
                                html += '<th>phone</th>';
                                html += '<th>';
                                html += '<div class="add-member" id="add-'+jh+'">';
                                html += '<a href="javascript:;" onclick="member_add('+jh+')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';	
                                html += '</th>';
                                html += '</tr>';
                                
                                html += '<tr id="member_row_'+jh+'" class="member_row">';
                                html += '<td>';
				html += '<input type="checkbox" name="member_customer_check[]" value='+jh+' >';
				html += '</td>';
                              
                                 html += '<td>';
				 html += '<select id="member-'+jh+'" class="form-control choosen_select" name="member_customer['+jh+']" onChange="getMemberData(this.value,'+jh+')">';
				 html += '<option value="">-Members-</option>';
				 $.each(data, function(index, element) {
						/*html +='<div class="row customer_row" id="customer_count-'+jh+'">';
						html += '<div class="col-lg-1">';
						html += '<input type="checkbox" name="member_customer[]" value="'+element.id+'">';
						html += '</div>';
						html += '<div class="col-lg-3">';*/
						
						html += '<option value="'+element.id+'">'+element.name+'</option>';
						
						/*html += '</div>';
		
						html += '<div class="col-lg-2">';
						html += element.role;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.email;
						html += '</div>';
						html += '<div class="col-lg-3">';
						html += element.phone;
						html += '</div>';
						html += '</div>';*/
				 });
				 html += '</select>';
				 html += '</td>';
                                
                                
                                 html += '<td>';
				 html += '<span id="title-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 
				 html += '<td>';
				 html += '<span id="email-'+jh+'"></span>';
				 html += '</td>';
                                 
                                 html += '<td>';
				 html += '<span id="phone-'+jh+'"></span>';
				 html += '</td>';
                                 
				 html += '<td>';
				 html += '';
				 html += '</td>';
                                 
				 
				 html += '</table>';
				
			 }
             $("#project_customer_members").html(html); 
			 $(".choosen_select").chosen();           
		}
	});
	
	
	//report send email
	
	$("#report_send").submit(function(e){
		e.preventDefault();
		var email=$("#report_email").val();
		$.ajax({
			type: 'post',
			url: base_url+'/admin/report/sendadminreport',
			data:{email:email},
			success: function(data) {
				alert('Email sent');
				$('#report_send')[0].reset();
			}
		});	
	});
	
	$("#estimate_report_send").submit(function(e){
		e.preventDefault();
		var email=$("#estimate_report_email").val();
		$.ajax({
			type: 'post',
			url: base_url+'/admin/report/estimatesendadminreport',
			data:{email:email},
			success: function(data) {
				alert('Email sent');
				$('#estimate_report_send')[0].reset();
			}
		});	
	});
	
	
	$("#check-all-survey").change(function(){  //"select all" change
		var status = this.checked; // "select all" checked status
		$('.check-all-child').each(function(){ //iterate all listed checkbox items
			this.checked = status; //change ".checkbox" checked status
		});
	});
	
	$('.check-all-child').change(function(){ //".checkbox" change 
		//uncheck "select all", if one of the listed checkbox item is unchecked
		if(this.checked == false){ //if this item is unchecked
			$("#check-all-survey")[0].checked = false; //change "select all" checked status to false
		}
		
		//check "select all" if all checkbox items are checked
		if ($('.check-all-child:checked').length == $('.check-all-child').length ){ 
			$("#check-all-survey")[0].checked = true; //change "select all" checked status to true
		}
	});
});


function validateText(index){
	if($("#check-"+index).is(':checked')){
		$("#input-"+index).attr("required","required");
		$("#input-"+index).removeAttr("readonly");
	}else{
		$("#input-"+index).removeAttr("required");
		
	}
}

var count_check=22222;
function check_subcontractor_manpower(this1,val,workers,index){
	if($.isNumeric(val)){
		var total=0;
		var value_chk=1;
		$("#sab_to_appand_"+index+" .subcontractor_manpower").each(function(){
			total += parseInt($(this).val()) || 0 ;
			if($(this).val().length  < 0){
				value_chk=0;
			}
		});
		
		var total_container=0;
		$("#sab_to_appand_"+index+" .col-md-7").each(function(){
			
			total_container++;
		});
	
		//if(total < parseInt(workers) && total_container < parseInt(workers)){
		if(total < parseInt(workers)){
			console.log(value_chk);
			/*var html_sub="";
			
			var html_sub='<div id="sab_to_appand_inner'+count_check+'"> ';
			html_sub += '<div class="form-group col-md-7">';
			html_sub +='<select   name="subcontractor_id[]"   class="form-control trigger_update" required >';
			html_sub += $("#select_to_copy_new"+index).html();
			html_sub += '</select>';
			html_sub += '</div>';
			html_sub += '<div class="form-group col-md-4">';
			html_sub += '<input type="text" name="subcontractor_quantity[]" class="form-control subcontractor_manpower" required="" onblur="check_subcontractor_manpower(this,this.value,'+workers+','+index+')" id="subcontractor_manpower_'+index+'" placeholder="manpower">';
			html_sub += '</div>';
			html_sub += '<div class="form-group col-md-1" style="margin-top: 7px;" >';
			html_sub += '<a href="javascript:;" onclick="subctr_remove('+count_check+')"><span class="glyphicon glyphicon-minus" style="color:red"></span></a>';
			html_sub += '</div>';
			console.log($("#select_to_copy").html());
			html_sub += '</div>';
			
			$("#sab_to_appand_"+index).append(html_sub);
			$("select").chosen();     
			count_check ++ ;*/
			//var con=confirm("Do you want to proceed?");
			/*if(con){
				
			}else{
				
				$(this1).val('');
			}*/
			
		}else if(total > parseInt(workers)){
			
			var con=confirm("Do you want to override manpower?");
			if(con){
				
			}else{
				$(this1).val('');
			}
		}
	}else{
		alert('Value should be a number!!');
		$(this1).val('');
	}
}	

function check_subcontractor_manpower_save(index,workers){
		var total=0;
		$("#sab_to_appand_"+index+" .subcontractor_manpower").each(function(){
			total += parseInt($(this).val()) || 0 ;
		});
		
		if(total < parseInt(workers)){
			var con=confirm("Do you want to underride manpower?");
			if(con){
				$("#register-form-submit"+index).trigger('click');
			}else{
				return false;
			}
		}else if(total > parseInt(workers)){
			
			var con=confirm("Do you want to override manpower?");
			if(con){
				$("#register-form-submit"+index).trigger('click');
			}else{
				return false;
			}
		}else{
			$("#register-form-submit"+index).trigger('click');
		}
}

function subctr_remove(count_check){
	$("#sab_to_appand_inner"+count_check).remove();
}

function finalizemanpowerschedule(){
	$("#loading_text").html('Sending Emails...');
	$.ajax({
		type: 'post',
		url: base_url+'/admin/report/finalizemanpowerschedule',
		success: function(data) {
			$("#loading_text").html('');
			alert('Email sent!!');
			location.reload();
		}
	});	
}

function updateProject(project_id){ 
	$("#loading_text_update").html('Sending Emails...');
	$.ajax({
		type: 'post',
		data:{project_id:project_id},
		url: base_url+'/admin/projects/update_project_email',
		success: function(data) {
			$("#loading_text_update").html('');
			alert('Email sent!!');
			location.reload();
		}
	});	
}

function setContractorHidden(this1,index){

     var textToAppend = "";
     var selMulti = $("#contractor_select"+index+"  option:selected").each(function(){
           textToAppend += (textToAppend == "") ? "" : ",";
           textToAppend += $(this).val();           
     });
     $("#contractor_hidden"+index).val(textToAppend);

}
jQuery(document).ready(function(){
	$(".manpower_check").each(function(){
		var id = $(this).attr('id');
		console.log(id);
		var index=id.split('-');
		if ($(this).is(':checked')) {
			$("#input-"+index[1]).removeAttr('readonly');
			console.log($("#input-"+index[1]));
		}
	});
});

