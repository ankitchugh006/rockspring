<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\FreeEvent;
use App\Models\Invitations;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\UrlGenerator;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Laravel\Lumen\Routing\Controller as BaseController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FreeEventController extends BaseController {

    public function createFreeEvent(Request $request, UrlGenerator $URLGEN) {

        $validator = Validator::make($request->all(), array(
                    'user_id' => 'required',
                    'event_title' => 'required',
                    'event_duration' => 'required',
                    'user_mobile' => 'required',
                    'location' => 'required',
                    'invites_users' => 'required',
                    'event_description' => 'required',
                    'slots' => 'required'
        ));
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => 'Input Validation Failed'
            ]);
        } else {
            $free_event = FreeEvent::create(array(
                        'user_id' => $request->input('user_id'),
                        'event_title' => $request->input('event_title'),
                        'event_duration' => $request->input('event_duration'),
                        'user_mobile' => $request->input('user_mobile'),
                        'location' => $request->input('location'),
                        'invites_users' => $request->input('invites_users'),
                        'event_description' => $request->input('event_description'),
                        'slots' => $request->input('slots')
            ));
                $insertedEventId = $free_event ->id;
                $invitees=json_decode($request->input('invited_users'),true);
                if(!empty($invitees)){
	                foreach($invitees as $i){
		

		                $insertIn = Invitations::create(array(
			                'event_id' => $insertedEventId,
			                'mobile_number' => $i['user_mobile'],
			                'name' => $i['first_name'],
			                'status' => 'pending'
		                ));
		        
	                }
                }
                    
            
            if ($free_event) {
               
                return response()->json([
                            'success' => true,
                            'code' => 201,
                            'message' => 'Event Add Successfully'
                ]);
            } else {
                return response()->json([
                            'success' => false,
                            'code' => 500,
                            'message' => 'Internal Server Error'
                ]);
            }
        }
    }

}
