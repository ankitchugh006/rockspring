<?php namespace App\Http\Middleware;

use Closure;

class ApiAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->input('key') || $request->input('key') != 'D4788EBA535DC57377BAD975DED90'){
            
            return response()->json(
                    [
                        'code'          =>  404,
                        'success'        =>  false,
                        'error'         =>  'Invalid Api Key, Permission Denied'
                    ]);
        }
        return $next($request);
    }

}
