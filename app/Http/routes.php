<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'admin\HomeController@index'
]);

Route::group(['prefix' => 'api'], function () {
    Route::post('login', [
        'as' => 'login',
       // 'uses' => 'api\ApiAuthController@login'
        'uses' => 'ApiAuthController@login',
        'middleware' => 'ApiAuth'
    ]);
    Route::post('register', [
        'as' => 'register',
        'uses' => 'ApiAuthController@register',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('getprofile', [
        'as' => 'getprofile',
        'uses' => 'ApiAuthController@get_profile',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('updateprofile', [
        'as' => 'updateprofile',
        'uses' => 'ApiAuthController@update_profile',
        'middleware' => 'ApiAuth'
    ]); 
    
	Route::post('logout', [
        'as' => 'logout',
        'uses' => 'ApiAuthController@logout',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('recoverpassword', [
        'as' => 'recoverpassword',
        'uses' => 'ApiAuthController@recoverpassword',
        'middleware' => 'ApiAuth'
    ]); 
    Route::get('update_password', [
        'as' => 'update_password',
        'uses' => 'ApiAuthController@update_password',
        'middleware' => 'ApiAuth'
    ]); 	
	Route::post('changepassword', [
        'as' => 'changepassword',
        'uses' => 'ApiAuthController@changepassword',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('change_password_app', [
        'as' => 'update_password_app',
        'uses' => 'ApiAuthController@update_password_app',
        'middleware' => 'ApiAuth'
    ]); 
    
    
    /**********projects api*************/
    Route::post('myprojects', [
        'as' => 'myprojects',
        'uses' => 'ApiProjectController@myprojects',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('requestresources', [
        'as' => 'requestresources',
        'uses' => 'ApiProjectController@request_resources',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('worklog', [
        'as' => 'worklog',
        'uses' => 'ApiProjectController@worklog',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('checkworklog', [
        'as' => 'checkworklog',
        'uses' => 'ApiProjectController@checkworklog',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('submitworklog', [
        'as' => 'submitworklog',
        'uses' => 'ApiProjectController@submit_worklog',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('getcontractors', [
        'as' => 'getcontractors',
        'uses' => 'ApiProjectController@getContractors',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('checksurvey', [
        'as' => 'checksurvey',
        'uses' => 'ApiProjectController@checksurvey',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('submitdailysurvey', [
        'as' => 'submitdailysurvey',
        'uses' => 'ApiProjectController@submit_daily_survey',
        'middleware' => 'ApiAuth'
    ]);
    
    Route::get('getfilecategories', [
        'as' => 'getfilecategories',
        'uses' => 'ApiProjectController@getFileCategories',
        'middleware' => 'ApiAuth'
    ]);
     Route::post('addfile', [
        'as' => 'addfile',
        'uses' => 'ApiProjectController@submitFile',
        'middleware' => 'ApiAuth'
    ]);
    Route::post('submittask', [
        'as' => 'submittask',
        'uses' => 'ApiProjectController@submitTask',
        'middleware' => 'ApiAuth'
    ]);
    
     Route::post('tasks', [
        'as' => 'tasks',
        'uses' => 'ApiProjectController@getTasks',
        'middleware' => 'ApiAuth'
    ]);
    Route::post('updatetask', [
        'as' => 'updatetask',
        'uses' => 'ApiProjectController@updateTaskStatus',
        'middleware' => 'ApiAuth'
    ]);
     Route::post('notifications', [
        'as' => 'updatetask',
        'uses' => 'ApiProjectController@getNotifications',
        'middleware' => 'ApiAuth'
    ]);
     Route::post('notification_file_read', [
        'as' => 'updatetask',
        'uses' => 'ApiProjectController@notificationFileRead',
        'middleware' => 'ApiAuth'
    ]);
    Route::post('notification_task_read', [
        'as' => 'updatetask',
        'uses' => 'ApiProjectController@notificationTaskRead',
        'middleware' => 'ApiAuth'
    ]);
    
     Route::post('viewbudget', [
        'as' => 'updatetask',
        'uses' => 'ApiProjectController@viewBudget',
        'middleware' => 'ApiAuth'
    ]);
});

// route to show the login form
Route::group(array('namespace'=>'admin'), function()
{
    Route::get('/', 'LoginController@index');
    Route::get('/login', 'LoginController@index');
	Route::post('/admin/login', 'LoginController@login');

    Route::get('/privacy-policy', 'LoginController@privacyPolicy');
	
	Route::get('/admin/logout', 'LoginController@logout');
	Route::get('/admin', 'HomeController@index');
	Route::get('/admin/users', 'UserController@index');
	Route::get('/admin/users/edit', 'UserController@index');
	Route::get('/admin/users/edit/{id}', 'UserController@edit');
	Route::post('/admin/users/update', 'UserController@update');
	
	
	Route::get('/admin/subcontractor', 'UserController@subcontractor');
	
	Route::get('/admin/users/add', 'UserController@add');
	Route::post('/admin/users/save', 'UserController@save');
	Route::post('/admin/users/change_status', 'UserController@change_status');
	
	Route::get('/admin/users/delete/{id}', 'UserController@delete');
	Route::get('/admin/users/delete_sub/{id}', 'UserController@delete_sub');
	
    Route::get('/admin/users/autocomplete', 'UserController@autocomplete');
    
	Route::get('/admin/myaccount/', 'AccountController@index');
	Route::post('/admin/myaccount/update', 'AccountController@update');
	Route::get('/admin/myaccount/resetpassword', 'AccountController@resetpassword');
	Route::post('/admin/myaccount/reset', 'AccountController@reset');
	
	Route::get('/admin/projects/', 'ProjectController@index');
	Route::get('/admin/projects/add', 'ProjectController@add');
	Route::post('/admin/projects/save', 'ProjectController@save');
	
	Route::get('/admin/projects/edit/{id}', 'ProjectController@edit');
	Route::get('/admin/projects/delete/{id}', 'ProjectController@delete');
	Route::get('/admin/projects/view/{id}', 'ProjectController@view');
	Route::get('/admin/projects/view_project/{id}', 'ProjectController@view_project_id');
	Route::get('/admin/projects/estimate/{id}', 'ProjectController@estimate');
	Route::get('/admin/projects/view', 'ProjectController@index');
	
	Route::post('/admin/projects/update_project_email', 'ProjectController@update_project_email');
	
	
	Route::post('/admin/projects/update', 'ProjectController@update');
	Route::post('/admin/projects/update_estimate', 'ProjectController@update_estimate');
	Route::post('/admin/projects/remove_resources', 'ProjectController@remove_resources');
	
	Route::post('/admin/notification/unhighlight', 'NotificationController@unhighlight');
	
	//Route::get('/admin/projects/requests/{id}', 'ProjectController@projectrequests');
	//Route::get('/admin/projects/requests', 'ProjectController@allrequests');
	Route::get('/admin/projects/view_all_requests', 'ProjectController@view_all_requests');
	Route::get('/admin/projects/project_requests/{id}', 'ProjectController@project_requests');
	Route::post('/admin/projects/requests/overrideManpowerAjax', 'ProjectController@overrideManpowerAjax');
	Route::post('/admin/projects/requests/assigncontractor', 'ProjectController@assignContractor');
	
	
	Route::get('/admin/projects/logs/{id}', 'ProjectController@logs');
	Route::get('/admin/projects/logs', 'ProjectController@all_logs');
	Route::get('/admin/projects/logs/downloadpdf/{id}', 'ProjectController@downloadworklogpdf');
	Route::get('/admin/projects/projectlogs/{id}', 'ProjectController@projectlogs');
	Route::post('/admin/projects/logs/override', 'ProjectController@overrideLog');
	
	Route::post('/admin/projects/logs/overrideAjax', 'ProjectController@overrideAjax');
	
	Route::get('/admin/projects/survey/{id}', 'ProjectController@survey');
	Route::get('/admin/projects/survey/pdf/{id}', 'ProjectController@downloadsurveypdf');
	Route::get('/admin/projects/survey', 'ProjectController@all_survey');
	Route::get('/admin/projects/projectsurveys/{id}', 'ProjectController@projectsurveys');
	
	Route::post('/admin/projects/change_complete_status', 'ProjectController@change_complete_status');
	
    Route::get('/admin/projects/autocomplete', 'ProjectController@autocomplete');
    Route::get('/admin/projects/resourceautocomplete', 'ProjectController@resourceAutocomplete');
    
    Route::get('/admin/projects/tasks', 'ProjectController@tasks');
    Route::get('/admin/projects/project_tasks/{id}', 'ProjectController@project_tasks');
    Route::get('/admin/projects/tasks/add', 'ProjectController@addTask');
     Route::post('/admin/projects/tasks/save', 'ProjectController@saveTask');
     Route::get('/admin/projects/tasks/delete/{id}', 'ProjectController@deleteTask');
    
    Route::post('/admin/projects/tasks/change_task_status', 'ProjectController@change_task_status');
    Route::get('/admin/projects/tasks/task_autocomplete', 'ProjectController@task_autocomplete');
   
    Route::post('/admin/projects/getCustomerMember', 'ProjectController@getCustomerMember');
    Route::post('/admin/projects/getMemberData', 'ProjectController@getMemberData');
    
	Route::get('/admin/manpowers', 'ManpowerController@index');
	Route::get('/admin/manpowers/add', 'ManpowerController@add');
	Route::post('/admin/manpowers/save', 'ManpowerController@save');
	Route::get('/admin/manpowers/edit/{id}', 'ManpowerController@edit');
	Route::get('/admin/manpowers/delete/{id}', 'ManpowerController@delete');
	Route::post('/admin/manpowers/update', 'ManpowerController@update');
    
    
	Route::get('/admin/customers', 'CustomerController@index'); 
	Route::get('/admin/customers/add', 'CustomerController@add');
	Route::post('/admin/customers/save', 'CustomerController@save');
	Route::get('/admin/customers/delete/{id}', 'CustomerController@delete');
	Route::get('/admin/customers/edit/{id}', 'CustomerController@edit');
    Route::post('/admin/customers/update', 'CustomerController@update');
    Route::get('/admin/customers/autocomplete', 'CustomerController@autocomplete');
    
	
	/*Route::get('/admin/subcontractors', 'SubcontractorController@index'); 
	Route::get('/admin/subcontractors/add', 'SubcontractorController@add');
	Route::post('/admin/subcontractors/save', 'SubcontractorController@save');
	Route::get('/admin/subcontractors/delete/{id}', 'SubcontractorController@delete');
	Route::get('/admin/subcontractors/edit/{id}', 'SubcontractorController@edit');
    Route::post('/admin/subcontractors/update', 'SubcontractorController@update');*/
    
	Route::get('/admin/forgotpassword', 'ForgotPasswordController@index');
	Route::post('/admin/forgotpassword/send', 'ForgotPasswordController@send');
    
    
    Route::get('/admin/surveys', 'SurveysController@index'); 
    Route::get('/admin/surveys/add', 'SurveysController@add'); 
    Route::post('/admin/surveys/save', 'SurveysController@save');
    Route::get('/admin/surveys/edit/{id}', 'SurveysController@edit');
    Route::post('/admin/surveys/update', 'SurveysController@update');
    Route::get('/admin/surveys/delete/{id}', 'SurveysController@delete');
    Route::get('/admin/surveys/projectsurveys/', 'SurveysController@projectSurveys');
    Route::get('/admin/surveys/addprojectsurvey/', 'SurveysController@addProjectSurvey');
    Route::post('/admin/surveys/savesurvey/', 'SurveysController@saveProjectSurvey');
    Route::get('/admin/surveys/deleteprojectsurvey/{id}', 'SurveysController@deleteprojectsurvey');
    
    
    Route::get('/admin/files', 'FileController@index'); 
    Route::get('/admin/projectfiles/{id}', 'ProjectController@projectfiles'); 
	Route::get('/admin/files/add', 'FileController@add');
	Route::post('/admin/files/save', 'FileController@save');
	Route::get('/admin/files/delete/{id}/{pid}', 'FileController@delete');
	Route::get('/admin/files/delete_single/{id}', 'FileController@delete_single');
	Route::get('/admin/files/edit/{id}', 'FileController@edit');
    Route::post('/admin/files/update', 'FileController@update');
    Route::get('/admin/files/categories', 'FileController@categories');
    Route::get('/admin/files/categories/add', 'FileController@add_category');
    Route::post('/admin/files/categories/save', 'FileController@save_category');
    Route::get('/admin/files/categories/delete/{id}', 'FileController@delete_category');
    
    
    Route::get('/admin/files/autocomplete', 'FileController@autocomplete');
    
	Route::get('/admin/reports', 'ReportController@index');
	Route::get('/admin/estimatedreports', 'ReportController@estimatedreports');
	Route::get('/admin/reports/view', 'ReportController@view');
	Route::get('/admin/reports/estimatedview', 'ReportController@estimatedview');
	Route::get('/admin/report/generate', 'ReportController@generate');
	Route::post('/admin/report/estimatedgenerate', 'ReportController@estimatedgenerate');
	
	
	Route::post('/admin/report/sendadminreport', 'ReportController@sendadminreport');
	Route::post('/admin/report/estimatesendadminreport', 'ReportController@estimatesendadminreport');
	Route::post('/admin/report/finalizemanpowerschedule', 'ReportController@finalizemanpowerschedule');
	Route::get('/admin/daily_report_view', 'ReportController@daily_report_view');
	
	
	Route::get('/admin/report/daterangereportdownload/{id}', 'ProjectController@daterangereportdownload');
	Route::get('/admin/report/latestreportdownload/{id}', 'ProjectController@latestreportdownload');
});


Route::post('/changepassword', [
    'as' => 'changepassword',
    'uses' => 'admin\ForgotPasswordController@resetPassword'
]);

Route::get('home', 'HomeController@index');


