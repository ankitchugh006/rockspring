<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	public function login(Request $request, Mail $mail, BcryptHasher $hash) {
        $validator = Validator::make(
                        $request->all(), array(
                    'password' => 'required',
                    'deviceType' => 'required',
                    'deviceOS' => 'required',
                    'deviceOSVersion' => 'required',
                    'countryCode' => 'required',
                    'pushID' => 'required'
                        )
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => 'Input Validation Failed'
            ]);
        } else {
            $user = User::where(
                            'email', '=', $request->input('email')
                    )
                    ->orWhere('phone', '=', $request->input('phone'))->get();
          //  print_r(count($user)); die;
            if (count($user)) {
              
                $user = $user->first();
                $user->PushID = $request->input('pushID');
                $user->DeviceType = $request->input('deviceType');
                $user->DeviceOS = $request->input('deviceOS');
                $user->DeviceOSVersion = $request->input('deviceOSVersion');
                $user->CountryCode = $request->input('countryCode');
                $user->session_id = str_random(40);
                $user->save();
                $event_user_list = Event::where('id', '=', 1)->get();
                //$this->invitedUserStatus($event_user_list);
                // print_r($event_user_list->invited_users); die;
                if ($hash->check($request->input('password'), $user->password)) {
                    return response([
                        'success' => true,
                        'code' => 200,
                        'message' => 'Authentication Credentials Verified',
                        'user' => $user
                    ]);
                }
            }
            return response()->json([
                        'success' => false,
                        'code' => 401,
                        'message' => 'Incorrect Authentication Credentials'
            ]);
        }
    }


}
