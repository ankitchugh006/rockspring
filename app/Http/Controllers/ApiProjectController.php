<?php 
namespace App\Http\Controllers;

use Validator;
use Input;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Intervention\Image\Facades\Image as Image;
use App\Models\User;
use App\Models\ProjectForeman;
use App\Models\Project;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\ProjectResources;
use App\Models\ProjectSurvey;
use App\Models\Notification;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\Subcontractor;
use App\Models\FileCategories;
use App\Models\File;
use App\Models\Task;
use App\Models\ProjectSubcontractors;
use App\Models\Manpower;
use App\Models\SubcontractorRequest;
use App\Models\SubcontractorWorklog;

class ApiProjectController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	public function __construct()
	{

	}	
	public function myprojects(Request $request, Mail $mail, BcryptHasher $hash) { 
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){

			$projects=ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
                            ->where('foreman_id',$request->input('foreman_id'))
                            ->where('projects.is_completed','!=',1)
							->distinct('assigned_projects_to_foreman.project_id')
							->select('assigned_projects_to_foreman.project_id','assigned_projects_to_foreman.foreman_id','projects.manual_id','projects.title','projects.description','projects.shift_type','projects.hps','projects.is_completed','projects.survey_questions');
                
        	
		
				        
                
				//get project assigned Resources
				
				if(!empty($projects->get()->toArray())){
					foreach($projects->get()->toArray() as $p){
						$assignedresources=ProjectResources::where('project_id','=',$p['project_id'])
															->select('project_assigned_resources.id as resource_id','project_assigned_resources.manpower_id as resource_title','project_assigned_resources.max_hours')
															->get();
						$p['total_estimated_hours']=0;
						$p['total_used_hours']=0;
						$p['total_used_hours_multiply']=0;
						
						$p['is_resource_requested']	=is_resource_requested($p['project_id'],$request->input('foreman_id'));
						//$p['is_resource_requested']	=is_request_approved($p['project_id'],$request->input('foreman_id'));
						$p['is_worklog_submitted']	=is_worklog_submitted($p['project_id'],$request->input('foreman_id'));
						$p['is_survey_submitted']	=is_survey_submitted($p['project_id'],$request->input('foreman_id'));
						
						if(count($assignedresources->toArray())>0){
							foreach($assignedresources as $a){
								$a['used_hours']=0;
								$a['used_hours_multiply']=0;
								
								
								
								$a['resource_title'] = get_resource_title($a['resource_id']);
								
								$p['total_estimated_hours']=$p['total_estimated_hours']+$a['max_hours'];
								$z=Worklog::where('project_id',$p['project_id'])->where('resource_id',$a['resource_id'])->where('foreman_id',$request->input('foreman_id'))->select('hours_logged','quantity')->get();
								if(count($z)>0){
									foreach($z as $log){
										if(isset($log->hours_logged) && !empty($log->hours_logged)){
										$p['total_used_hours']	   			+=		$log->hours_logged;
										$p['total_used_hours_multiply']	   	+=		$log->hours_logged;
										$a['used_hours']	   	   		 	+=		$log->hours_logged;
										$a['used_hours_multiply']	   	  	+=		$log->hours_logged;
										}
									}
								}
								
								
							}
						}
						
						$path='uploads/project_reports/'.$p['title'].'.pdf';
						
						$p['report_url']='';
						if(file_exists($path)){
							$p['report_url'] = url().'/'.$path;
						}
						
					
						$surveys='';
						$questions='';
						if(!empty($p['survey_questions'])){
							$questions=explode(',',$p['survey_questions']);
						}
						
						$processing_dates=get_all_processing_dates($p['project_id'],$request->input('foreman_id'));
						
						$surveys=	Surveys::whereIn('id', $questions)->get();
						$myprojects[]=array('project'=>$p,'resources'=>$assignedresources->toArray(),'surveys'=>$surveys,'processing_dates'=>$processing_dates);
					}
				}
							
				if($projects->count()){
					return response()->json([
							'success' => true,
							'code' => 200,
							'projects' => $myprojects,
							'message' => \Config::get('constants.MY_PROJECTS')
					]);
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
				]);
				}
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
        }
    }
    public function request_resources(Request $request, Mail $mail){
		
		$processing_date="";
		$validator = Validator::make(
				$request->all(), array(
				    'project_id' => 'required',
				    'foreman_id' => 'required',
				   // 'subcontractor_id' => 'required',
				    'resources' => 'required',
				    'device_token' => 'required',
				)
        );
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				//is my project check
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
					
					$is_exists = ForemanRequestMain::where('project_id', '=', $request->input('project_id'))
											->where('foreman_id','=',$request->input('foreman_id'))
											->count();
					//if ($is_exists == 0) {
					
						///chececk if resource requested for processing date
						$is_requested=is_resource_requested_for_date($request->input('project_id'),$request->input('foreman_id'),$request->input('processing_date'));
						
						//echo $is_requested;die;
						if($is_requested > 0){
							return response()->json([
								'success' => false,
								'code' => 400,
								'message' => 'Resources already requested for this processing date'
							]);
						}
					
					    if(is_array($request->input('resources'))){
							$resources=json_encode($request->input('resources'));     //for insomnia
							$resources=json_decode($resources,true);
						}else{
							$resources=json_decode($request->input('resources'),true);   //for phone
						}
						
						
						if(empty($resources)){
							return response()->json([
								'success' => false,
								'code' => 400,
								'message' => \Config::get('constants.SELECT_RESOURCES')
							]);
						}
						
						
						
						$r=ForemanRequestMain::create(array(
							'foreman_id'    =>  $request->input('foreman_id'),
							'project_id' 	=>  $request->input('project_id'),
							//'subcontractor_id' 	=>  $request->input('subcontractor_id'),
							'comment1' 	=>  $request->input('comment1'),
							'comment2' 	=>  $request->input('comment2'),
							'comment3' 	=>  $request->input('comment3'),

						));
						
						
						$requestid=$r->id;
							
						if(is_array($request->input('resources'))){
							$resources=json_encode($request->input('resources'));     //for insomnia
							$resources=json_decode($resources,true);
						}else{
							$resources=json_decode($request->input('resources'),true);   //for phone
						}
						
						//$resources=json_decode($request->input('resources'),true);   //for phone
						//print_r(json_encode(['message'=>$resources]));die;
						if(!empty($resources)){
                            
							//assigning resources
							foreach($resources as $i){

								/*$is_exists = ForemanRequest::where('project_id', '=', $request->input('project_id'))
										->where('foreman_id','=',$request->input('foreman_id'))
										->where('resource_id','=',$i['resource_id'])
										->count();

								$is_project_resource = ProjectResources::where('project_id', '=', $request->input('project_id'))
										->where('id','=',$i['resource_id'])
										->count();
								*/
								ForemanRequest::create(array(
									'request_id'    =>   $requestid,
									'project_id'    =>   $request->input('project_id'),
									'foreman_id' 	=>   $request->input('foreman_id'),
									'resource_id' =>$i['resource_id'],
									'processing_date' =>$i['processing_date'],
									'quantity' =>$i['quantity']
								));	
								$processing_date = 	$i['processing_date'];					
							}
							
						}
						
						//implementing admin notification
						$admin_id=get_admin_id();
						Notification::create(array(
						'notification_from'    =>  $request->input('foreman_id'),
						'notification_to' 	=>   $admin_id,
						'notification_type' =>'request',
						'project_id' =>$requestid
						));
						
						$email=getUserEmail($request->input('foreman_id'));
						$emaildata['resources']=$resources;
						$emaildata['foreman_id']=$request->input('foreman_id');
						$emaildata['project']=Project::Where('id',$request->input('project_id'))->first();
						//send email to foreman
						
						
						$newDate = date("m/d/Y", strtotime($processing_date));
						$subject = "Rock Spring Manpower ".$newDate;
						\Mail::send('emails.foremanReport', $emaildata, function($message) use ($email,$subject)
						{
							$message->to($email, 'Super')->subject($subject);
							
						});
						
						
						return response()->json([
								'success' => true,
								'code' => 200,
								'message' => \Config::get('constants.RESOURCE_REQUESTED')
						]);
					/*}else{
						return response()->json([
								'success' => false,
								'code' => 400,
								'message' => 'You have already Requested Resources for that project!!'
						]);
					}	*/
					
					updateProjectDate($request->input('project_id'));
				
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}
				
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
			
		}
	}
	public function worklog(Request $request){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				//is my project check
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
				
					$projects=ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
												->where('foreman_id',$request->input('foreman_id'))
												->where('project_id', '=', $request->input('project_id'))
												->select('assigned_projects_to_foreman.project_id','assigned_projects_to_foreman.foreman_id','projects.manual_id','projects.title','projects.description','projects.id','projects.shift_type','projects.hps','projects.is_completed');
					
					
					
					if(!empty($projects->first()->toArray())){
						$p=$projects->first()->toArray();
						$project_id=$p['project_id'];
					
						$assignedresources=ProjectResources::where('project_id','=',$p['project_id'])
														->select('project_assigned_resources.id as resource_id','project_assigned_resources.manpower_id as resource_title','project_assigned_resources.max_hours')
														->get();
						$p['total_estimated_hours']=0;
						$p['total_used_hours']=0;
						$p['total_used_hours_multiply']=0;
						
						$p['is_resource_requested']	=is_resource_requested($p['project_id'],$request->input('foreman_id'));
						$p['is_worklog_submitted']	=is_worklog_submitted($p['project_id'],$request->input('foreman_id'));
						$p['is_survey_submitted']	=is_survey_submitted($p['project_id'],$request->input('foreman_id'));
						
						if(count($assignedresources->toArray())>0){
							foreach($assignedresources as $a){
								$a['used_hours']=0;
								$a['used_hours_multiply']=0;
								$a['for_ios']="0";
								$a['quantity']='';
								$a['total_quantity']='';
								$a['is_subcontractor_assigned'] = is_subcontractor_assigned($p['project_id'],$request->input('foreman_id'),$a['resource_id']);
								
								
								
								$a['resource_title'] = get_resource_title($a['resource_id']);
								
								$p['total_estimated_hours']=$p['total_estimated_hours']+$a['max_hours'];
								$z=Worklog::where('project_id',$p['project_id'])->where('resource_id',$a['resource_id'])->where('foreman_id',$request->input('foreman_id'))->select('hours_logged','quantity')->get();
								//get  fillable manpower
								$qty=ForemanRequest::where('project_id',$project_id)->where('resource_id',$a['resource_id'])->where('foreman_id',$request->input('foreman_id'))->select('quantity')->get();
								
								if(count($z)>0){
									foreach($z as $log){
										if(isset($log->hours_logged) && !empty($log->hours_logged)){
										$p['total_used_hours']	   			+=		$log->hours_logged;
										$p['total_used_hours_multiply']	   	+=		$log->hours_logged;
										$a['used_hours']	   	   		 	+=		$log->hours_logged;
										$a['used_hours_multiply']	   	  	+=		$log->hours_logged;
										}
									}
								}
								if(count($qty)>0){
								
									foreach($qty as $q){
											$a['quantity'] += $q->quantity;
									}
								}
								
							}
						}
						
					
						
						
						$surveys='';
						$questions='';
						if(!empty($p['survey_questions'])){
							$questions=explode(',',$p['survey_questions']);
						}
						
						$processing_dates=get_all_processing_dates($p['project_id'],$request->input('foreman_id'));
						
						$surveys=	Surveys::whereIn('id', $questions)->get();
						$myprojects[]=array('project'=>$p,'requested_resources'=>$assignedresources->toArray(),'surveys'=>$surveys,'processing_dates'=>$processing_dates);
							
					}
								
					if($projects->count()){
						return response()->json([
								'success' => true,
								'code' => 200,
								'projects' => $myprojects,
								'survey_questions'=>$surveys,
								'message' => \Config::get('constants.MY_PROJECTS')
						]);
					}
					
				}else{
					return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}
				
				
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
        }
	}
	
	public function checkworklog(Request $request){
		
		//print_r($request->all());
		$validator = Validator::make(
				$request->all(), array(
				    'project_id' => 'required',
				    'foreman_id' => 'required',
				    'log_date' => 'required'
				)
        );
        
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
					$logdate=$request->input('log_date');
					//check worklog on requested log date
					$is_worklog_submitted=WorklogMain::where('project_id', '=', $request->input('project_id'))
											->where('foreman_id','=',$request->input('foreman_id'))
											->whereDate('logged_at','=',$logdate)
											->count();
					if($is_worklog_submitted){
						return response()->json([
							'success' => false,
							'code' => 400,
							'message' =>"Worklog has already been submitted for the mentioned date"
						]);
					}else{
						return response()->json([
							'success' => true,
							'code' => 200,
							'message' =>"You can submit worklog for that date"
						]);
					}
					
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
		
	}
	public function submit_worklog(Request $request){
		
		
		
		$validator = Validator::make(
				$request->all(), array(
				    'project_id' => 'required',
				    'foreman_id' => 'required',
				    'resources' => 'required',
				    'log_date' => 'required'
				)
        );
        
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$log_date = $request->input('log_date');
			$log_date = date_create($log_date);
			$log_date =  date_format($log_date, 'Y-m-d h:m:s');
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
					
						if(is_array($request->input('resources'))){
							$resources=json_encode(str_replace(array("\n", "\r"), '', $request->input('resources')));     //for insomnia
							$resources=json_decode($resources,true);
						}else{
							$resources=json_decode(str_replace(array("\n", "\r"), '', $request->input('resources')),true);   //for phone
						}
						
						if(!is_array($resources)){
							return response()->json([
								'success' => false,
								'code' => 400,
								'message' => \Config::get('constants.SELECT_RESOURCES')
							]);
						}
						
						//check worklog on requested log date
						$log_date_to_check = $request->input('log_date');
						$log_date_to_check = date_create($log_date_to_check);
						$log_date_to_check =  date_format($log_date_to_check, 'Y-m-d');

						$is_worklog_submitted=WorklogMain::where('project_id', '=', $request->input('project_id'))->where('foreman_id','=',$request->input('foreman_id'))
												->whereDate('logged_at','=',$log_date_to_check)
												->count();
						//echo $is_worklog_submitted;die;						
						if($is_worklog_submitted){
							return response()->json([
								'success' => false,
								'code' => 400,
								'message' =>"Worklog has already been submitted for the mentioned date"
							]);
						}  
						///////////////////////////////////////////////////
						
						
						$w=WorklogMain::create(array(
							'foreman_id'    =>  $request->input('foreman_id'),
							'project_id' 	=>  $request->input('project_id'),
							'description' 	=>  $request->input('description'),
							'additional_comments' 	=>  $request->input('additional_comments'),
							'logged_at' 	=>  $log_date
							//'photo' 	=>  $request->input('picture')
						));
						
						$worklogid=$w->id;
						
						//submit image if any
						if (Input::hasFile('picture')) {
							
							$path='uploads/worklog/'.$worklogid.'/';
							\File::cleanDirectory($path);
							\File::makeDirectory($path,$mode = 0777, true, true);

							$file            = Input::file('picture');
							$filename        = str_random(6) . '_' . $file->getClientOriginalName();
							$uploadSuccess   = $file->move($path, $filename);

							WorklogMain::where('id', $worklogid)->update(array(
								'photo'    =>  url().'/'.$path.$filename 
							));
						}
						
						/*********Saving resources logged************/
						
						
						
						if(is_array($request->input('resources'))){
							$resources=json_encode($request->input('resources'));     //for insomnia
							$resources=json_decode($resources,true);
						}else{
							$resources=json_decode($request->input('resources'),true);   //for phone
						}
						
						 if(!empty($resources)){
							foreach($resources as $i){
								
								$nested_worklog_id=Worklog::create(array(
									'worklog_id'    =>  $worklogid,
									'foreman_id'    =>  $request->input('foreman_id'),
									'project_id' 	=>  $request->input('project_id'),
									'resource_id' 	=>  $i['resource_id'],
									'quantity' 		=>  $i['quantity'],
									'hours_logged' 	=>	$i['hours_logged'],
									'description'   => 	$i['description'],
									'logged_at' 	=>  $log_date
								));
								
								/*$subcontracor=$i['subcontracor'];
								
								
								if(!empty($subcontracor)){
									foreach($subcontracor as $s){
										$code = get_resource_code($i['resource_id']);
										SubcontractorWorklog::create(array(
											'nested_worklog_id'    	=>  $nested_worklog_id->id,
											'foreman_id'    		=>  $request->input('foreman_id'),
											'project_id' 			=>  $request->input('project_id'),
											'resource_id' 			=>  $i['resource_id'],
											'resource_code' 		=>  $code ,
											'subcontractor_id' 		=>  $s['subcontractor_id'],
											'quantity' 				=>  $s['quantity'],
											'hours_logged' 			=>	$s['hours_logged']
											
										));
									}
								}*/
								
							}
						}
						//implementing admin notification
						$admin_id=get_admin_id();
						Notification::create(array(
							'notification_from'    =>  $request->input('foreman_id'),
							'notification_to' 	=>   $admin_id,
							'notification_type' =>'logging',
							'project_id' =>$worklogid
						));
					//}
					
					//
					updateProjectDate($request->input('project_id'));
					
					return response()->json([
							'success' => true,
							'code' => 200,
							'message' =>\Config::get('constants.HOURS_LOGGED')
					]);
					
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}							
											
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
	
	public function checksurvey(Request $request){
		//print_r($request->all());
		$validator = Validator::make(
				$request->all(), array(
				    'project_id' => 'required',
				    'foreman_id' => 'required',
				    'survey_date' => 'required'
				)
        );
        
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
					$survey_date=$request->input('survey_date');
					//check worklog on requested log date
					$is_survey_submitted=ProjectSurvey::where('project_id', '=', $request->input('project_id'))
											->where('foreman_id','=',$request->input('foreman_id'))
											->whereDate('survey_date','=',$survey_date)
											->count();
											
											
					$is_worklog_submitted=WorklogMain::where('project_id', '=', $request->input('project_id'))
											->where('foreman_id','=',$request->input('foreman_id'))
											->whereDate('logged_at','=',$survey_date)
											->count();
					if($is_worklog_submitted){							
						if($is_survey_submitted){
							return response()->json([
								'success' => false,
								'code' => 400,
								'message' =>"Survey has already been submitted for the mentioned date "
							]);
						}else{
							return response()->json([
								'success' => true,
								'code' => 200,
								'message' =>"You can submit survey for that date"
							]);
						}
					}else{
						return response()->json([
							'success' => false,
							'code' => 400,
							'message' =>"Please submit worklog for the mentioned date"
						]);
					}
					
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
		
	}
	public function submit_daily_survey(Request $request){
		

		$validator = Validator::make(
				$request->all(), array(
				    'project_id' => 'required',
				    'foreman_id' => 'required',
				    'survey_date' => 'required',
				    //'description' => 'required'
				)
        );
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				$is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				if($is_my_project!=0){
					
					$chck_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'))->first();;
					
					//if($chck_user->device_os != 'android'){
					
						$w=ProjectSurvey::create(array(
							'foreman_id'    =>  $request->input('foreman_id'),
							'project_id' 	=>  $request->input('project_id'),
							'survey_date' 	=>  $request->input('survey_date')
						));
						
						$surveyid=$w->id;
					
					
						if(is_array($request->input('surveys'))){
							$surveys=json_encode($request->input('surveys'));     //for insomnia
							$surveys=json_decode($surveys,true);
						}else{
							$surveys=json_decode($request->input('surveys'),true);   //for phone
						}
						
						
						$files=Input::file('images');
						$questions=array();
						if(!empty($surveys)){
							foreach($surveys as $key=>$s){
								
								
									Survey::create(array(
										'foreman_id'    =>  $request->input('foreman_id'),
										'project_id' 	=>  $request->input('project_id'),
										'question_id' 	=>  $s['question_id'],
										'status' 		=>  $s['status'],
										'survey_id' 	=>  $surveyid,
										'description' => 	$s['description']
									));
									
									$questions[]=$s['question_id'];
									$q=$s['question_id'];
									/*if (!empty($files[$key])){
												$path='uploads/survey/'.$surveyid.'/';
												
												\File::makeDirectory($path,$mode = 0777, true, true);

												$file = $files[$key];

												$filename        = str_random(6) . '_' . $file->getClientOriginalName();
												$uploadSuccess   = $file->move($path, $filename);

												Survey::where('survey_id', $surveyid)
														->where('question_id', $q)
														->update(array(	
															'image'    =>  url().'/'.$path.$filename 
												));
												
									}*/

							}
						}

						$files=Input::file('images');
					
						if($files){
							foreach($questions as $key=>$q){
								if (!empty($files[$key])){
											$path='uploads/survey/'.$surveyid.'/';
											
											\File::makeDirectory($path,$mode = 0777, true, true);

											$file = $files[$key];

											$filename        = str_random(6) . '_' . $file->getClientOriginalName();
											$uploadSuccess   = $file->move($path, $filename);

											Survey::where('survey_id', $surveyid)
													->where('question_id', $q)
													->update(array(	
														'image'    =>  url().'/'.$path.$filename 
											));
											
								}
							}
						}
						
						$main_images=Input::file('main_images');
						$main_images_path=array();
						if($main_images){
							foreach($main_images as $q){
								$path='uploads/mainsurvey/'.$surveyid.'/';
											
								\File::makeDirectory($path,$mode = 0777, true, true);

								$file = $q;

								$filename        = str_random(10) . '.png';
								
								$uploadSuccess   = Image::make($file->getRealPath())->resize(500, 500)->encode('jpg', 50)->save($path.$filename);
								
								//$uploadSuccess   = $file->move($path, $filename);
								
								$url=url().'/'.$path.$filename ;
								$main_images_path[]=$url;
								
							}
							Survey::where('survey_id', $surveyid)
							->update(array(	
								'main_images'    =>  implode(',',$main_images_path)
							));	
						}
						
						/*if((Input::file('images'))){
							
							foreach($files as $s){
										
										$path='uploads/survey/'.$surveyid.'/';
										\File::cleanDirectory($path);
										\File::makeDirectory($path,$mode = 0777, true, true);

										$file = $s;

								
										$filename        = str_random(6) . '_' . $file->getClientOriginalName();
										$uploadSuccess   = $file->move($path, $filename);

										Survey::where('survey_id', $surveyid)
												->update(array(	
													'image'    =>  url().'/'.$path.$filename 
										));
									
								
							}
						}
						*/
						//notification
						
						$admin_id=get_admin_id();
						Notification::create(array(
						'notification_from'    =>  $request->input('foreman_id'),
						'notification_to' 	=>   $admin_id,
						'notification_type' =>'survey',
						'project_id' =>$surveyid
						));
						
						updateProjectDate($request->input('project_id'));
					//}
					return response()->json([
							'success' => true,
							'code' => 200,
							'message' => \Config::get('constants.SURVEY_SUBMITTED')
					]);
					
				}else{
					 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.NO_PROJECTS')
					]);
				}							
											
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
    
    /****************************FILE API*************************************/
    
    public function getFileCategories(Request $request){
        
            $validator = Validator::make(
                    $request->all(), array(   
                        'foreman_id' => 'required',
                        'project_id'=>'required'
                    )
            );
            if ($validator->fails()) {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => $validator->errors()->all(),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
            }

			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
        
                $categories=FileCategories::get();
                
                $ids=getPojectUserIds($request->input('project_id'));
                
                $files=File::where('project_id',$request->input('project_id'))
							->select(\DB::raw('id, title,file,CONCAT(id,"_",file_name) as file_name,project_id,added_by,category_ids,created_at'))
							->get();
				
				
	
                $projects=ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
                        ->where('foreman_id',$request->input('foreman_id'))
                        ->where('projects.is_completed',0)
                        ->select('assigned_projects_to_foreman.project_id','assigned_projects_to_foreman.foreman_id','projects.title','projects.description','projects.shift_type','projects.hps')->get();
                
                return response()->json([
                        'success' => true,
                        'code' => 200,
                        'files' => $files->toArray(),
                        'categories' => $categories->toArray(),
                        'projects' =>  $projects->toArray(),
                        'message' => \Config::get('constants.MY_PROJECTS')
                ]);
                
            }else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}  
        
    }
    public function submitFile(Request $request){
            $validator = Validator::make(
                    $request->all(), array(   
                        'foreman_id' => 'required',
                        'project_id' => 'required'
                    )
            );
            if ($validator->fails()) {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => $validator->errors()->all(),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
            }
        
            $is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
        
            if($is_valid_user->count()>0){
                
                    /*$w=File::create(array(
					    'title'    =>  $request->input('title'),
						'project_id'    =>  $request->input('project_id'),
						'added_by'    =>  $request->input('foreman_id'),
						'category_ids'    =>  $request->input('category_id')
					));	
			         
               
                    $fileid=$w->id;*/

					if (Input::hasFile('file')) {
						$path='uploads/files/';
						//\File::cleanDirectory($path);
						\File::makeDirectory($path,$mode = 0777, true, true);

						$file            = Input::file('file');
						$filename        = str_random(6) . '_' . $file->getClientOriginalName();
						$uploadSuccess   = $file->move($path, $filename);
						
						if($uploadSuccess){
							File::create(array(
								'title'    =>  $request->input('title'),
								'project_id'    =>  $request->input('project_id'),
								'added_by'    =>  $request->input('foreman_id'),
								'category_ids'    =>  $request->input('category_id'),
								'file'    =>       url().'/'.$path.$filename,
								'file_name'    =>  $file->getClientOriginalName() 
							));	
						}
						/*File::where('id', $fileid)->update(array(
							'file'    =>       url().'/'.$path.$filename,
							'file_name'    =>  $file->getClientOriginalName() 
						));*/
					}
                    
                    updateProjectDate($request->input('project_id'));
                    
                    return response()->json([
							'success' => true,
							'code' => 200,
							'message' => 'File Submitted'
					]);
                
            }else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}  
    }
   
    /********************* SUBMIT TASK**********************************/
    public function getTasks(Request $request){
            $validator = Validator::make(
                    $request->all(), array(   
                        'foreman_id' => 'required',
                        'project_id' => 'required',
                        'device_token' => 'required'
                    )
            );
            if ($validator->fails()) {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => $validator->errors()->all(),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
            } 
        
             $is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
        
            if($is_valid_user->count()>0){
                
                    $tasks=Task::where('project_id',$request->input('project_id'))->get();
                
                    
                     return response()->json([
                            'success' => true,
                            'code' => 200,
                            'tasks' => $tasks->toArray(),
                            'message' => 'All Tasks'
                    ]);
                
            }else{
                 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
            }
        
        
    }
    public function submitTask(Request $request){
            $validator = Validator::make(
                    $request->all(), array(   
                        'foreman_id' => 'required',
                        'project_id' => 'required',
                        'task' => 'required'
                    )
            );
            if ($validator->fails()) {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => $validator->errors()->all(),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
            }
        
            $is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
        
            if($is_valid_user->count()>0){
                    $w=Task::create(array(
						'project_id'    =>  $request->input('project_id'),
						'foreman_id'    =>  $request->input('foreman_id'),
						'task'    =>  $request->input('task'),
						'description'    =>  $request->input('description')
					));	
                    $fileid=$w->id;
                
                    if (Input::hasFile('task_image')) {
						$path='uploads/tasks/'.$fileid.'/';
						\File::cleanDirectory($path);
						\File::makeDirectory($path,$mode = 0777, true, true);
						
						$thumbpath='uploads/tasks/'.$fileid.'/thumb/';
						\File::cleanDirectory($thumbpath);
                        \File::makeDirectory($thumbpath,$mode = 0777, true, true);
						
						$file            = Input::file('task_image');
						$filename        = str_random(6) . '_' . str_replace(" ","_",$file->getClientOriginalName());
						$uploadSuccess   = $file->move($path, $filename);
				
						//uploading thumbnail
                       
                        copy($path.$filename,$thumbpath.$filename);
						$image = \Image::make(sprintf($thumbpath.$filename, $file->getClientOriginalName()))->resize(50, 50)->save(); 
				
				
						Task::where('id', $fileid)->update(array(
							'task_image'    =>  url().'/'.$path.$filename,
							'task_thumbnail'    =>  url().'/'.$thumbpath.$filename
						));
					}
                    
                    updateProjectDate($request->input('project_id'));
                    
                    return response()->json([
							'success' => true,
							'code' => 200,
							'message' => 'Task Submitted'
					]);
                
            }else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}  
    }
    public function updateTaskStatus(Request $request){
            $validator = Validator::make(
                    $request->all(), array(   
                        'task_id' => 'required',
                        'status' => 'required',
                        'foreman_id' => 'required'
                    )
            );
            if ($validator->fails()) {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => $validator->errors()->all(),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
            }
            
             $is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
        
            if($is_valid_user->count()>0){
                    Task::where('id', $request->input('task_id'))->update(array(
								'status'    =>  $request->input('status')
							));
                
                     return response()->json([
                            'success' => true,
                            'code' => 200,
                            
                            'message' => 'Task Status Updated'
                    ]);
                
            }else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}  
        
    }
    
    public function getNotifications(Request $request){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				 $is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				 if($is_my_project!=0){
				
					 return response()->json([
							'success' => true,
							'code' => 200,
							'is_any_notification_file' => is_any_new_notification_file($request->input('foreman_id'),$request->input('project_id')),
							'is_any_notification_task' => is_any_new_notification_task($request->input('foreman_id'),$request->input('project_id')),
							'notifications' => get_notifications_app($request->input('foreman_id'),$request->input('project_id'))
					 ]);
				 
			     }else{
					  return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
						]);
				 }
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
	public function notificationFileRead(Request $request){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				 $is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				 if($is_my_project!=0){
				
						Notification::where('project_id', $request->input('project_id'))
						->where('notification_to', $request->input('foreman_id'))
						->where('notification_type', 'file_added')
						->update(array(
							'highlight'    => 1
						));
						
						 return response()->json([
							'success' => true,
							'code' => 200,
							'message' => "success"
						]);
						
			     }else{
					  return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
						]);
				 }
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
	public function notificationTaskRead(Request $request){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				 $is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				 if($is_my_project!=0){
				
						Notification::where('project_id', $request->input('project_id'))
						->where('notification_to', $request->input('foreman_id'))
						->where('notification_type', 'task_added')
						->update(array(
							'highlight'    => 1
						));
						
						 return response()->json([
							'success' => true,
							'code' => 200,
							'message' => "success"
						]);
						
			     }else{
					  return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
						]);
				 }
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
	
	public function viewBudget(Request $request){
		
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				 $is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				 if($is_my_project!=0){
					 
					 $budget=array();
					 
					 $manpowers=Manpower::orderBy('number','ASC')->get();
					 $assignedresources=ProjectResources::where('project_id','=',$request->input('project_id'))->get();
					 
					if (!empty($manpowers)) {
						$i = 0;
						foreach ($manpowers as $m) {

							$has_manpower = project_has_resource($assignedresources, $m->id);
							$asssigned_r_id = project_has_resource_get_resource_id($assignedresources, $m->id);
							
							$budget[$i]['manpower_type']=$m->title;
							$budget[$i]['available_hours']='';
							$budget[$i]['used_hours']='';
							$budget[$i]['used_hours_total']='';
							
							$z=Worklog::where('project_id',$request->input('project_id'))->where('resource_id',$asssigned_r_id)->select('hours_logged','quantity')->get();
							if(count($z)>0){
								foreach($z as $log){
										if(isset($log->hours_logged) && !empty($log->hours_logged)){
											//$budget[$i]['used_hours']	   			+=		$log->hours_logged;
											$budget[$i]['used_hours_total']	   		+=		$log->hours_logged;
										}
								}
							}
							
							 if (($has_manpower >= 0)) {
								 $budget[$i]['available_hours']=$has_manpower;
							 }
							
							$i++;
						}
					}
					 
					 return response()->json([
							'success' => true,
							'code' => 200,
							'budget' => $budget
						]); 
					 
				 }else{
					  return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
						]);
				 }
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
		
	}
	
	public function getContractors(Request $request){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'project_id' => 'required',
				    'resource_id' => 'required',
				    'device_token' => 'required',
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				
				 $is_my_project = is_my_project($request->input('project_id'),$request->input('foreman_id')); 
				
				 if($is_my_project!=0){
			
						$Subcontractors=array();
						$chkcount=SubcontractorRequest::where('project_id',$request->input('project_id'))->where('resource_id',$request->input('resource_id'))->where('foreman_id',$request->input('foreman_id'))->count();
						if($chkcount>0){
							$date=date('Y-m-d');
							$Subcontractors=SubcontractorRequest::leftJoin('subcontractors', function($join) {
							  $join->on('subcontractors.id', '=', 'manpower_requests_subcontractors.subcontractor_id');
							})
							->leftJoin('manpower_requests', function($join) {
							  $join->on('manpower_requests.id', '=', 'manpower_requests_subcontractors.request_id');
							})
							->where('manpower_requests_subcontractors.project_id',$request->input('project_id'))
							->where('manpower_requests_subcontractors.resource_id',$request->input('resource_id'))
							->where('manpower_requests_subcontractors.foreman_id',$request->input('foreman_id'))
							->whereDate('manpower_requests_subcontractors.processing_date','=',$date)
							->groupBy('manpower_requests_subcontractors.subcontractor_id')
							->select(\DB::raw('tbl_manpower_requests_subcontractors.subcontractor_id, SUM(tbl_manpower_requests_subcontractors.quantity) as quantity ,tbl_subcontractors.subcontractor'))->get();
						}
						 return response()->json([
							'success' => true,
							'code' => 200,
							'subcontractors' => $Subcontractors
						]); 
				}else{
					  return response()->json([
							'success' => false,
							'code' => 400,
							'message' => \Config::get('constants.NO_PROJECTS')
						]);
				 }
			}else{
				 return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}	
		}
	}
}
