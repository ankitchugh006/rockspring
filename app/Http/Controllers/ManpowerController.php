<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ForemanRequest;

class ManpowerController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		$manpowers=Manpower::orderBy('number', 'ASC')->paginate(100); 
		
		return view('admin/manpower/manage', array('title' => 'Manpower Types','manpowers'=>$manpowers,'active'=>'MANPOWER'));
	}
	public function add(){
		
		return view('admin/manpower/add', array('title' => 'Add New Type','active'=>'MANPOWER')); 
	}
	public function save(){
		
			$data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'title' => 'required',
				'number' => 'required'
				
			);
	       $validator = Validator::make($data, $rules);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/manpowers/add')->withErrors($validator)->withInput(); 
			}else{
				$check_name = Manpower::where('title', '=', Input::get('title'))->count();
				$check_number = Manpower::where('number', '=', Input::get('number'))->count();
			
				if ($check_name != 0) {
					Session::flash('error', 'Title  has already been used. Please select another one!'); 
					return Redirect::to('admin/manpowers/add')->with('data',array('title' => 'Add New Type'))->withInput(); ;
				}if ($check_number != 0) {
					Session::flash('error', 'Number  has already been used. Please select another one!'); 
					return Redirect::to('admin/manpowers/add')->with('data',array('title' => 'Add New Type'))->withInput(); ;
				}else{
					$project=Manpower::create(array(
						'title'    =>  Input::get('title'),
						'number' =>   Input::get('number'),
						'added_by' =>Auth::user()->id,
						'status' => 1
					));	
				}
				Session::flash('success', 'Manpower Type Added successfully'); 
				return Redirect::to('admin/manpowers/')->with('data',array('title' => 'Manpowers'));
			}
	}
	
	public function edit($id=null){
        
		$manpower=Manpower::where('id','=',$id)->first();
		$data['title']='Edit Type';
		$data['manpower']=$manpower;
		return view('admin/manpower/edit',$data);
	}
	
	public function update(){
            echo 'test';die;
			$data = Input::all();
			$manpowerid   = Input::get('manpowerid');
			$rules = array(
				'title' => 'required',
				'number' => 'required'
				
			);
	       $validator = Validator::make($data, $rules);
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/manpowers/edit/'.$manpowerid)->withErrors($validator);
                     
			}else{
				$check_title = Manpower::where('title', '=', Input::get('title'))->where('id','!=',Input::get('manpowerid'))->count();
				if($check_title != 0){
					Session::flash('error', 'Task has already been used. Please select another one!'); 
					return Redirect::to('admin/manpowers/edit/'.$manpowerid)->with('data',array('title' => 'Edit Task'));
				}else{
					Manpower::where('id', $manpowerid)->update(array(
						'title'    =>  Input::get('title'),
						'number' =>   Input::get('number'),
					));
				}
				Session::flash('success', 'Task  updated successfully'); 
				return Redirect::to('admin/manpowers/edit/'.$manpowerid)->with('data',array('title' => 'Edit Task'));
           }
	}
	
	public function delete($id=null){
			Manpower::where('id','=',$id)->delete();
			Session::flash('success', 'Manpower Deleted successfully'); 
			return Redirect::to('admin/manpowers/')->with('data',array('title' => 'Manpowers'));
	}
	
	
	
}
