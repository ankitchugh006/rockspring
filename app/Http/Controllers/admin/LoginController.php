<?php 
namespace App\Http\Controllers\Admin; //admin add
session_start();
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

class LoginController extends Controller {
	
	public function __construct(){
	  if (Auth::check()){
		 return Redirect::to('/admin')->send();;
	   }
	}
	public function index(){
		return view('admin/login/login', array('title' => 'Login'));
	}
	public function privacyPolicy(){
		return view('admin/login/privacypolicy', array('title' => 'Privacy Policy'));
	}
	public function login(Mail $mail, BcryptHasher $hash) {
			 $data = Input::all();
             $rules = array(
		        'username' => 'required',
		        'password' => 'required',
	         );
			
			/**********REMEMBER ME************/
			
			
			if(Input::get('remember')) {
				$_SESSION['remember_me']=Input::get('username');
				$_SESSION['remember_me_p']=Input::get('password');
			}
			elseif(!Input::get('remember')) {
				
				if(isset($_SESSION['remember_me'])) {

					$_SESSION['remember_me']='';
					$_SESSION['remember_me_p']='';
				}
			}
			
			
			
			/**********END REMEMBER ME************/
			
						
			$validator = Validator::make($data, $rules);
	         if ($validator->fails()){
                      // If validation falis redirect back to login.
                      return Redirect::to('/')->withErrors($validator);
                     
			
			} else {
				$userdata = array(
                          'username' => Input::get('username'),
                          'password' => Input::get('password')
                );
				if (Auth::validate($userdata)) {
					if (Auth::attempt($userdata)){						
						//updating some data on login
						$userdata = User::where('username', '=', Input::get('username'))->get();
						$userdata1 = $userdata->first();
						$userdata1->last_login = date('Y-m-d h:m:s');
						$userdata1->save();
						
						 return Redirect::to('/admin')->with(array('title','Admin'));
					}
				} else{
					// If validation falis redirect back to login.
					  Session::flash('error', 'Oops!! Invalid username and password combination'); 
                      return Redirect::to('/')->withErrors($validator);
				}
			}
    }
    public function logout(){
		  Auth::logout(); // logout user
          return Redirect::to('/'); //redirect back to login
	}
}
