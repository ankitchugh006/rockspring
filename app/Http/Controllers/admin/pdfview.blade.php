
<?php
$projects=\Session::get('projects');
$manpower=\Session::get('manpower');
echo $date_from=\Session::get('date_from');
echo $date_to=\Session::get('date_to');
?>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Report
                </h1>
            </div>
            
        </div>
       
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project Logs</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table border="1" cellpadding="5" cellspacing="0" width="500" style="text-align:left;">
                                <thead>
                                    <tr>
                                        <th style="width:100px;" class="project-name">Job</th>
                                        <th style="width:100px;" class="project-name">Manpower Type</th>
                                        <th style="width:100px;" class="project-name">Foreman</th>
                                        <th style="width:100px;" class="project-name">Hours</th>
                                        <th style="width:100px;" class="project-name">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                 
                                    if (!empty($projects)) {
                                        $i = 0;
                                        foreach ($projects as $u) {
											
											
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->id ?>"><?php echo get_project_title($u->id) ?></a></td>
                                                <td colspan="4">
													<?php 
													if(!empty($manpower)){
														
														echo '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
														$total=0;
														foreach($manpower as $m){
															$log=get_hours_logged_report($u->id,$m);
															if(!empty($log)){
																$cou=0;
																foreach($log as $l){
																	$total += $l->hours_logged;
																	$cou++;
																	$rowspan=get_row_span_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																	$sum	=get_foreman_log_sum_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																	 ?>
																
																	<tr>
																		<td class="tbl-report-td" style="width:100px;"><?php echo get_resource_title_direct($m) ?></td>
																		<td class="tbl-report-td" style="width:100px;"><?php echo get_name($l->foreman_id) ?></td>
																		<td class="tbl-report-td" style="width:100px;"><?php echo $l->hours_logged ?></td>
																		<?php if($cou==1){ ?>
																			<td class="tbl-report-td" style="width:100px;" rowspan=<?php echo $rowspan ?>><?php echo $sum; ?></td>
																		<?php } ?>
																	</tr>
																<?php } ?>
																
															<?php }
														}
														if($total>0){	?>
															<tr>
																	<td class="tbl-report-td" colspan=3><b>Total</b></td>
																	<td class="tbl-report-td"><b><?php echo $total ?></b></td>
															</tr>
														<?php }
														echo '</table>';
													}else{
															//get project manpower
															$manpower1=get_manpower_report();
															echo '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
															$total=0;
															foreach($manpower1 as $m){
																$log=get_hours_logged_report($u->id,$m->id);
																if(!empty($log)){	
																	$cou=0;
																	foreach($log as $l){ 
																		$total += $l->hours_logged;
																		$cou++;
																		$rowspan=get_row_span_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																		$sum	=get_foreman_log_sum_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																		?>
																	
																		<tr>
																			<td class="tbl-report-td" style="width:100px;"><?php echo get_resource_title_direct($m->id) ?></td>
																			<td class="tbl-report-td" style="width:100px;"><?php echo get_name($l->foreman_id) ?></td>
																			<td class="tbl-report-td" style="width:100px;"><?php echo $l->hours_logged ?></td>
																			<?php if($cou==1){ ?>
																				<td class="tbl-report-td" style="width:100px;"  rowspan=<?php echo $rowspan ?>><?php echo $sum; ?></td>
																			<?php } ?>
																		</tr>
																	<?php } ?>
																<?php }
															} 
																
																
																
															if($total>0){	?>
															<tr>
																	<td class="tbl-report-td" colspan=3><b>Total</b></td>
																	<td class="tbl-report-td"><b><?php echo $total ?></b></td>
															</tr>
															<?php }
															 echo '</table>';
															
														}
													 ?>
                                                </td>
                                               
                                            </tr>
                                            <?php
                                        }
                                    } else { 
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

