<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;


use App\Models\Subcontractor;
use App\Models\SubcontractorMember;

class SubcontractorController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		$subcontractors=Subcontractor::orderBy('subcontractor', 'ASC')->paginate(100);
		return view('admin/subcontractor/manage', array('title' => 'Subcontractors','subcontractors'=>$subcontractors,'active'=>'SUBCONTACTOR'));
	}
	public function add(){
		
		return view('admin/subcontractor/add', array('title' => 'Add New Contractor','active'=>'ADD_SUBCONTACTOR')); 
	}
	public function save(){
		
			$data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'subcontractor' => 'required',
                'owner' => 'required',
                'email' => 'required|email',
                'phone' => 'required|numeric'
			);
	       $validator = Validator::make($data, $rules);
	      
			$c_name=Input::get('c_name');
			$c_role=Input::get('c_role');
			$c_email=Input::get('c_email');
			$c_phone=Input::get('c_phone');
	      
			Session::flash('c_name', $c_name);
			Session::flash('c_role', $c_role);
			Session::flash('c_email', $c_email);
			Session::flash('c_phone', $c_phone);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/subcontractors/add')->withErrors($validator)->withInput(); 
			}else{
				$check_name = Subcontractor::where('subcontractor', '=', Input::get('subcontractor'))->count();
				
			
				if ($check_name != 0) {
					Session::flash('error', 'Subcontractors Name  has already been used. Please select another one!'); 
					return Redirect::to('admin/subcontractors/add')->with('data',array('title' => 'Add New Contractor'))->withInput(); ;
				}else{
					$sub=Subcontractor::create(array(
						'subcontractor'    =>  Input::get('subcontractor'),
						'owner'    =>  Input::get('owner'),
						'email'    =>  Input::get('email'),
						'phone'    =>  Input::get('phone')
					));	
					
					
					
					
					
					if(!empty($c_name)){
						foreach($c_name as $key=>$c){
							SubcontractorMember::create(array(
								'subcontractor_id'    =>  $sub->id,
								'name'    =>  $c,
								'role'    =>  $c_role[$key],
								'email'    =>  $c_email[$key],
								'phone'    =>  $c_phone[$key]
							));
						}
					}
					
					
				}
				Session::flash('success', 'Subcontractor Type Added successfully'); 
				return Redirect::to('admin/subcontractors/')->with('data',array('title' => 'SUBCONTRACTOR'));
			}
	}
	
	public function edit($id=null){
		$Subcontractor=Subcontractor::where('id','=',$id)->first();
		$data['title']='Edit Subcontractor';
        $data['active']='SUBCONTACTOR';
		$data['subcontractor']=$Subcontractor;
		$data['subcontractor_members']=SubcontractorMember::where('subcontractor_id','=',$id)->orderBy(\DB::raw("SUBSTRING_INDEX(name, ' ',-1)"))->get();
		
		return view('admin/subcontractor/edit',$data);
	}
	
	public function update(){
			$data = Input::all();
			$subcontractorid   = Input::get('subcontractorid');
			$rules = array(
				'subcontractor' => 'required',
                'owner' => 'required',
                'email' => 'required|email',
                'phone' => 'required|numeric'
			);
	       $validator = Validator::make($data, $rules);
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/subcontractors/edit/'.$subcontractorid)->withErrors($validator);
                     
			}else{
				$check_title = Subcontractor::where('subcontractor', '=', Input::get('subcontractor'))->where('id','!=',Input::get('subcontractorid'))->count();
				if($check_title != 0){
					Session::flash('error', 'Subcontractor has already been used. Please select another one!'); 
					return Redirect::to('admin/subcontractors/edit/'.$subcontractorid)->with('data',array('title' => 'Edit Subcontractor'));
				}else{
					Subcontractor::where('id', $subcontractorid)->update(array(
						'subcontractor'    =>  Input::get('subcontractor'),
						'owner' =>   Input::get('owner'),
						'email' =>   Input::get('email'),
						'phone' =>   Input::get('phone')
					));
					
					
					$c_name=Input::get('c_name');
					$c_role=Input::get('c_role');
					$c_email=Input::get('c_email');
					$c_phone=Input::get('c_phone');
					$c_id=Input::get('c_id');
					
					if(!empty($c_name)){						
						//CustomerMember::where('customer_id','=',$customerid)->delete();
						foreach($c_name as $key=>$c){
							
							//check if member exists
							$is_exists=SubcontractorMember::where('id',$c_id[$key])
													->where('subcontractor_id',$subcontractorid)
													->count();
							
							//echo $is_exists;die;
							
							if($is_exists){
								
								$data=SubcontractorMember::where('id',$c_id[$key])
													->where('subcontractor_id',$subcontractorid)
													->first();
								$member_id=$data->id;
								
								SubcontractorMember::where('id', $member_id)->update(array(
									'name'    =>   $c,
									'role'    =>   $c_role[$key],
									'email'    =>  $c_email[$key],
									'phone'    =>  $c_phone[$key]
								));
							}else{
								SubcontractorMember::create(array(
									'subcontractor_id'    =>  $subcontractorid,
									'name'    =>   $c,
									'role'    =>   $c_role[$key],
									'email'    =>  $c_email[$key],
									'phone'    =>  $c_phone[$key]
								));
							}
						}
					}
					
					
				}
				Session::flash('success', 'Subcontractor  updated successfully'); 
				return Redirect::to('admin/subcontractors/edit/'.$subcontractorid)->with('data',array('title' => 'Edit Subcontractor'));
           }
	}
	
	public function delete($id=null){
			Subcontractor::where('id','=',$id)->delete();
			Session::flash('success', 'Contractor Deleted successfully'); 
			return Redirect::to('admin/subcontractors/')->with('data',array('title' => 'SUBCONTRACTOR'));
	}
	
	
	
}
