<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

class UserController extends Controller {
	
	public function __construct(){
	  
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		
		$data['title']	= 	'Users';
		$data['active']	= 	'FOREMEN';
		
		$name=Input::get('name');
		$orderby=Input::get('order_by');
		
		if(isset($name) && !empty($name)){
				
				$data['users']	= 	User::where('role','!=','role_superadmin')
                                        ->where('name','LIKE','%'.$name.'%')
										->orWhere('job_role','LIKE','%'.$name.'%')
										->orderBy('name', 'ASC')
                                        ->get();
			
		}else{
			$data['users']	= 	User::where('role','!=','role_superadmin')->orderBy('name', 'ASC')->get();
		}
		return view('admin/user/manage', $data);
	}
	
	public function edit($username=null){
		$user=User::where('username','=',$username)->first();
		return view('admin/user/edit', array('title' => 'Edit users','user'=>$user,'active'=>'USER'));
	}
	public function update(){
			$data = Input::all();
			
			//echo '<pre>';
			//print_r($data);die;
			
			$userid   = Input::get('id');
			$username   = Input::get('username');
			// Applying validation rules.
			$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'username' => 'required'
			);
	       $validator = Validator::make($data, $rules);
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/users/edit/'.$username)->withErrors($validator);
                     
			}else{
				$check_email = User::where('email', '=', Input::get('email'))->where('id','!=',Input::get('id'))->count();
				$check_username = User::where('username', '=', Input::get('username'))->where('id','!=',Input::get('id'))->count();
				if ($check_email != 0) {
					Session::flash('error', 'Email has already been used. Please select another one!'); 
					return Redirect::to('admin/users/edit/'.$username)->with('data',array('title' => 'My Account'));
				}elseif($check_username != 0){
					Session::flash('error', 'Username has already been used. Please select another one!'); 
					return Redirect::to('admin/users/edit/'.$username)->with('data',array('title' => 'My Account'));
				}else{
					User::where('id', $userid)->update(array(
						'name'    =>  Input::get('name'),
						'username' =>   Input::get('username'),
						'email'  => Input::get('email'),
						'user_phone'  => Input::get('userphone')
					));
				}
				Session::flash('success', 'User  updated successfully'); 
				return Redirect::to('admin/users/edit/'.$username)->with('data',array('title' => 'My Account'));
           }
	}
	
	public function add(){
		return view('admin/user/add', array('title' => 'Add User','active'=>'ADD_USER'));
	}
	public function save(BcryptHasher $hash , Mail $mailer){
			$data = Input::all();
			$userid   = Input::get('id');
			// Applying validation rules.
			$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'username' => 'required',
			'password' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,9})',
			);
	       $validator = Validator::make($data, $rules,array(
							'password.regex' => 'Password must contain at least one number and both uppercase and lowercase letters.'
					));
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/users/add')->withErrors($validator)->withInput();
                     
			}else{
				$check_email = User::where('email', '=', Input::get('email'))->count();
				$check_username = User::where('username', '=', Input::get('username'))->count();
				if ($check_email != 0) {
					Session::flash('error', 'Email has already been used. Please select another one!'); 
					return Redirect::to('admin/users/add/')->with('data',array('title' => 'My Account'))->withInput();;
				}elseif($check_username != 0){
					Session::flash('error', 'Username has already been used. Please select another one!'); 
					return Redirect::to('admin/users/add/')->with('data',array('title' => 'My Account'))->withInput();;
				}else{
					
					$have_spaces=preg_match('/\s/',Input::get('username'));
					if($have_spaces){
						
						Session::flash('error', 'Please Remove Spaces from Username!'); 
						return Redirect::to('admin/users/add/')->with('data',array('title' => 'My Account'))->withInput();;
					}
					
					$have_uppercase=preg_match('/[A-Z]/', Input::get('username'));
					if($have_uppercase){
						
						Session::flash('error', 'Username must be in lowercase!'); 
						return Redirect::to('admin/users/add/')->with('data',array('title' => 'My Account'))->withInput();;
					}
					
					User::create(array(
						'name'    =>  Input::get('name'),
						'username' =>   Input::get('username'),
						'password' => $hash->make(Input::get('password')),
						'email'  => Input::get('email'),
						'user_phone'  => Input::get('userphone'),
						//'user_desc'  => Input::get('userdesc'),
						'role'  => Input::get('role'),
						'job_role'  => Input::get('job_role'),
                        
					));
					
					//sending email
					/*$mailer->send('emails.welcome', array(
                        'name' => $request->input('username')
                            ), function ($message) use($username, $email) {
                        $message->to($email, $username)->subject('Welcome to Rockspring');
					});*/
				}
				Session::flash('success', 'User  added successfully'); 
				return Redirect::to('admin/users')->with('data',array('title' => 'Users'));
           }
	}
	public function delete($id=null){
		$user=User::where('id','=',$id)->delete();
		Session::flash('success', 'User  deleted successfully'); 
		return Redirect::to('admin/users');
	}
	public function delete_sub($id=null){ 
		$user=User::where('id','=',$id)->delete();
		return Redirect::to('admin/subcontractor');
	}
	public function subcontractor(){
		$data['title']	= 	'Subcontractors';
		$data['active']	= 	'SUBCONTRACTOR';
		$data['users']	= 	User::where('role','=','role_subcontractor')->orderBy('id', 'DESC')->paginate(25);
		return view('admin/user/manage_subcontractor', $data);
	}
	
	public function change_status(){
			$id=Input::get('id');
			$status=Input::get('status');
			
			User::where('id', $id)->update(array(
						'status'  => $status
					));
			echo 1;die;
	}
    
    public function autocomplete(){
        $searchTerm =Input::get('term');
        $users	= 	User::where('role','!=','role_superadmin')->where('name','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->name;
        }
        //return json data
        echo json_encode($data);die;
       
    }
}
