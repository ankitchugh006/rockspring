<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ForemanRequest;
use App\Models\Surveys;
use App\Models\ProjectSurvey;

class SurveysController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		$surveys=Surveys::get();
		return view('admin/surveys/manage', array('title' => 'Surveys','surveys'=>$surveys,'active'=>'SURV'));
	}
	public function add(){
		
		return view('admin/surveys/add', array('title' => 'Add New Survey','active'=>'SURV'));
	}
	public function save(){
		
			$data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'survey' => 'required'
				
			);
	       $validator = Validator::make($data, $rules);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/surveys/add')->withErrors($validator)->withInput(); 
			}else{
                $project=Surveys::create(array(
                    'survey'    =>  Input::get('survey')
                ));	
				
				Session::flash('success', 'Survey Added successfully'); 
				return Redirect::to('admin/surveys/')->with('data',array('title' => 'Surveys'));
			}
	}
	
	public function edit($id=null){
		$surveys=Surveys::where('id','=',$id)->first();
		$data['title']='Edit Survey';
        $data['active']='SURV';
		$data['surveys']=$surveys;
		return view('admin/surveys/edit',$data);
	}
	
	public function update(){
            
			$data = Input::all();
			$surveyid   = Input::get('surveyid');
			$rules = array(
				'survey' => 'required'
				
			);
	       $validator = Validator::make($data, $rules);
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/surveys/edit/'.$surveyid)->withErrors($validator);
                     
			}else{
				
					Surveys::where('id', $surveyid)->update(array(
					
						'survey' =>   Input::get('survey'),
					));
			
				Session::flash('success', 'Survey  updated successfully'); 
				return Redirect::to('admin/surveys/edit/'.$surveyid)->with('data',array('title' => 'Edit Survey'));
           }
	}
	
	public function delete($id=null){
			Surveys::where('id','=',$id)->delete();
			Session::flash('success', 'Survey Deleted successfully'); 
			return Redirect::to('admin/surveys/')->with('data',array('title' => 'Surveys'));
	}
	
	public function projectSurveys(){    
        $ProjectSurvey=ProjectSurvey::get();
		return view('admin/surveys/projectsurvey', array('title' => 'Project Surveys','ProjectSurvey'=>$ProjectSurvey,'active'=>'SURV'));
        
    }
	
    public function addProjectSurvey(){
		return view('admin/surveys/addprojectsurvey', array('title' => 'Add New','active'=>'SURV'));
    }
    
    public function saveProjectSurvey(){
        
            $data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'project_id' => 'required',
                'question' => 'required'
			);
	       $validator = Validator::make($data, $rules);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/surveys/addprojectsurvey')->withErrors($validator)->withInput(); 
			}else{
               
                $questions=Input::get('question');
              
                if(!empty($questions)){
                    foreach($questions as $q){
                        
                        $count=ProjectSurvey::where('project_id',Input::get('project_id'))->where('question_id',$q)->count();
                        if($count==0){
                            $project=ProjectSurvey::create(array(
                                'project_id'    =>  Input::get('project_id'),
                                'question_id'    =>  $q
                            ));	
                        }
                    }
                    
                }
                
				Session::flash('success', 'Survey Added successfully'); 
				return Redirect::to('admin/surveys/projectsurveys')->with('data',array('title' => 'Project Surveys'));
			}
    }
    
    public function deleteprojectsurvey($id=0){
            ProjectSurvey::where('id','=',$id)->delete();
			Session::flash('success', 'Project Survey Deleted successfully'); 
			return Redirect::to('admin/surveys/projectsurveys')->with('data',array('title' => 'Project Surveys'));
    }
}
