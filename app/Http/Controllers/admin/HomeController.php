<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Project;
use App\Models\ForemanRequest;
use App\Models\ProjectForeman;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\ProjectSurvey;

class HomeController extends Controller {
	public function __construct(){
	  
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		
		$data['title']			=   'Admin';
		$data['active']	= 	'DASHBOARD';
		
		
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		
		if($user_role=='role_superadmin'){
			$data['projects']		= 	Project::count();
			$data['users']			= 	User::where('role','=','role_foreman')->count();
			$data['hourslogged']	=   Worklog::sum('hours_logged');
			$data['requests']		= 	ForemanRequestMain::count();
			$data['todaysrequests']	= 	ForemanRequestMain::whereDate('created_at','=',date('Y-m-d'))->whereRaw("TIME(created_at) < '11:00:00'")->orderBy('id', 'DESC')->get();
			$data['worklogs']		= 	WorklogMain::orderBy('id', 'DESC')->paginate(5);
			$data['surveys']		= 	ProjectSurvey::orderBy('id', 'DESC')->paginate(5);
		}elseif($user_role=='role_administrator'){
				$data['projects']		= 	Project::count();
				$data['users']			= 	User::where('role','=','role_foreman')->count();
				$data['hourslogged']	=   Worklog::sum('hours_logged');
				$data['requests']		= 	ForemanRequestMain::count();
				
				//Conditional Logic as per Job Role
				$projects_ids=getUserPojectIds(Auth::user()->id);				
				$data['todaysrequests']	= 	ForemanRequestMain::whereDate('created_at','=',date('Y-m-d'))
											->whereRaw("TIME(created_at) < '11:00:00'")
											->whereIn('project_id',$projects_ids)
											->orderBy('id', 'DESC')->get();
											
				$data['worklogs']		= 	WorklogMain::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->paginate(5);
				
				$data['surveys']		= 	ProjectSurvey::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->paginate(5);
		}	
		
		
		return view('admin/home/home', $data);
	}
	
	public function login(Mail $mail, BcryptHasher $hash) {
			 $data = Input::all();
             $rules = array(
		        'username' => 'required',
		        'password' => 'required',
	         );

			$validator = Validator::make($data, $rules);
	         if ($validator->fails()){
                      // If validation falis redirect back to login.
                      return Redirect::to('/')->withErrors($validator);
                     
			
			} else {
				$userdata = array(
                          'username' => Input::get('username'),
                          'password' => Input::get('password')
                );
				if (Auth::validate($userdata)) {
					if (Auth::attempt($userdata)){						
						//updating some data on login
						$userdata = User::where('username', '=', Input::get('email'))->get();
						$userdata1 = $userdata->first();
						$userdata1->access_token = str_random(40);
						$userdata1->last_login = date('Y-m-d h:m:s');
						$userdata1->save();
						
						 return Redirect::to('/admin')->with(array('title','Admin'));
					}
				} else{
					// If validation falis redirect back to login.
					  Session::flash('error', 'Oops!! Invalid username and password combination'); 
                      return Redirect::to('/')->withErrors($validator);
				}
			}
    }
}
