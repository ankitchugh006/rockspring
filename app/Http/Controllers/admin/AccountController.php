<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

class AccountController extends Controller {
	
	public function __construct(){
	  
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		$user=User::where('id','=', Auth::user()->id)->first();
		return view('admin/account/myaccount', array('title' => 'MyAccount','user'=>$user));
	}
	
	public function edit($id=null){
		$user=User::where('id','=',$id)->first();
		return view('admin/user/edit', array('title' => 'Edit users','user'=>$user));
	}
	public function update(){
			$data = Input::all();
			$userid   = Input::get('id');
			// Applying validation rules.
			$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'username' => 'required'
			);
	       $validator = Validator::make($data, $rules);
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/users/edit/'.$userid)->withErrors($validator);
                     
			}else{
				$check_email = User::where('email', '=', Input::get('email'))->where('id','!=',Input::get('id'))->count();
				$check_username = User::where('username', '=', Input::get('username'))->where('id','!=',Input::get('id'))->count();
				if ($check_email != 0) {
					Session::flash('error', 'Email has already been used. Please select another one!'); 
					return Redirect::to('admin/myaccount')->with('data',array('title' => 'My Account'));
				}elseif($check_username != 0){
					Session::flash('error', 'Username has already been used. Please select another one!'); 
					return Redirect::to('admin/myaccount')->with('data',array('title' => 'My Account'));
				}else{
					User::where('id', $userid)->update(array(
						'name'    =>  Input::get('name'),
						'username' =>   Input::get('username'),
						'email'  => Input::get('email'),
						'user_phone'  => Input::get('userphone'),
						'user_desc'  => Input::get('userdesc')
					));
				}
				Session::flash('success', 'Information  updated successfully'); 
				return Redirect::to('admin/myaccount/')->with('data',array('title' => 'My Account'));
           }
	}
	public function resetpassword(){
		
		return view('admin/account/reset', array('title' => 'Reset Password'));
	}
	public function reset(Mail $mail){
		
		
		$validator = Validator::make(
				Input::all(), array(
				    'password_confirmation' => 'required',
				    'password' => 'required',
				    'email' => 'required|email'
				)
        );
		 if ($validator->fails()){
                 // If validation falis redirect back to signup.
                //Session::flash('error', 'Incorrect Password!'); 
                return Redirect::to('admin/myaccount/resetpassword')->withErrors($validator);
                     
		 }
		$user = User::where('email', '=', Input::get('email'));
		if($user->count()>0){
			$conformedpassword=Input::get('password_confirmation');
			
			$credentials = [
					'email' => Input::get('email'),
					'password' => Input::get('password_confirmation')
			];
			if( Auth::attempt($credentials) )
			{
				$user = User::where('email', '=', Input::get('email'));
				$code = str_random(60);
				$user = $user->first();
				$user->recoverycode = $code;
				$email = $user->email;
				$username = $user->username;

				if ($user->save()) {
					$mail->send('emails.password', array(
						'link' => url() . '/api/update_password?key=D4788EBA535DC57377BAD975DED90&code=' . $code,
						'username' => $user->username
							), function ($message) use($username, $email) {
						$message->to($email, $username)->subject('Password Recovery');
					});
				}

				Session::flash('success', 'Recovery Email Sent!!!'); 
				return Redirect::to('admin/myaccount/resetpassword')->with('data',array('title' => 'Reset Password'))->withInput();
			}else{
					Session::flash('error', 'Incorrect Password!'); 
					return Redirect::to('admin/myaccount/resetpassword')->with('data',array('title' => 'Reset Password'))->withInput();
			}
		}else{
			Session::flash('error', 'Email not Exists!!!'); 
			return Redirect::to('admin/myaccount/resetpassword')->with('data',array('title' => 'Reset Password'))->withInput();
		}
	}
	
}
