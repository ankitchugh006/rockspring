@extends('layout.admin')

@section('content')

<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
					<div class="col-lg-12">   
						<h1 class="page-header">
								{{{$title}}}
						 </h1>
                     </div>
                    <div class="col-lg-8">   
						<?php  echo display_breedcrump(); ?>
                    </div>
                    
                </div>
                <!-- /.row -->

                <div class="row">
					<div class="col-lg-12">
						<h3>Project Detail</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
							  <tbody>
								   <tr>
									<td><strong>Project Id</strong></td>
									<td>#<?php  echo $project->id;?></td>     
								  </tr>
								  <tr>
									<td><strong>Project Title</strong></td>
									<td><?php  echo $project->title;?></td>     
								  </tr>
								   <tr>
									<td><strong>Project Description</strong></td>
									<td><?php  echo $project->description;?></td>     
								  </tr>

								</tbody>
							</table>
							</div>
					</div>
					<br/><br/>
                    <div class="col-lg-12">
							<h3>Requested Resources </h3>
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Project Title</th>
											<th>Foreman</th>
											<th>Requested Resource</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$i=0; 
										if(!empty($requests)){
										foreach($requests as $u){ $i++; ?>
										<tr <?php if($i==1) { echo 'style="background:#d9edf7;"';} ?>>
											<td><?php echo $u->id ?></td>
											<td><?php echo get_project_title($u->project_id) ?></td>
											<td><?php echo get_name($u->foreman_id) ?></td>
											<td><?php echo get_resource_title($u->resource_id) ?></td>
										</tr>
										<?php } }?>
									</tbody>
								</table>
								
							</div>
                       
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
