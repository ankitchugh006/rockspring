<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Notification;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorRequest;
use App\Models\SubcontractorWorklog;
use App\Models\Manpowerrecord;
use App\Models\Subcontractor;
use App\Models\SubcontractorMember;
use PDF;
use Illuminate\Contracts\Mail\Mailer as Mail;

class ProjectController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		
		$title=Input::get('title');
		$orderby=Input::get('order_by');
		$complete=Input::get('complete');
		
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		
		if($user_role=='role_superadmin'){
			if(!empty($title) && $complete =='' ){
							$projects	= 	Project::where('title','LIKE','%'.$title.'%')
											->orWhere('id','LIKE','%'.$title.'%')
											->orderBy('manual_id', 'ASC')->get();
					
			
			}elseif(empty($title) && $complete!=''){
					 
						$projects	= 	Project::where('is_completed','=',$complete)->orderBy('manual_id', 'ASC')->get();

			}elseif(!empty($title) && $complete !='' ){
					 
						$projects	= 	Project::where('title','LIKE','%'.$title.'%')
										->Where('is_completed','=',$complete)
										->orWhere('id','LIKE','%'.$title.'%')
										
										->orderBy('manual_id', 'ASC')->get();
					
			
			}elseif(empty($title) &&  isset($complete) && empty($complete) ){
				
						$projects	= 	Project::orderBy('manual_id', 'ASC')->get();
			}else{
				
				$projects=Project::where('is_completed','=',0)->orderBy('manual_id', 'ASC')->get();
			}
		}elseif($user_role=='role_administrator'){
			
			if(!empty($title) && $complete =='' ){
				
						 $projects=ProjectForeman::leftJoin('projects', function($join) {
						  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
						})->where('foreman_id', Auth::user()->id)
						
						->where('title','LIKE','%'.$title.'%')
						->orWhere('projects.id','LIKE','%'.$title.'%')
						->distinct('project_id')
						->select('projects.*','assigned_projects_to_foreman.foreman_id')
						->orderBy('projects.manual_id', 'ASC')
						->get();
					
			
			}elseif(empty($title) && $complete!=''){
					 
							 $projects=ProjectForeman::leftJoin('projects', function($join) {
						  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
						})->where('foreman_id', Auth::user()->id)
						->where('projects.is_completed', $complete)
						->distinct('project_id')
						->select('projects.*','assigned_projects_to_foreman.foreman_id')
						->orderBy('projects.manual_id', 'ASC')
						->get();

			}elseif(!empty($title) && $complete !='' ){
					 
					 $projects=ProjectForeman::leftJoin('projects', function($join) {
						  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
						})->where('foreman_id', Auth::user()->id)
						->where('projects.is_completed', $complete)
						->where('title','LIKE','%'.$title.'%')
						->distinct('project_id')
						->select('projects.*','assigned_projects_to_foreman.foreman_id')
						->orderBy('projects.manual_id', 'ASC')
						->get();
					 
					
			
			}elseif(empty($title) &&  isset($complete) && empty($complete) ){
						 $projects=ProjectForeman::leftJoin('projects', function($join) {
						  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
						})->where('foreman_id', Auth::user()->id)
						->distinct('project_id')
						->select('projects.*','assigned_projects_to_foreman.foreman_id')
						->orderBy('projects.manual_id', 'ASC')
						->get();
			}else{
				$projects=ProjectForeman::leftJoin('projects', function($join) {
						  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
				})->where('foreman_id', Auth::user()->id)
				->where('projects.is_completed','=',0)
				->distinct('project_id')
				->select('projects.*','assigned_projects_to_foreman.foreman_id')
				->orderBy('projects.manual_id', 'ASC')
				->get();
			}
			
		}
		
		return view('admin/project/manage', array('title' => 'Projects','projects'=>$projects,'active'=>'PROJECT'));
	}
	public function add(){
		$manpowers=Manpower::orderBy('number', 'ASC')->get();
		
		$users=User::where('role','=','role_foreman')->get();
		return view('admin/project/add', array('title' => 'Add New Project','manpowers'=>$manpowers,'users'=>$users,'active'=>'ADD_PROJECT'));
	}
	public function save(Mail $mail){
		
			$data = Input::all();
			
			// Applying validation rules.
			$rules = array(
				'project_id' => 'required',
				'title' => 'required',
				'description' => 'required',
				'customer' => 'required',
				'resources' => 'required',
				'hps' => 'numeric',
				'max_hours'=>'required',
				//'contractor_hidden'=>'required',
				'pm' => 'required',
				'apm' => 'required',
				'super' => 'required',
				'foremans' => 'required',
				'estimator' => 'required'
			);
	       $validator = Validator::make($data, $rules);
	       
	       Session::flash('resources', Input::get('resources')); 
	       //Session::flash('subcontractors', Input::get('subcontractors')); 
		   Session::flash('hours', Input::get('max_hours'));
		   Session::flash('foremans', Input::get('foremans'));
           Session::flash('pm', Input::get('pm'));
           Session::flash('apm', Input::get('apm'));
           Session::flash('super', Input::get('super'));
           Session::flash('estimator', Input::get('estimator'));
           Session::flash('customer', Input::get('customer'));
           Session::flash('scope', Input::get('scope'));
           Session::flash('surveys', Input::get('surveys'));
           Session::flash('member_customer', Input::get('member_customer'));
           Session::flash('member_customer_check', Input::get('member_customer_check'));
          
			
		   //echo '<pre>';
			//print_r(Input::get('member_customer'));
			//print_r(Input::get('member_customer_check'));
		   //echo '</pre>';die;
        
	       if ($validator->fails()){
                return Redirect::to('admin/projects/add')->withErrors($validator)->withInput(); 
			}else{
				$check_title = Project::where('title', '=', Input::get('title'))->count();
				
				$check_id = Project::where('manual_id', '=', Input::get('project_id'))->count();
				if ($check_id != 0) {
					Session::flash('error', 'Job # already been used. Please select another one!'); 
					return Redirect::to('admin/projects/add')->with('data',array('title' => 'Add New Project'))->withInput();
				}elseif ($check_title != 0) {
					Session::flash('error', 'Title  has already been used. Please select another one!'); 
					return Redirect::to('admin/projects/add')->with('data',array('title' => 'Add New Project'))->withInput();
				}else{
					
					//check if any value contains 0
					$resources=Input::get('resources');
					$hours=Input::get('max_hours');
					$subcontractors=Input::get('contractor_hidden');
								 
					
					if(!empty($resources)){
						foreach($resources as $key=>$r){
							$h=empty($hours[$key])?0:$hours[$key];
							/*if(empty($hours[$key])){
								
								Session::flash('error', 'Available hours must be a valid number!!'); 
								return Redirect::to('admin/projects/add')->with('data',array('title' => 'Add New Project'))->withInput();
							}*/
							if(!is_numeric($hours[$key])){
								Session::flash('error', 'Available hours must be a number!!'); 
								return Redirect::to('admin/projects/add')->with('data',array('title' => 'Add New Project'))->withInput();
							}
						}
					}
				    
                    
                    
                    $scope='';
                    if(Input::get('scope')){
                        $scope=implode(',',Input::get('scope'));
                    }
                    
                    
                    
                    //saving  member customers for report
					$member_customer_check=Input::get('member_customer_check');
					$member_customer=Input::get('member_customer');
					$member_customer_to_save=array()  ;  
					
					
					if(!empty($member_customer)){
						foreach($member_customer as $key=>$mc){
							
							if(isset($mc) && !empty($mc)){
								
								$member_customer_to_save[$key]['member_id']=$mc;
								if(!empty($member_customer_check)){
									if(!empty($member_customer_check) && in_array($key,$member_customer_check)){
										$member_customer_to_save[$key]['report']=1;
									}else{
										$member_customer_to_save[$key]['report']=0;
									}
								}
							}
						}
					}
				
                    
                    //linking survey questions
						
					$surveyquestions=Input::get('surveys');
					
					if(!empty($surveyquestions)){
						$surveyquestions=implode(',',$surveyquestions);
					}
					
                    
					$project=Project::create(array(
						'manual_id'    =>  Input::get('project_id'),
						'title'    =>  Input::get('title'),
						'description' =>   Input::get('description'),
						'city' =>   Input::get('city'),
						'state' =>   Input::get('state'),
						'zip' =>   Input::get('zip'),
						'customer_id' =>   Input::get('customer'),
						'address' =>   Input::get('address'),
						'scope' =>  $scope,
                        'hps' =>   Input::get('hps'),
						'shift_type' =>   Input::get('shift_type'),
                        'start_date' =>   Input::get('start_date'),
						'added_by' =>Auth::user()->id,
						'member_customer' =>json_encode($member_customer_to_save),
						'survey_questions' =>$surveyquestions,
						'created_at' =>new \DateTime(),
						'status' => 1
					));
					
					$project_id=$project->id;
                    
					if(isset($project_id)){
						$resources=Input::get('resources');
						$hours=Input::get('max_hours');
						
						if(!empty($resources)){
							foreach($resources as $key=>$r){
								
								//saving to manpower table if new resource type
								/*$is_r_exists	= 	Manpower::where('title','LIKE','%'.$r.'%')->count();
								
								if(!$is_r_exists){
									//saving
									  Manpower::create(array(
										'title'    =>  $r,
										'number' =>   str_random(3),
										'added_by' => Auth::user()->id,
										'status' =>1
									));	
                        
									
								}*/
								//$s=$subcontractors[$key];
							    $h=empty($hours[$key])?0:$hours[$key];
								ProjectResources::create(array(
									'project_id'    =>  $project_id,
									'manpower_id' =>   $r,
									'max_hours' =>$h,
									//'subcontractors'=>$s
								));
							}
						}
                        
                        
                        /***************************/
                        
                        /*$subcontractors=Input::get('subcontractors');
                    
                            if(!empty($subcontractors)){
                                foreach($subcontractors as $key=>$r){
                                    ProjectSubcontractors::create(array(
                                        'project_id'    =>  $project_id,
                                        'subcontractor_id' =>   $r
                                    ));
                                }
                            }
						*/
                            

						/**************************/
						
                        
                        $pm=Input::get('pm');
                        if(!empty($pm)){
							foreach($pm as $p){
								ProjectForeman::create(array(
									'project_id'    =>  $project_id,
									'foreman_id' =>   $p,
									'type' =>   'pm',
									'is_completed' =>0
								));	
							}
						}
                        
                        $apm=Input::get('apm');
                         if(!empty($apm)){
							foreach($apm as $p){
								ProjectForeman::create(array(
									'project_id'    =>  $project_id,
									'foreman_id' =>   $p,
									'type' =>   'apm',
									'is_completed' =>0
								));	
						    }
						 }
						
                        $super=Input::get('super');
 
                        if(!empty($super)){
							foreach($super as $p){
								 ProjectForeman::create(array(
									'project_id'    =>  $project_id,
									'foreman_id' =>   $p,
									'type' =>   'super',
									'is_completed' =>0
								));	
                        
						    }
						 }
                        
                        $estimator=Input::get('estimator');
 
                        if(!empty($estimator)){
							foreach($estimator as $p){
								 ProjectForeman::create(array(
									'project_id'    =>  $project_id,
									'foreman_id' =>   $p,
									'type' =>   'estimator',
									'is_completed' =>0
								));	
                        
						    }
						 }
                        
                       
                        $foremans=Input::get('foremans');
                        ProjectForeman::create(array(
                            'project_id'    =>  $project_id,
                            'foreman_id' =>   $foremans,
							'type' =>   'foreman',
                            'is_completed' =>0
                        ));	

						//assign project to superadmin
						ProjectForeman::create(array(
                            'project_id'    =>  $project_id,
                            'foreman_id' =>   129,
							'type' =>   'foreman',
                            'is_completed' =>0
                        ));	
						
						
						
                        
					}
				}
				Session::flash('success', 'Project Added successfully'); 
				return Redirect::to('admin/projects/')->with('data',array('title' => 'Projects'));
			}
	}
	
	public function edit($id=null){
		$project=Project::where('projects.manual_id','=',$id)->first();
		
		$id=$project->id;
						
		$assignedresources=ProjectResources::where('project_id','=',$id)->get();
		$assignedforemans = ProjectForeman::where('project_id','=',$id)->get();
		
		$manpowers=Manpower::orderBy('number', 'ASC')->get();
		
		$users=User::where('role','=','role_foreman')->get();
		
		$data['active']='PROJECT';
		$data['title']='Edit Project';
		$data['project']=$project;
		$data['assignedresources']=$assignedresources->toArray();
		$data['assignedforemans']=$assignedforemans;
		$data['users']=$users;
		$data['manpowers']=$manpowers;
		$data['active']='PROJECT';
		
		return view('admin/project/edit',$data);
	}
	
	public function update(){
			$data = Input::all();
			$projectid   = Input::get('projectid');
			$manual_id   = Input::get('manual_id');
			$rules = array(
				'title' => 'required',
				'description' => 'required',
				'customer' => 'required',
				'pm' => 'required',
				'apm' => 'required',
				'super' => 'required',
				'foremans' => 'required',
				'estimator' => 'required'
			);
	       $validator = Validator::make($data, $rules);
	       
	       
	       
	       Session::flash('resources', Input::get('resources')); 
		   Session::flash('hours', Input::get('max_hours'));
		   Session::flash('foremans', Input::get('foremans'));
           Session::flash('pm', Input::get('pm'));
           Session::flash('apm', Input::get('apm'));
           Session::flash('super', Input::get('super'));
           Session::flash('estimator', Input::get('estimator'));
           Session::flash('customer', Input::get('customer'));
           Session::flash('scope', Input::get('scope'));
           Session::flash('surveys', Input::get('surveys'));
           Session::flash('member_customer', Input::get('member_customer'));
           Session::flash('member_customer_check', Input::get('member_customer_check'));
           Session::flash('shift_type', Input::get('shift_type'));
	       
	       
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/projects/edit/'.$manual_id)->withErrors($validator)->withInput();
                     
			}else{
				$check_title = Project::where('title', '=', Input::get('title'))->where('id','!=',Input::get('projectid'))->count();
				if($check_title != 0){
					Session::flash('error', 'Project Title has already been used. Please select another one!'); 
					return Redirect::to('admin/projects/edit/'.$manual_id)->with('data',array('title' => 'Edit Project'))->withInput();
				}else{
					
					$resources=Input::get('resources');
					//$hours=array_merge(array(),array_filter(Input::get('max_hours')));
					$hours=Input::get('max_hours');
					
					/*if(!empty($resources)){
						foreach($resources as $key=>$r){
							$h=empty($hours[$key])?0:$hours[$key];
							if(!is_numeric($hours[$key])){
								Session::flash('error', 'Available hours must be a number!!'); 
								return Redirect::to('admin/projects/edit/'.$manual_id)->with('data',array('title' => 'Edit Project','active'=>'PROJECT'))->withInput();
							}
						}
					}*/
					
                    $scope='';
                    if(Input::get('scope')){
                        $scope=implode(',',Input::get('scope'));
                    }
                    

					//saving  member customers for report
					$member_customer_check=Input::get('member_customer_check');
					$member_customer=Input::get('member_customer');
					$member_customer_to_save=array()  ;  
					
					
					if(!empty($member_customer)){
						foreach($member_customer as $key=>$mc){
							
							if(isset($mc) && !empty($mc)){
								
								$member_customer_to_save[$key]['member_id']=$mc;
								if(!empty($member_customer_check) && in_array($key,$member_customer_check)){
									$member_customer_to_save[$key]['report']=1;
								}else{
									$member_customer_to_save[$key]['report']=0;
								}
							}
						}
					}
				
				
					//linking survey questions
						
					$surveyquestions=Input::get('surveys');
					
					if(!empty($surveyquestions)){
						$surveyquestions=implode(',',$surveyquestions);
					}
					
					Project::where('id', $projectid)->update(array(
						'title'    =>  Input::get('title'),
						'description' =>   Input::get('description'),
						'city' =>   Input::get('city'),
						'state' =>   Input::get('state'),
						'zip' =>   Input::get('zip'),
						'customer' =>   Input::get('customer'),
						'customer_id' =>   Input::get('customer'),
						'address' =>   Input::get('address'),
                        'scope' =>  $scope,
						'hps' =>   Input::get('hps'),
						'shift_type' =>   Input::get('shift_type'),
                        'start_date' =>   Input::get('start_date'),
                        'member_customer' =>json_encode($member_customer_to_save),
						'is_completed' =>   Input::get('is_completed'),
						'survey_questions' =>$surveyquestions
					));
					
					/***************updating other data****************/
					
					if(isset($projectid)){
						$resources=Input::get('resources');
						$hours=Input::get('max_hours');
						$subcontractors=(Input::get('contractor_hidden'));
						
						
						/*$hours=array_filter($hours, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
						});*/
						
						/*$subcontractors=array_filter($subcontractors, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
						});*/
						
						/*$resources=array_filter($resources, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
						});*/
						
						
					
						if(!empty($resources)){
							
							foreach($resources as $key=>$r){
								$is_exists = ProjectResources::where('project_id', '=', $projectid)
															->where('manpower_id','=',$r)->count();
								if($is_exists != 0){									
									ProjectResources::where('project_id', $projectid)->where('manpower_id','=',$r)->update(array(
										'max_hours'    =>  $hours[$key],
										//'subcontractors' =>@$subcontractors[$key]
									));								
								}else{
									if((isset($hours[$key]) || $hours[$key]==0)){
										ProjectResources::create(array(
											'project_id'    =>  $projectid,
											'manpower_id' =>   $r,
											'max_hours' =>$hours[$key],
											//'subcontractors' =>@$subcontractors[$key]
										));
									}
								}
							}
						}
						/**************************/
						
						
						ProjectForeman::where('project_id', $projectid)->delete();
                        
                        $pm=Input::get('pm');
                        if(!empty($pm)){
							foreach($pm as $p){
								ProjectForeman::create(array(
									'project_id'    =>  $projectid,
									'foreman_id' =>   $p,
									'type' =>   'pm',
									'is_completed' =>0
								));	
							}
						}
                        
                        $apm=Input::get('apm');
                         if(!empty($apm)){
							foreach($apm as $p){
								ProjectForeman::create(array(
									'project_id'    =>  $projectid,
									'foreman_id' =>   $p,
									'type' =>   'apm',
									'is_completed' =>0
								));	
						    }
						 }
						
                        $super=Input::get('super');
 
                        if(!empty($super)){
							foreach($super as $p){
								 ProjectForeman::create(array(
									'project_id'    =>  $projectid,
									'foreman_id' =>   $p,
									'type' =>   'super',
									'is_completed' =>0
								));	
                        
						    }
						 }
                        
                        $estimator=Input::get('estimator');
 
                        if(!empty($estimator)){
							foreach($estimator as $p){
								 ProjectForeman::create(array(
									'project_id'    =>  $projectid,
									'foreman_id' =>   $p,
									'type' =>   'estimator',
									'is_completed' =>0
								));	
                        
						    }
						 }
                        
                       
                        $foremans=Input::get('foremans');
                        ProjectForeman::create(array(
                            'project_id'    =>  $projectid,
                            'foreman_id' =>   $foremans,
							'type' =>   'foreman',
                            'is_completed' =>0
                        ));	
                        
                        
                        //assign project to superadmin
						ProjectForeman::create(array(
                            'project_id'    =>  $projectid,
                            'foreman_id' =>   129,
							'type' =>   'foreman',
                            'is_completed' =>0
                        ));	
                        
                        /************************************/
                        
                        /*$subcontractors=Input::get('subcontractors');

                        if(!empty($subcontractors)){
                            foreach($subcontractors as $key=>$r){
                                $count=project_has_subcontractor($projectid,$r);
                                if($count == 0){
                                    ProjectSubcontractors::create(array(
                                        'project_id'    =>  $projectid,
                                        'subcontractor_id' =>   $r
                                    ));
                                }
                            }
                        }
                        */
                        
					}
					
				}
				
				updateProjectDate($projectid);
				Session::flash('success', 'Project  updated successfully'); 
				//return Redirect::to('admin/projects/edit/'.$manual_id)->with('data',array('title' => 'Edit Project','active'=>'PROJECT'))->withInput();
				return Redirect::to('admin/projects/')->with('data',array('title' => 'Projects'));
           }
	}
	public function view($id=null){
		
		$date=date('Y-m-d');
		$project=Project::where('projects.manual_id',$id)->first();
		

		$id=$project->id;
						
		$assignedresources=ProjectResources::where(	'project_id','=',$id)->get();
		$assignedforemans = ProjectForeman::where('project_id','=',$id)->get();
		
		$manpowers=Manpower::orderBy('number', 'ASC')->get();
		$users=User::where('role','=','role_foreman')->get();
		
		$data['active']='PROJECT';
		$data['title']='View Project';
		$data['project']=$project;
		
		$data['hourslogged']=0;
		
		$logged	=   Worklog::where('project_id','=',$id)->select('hours_logged','quantity')->get();
		
		if(count($logged)){
			foreach($logged as $a){
				$data['hourslogged']	+=	$a->hours_logged; 
			}
		}
		
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$id)->sum('max_hours');
															
		$data['surveys']=ProjectSurvey::where('project_id','=',$id)->orderBy('id', 'DESC')->take(5)->get();
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$id)->count();
		$data['projectlogs']	=	WorklogMain::where('project_id','=',$id)->orderBy('id', 'DESC')->take(5)->get();
		$data['projectrequests']=	ForemanRequestMain::where('project_id','=',$id)->whereDate('created_at','=',$date)->orderBy('id', 'DESC')->take(5)->get();
		$data['tasks']=	Task::where('project_id','=',$id)->orderBy('id', 'DESC')->take(5)->get();
		$data['assignedresources']=$assignedresources->toArray();
		$data['assignedforemans']=$assignedforemans;
		$data['users']=$users;
		$data['manpowers']=$manpowers;
        
        $data['files']=File::where('project_id','=',$id)->orderBy('id', 'DESC')->take(5)->get();
        
		$data['active']='PROJECT';
		
		return view('admin/project/view',$data);
	}
	
	public function view_project_id($id=null){
		$date=date('Y-m-d');
		$project=Project::where('projects.id',$id)->first();

						
		$assignedresources=ProjectResources::where(	'project_id','=',$id)->get();
		$assignedforemans = ProjectForeman::where('project_id','=',$id)->get();
		
		$manpowers=Manpower::all();
		$users=User::where('role','=','role_foreman')->get();
		
		$data['active']='PROJECT';
		$data['title']='View Project';
		$data['project']=$project;
		
		$data['hourslogged']=0;
		
		$logged	=   Worklog::where('project_id','=',$id)->select('hours_logged','quantity')->get();
		
		if(count($logged)){
			foreach($logged as $a){
				$data['hourslogged']	+=	$a->hours_logged;
			}
		}
		
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$id)->sum('max_hours');
															
		$data['surveys']=ProjectSurvey::where('project_id','=',$id)->orderBy('id', 'DESC')->get();
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$id)->count();
		$data['projectlogs']	=	WorklogMain::where('project_id','=',$id)->orderBy('id', 'DESC')->get();
		$data['projectrequests']=	ForemanRequestMain::where('project_id','=',$id)->whereDate('created_at','=',$date)->orderBy('id', 'DESC')->get();
		$data['tasks']=	Task::where('project_id','=',$id)->orderBy('id', 'DESC')->get();
		$data['assignedresources']=$assignedresources->toArray();
		$data['assignedforemans']=$assignedforemans;
		$data['users']=$users;
		$data['manpowers']=$manpowers;
        
        $data['files']=File::where('project_id','=',$id)->orderBy('id', 'DESC')->get();
        
		$data['active']='PROJECT';
		
		return view('admin/project/view',$data);
	}
	public function estimate($id=null){
		$data['active']='PROJECT';
		$data['title']='View Project';
		
		$data['project']=Project::where('projects.manual_id','=',$id)->first();
		$id=$data['project']->id;
		
		
		$assignedresources=ProjectResources::where(	'project_id','=',$id)->get();
		$assignedforemans = ProjectForeman::where('project_id','=',$id)->get();
		
		
		
		$data['hourslogged']=0;
		
		$logged	=   Worklog::where('project_id','=',$id)->select('hours_logged','quantity')->get();
		
		if(count($logged)){
			foreach($logged as $a){
				$data['hourslogged']	+=	$a->hours_logged;
			}
		}
		
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$id)->sum('max_hours');
		$data['surveys']=Survey::where('project_id','=',$id)->orderBy('id', 'DESC')->get();
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$id)->count();
		
		$data['assignedresources']=$assignedresources->toArray();
		$data['assignedforemans']=$assignedforemans;
		
		$data['users']=User::where('role','=','role_foreman')->get();

		$data['manpowers']=Manpower::orderBy('number', 'ASC')->get();
		
		return view('admin/project/estimate',$data);
	}
	
	public function update_estimate(){
		$data = Input::all();
		$rules = array(
			'resources' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		 if ($validator->fails()){
			 return Redirect::to('admin/projects/estimate/'.$manual_id)->with('data',array('title' => 'Project Estimate','active'=>'PROJECT'))->withErrors($validator);
		 }
		
		
		$projectid   = Input::get('projectid');
		$manual_id   = Input::get('manual_id');
		$resources=Input::get('resources');


		

		$hours=array_merge(array(),array_filter(Input::get('max_hours')));
		
		//echo '<pre>';
		//print_r($resources);
		//print_r($hours);die;
		
		if(!empty($resources)){
			foreach($resources as $key=>$r){
				$h=empty($hours[$key])?0:$hours[$key];
				if(empty($hours[$key]) || $hours[$key]==0){
					Session::flash('error', 'Available hours must be filled and should be greater than 0'); 
					return Redirect::to('admin/projects/estimate/'.$manual_id)->with('data',array('title' => 'Project Estimate','active'=>'PROJECT'));
				}
			}
		}
		

		if(!empty($resources)){
			
			foreach($resources as $key=>$r){
				$is_exists = ProjectResources::where('project_id', '=', $projectid)
											->where('manpower_id','=',$r)->count();
				if($is_exists != 0){
					ProjectResources::where('project_id', $projectid)->where('manpower_id','=',$r)->update(array(
						'max_hours'    =>  $hours[$key]
					));
				}
				else{
					if(isset($hours[$key]) && !empty($hours[$key])){
						ProjectResources::create(array(
							'project_id'    =>  $projectid,
							'manpower_id' =>   $r,
							'max_hours' =>$hours[$key]
						));
					}
				}
			}
		}
		
		updateProjectDate($projectid);
		
		Session::flash('success', 'Budget Updated'); 
		return Redirect::to('admin/projects/estimate/'.$manual_id)->with('data',array('title' => 'Project Estimate','active'=>'PROJECT'));
	}
	
	public function survey($id=null){
		$survey=Survey::where('survey_id',$id)->get();
		$project=Project::where('id',$survey[0]->project_id)->first();
		return view('admin/project/survey', array('title' => 'Survey Details','project'=>$project,'survey'=>$survey,'active'=>'SURVEY'));
	}
	
	public function delete($id=null){
			Project::where('manual_id','=',$id)->delete();
			Session::flash('success', 'Project Deleted successfully'); 
			return Redirect::to('admin/projects/')->with('data',array('title' => 'Projects','active'=>'PROJECT'));
	}
	
	
	public function projectrequests($id=null){
		
		$date=date('Y-m-d');
		$requests=ForemanRequest::where('request_id',$id)->orderBy('id', 'ASC')->get();
		$pid=ForemanRequest::where('request_id',$id)->orderBy('id', 'DESC')->first();
		$detail=Project::where('id',$pid->project_id)->first();
		
		
		
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$pid->project_id)->orderBy('id','DESC')->count();
		$data['hourslogged']	=   Worklog::where('project_id','=',$pid->project_id)->sum('hours_logged');
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$pid->project_id)->sum('max_hours');
	
		return view('admin/project/requests', array('title' => 'Project Details','data'=>$data,'project'=>$detail,'requests'=>$requests,'active'=>'REQUEST'));
	}
	public function allrequests(){
		$date=date('Y-m-d');
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		if($user_role=='role_superadmin'){
			$requests=ForemanRequestMain::orderBy('id', 'DESC')->whereDate('created_at','=',$date)->get();
		}elseif($user_role=='role_administrator'){
			$projects_ids=getUserPojectIds(Auth::user()->id);
			$requests=ForemanRequestMain::whereIn('project_id',$projects_ids)->whereDate('created_at','=',$date)->orderBy('id', 'DESC')->get();		
			
		}
		
		
		return view('admin/project/allrequests', array('title' => 'All Requests','requests'=>$requests,'active'=>'REQUEST'));
	}
	
	public function view_all_requests(){
		
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		if($user_role=='role_superadmin'){
			$requests=ForemanRequestMain::orderBy('id', 'DESC')->get();
		}elseif($user_role=='role_administrator'){
			$projects_ids=getUserPojectIds(Auth::user()->id);
			$requests=ForemanRequestMain::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->get();		
			
		}
		
		
		return view('admin/project/allrequests', array('title' => 'All Requests','requests'=>$requests,'active'=>'REQUEST'));
	}
	
	public function project_requests($id=0){
		$project=Project::where('projects.manual_id',$id)->first();
		$id=$project->id;
		$requests=ForemanRequestMain::where('project_id',$id)->orderBy('id', 'DESC')->get();
		return view('admin/project/projectrequests', array('title' => 'Project Requests','requests'=>$requests,'active'=>'REQUEST'));
	}
	
	
	public function logs($id=null){
		$logs=Worklog::where('worklog_id',$id)->orderBy('id', 'ASC')->get();
		$pid=Worklog::where('worklog_id',$id)->orderBy('id', 'DESC')->first();
		$detail=Project::where('id',$pid->project_id)->first();
		
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$pid->project_id)->count();
		
		//$data['hourslogged']	=   Worklog::where('project_id','=',$pid->project_id)->sum('hours_logged');
		
		$data['hourslogged']=0;
		
		$logged	=   Worklog::where('project_id','=',$pid->project_id)->select('hours_logged','quantity')->get();
		
		if(count($logged)){
			foreach($logged as $a){
				$data['hourslogged']	+=	$a->hours_logged; 
			}
		}
		
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$pid->project_id)->sum('max_hours');
		
		
		return view('admin/project/logs', array('title' => 'Project Details','data'=>$data,'project'=>$detail,'logs'=>$logs,'active'=>'WORKLOG'));
	}
	public function projectlogs($id=0){
		
		$project=Project::where('projects.manual_id',$id)->first();
		$id=$project->id;
		
		$logs=WorklogMain::where('project_id',$id)->orderBy('id', 'DESC')->paginate(25);
		
		return view('admin/project/alllogs', array('title' => 'Project Logs','logs'=>$logs,'active'=>'WORKLOG'));
	}
	public function all_logs(){
		
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		if($user_role=='role_superadmin'){
			$logs=WorklogMain::orderBy('id', 'DESC')->paginate(25);
		}elseif($user_role=='role_administrator'){
			$projects_ids=getUserPojectIds(Auth::user()->id);
			$logs=WorklogMain::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->paginate(25);
			
		}
		
		return view('admin/project/alllogs', array('title' => 'Project Logs','logs'=>$logs,'active'=>'WORKLOG'));
	}
	
	public function all_survey(){
		
		$user_role= getUserRole(Auth::user()->id);
		$job_role=  getJobRole(Auth::user()->id);
		
		if($user_role=='role_superadmin'){
			$surveys=ProjectSurvey::orderBy('id', 'DESC')->paginate(25);
			
		}elseif($user_role=='role_administrator'){
			$projects_ids=getUserPojectIds(Auth::user()->id);
			$surveys=ProjectSurvey::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->paginate(25);
		}
		return view('admin/project/allsurveys', array('title' => 'Project Surveys','surveys'=>$surveys,'active'=>'SURVEY'));
	}
	public function projectsurveys($id=0){
		
		$project=Project::where('projects.manual_id',$id)->first();
		$id=$project->id;
		
		$surveys=ProjectSurvey::where('project_id',$id)->orderBy('id', 'DESC')->paginate(25);
		
		return view('admin/project/allsurveys', array('title' => 'Project Surveys','surveys'=>$surveys,'active'=>'SURVEY'));
	}
	
	/* 
	* 
	* Override Worklogs from admin
	* @Author Ankit Chugh
	* @return view for particular log
	*/

	function overrideLog(){
		$log_id=Input::get('log_id');
		$main_log_id=Input::get('main_log_id');
		$hours_logged=Input::get('hours_logged');
		$foreman_id=Input::get('foreman_id');
		
		$data=array(
			'hours_logged'  => $hours_logged
		);
		
		#update hours
		$update=Worklog::where('id', $log_id)->update($data);
		
		if($update){
			/*$push_id=get_push_id($foreman_id);
			
			#sending notification to foreman
			$message = array (
					'message' => "Super Override Your logged hours",
					'title'	=>    'Hours Override',
					'subtitle'	=>'Hours Override',
					'tickerText'	=> 'Hours Override',
					'vibrate'	=> 1,
					'sound'	=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
			);
			
			if(isset($push_id)){									   
				send_push_notification(array($push_id),$message);
			}*/
			
		}

		Session::flash('success', 'Hours Override Successfully'); 
		return Redirect::to('admin/projects/logs/'.$main_log_id);
		
		
	}
	
	/* 
	* 
	* Override Worklogs from admin
	* @Author Ankit Chugh
	* @return 1
	*/
	function overrideAjax(){
		$log_id=Input::get('log_id');
		$hours_logged=Input::get('hours_logged');
		$foreman_id=Input::get('foreman_id');
		
		$data=array(
			'hours_logged'  => $hours_logged
		);
		
		#update hours
		$update=Worklog::where('id', $log_id)->update($data);
		if($update){
			$push_id=get_push_id($foreman_id);
			
			#sending notification to foreman
			$message = array (
					'message' => "Super Override Your logged hours",
					'title'	=>    'Hours Override',
					'subtitle'	=>'Hours Override',
					'tickerText'	=> 'Hours Override',
					'vibrate'	=> 1,
					'sound'	=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
			);
			
			if(isset($push_id)){									   
				send_push_notification(array($push_id),$message);
			}
			
		}
		echo 1;die;
		
	}
	
	/* 
	* 
	* Override Manpower from admin
	* @Author Ankit Chugh
	* @return 1
	*/
	function overrideManpowerAjax(){
		$request_id=Input::get('request_id');
		$quantity=Input::get('quantity');
		$foreman_id=Input::get('foreman_id');
		
		$data=array(
			'quantity'  => $quantity
		);
		
		#update Manpower
		$update=ForemanRequest::where('id', $request_id)->update($data);
		if($update){
			$push_id=get_push_id($foreman_id);
			
			#sending notification to foreman
			$u=User::Where('id',$foreman_id)->first();	
			if(isset($push_id)){
				if($u->device_os=="android"){
					$message = array (
							'message' => "Manpower request has been overridden by admin",
							'title'	=>    'Manpower Override',
							'subtitle'	=>'Manpower Override',
							'tickerText'	=> 'Manpower Override',
							'vibrate'	=> 1,
							'sound'	=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon'
					);  
					send_push_notification(array($push_id),$message);
				
				}else{
					$iosmessage=array(
						'badge' => 1,
						'alert' => 'Manpower request has been overridden by admin',
						'sound' => 'default'
					);
					
					send_push_notification_ios($push_id,$iosmessage);
				}
			}
			
			
		}
		echo 1;die;
	}
	/* 
	* 
	* Assign subcontractor for resource
	* @Author Ankit Chugh
	* @return none
	*/
    function assignContractor(Mail $mail){
		$data = Input::all();
		$main_request_id=Input::get('main_request_id');
		$rules = array(
			'subcontractor_id' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()){
			return Redirect::to('/admin/projects/requests/'.$main_request_id)->with('data',array('title' => 'Project Details','active'=>'PROJECT'))->withErrors($validator);
		}
		
		$request_id=Input::get('request_id');
		$project_id=Input::get('project_id');
		$foreman_id=Input::get('foreman_id');
		$resource_id=Input::get('resource_id');
		$subcontractor_id=Input::get('subcontractor_id');
		$quantity=Input::get('subcontractor_quantity');
		$main_request_id=Input::get('main_request_id');
		$processing_date=Input::get('processing_date');
		
		SubcontractorRequest::where('request_id',Input::get('request_id'))->delete();
		
	
		
		if(!empty($quantity)){
			foreach($quantity as $key=>$s){
				if(isset($s) && !empty($s)){
				$data=array(
					'subcontractor_id'  => $subcontractor_id[$key],
					'request_id'  => $request_id,
					'project_id'  => $project_id,
					'foreman_id'  => $foreman_id,
					'resource_id'  => $resource_id,
					'quantity'  => $s,
					'processing_date'  => $processing_date,
				);
			      SubcontractorRequest::create($data);

					//mail to foreman
				   $user=User::where('id',$foreman_id)->first();
				   $useremail=$user->email;
				   $emaildata=array();
				   $emaildata['name'] = $user->name;
				   $emaildata['project'] = get_project_title($project_id);
				   $emaildata['manpower_type'] = get_resource_title($resource_id);
				   /*$mail->send('emails.foremanApprove', $emaildata, function($message) use ($useremail)
					{
						$message->to($useremail, 'Super')->subject('Request Approved!');
						
					});	*/	
	
				}
			}
		}
		updateProjectDate($project_id);
		
		
		return Redirect::to('/admin/projects/requests/'.$main_request_id)->with('data',array('title' => 'All Requests','active'=>'PROJECT'));
		
	}
	
    /***************************Tasks functions******************************/
    
    public function tasks(){
		
		$user_role= getUserRole(Auth::user()->id);
		
        $title=Input::get('name');
        
        
        
        
        if($user_role=='role_superadmin'){
			
			if(isset($title) && !empty($title)){
           
				$tasks=Task::where('task','LIKE','%'. $title.'%')->orderBy('id', 'DESC')->get();
			}else{
				$tasks=Task::orderBy('id', 'DESC')->get();
			}
		}elseif($user_role=='role_administrator'){
			  //Conditional Logic as per Job Role
				$projects_ids=getUserPojectIds(Auth::user()->id);
				
				
				if(isset($title) && !empty($title)){
	   
					$tasks=Task::where('task','LIKE','%'. $title.'%')->whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->get();
				}else{
					$tasks=Task::orderBy('id', 'DESC')->whereIn('project_id',$projects_ids)->get();
				}
					
		}
        
        
        return view('admin/project/manage_tasks', array('title' => 'Project Tasks','tasks'=>$tasks,'active'=>'TASK'));
    }
    
    public function project_tasks($id=0){
		$project=Project::where('projects.manual_id',$id)->first();
		$id=$project->id;
		
		$tasks=Task::where('project_id',$id)->orderBy('id', 'DESC')->get();
		return view('admin/project/manage_tasks', array('title' => 'Project Tasks','tasks'=>$tasks,'active'=>'TASK'));
	}
	
    public function addTask(){
       return view('admin/project/add_task', array('title' => 'Add Task','active'=>'TASK'));
    }
    
    public function saveTask(){
            $data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'task' => 'required',
				'project' => 'required',
				'foreman' => 'required'
			);
	       $validator = Validator::make($data, $rules);

	       if ($validator->fails()){
                return Redirect::to('/admin/projects/tasks/add')->withErrors($validator)->withInput(); 
           }else{
                    $w=Task::create(array(
						'project_id'    =>  Input::get('project'),
						'foreman_id'    =>  Input::get('foreman'),
						'task'    =>  Input::get('task'),
						'description'    =>  Input::get('description'),
					));	
                    $fileid=$w->id;
                
                    if (Input::hasFile('task_image')) {
						$path='uploads/tasks/'.$fileid.'/';
						\File::cleanDirectory($path);
						\File::makeDirectory($path,$mode = 0777, true, true);

                        $thumbpath='uploads/tasks/'.$fileid.'/thumb/';
						\File::cleanDirectory($thumbpath);
                        \File::makeDirectory($thumbpath,$mode = 0777, true, true);
                        
						$file            = Input::file('task_image');
						$filename        = str_random(6) . '_' . str_replace(" ","_",$file->getClientOriginalName());
						$uploadSuccess   = $file->move($path, $filename);
                        
                        copy($path.$filename,$thumbpath.$filename);
						//$file->move($thumbpath, $filename);
						$image = \Image::make(sprintf($thumbpath.$filename, $file->getClientOriginalName()))->resize(50, 50)->save();  
					
						Task::where('id', $fileid)->update(array(
							'task_image'    =>  url().'/'.$path.$filename,
                            'task_thumbnail'    =>  url().'/'.$thumbpath.$filename
						));
					}
                    updateProjectDate(Input::get('project'));
                    
                    
                    //set  notification
                    $members=getProjectTeam(Input::get('project'));
					if(count($members)){
						foreach($members as $m){
							//set  notification
							$admin_id=get_admin_id();
							Notification::create(array(
								'notification_from'    =>  $admin_id,
								'notification_to' 	=>  $m->foreman_id ,
								'notification_type' =>'task_added',
								'project_id' =>Input::get('project')
							));
						}
					}
                    
                    Session::flash('success', 'Task added'); 
                    
                    if(Input::get('redirect')==1){
						return Redirect::to('/admin/projects/view/'.Input::get('manual_id'))->with('data',array('title' => 'Project'));
					}
                    
				    return Redirect::to('admin/projects/tasks/')->with('data',array('title' => 'Add Task','active'=>'TASK'));
           }
    }
    
    public function change_task_status(){
            $id=Input::get('id');
			$status=Input::get('status');
			
			Task::where('id', $id)->update(array(
						'status'  => $status
					));
			echo 1;die;
    }
    public function deleteTask($id=0){
		Task::where('id', $id)->delete();
		return Redirect::to('admin/projects/tasks/')->with('data',array('title' => 'Manage Tasks','active'=>'TASK'));
	}
    
      public function task_autocomplete(){
        $searchTerm =Input::get('term');
        $users	= 	Task::where('task','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->task;
        }
        //return json data
        echo json_encode($data);die;
       
    }
    
    
    public function projectfiles($id=0){
			$project=Project::where('projects.manual_id',$id)->first();
			$id=$project->id;
			$files=File::where('project_id',$id)->orderBy('id', 'DESC')->get();
			return view('admin/file/manage', array('title' => 'Project Files','files'=>$files,'active'=>'FILES'));
	}
    
    /*************************************************************************/
	/* 
	* 
	* Change Project Status
	* @Author Ankit Chugh
	* @return 1
	*/
	public function change_complete_status(){
			$id=Input::get('id');
			$status=Input::get('status');
			
			Project::where('id', $id)->update(array(
						'is_completed'  => $status
					));
			echo 1;die;
	}
    
   public function autocomplete(){
        $searchTerm =Input::get('term');
        $users	= 	Project::where('title','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->title;
        }
        //return json data
        echo json_encode($data);die;
       
   }
   
   public function resourceAutocomplete(){
	    $searchTerm =Input::get('term');
        $users	= 	Manpower::distinct('title')->select('title')->where('title','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->title;
        }
        //return json data
        echo json_encode($data);die;
   }
   
   public function getCustomerMember(){
	   $customer_id =Input::get('customer_id');
	   $members	= 	CustomerMember::where('customer_id',$customer_id)->orderBy(\DB::raw("SUBSTRING_INDEX(name, ' ',-1)"))->get();
	   if(count($members)>0){
		   return json_encode($members->toArray());die;
	   }else{
		   return 0;die;
	   }
	   
   }
   public function getMemberData(){
		$member_id =Input::get('member_id');
		$members	     = 	CustomerMember::where('id',$member_id)->first();
	   if(count($members)>0){
		   return json_encode($members->toArray());die;
	   }else{
		   return 0;die;
	   }
   }
   
   public function update_project_email(){
	   //get Suprintendent projects
		
		$project_id=Input::get('project_id');
		
		$subject="Updated Manpower Schedule for ".get_project_title($project_id);
		
		$userProjects = Project::where('id',$project_id)->get();							
		$pdata['projects'] = array();
		$email = Auth::user()->email;
		
		$is_sent_email=0;
		$pms=array();
		$apms=array();
		$ests=array();
		$foreman=array();
		
		if(count($userProjects) > 0){
			foreach($userProjects as $p){
				$subs1=SubcontractorRequest::where('project_id',$p->id)->get();
				if(count($subs1) >0){
					$is_sent_email=1;
					$pdata['projects'][]=$p;
				
				
				//get all project pms 
				//get project pm's ids
				$pm = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','pm')
						->distinct('foreman_id')
						->select('users.id')->first();

				
				$pms[]=$pm->id;
				
				//get project apm's ids
				$apm = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','apm')
						->distinct('foreman_id')
						->select('users.id')->first();
				$apms[] = $apm->id;
				
				//get project estimators's ids
				$est = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','estimator')
						->distinct('foreman_id')
						->select('users.id')->first();
				$ests[] = $est->id;
				
				//get project foreman's ids
				$for = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','foreman')
						->distinct('foreman_id')
						->select('users.id')->first();
				$foreman[] = $for->id;
				
				}
				
			}
				
				
		}
		
		if($is_sent_email==1){ 
			\Mail::send('emails.schedule_PM_APM', $pdata, function($message) use ($email,$subject)
			{
				$message->to($email, 'Super')->subject($subject);
				
			});	
		}
		
		
		//send email to all pms
		if(!empty($pms)){
			
			$upm=array_unique($pms);
			//print_r($upm);
			foreach($upm as $p){
				$mpdata=array();
				 $pemail = getUserEmail($p);
				
				 $allpmprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','pm')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$mpdata['projects'][]=is_pm_project($allpmprojects,$p);
					}
				}
				
				$mpdata['projects']=array_filter($mpdata['projects']);
									
				\Mail::send('emails.schedule_PM_APM', $mpdata, function($message) use ($pemail,$subject)
				{
					$message->to($pemail, 'Super')->subject($subject);
					
				});	
			}
		}
		
		//send email to all apms
		if(!empty($apms)){
			
			$uapm=array_unique($apms);
			
			foreach($uapm as $p){
				$ampdata=array();
				$aemail = getUserEmail($p);
				
				$allapmprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','apm')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$ampdata['projects'][]=is_pm_project($allapmprojects,$p);
					}
				}
				
				$ampdata['projects']=array_filter($ampdata['projects']);
				
				
				
				\Mail::send('emails.schedule_PM_APM', $ampdata, function($message) use ($aemail,$subject)
				{
					$message->to($aemail, 'Super')->subject($subject);
					
				});	
			}
		}
		//send email to all ests
		if(!empty($ests)){
			
			$uests=array_unique($ests);
			
			
			foreach($uests as $p){
				$estdata=array();
				$estmail = getUserEmail($p);
				
				$allestprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','estimator')
							->whereDate('projects.updated_at','=',date('Y-m-d'))
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$estdata['projects'][]=is_pm_project($allestprojects,$p);
					}
				}
				
				$estdata['projects']=array_filter($estdata['projects']);
				
				
				
				\Mail::send('emails.schedule_PM_APM', $estdata, function($message) use ($estmail,$subject)
				{
					$message->to($estmail, 'Super')->subject($subject);
					
				});	
			}
		}
		//send email to all foremans
		if(!empty($foreman)){
			
			$uforeman=array_unique($foreman);
			
			$foremandata=array();
			foreach($uforeman as $p){
				$foremanmail = getUserEmail($p);
				
				$allforemanprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','foreman')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$foremandata['projects'][]=is_pm_project($allforemanprojects,$p);
					}
				}
				
				$foremandata['projects']=array_filter($foremandata['projects']);
				
				
				
				\Mail::send('emails.schedule_PM_APM', $foremandata, function($message) use ($foremanmail,$subject)
				{
					$message->to($foremanmail, 'Super')->subject($subject);
					
				});	
			}
		}
		$pdata['projects']=array();
		//send email to all project subcontractors
		$is_sent_email=0;
		$userProjects = Project::where('id',$project_id)->get();	
		if(count($userProjects) > 0){
			foreach($userProjects as $p){
				$subs1=SubcontractorRequest::where('project_id',$p->id)->get();
				if(count($subs1) >0){
					$is_sent_email=1;
					$pdata['projects'][]=$p;
				}
			}
		}
		
		$contractors=SubcontractorRequest::where('project_id',$project_id)->get();
		if(count($contractors)){
			foreach($contractors as $c){
				$sub_id=$c->subcontractor_id;
				$subemail=Subcontractor::where('id',$sub_id)->first();
				$email=$subemail->email;
				if($is_sent_email==1){ 
					/*\Mail::send('emails.schedule_PM_APM', $pdata, function($message) use ($email,$subject)
					{
						$message->to($email, 'Super')->subject($subject);
						
					})*/
					
					//send email to subcontractor members
					
					$members=SubcontractorMember::where('subcontractor_id',$sub_id)->get();
					if(count($members)){
						foreach($members as $m){
							$emailm=$m->email;
							\Mail::send('emails.schedule_PM_APM', $pdata, function($message) use ($emailm,$subject)
							{
								$message->to($emailm, 'Super')->subject($subject);
								
							});
						}
					}
				}
		
			}
		}
		
		
		echo 'success';die;
		/*********END OF FUNCTION**************/
   }
   public function downloadworklogpdf($id=0){
	   
		$logs=Worklog::where('worklog_id',$id)->orderBy('id', 'ASC')->get();
		$pid=Worklog::where('worklog_id',$id)->orderBy('id', 'DESC')->first();
		$detail=Project::where('id',$pid->project_id)->first();
		
		$data['requests']		= 	ForemanRequestMain::where('project_id','=',$pid->project_id)->count();
		
		//$data['hourslogged']	=   Worklog::where('project_id','=',$pid->project_id)->sum('hours_logged');
		
		$data['hourslogged']=0;
		
		$logged	=   Worklog::where('project_id','=',$pid->project_id)->select('hours_logged','quantity')->get();
		
		if(count($logged)){
			foreach($logged as $a){
				$data['hourslogged']	+=	$a->hours_logged; 
			}
		}
		
		$data['total_estimated_hours']=ProjectResources::where('project_id','=',$pid->project_id)->sum('max_hours');
		
	    $pdf = PDF::loadView('admin/project/alllogspdf',  array('title' => 'Project Details','data'=>$data,'project'=>$detail,'logs'=>$logs,'active'=>'WORKLOG'));
		//\File::makeDirectory('uploads/project_reports/',$mode = 0777, true, true);
		return $pdf->download('LOG_'.$id.'.pdf');
   }
   public function downloadsurveypdf($id=null){
	   
		$survey=Survey::where('survey_id',$id)->get();
		$project=Project::where('id',$survey[0]->project_id)->first();
	    $pdf = PDF::loadView('admin/project/surveypdf',  array('title' => 'Survey Details','project'=>$project,'survey'=>$survey,'active'=>'SURVEY'));
		//\File::makeDirectory('uploads/project_reports/',$mode = 0777, true, true);
		return $pdf->download('SURVEY_'.$id.'.pdf');
	}
	
	public function daterangereportdownload($id){
		
		rrmdir('uploads/temp/');
		
		$subject = "Daily Report for ".date('m/d/Y',strtotime("-1 days"));		
		$superemail=getSuperEmail();
		
		
		$from=date('Y-m-d',strtotime($_GET['from_date']));
		$to=date('Y-m-d',strtotime($_GET['to_date']));
		
		$period = new \DatePeriod(
			 new \DateTime($from),
			 new \DateInterval('P1D'),
			 new \DateTime($to)
		);

		//$start  = new \DateTime('-7 days', new \DateTimeZone('UTC'));
		//$period = new \DatePeriod($start, new \DateInterval('P1D'), 7);

		$project = Project::Where('id',$id)->first(); 
		
		//echo '<pre>';
		//print_r($period);die;
		
		$files=array();
		$dates=array();
		//$dates[]=$from;
		foreach ($period as $date) {
					
					$date = $date->format('Y-m-d');
					$dates[]=$date;
		}
		$dates[]=$to;
		
		//echo '<pre>';
		//print_r($dates);die;
		
		foreach($dates as $date){
			//echo $date;
				$projectlogs	=			WorklogMain::where('project_id','=',$id)->whereDate('created_at','=',$date)->orderBy('id', 'DESC')->get();
				$surveys        =			Survey::where('project_id','=',$id)->whereDate('created_at','=',$date)->orderBy('id', 'DESC')->get();
				
				//echo count($projectlogs);
				if(count($projectlogs) > 0)  {
					
					
					
					\File::makeDirectory('uploads/temp/',$mode = 0777, true, true);
					
					$newdata['customer']=$superemail;
					//$newdata['log_date']=$date;
					$newdata['submitted_for']=date('Y-m-d',strtotime($projectlogs[0]->logged_at));
					$newdata['project']=$project;
					$pdf = PDF::loadView('emails.superReport4', $newdata);
					$pdf->output();
					$pdf->save('uploads/temp/'.$project->title.'_'.$newdata['submitted_for'].'.pdf'); 
					
					$files[]='uploads/temp/'.$project->title.'_'.$newdata['submitted_for'].'.pdf';
					//return $pdf->download($project->title.'.pdf');								
				}
		}
		
		
		//echo '<pre>';
		//print_r($files);die; 
		
		$zip = "";
		if(!empty($files)){
			$zipname = $project->title.'-'.time().'_reports1.zip';
			$zip = new \ZipArchive;
			$zip->open($zipname, \ZipArchive::CREATE);
			foreach ($files as $file) {
				//echo $file;
				//if(file_exists($file)){
					$zip->addFile($file);
				//}
			}
			$zip->close(); 
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zipname));
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($zipname);
			
			rrmdir('uploads/temp/');
			die;
		}else{
			 Session::flash('error', "No report available"); 
			 return Redirect::to('admin/projects');
		}				
	} 
	public function latestreportdownload($id){
		rrmdir('uploads/temp/');

		$subject = "Daily Report for ".date('m/d/Y',strtotime("-1 days"));		
		$superemail=getSuperEmail();
		$project = Project::Where('id',$id)->first();
		$files=array();


		$date = date('Y-m-d');

		$projectlogs	=			WorklogMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
		$surveys        =			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();


		if(count($projectlogs) > 0 || count($surveys) > 0)  {

			\File::makeDirectory('uploads/temp/',$mode = 0777, true, true);

			$newdata['customer']=$superemail;
			$newdata['submitted_for']=date('Y-m-d',strtotime($projectlogs[0]->logged_at));
			$newdata['project']=$project;
			$pdf = PDF::loadView('emails.superReport2', $newdata);
			$pdf->output();
			$pdf->save('uploads/temp/'.$project->title.'_'.$date.'.pdf'); 

			$files[]='uploads/temp/'.$project->title.'_'.$date.'.pdf';
			//return $pdf->download($project->title.'.pdf');
		}


		//add to zip
		if(!empty($files)){
			$filename = $project->title.".pdf";
			header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($files[0])) . ' GMT');
			header('Accept-Ranges: bytes');  // For download resume
			header('Content-Length: ' . filesize($files[0]));  // File size
			header('Content-Encoding: none');
			header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
			header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
			readfile($files[0]);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb


			rrmdir('uploads/temp/');
		}else{
			echo 'No data found';
		}		
	}
}
function myFilter($var){
  return ($var !== NULL && $var !== FALSE && $var !== '');
}
 function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }
