<?php 
namespace App\Http\Controllers\Admin; //admin add
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use App\Models\Manpower;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\ProjectForeman;
use App\Models\ForemanRequestMain;
use App\Models\Project;
use App\Models\ProjectResources;
use App\Models\SubcontractorRequest;
use App\Models\SubcontractorMember;
use App\Models\Manpowerrecord;
use App\Models\Survey;
use App\Models\Task;
use App\Models\File;
use PDF;



class ReportController extends Controller {
	
	public function __construct(){
	  
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
		
		$data['title']	= 	'Reports';
		$data['active']	= 	'REPORT';
		$data['manpowers']=Manpower::all();
		$data['users']=User::where('role','=','role_foreman')->get();
		return view('admin/reports/reports', $data);
	}
	public function generate(){
		
		$data = Input::all();
		//print_r($data);die;

		$conditions = '';
		$ids='';
		#################################
		if (!empty($data['date_from'])) {
		  $from=$data['date_from'];
		  $conditions  .= 'UNIX_TIMESTAMP(tbl_worklogs.updated_at) >= "'.strtotime($data['date_from']).'"';
		  Session::flash('date_from', ($data['date_from']));		
		}else{
			$from=date("Y-m-d", strtotime("-10 day"));
			$conditions  .= 'UNIX_TIMESTAMP(tbl_worklogs.updated_at) >= "'.strtotime(date("Y-m-d") . ' -10 day').'"';
			Session::flash('date_from', date("Y-m-d", strtotime("-10 day")));		
		}
		
		if (!empty($data['date_to'])) {
			$to=$data['date_to'];
			if(!empty($data['date_to']) && !empty($data['date_from']) && strtotime($data['date_to'])==strtotime($data['date_from'])){
			
				$conditions .= ' AND UNIX_TIMESTAMP(tbl_worklogs.updated_at) <= "'.strtotime('+22 hours',strtotime($data['date_to'])).'"';
				
			}
			else{
				$conditions .= ' AND UNIX_TIMESTAMP(tbl_worklogs.updated_at) <= "'.strtotime('+22 hours',strtotime($data['date_to'])).'"';
				
			}
			Session::flash('date_to', ($data['date_to']));		
		}else{
			$to=date("Y-m-d", strtotime("+1 day"));
			$conditions  .= ' AND UNIX_TIMESTAMP(tbl_worklogs.updated_at) <= "'.strtotime(date("Y-m-d") . ' +1 day').'"';
			Session::flash('date_to', date("Y-m-d", strtotime("+1 day")));	
		}
		
		##################################
		
		if (!empty($data['job'])) {
			$conditions .= ' AND tbl_projects.id IN ('.implode(',',$data["job"]).')';
		}
		
		if (!empty($data['foreman'])) {
			
			foreach($data['foreman'] as $d){
				$ids[]=$d;
			}
			
			
			
		}
		if (!empty($data['supri'])) {
			foreach($data['supri'] as $d){
				$ids[]=$d;
			}
			
		}
		if (!empty($data['pm'])) {
			foreach($data['pm'] as $d){
				$ids[]=$d;
			}
			
		}
		if (!empty($data['apm'])) {
			foreach($data['apm'] as $d){
				$ids[]=$d;
			}
			
		}
		if (!empty($data['estimator'])) {
			foreach($data['estimator'] as $d){
				$ids[]=$d;
			}
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["estimator"].' AND tbl_assigned_projects_to_foreman.type = "estimator")';
		}
		if (!empty($data['customer'])) {
			$conditions .= ' AND tbl_projects.customer_id = '.$data["customer"];
		}
		if (!empty($data['manpower'])) {
			Session::flash('manpower', $data['manpower']);
			//$conditions .= ' AND tbl_project_assigned_resources.manpower_id='.$data['manpower'];
		}
		
		if (!empty($ids)) {
			$conditions .= ' AND tbl_assigned_projects_to_foreman.foreman_id IN ('.implode(',',$ids).')';
		}
		//echo $conditions;die;
		//print_r($ids);die; 
		
		$mainworklog='';
		
		$user_role= getUserRole(Auth::user()->id);
		
		if($user_role=='role_superadmin'){
		
			$projects = Project::leftJoin('assigned_projects_to_foreman', function($join) {
							  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
							})
							->leftJoin('worklogs', function($join) {
							  $join->on('projects.id', '=', 'worklogs.project_id');
							})
							->whereRaw($conditions)
							->distinct('assigned_projects_to_foreman.project_id,worklogs.project_id')
							->select('projects.*')->get();
		
		}else{
			$pids = getUserPojectIds(Auth::user()->id);
			$projects = Project::leftJoin('assigned_projects_to_foreman', function($join) {
							  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
							})
							->leftJoin('worklogs', function($join) {
							  $join->on('projects.id', '=', 'worklogs.project_id');
							})
							->whereRaw($conditions)
							->WhereIn('projects.id', $pids)
							->distinct('assigned_projects_to_foreman.project_id,worklogs.project_id')
							
							->select('projects.*')->get();
		}
		
		$projectids=array();
		if(count($projects->toArray())){
			foreach($projects as $p){
				$projectids[]=$p->id;
			}
		}
		
		
		
		Session::flash('projects', $projects);
		Session::flash('date_from', $from);	
		Session::flash('date_to', $to);	
	
		$pdf = PDF::loadView('admin.reports.pdfview', array())->save($_SERVER['DOCUMENT_ROOT'].'/uploads/adminreport.pdf')->stream('download.pdf');;
		Session::flash('date_from', $from);	
		Session::flash('date_to', $to);	

		
		$data['projects']	= 	$projects;
		$data['title']	= 	'Reports';
		$data['active']	= 	'REPORT';
		$data['mainworklog']	= 	$mainworklog;
		$data['date_from']	= 	$from;
		$data['date_to']	= 	$to;
		
		
		
		return view('admin/reports/view',$data);
	}
	public function view(){
		$data['title']	= 	'Reports';
		$data['active']	= 	'REPORT';
		
		return view('admin/reports/view', $data);
	}
	
	
	/******************************************************/
	
	
	public function estimatedreports(){
		$data['title']	= 	'Estimated Vs Actual Report';
		$data['active']	= 	'EREPORT';
		$data['manpowers']=Manpower::all();
		$data['users']=User::where('role','=','role_foreman')->get();
		return view('admin/reports/estimatedreports', $data);
	}
	public function estimatedgenerate(){
	
		$data = Input::all();

		$conditions = '';
		$ids='';
		/*if (!empty($data['date_from'])) {
		  $conditions  .= 'tbl_projects.updated_at >= "'.$data['date_from'].'"';
		}else{
			$conditions  .= 'tbl_projects.updated_at >= "2015-10-10"';
		}
		
		if (!empty($data['date_to'])) {
			$conditions .= ' AND tbl_projects.updated_at <= "'.$data['date_to'].'"';
		}*/
		
		if (!empty($data['job'])) {
			$conditions .= ' tbl_projects.id IN ('.implode(',',$data["job"]).')';
		}else{
			$conditions .= ' tbl_projects.id LIKE "%%"';
		}
		
		if (!empty($data['foreman'])) {
			
			foreach($data['foreman'] as $d){
				$ids[]=$d;
			}
			
			
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["foreman"].' AND tbl_assigned_projects_to_foreman.type = "foreman")';
		}
		
		if (!empty($data['supri'])) {
			foreach($data['supri'] as $d){
				$ids[]=$d;
			}
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["supri"].' AND tbl_assigned_projects_to_foreman.type = "super")';
		}
		if (!empty($data['pm'])) {
			foreach($data['pm'] as $d){
				$ids[]=$d;
			}
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["pm"].' AND tbl_assigned_projects_to_foreman.type = "pm")';
		}
		if (!empty($data['apm'])) {
			foreach($data['apm'] as $d){
				$ids[]=$d;
			}
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["apm"].' AND tbl_assigned_projects_to_foreman.type = "apm")';
		}
		if (!empty($data['estimator'])) {
			foreach($data['estimator'] as $d){
				$ids[]=$d;
			}
			//$conditions .= ' AND (tbl_assigned_projects_to_foreman.foreman_id = '.$data["estimator"].' AND tbl_assigned_projects_to_foreman.type = "estimator")';
		}
		
		if (!empty($data['customer'])) {
			$conditions .= ' AND tbl_projects.customer_id = '.$data["customer"];
		}
		if (!empty($data['manpower'])) {
			Session::flash('manpower', $data['manpower']);
			//$conditions .= ' AND tbl_project_assigned_resources.manpower_id='.$data['manpower'];
		}
		
		if (!empty($ids)) {
			$conditions .= ' AND tbl_assigned_projects_to_foreman.foreman_id IN ('.implode(',',$ids).')';
		}
		
		$mainworklog='';
		$user_role= getUserRole(Auth::user()->id);
		if($user_role=='role_superadmin'){
			$projects = Project::leftJoin('assigned_projects_to_foreman', function($join) {
							  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
							})
							
							->whereRaw($conditions)
							->distinct('assigned_projects_to_foreman.project_id')
							->select('projects.*')->get();
		}else{
			$pids = getUserPojectIds(Auth::user()->id);
			$projects = Project::leftJoin('assigned_projects_to_foreman', function($join) {
							  $join->on('projects.id', '=', 'assigned_projects_to_foreman.project_id');
							})
							
							->whereRaw($conditions)
							->WhereIn('projects.id', $pids)
							->distinct('assigned_projects_to_foreman.project_id')
							->select('projects.*')->get();
		}

		
		$projectids=array();
		if(count($projects->toArray())){
			foreach($projects as $p){
				$projectids[]=$p->id;
			}
		}
		
		
		$mainworklog=Worklog::whereIn('project_id',$projectids)->get();
		
		Session::flash('projects', $projects);
		$pdf = PDF::loadView('admin.reports.estimatedpdfview', array())->save($_SERVER['DOCUMENT_ROOT'].'/uploads/estimatedadminreport.pdf')->stream('download.pdf');;
		return Redirect::to('admin/reports/estimatedview')->with('data',array('title' => 'Reports','mainworklog'=>$mainworklog))->withInput();
	}
	public function estimatedview(){
		$data['title']	= 	'Reports';
		$data['active']	= 	'EREPORT';
		
		return view('admin/reports/estimatedview', $data);
	}
	
	public function sendadminreport(Mail $mailer){
		$email=Input::get('email');

		if(!empty($email)){
			foreach($email as $e){
				$username='Foreman Feed';
				$pathfile=url().'/uploads/adminreport.pdf';
				
				\Mail::send(array(), array(), function($message) use ($e,$pathfile)
				{
					$message->to($e, 'Super')->subject('Admin Report!');
					$message->attach($pathfile);
				});
			}
		}
		Session::flash('success', 'Emails Delivered!'); 
		return Redirect::to('admin/reports')->with('data',array('title' => 'Reports'))->withInput();
		
	}
	public function estimatesendadminreport(){
		$email=Input::get('email');

		if(!empty($email)){
			foreach($email as $e){
				$username='Foreman Feed';
				$pathfile=url().'/uploads/estimatedadminreport.pdf';
				
				\Mail::send(array(), array(), function($message) use ($e,$pathfile)
				{
					$message->to($e, 'Super')->subject('Admin Report!');
					$message->attach($pathfile);
				});
			}
		}
		Session::flash('success', 'Emails Delivered!'); 
		return Redirect::to('admin/estimatedreports')->with('data',array('title' => 'Reports'))->withInput();
	}
	
	public function finalizemanpowerschedule(Mail $mail){
		//get Suprintendent projects
		$subject = "Rock Spring Manpower ".date("m/d/Y");
		$userProjects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',Auth::user()->id)
							->where('type','=','super')
							->where('projects.is_completed','=',0)  
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->distinct('project_id')
							->select('projects.*')->get();
							
		$pdata['projects'] = array();
		$email = Auth::user()->email;
		
		$is_sent_email=0;
		$pms=array();
		$apms=array();
		$ests=array();
		$foreman=array();
		$contractors=array();
		
		if(count($userProjects) > 0){
			foreach($userProjects as $p){
				$subs1=SubcontractorRequest::where('project_id',$p->id)->get();
				if(count($subs1) >0){
					$is_sent_email=1;
					$pdata['projects'][]=$p;
				
				
				//get all project pms 
				//get project pm's ids
				$pm = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','pm')
						->distinct('foreman_id')
						->select('users.id')->first();

				
				$pms[]=$pm->id;
				
				//get project apm's ids
				$apm = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','apm')
						->distinct('foreman_id')
						->select('users.id')->first();
				$apms[] = $apm->id;
				
				//get project estimators's ids
				$est = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','estimator')
						->distinct('foreman_id')
						->select('users.id')->first();
				$ests[] = $est->id;
				
				//get project foreman's ids
				$for = ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
						->where('project_id',$p->id)
						->where('type','=','foreman')
						->distinct('foreman_id')
						->select('users.id')->first();
				$foreman[] = $for->id;
				
				//get project subcontractors
				$contr=SubcontractorRequest::where('project_id',$p->id)->groupBy('subcontractor_id')->get();
				
					foreach($contr as $ctr){
						$contractors[]=$ctr->subcontractor_id;
					}
				
				}
				
			}
				
				
		}
		
		if($is_sent_email==1){ 
			\Mail::send('emails.schedule_PM_APM', $pdata, function($message) use ($email,$subject)
			{
				$message->to($email, 'Super')->subject($subject);
				
			});	
		}
		
		
		//send email to all pms
		if(!empty($pms)){
			
			$upm=array_unique($pms);
			//print_r($upm);
			foreach($upm as $p){
				$mpdata=array();
				 $pemail = getUserEmail($p);
				
				 $allpmprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','pm')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$mpdata['projects'][]=is_pm_project($allpmprojects,$p);
					}
				}
				
				$mpdata['projects']=array_filter($mpdata['projects']);
									
				\Mail::send('emails.schedule_PM_APM', $mpdata, function($message) use ($pemail,$subject)
				{
					$message->to($pemail, 'Super')->subject($subject);
					
				});	
			}
		}
		
		//send email to all apms
		if(!empty($apms)){
			
			$uapm=array_unique($apms);
			
	
			
			foreach($uapm as $p){
				$ampdata=array();
				$aemail = getUserEmail($p);
				
				$allapmprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','apm')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$ampdata['projects'][]=is_pm_project($allapmprojects,$p);
					}
				}
				
				$ampdata['projects']=array_filter($ampdata['projects']);
				
			
				
				\Mail::send('emails.schedule_PM_APM', $ampdata, function($message) use ($aemail,$subject)
				{
					$message->to($aemail, 'Super')->subject($subject);
					
				});	
			}
		}
		//send email to all ests
		if(!empty($ests)){
			
			$uests=array_unique($ests);
			
			
			foreach($uests as $p){
				$estdata=array();
				$estmail = getUserEmail($p);
				
				$allestprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','estimator')
							->whereDate('projects.updated_at','=',date('Y-m-d'))
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$estdata['projects'][]=is_pm_project($allestprojects,$p);
					}
				}
				
				$estdata['projects']=array_filter($estdata['projects']);
				
				
				
				\Mail::send('emails.schedule_PM_APM', $estdata, function($message) use ($estmail,$subject)
				{
					$message->to($estmail, 'Super')->subject($subject);
					
				});	
			}
		}
		//send email to all foremans
		if(!empty($foreman)){
			
			$uforeman=array_unique($foreman);
			
			$foremandata=array();
			foreach($uforeman as $p){
				$foremanmail = getUserEmail($p);
				
				$allforemanprojects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
							->where('foreman_id',$p)
							->where('type','=','foreman')
							->whereDate('projects.updated_at','=',date('Y-m-d')) 
							->where('projects.is_completed','=',0)  
							->distinct('project_id')
							->select('projects.*')->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$foremandata['projects'][]=is_pm_project($allforemanprojects,$p);
					}
				}
				
				$foremandata['projects']=array_filter($foremandata['projects']);
				
				
				
				\Mail::send('emails.schedule_PM_APM', $foremandata, function($message) use ($foremanmail,$subject)
				{
					$message->to($foremanmail, 'Super')->subject($subject);
					
				});	
			}
		}
		
				
		
		//send email to project contractors			
		//$allContractors=getSubcontractors();
		$contractors = array_unique($contractors);
		
		//print_r($contractors);die;
		
		if(count($contractors) > 0){
			foreach($contractors as $u){
				$sub_id = $u;
				$contractordata=array();
				//get contractor projects
				$contractorprojects=SubcontractorRequest::join('projects','projects.id','=','manpower_requests_subcontractors.project_id')
					->where('subcontractor_id',$sub_id)
					->whereDate('projects.updated_at','=',date('Y-m-d')) 
					->where('projects.is_completed','=',0)  
					->distinct('manpower_requests_subcontractors.project_id')
					->select('projects.*')
					->get();
				
				
				
				if(!empty($userProjects)){
					foreach($userProjects as $p){
						$contractordata['projects'][]=is_pm_project($contractorprojects,$p);
					}
				}
				
				if(!empty($contractordata['projects'])){
					$contractordata['projects']=array_filter($contractordata['projects']);
				}
				
				/*\Mail::send('emails.schedule_PM_APM', $contractordata, function($message) use ($subemail)
				{
					$message->to($subemail, 'Super')->subject('Manpower Schedule!!');
					
				});	*/
				//send email to contractor members*/.
				$members=SubcontractorMember::where('subcontractor_id',$sub_id)->get();
				if(count($members)){
					foreach($members as $m){
						$emailm=$m->email;
						\Mail::send('emails.schedule_PM_APM', $contractordata, function($message) use ($emailm,$subject)
						{
							$message->to($emailm, 'Super')->subject($subject);
							
						});
					}
				}
			}
		}
		$scount=Manpowerrecord::where('super_id', '=', Auth::user()->id)->count();
		if($scount==0){
			Manpowerrecord::create(array(
				'super_id'    =>  Auth::user()->id			
			));
		}else{
			Manpowerrecord::where('super_id', Auth::user()->id)->delete();
			Manpowerrecord::create(array(
				'super_id'    =>  Auth::user()->id			
			));
		}
		echo 'success';die;
		/*********END OF FUNCTION**************/
		
	}
	
	public function daily_report_view(){
		$subject = "Daily Report for ".date('m/d/Y',strtotime("-1 days"));
		
		//$superemail=getSuperEmail();
		$superemail = 'ankit.chugh@42works.net';
		
		//send email to super
		$ten_days_ago = date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d'))));
		$projects	= 	Project::get();
		
		$data = [
				   'projects' => array()
				  
		];
		
		$is_sent_mail=0;
		
		if(count($projects) > 0){
			foreach($projects as $up){
				$id=$up->id;
				$projectrequests		= 	ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
				$projectlogs	=			WorklogMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$surveys        =			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$tasks      	=			Task::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$files			=			File::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
			//	if(count($projectlogs) > 0 || count($surveys) > 0)  {
					$is_sent_mail=1;
					foreach($projectlogs as $log){
						$data['projects'][$log->logged_at] = $up;												
					}										
				//}
			
			}
		}
		
		if(!empty($data['projects'])){
			foreach($data['projects'] as $key=>$p){
				$newdata['customer']=$superemail;
				$newdata['submitted_for']=date('Y-m-d',strtotime($key));
				$newdata['project']=$p;
				
				$htm = view('emails/superReport3', $newdata)->render();
				$pdf = PDF::loadHTML($htm);
				return $pdf->stream();
				//return view('emails/superReport3', $newdata);
			}			
		}
		

		//$pdf->stream();
		//return view('emails/superReport2', $newdata);
			
	}
}
