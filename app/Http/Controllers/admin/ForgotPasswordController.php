<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;
use Request;

use Illuminate\Hashing\BcryptHasher;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

class ForgotPasswordController extends Controller {
	
	public function __construct(){
	 
	}
	public function index(){
		return view('admin/forgotpassword/forgotpassword', array('title' => 'Forgot Password'));
	}
	public function send(Mail $mail){
		$user = User::where('email', '=', Input::get('email'));
		
		if($user->count()>0){
				$code = str_random(60);
                $user = $user->first();
                $user->recoverycode = $code;
                $email = $user->email;
                $username = $user->username;

                if ($user->save()) {
                    $mail->send('emails.password', array(
                        'link' => url() . '/api/update_password?key=D4788EBA535DC57377BAD975DED90&code=' . $code,
                        'username' => $user->username
                            ), function ($message) use($username, $email) {
                        $message->to($email, $username)->subject('Password Recovery');
                    });
                }

				Session::flash('success', 'Recovery Email Sent!!!'); 
				return Redirect::to('admin/forgotpassword')->with('data',array('title' => 'Forgot Password'));

		}else{
				Session::flash('error', 'Email not Exists!!!'); 
				return Redirect::to('admin/forgotpassword')->with('data',array('title' => 'Forgot Password'));
		}
	}
	
	public function resetPassword(Request $request,BcryptHasher $hash){
		$data = Input::all();
		 

		$validator = Validator::make($data, [
                    'password1' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,9})',
                    'code' => 'required'
        ],
        array(
							'password1.regex' => \Config::get('constants.PASSWORD_FAILED')
		));
		
		$key=$data['key'];
		$code=$data['code'];
        if ($validator->fails()) {
			return Redirect::to('api/update_password?key='.$key.'&code='.$code)->withErrors($validator);
        } else {
            $user = User::where('recoverycode', '=', $code);

            if ($user->count()) {
                $user = $user->first();
                if ($user) {
                    $user->password = $hash->make($data['password2']);
                    if ($user->save()) {
					   Session::flash('success', 'Password updated successfully!');
                       return Redirect::to('api/update_password?key='.$key.'&code='.$code)->withErrors($validator);
                    } else {
						Session::flash('error', 'Some error!');	
						return Redirect::to('api/update_password?key='.$key.'&code='.$code)->withErrors($validator);
                    }
                } else {
				   Session::flash('error', 'Invalid User!');	
                   return Redirect::to('api/update_password?key='.$key.'&code='.$code)->withErrors($validator);
                }
            } else {
			    Session::flash('error', 'Invalid User or Authentication Code!');	
               return Redirect::to('api/update_password?key='.$key.'&code='.$code)->withErrors($validator);
            }
        }
	}
}
