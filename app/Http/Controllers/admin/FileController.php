<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Intervention\Image\Facades\Image as Image;
use App\Models\File;
use App\Models\FileCategories;
use App\Models\Notification;

class FileController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
       
        $user_role= getUserRole(Auth::user()->id);
       
        $name=Input::get('name');
        
        if($user_role=='role_superadmin'){
			
			if(isset($name) && !empty($name)){
				
				  $files=File::where('file_name','LIKE','%'.$name.'%')->orWhere('title','LIKE','%'.$name.'%')->orderBy('id', 'DESC')->get();
				  return view('admin/file/manage', array('title' => 'Files','files'=>$files,'active'=>'FILES'));
			}else{
			
				$files=File::orderBy('id', 'DESC')->get();
				return view('admin/file/manage', array('title' => 'Files','files'=>$files,'active'=>'FILES'));
			}
		}elseif($user_role=='role_administrator'){
			  //Conditional Logic as per Job Role
				$projects_ids=getUserPojectIds(Auth::user()->id);
				
				if(isset($name) && !empty($name)){
				
					  $files=File::where('file_name','LIKE','%'.$name.'%')->whereIn('project_id',$projects_ids)->orWhere('title','LIKE','%'.$name.'%')->orderBy('id', 'DESC')->get();
					  return view('admin/file/manage', array('title' => 'Files','files'=>$files,'active'=>'FILES'));
				}else{
					$files=File::whereIn('project_id',$projects_ids)->orderBy('id', 'DESC')->get();
					return view('admin/file/manage', array('title' => 'Files','files'=>$files,'active'=>'FILES'));
				}
					
		}
	}
    
    public function categories(){
        $cat=FileCategories::get();
        return view('admin/file/categories', array('categories'=>$cat,'title' => 'All Categories','active'=>'CATEGORIES')); 
    }
    
	public function add(){
		
		return view('admin/file/add', array('title' => 'Add New File','active'=>'ADD_FILES')); 
	}
	public function save(){
		
			$data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'title' => 'required',
				'file' => 'required|max:500000',
				'project' => 'required',
				'categories' => 'required'
			);
        
	       $validator = Validator::make($data, $rules);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/files/add')->withErrors($validator)->withInput(); 
			}else{
				
					/*$w=File::create(array(
					
						'title'    =>  Input::get('title'),
						'project_id'    =>  Input::get('project'),
						'added_by'    =>  Auth::user()->id,
						'category_ids'    =>  implode(',',Input::get('categories'))
					));	*/
			         
               
                    //$fileid=$w->id;

					if (Input::hasFile('file')) {
						$path='uploads/files/';
						//\File::cleanDirectory($path);
						\File::makeDirectory($path,$mode = 0777, true, true);

						$file            = Input::file('file');
						$filename        = str_random(6) . '_' . $file->getClientOriginalName();
						$filename = str_replace(' ', '_', $filename);
						//$uploadSuccess = Image::make($file->getRealPath())->resize(300, 300)->save($path.$filename);
						$uploadSuccess   = $file->move($path, $filename);
						
						if($uploadSuccess){
							$w=File::create(array(
								'title'    =>  Input::get('title'),
								'project_id'    =>  Input::get('project'),
								'added_by'    =>  Auth::user()->id,
								'category_ids'    =>  implode(',',Input::get('categories')),
								'file'    =>       url().'/'.$path.$filename,
								'file_name'    =>  $file->getClientOriginalName() 
							));	
						}
						/*File::where('id', $fileid)->update(array(
							'file'    =>       url().'/'.$path.$filename,
							'file_name'    =>  $file->getClientOriginalName() 
						));*/
					}
					
					updateProjectDate(Input::get('project'));
					
					$members=getProjectTeam(Input::get('project'));
					
					
					
					if(count($members)){
						foreach($members as $m){
							//set  notification
							$admin_id=get_admin_id();
							Notification::create(array(
								'notification_from'    =>  $admin_id,
								'notification_to' 	=>  $m->foreman_id ,
								'notification_type' =>'file_added',
								'project_id' =>Input::get('project')
							));
							$email=getUserEmail($m->foreman_id);
							$title=get_project_title(Input::get('project'));
							//send email
							
							\Mail::send([], [], function ($message) use ($email,$title) {
							  $message->to($email)
								->subject($title.': File added')
								// here comes what you want
								->setBody('Hi,<br/><br/> A new file has been added to project:<b> '.$title.'</b>', 'text/html');
							});
						}
					}
					
					
				
                    Session::flash('success', 'File Added successfully');
                    
                    if(Input::get('redirect')==1){
						return Redirect::to('/admin/projects/view/'.Input::get('manual_id'))->with('data',array('title' => 'Files'));
					}
                     
                    return Redirect::to('admin/files/')->with('data',array('title' => 'Files'));
			}
	}
	
	public function edit($id=null){
		$manpower=Customer::where('id','=',$id)->first();
		$data['title']='Edit Customer';
		$data['customer']=$manpower;
		return view('admin/customer/edit',$data);
	}
	
	public function update(){
			$data = Input::all();
			$customerid   = Input::get('customerid');
			$rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'phone' => 'required|numeric',
				'address' => 'required'
			);
        
	       $validator = Validator::make($data, $rules);
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/customers/edit/'.$customerid)->withErrors($validator);
                     
			}else{
				$check_title = Customer::where('customer', '=', Input::get('name'))->where('id','!=',Input::get('customerid'))->count();
				if($check_title != 0){
					Session::flash('error', 'Customer Name  has already been used. Please select another one!'); 
					return Redirect::to('admin/customers/edit/'.$customerid)->with('data',array('title' => 'Edit Customer'));
				}else{
					Customer::where('id', $customerid)->update(array(
						'customer'    =>  Input::get('name'),
						'email'    =>  Input::get('email'),
						'phone'    =>  Input::get('phone'),
						'address' =>   Input::get('address')
					));
					
					
				Session::flash('success', 'Customer  updated successfully'); 
				return Redirect::to('admin/customers/edit/'.$customerid)->with('data',array('title' => 'Edit Customer'));
           }
        }
	}
	
	public function delete($id=null,$pid=null){
			
			File::where('id','=',$id)->delete();
            $directory='uploads/files/'.$id.'/';
            \File::deleteDirectory($directory);
        
			Session::flash('success', 'File Deleted successfully'); 
			if(!empty($pid)){
				return Redirect::to('/admin/projects/view/'.$pid)->with('data',array('title' => 'Project'));
			}
			return Redirect::to('admin/files/')->with('data',array('title' => 'Files'));
	}
	public function delete_single($id=null){
			
			File::where('id','=',$id)->delete();
            $directory='uploads/files/'.$id.'/';
            \File::deleteDirectory($directory);
        
			Session::flash('success', 'File Deleted successfully'); 
			
			return Redirect::to('admin/files/')->with('data',array('title' => 'Files'));
	}
    public function add_category(){
        return view('admin/file/add_category', array('title' => 'Add New Category','active'=>'CATEGORIES')); 
    }
    
    public function save_category(){
		
			$data = Input::all();
			//print_r($data);die;
			// Applying validation rules.
			$rules = array(
				'name' => 'required',
				'description' => 'required'
			);
        
	       $validator = Validator::make($data, $rules);
	      
	       if ($validator->fails()){
                return Redirect::to('admin/files/categories/add')->withErrors($validator)->withInput(); 
			}else{
				
					$w=FileCategories::create(array(
						'category'    =>  Input::get('name'),
						'description'    =>  Input::get('description')
					));	
               
                    Session::flash('success', 'Category Added successfully'); 
                    return Redirect::to('admin/files/categories')->with('data',array('title' => 'All Categories'));
			}
	}
    
    public function delete_category($id=0){
            FileCategories::where('id','=',$id)->delete();
           
			Session::flash('success', 'Category  Deleted successfully'); 
			return Redirect::to('admin/files/categories')->with('data',array('title' => 'All Categories'));
    }
    
	public function autocomplete(){
        
        $searchTerm =Input::get('term');
        $users	= 	File::where('file_name','LIKE','%'.$searchTerm.'%')->orWhere('title','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->file_name;
        }
        //return json data
        echo json_encode($data);die;
    }
	
}
