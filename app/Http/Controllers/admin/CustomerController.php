<?php 
namespace App\Http\Controllers\Admin; //admin add
use App\Http\Controllers\Controller;

use Validator;
use Input;
use Auth;
use Session;

use Illuminate\Support\Facades\Redirect;


use App\Models\Customer;
use App\Models\CustomerMember;

class CustomerController extends Controller {
	
	public function __construct(){
	  if (!Auth::check()){
		 return Redirect::to('/')->send();;
	   }
	}
	public function index(){
        
        $name=Input::get('name');
        
        if(isset($name) && !empty($name)){
			
             $customers=Customer::where('customer','LIKE','%'.$name.'%')->orderBy('customer','ASC')->get();
              return view('admin/customer/manage', array('title' => 'Customers','customers'=>$customers,'active'=>'CUSTOMER'));
		}else{
        
            $customers=Customer::orderBy('customer','ASC')->get(); 
            return view('admin/customer/manage', array('title' => 'Customers','customers'=>$customers,'active'=>'CUSTOMER'));
        }
	}
	public function add(){
		
		return view('admin/customer/add', array('title' => 'Add New Customer','active'=>'ADD_CUSTOMER')); 
	}
	public function save(){
		
			$data = Input::all();
			
			$rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'phone' => 'required|numeric',
				'address' => 'required'
			);
        
	       $validator = Validator::make($data, $rules);
	      
	       Session::flash('pm', Input::get('pm'));
           Session::flash('sd', Input::get('sd'));
	      
	       	
			$c_name=Input::get('c_name');
			$c_role=Input::get('c_role');
			$c_email=Input::get('c_email');
			$c_phone=Input::get('c_phone');
			$o_phone=Input::get('o_phone');
			
			Session::flash('c_name', $c_name);
			Session::flash('c_role', $c_role);
			Session::flash('c_email', $c_email);
			Session::flash('c_phone', $c_phone);
			Session::flash('o_phone', $o_phone);		
	      
	       if ($validator->fails()){
                return Redirect::to('admin/customers/add')->withErrors($validator)->withInput(); 
			}else{
				$check_name = Customer::where('customer', '=', Input::get('name'))->count();
				
			
				if ($check_name != 0) {
					Session::flash('error', 'Customer Name  has already been used. Please select another one!'); 
					return Redirect::to('admin/customers/add')->with('data',array('title' => 'Add New Customer'))->withInput(); ;
				}else{
					$cid=Customer::create(array(
						'customer'    =>  Input::get('name'),
						'email'    =>  Input::get('email'),
						'phone'    =>  Input::get('phone'),
						'address' =>   Input::get('address'),
						'pm_apm' =>   "pm_apm",
						//'project_manager' =>   Input::get('pm'),
						//'superintendent' =>   Input::get('sd')
						
					));
					
					
				
					if(!empty($c_name)){
						foreach($c_name as $key=>$c){
							CustomerMember::create(array(
								'customer_id'    =>  $cid->id,
								'name'    =>  $c,
								'role'    =>  $c_role[$key],
								'email'    =>  $c_email[$key],
								'phone'    =>  $c_phone[$key],
								'office_phone' =>  $o_phone[$key]
							));
						}
					}
						
				}
				
				Session::flash('success', 'Customer Type Added successfully'); 
				return Redirect::to('admin/customers/')->with('data',array('title' => 'CUSTOMER'));
			}
	}
	
	public function edit($id=null){
		$manpower=Customer::where('id','=',$id)->first();
		$customer_members=CustomerMember::where('customer_id','=',$id)->orderBy(\DB::raw("SUBSTRING_INDEX(name, ' ',-1)"))->get(); 
		$data['title']='Edit Customer';
        $data['active']='CUSTOMER';
		$data['customer']=$manpower;
		$data['customer_members']=$customer_members;
		return view('admin/customer/edit',$data);
	}
	
	public function update(){
			$data = Input::all();
			$customerid   = Input::get('customerid');
			$rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'phone' => 'required|numeric',
				'address' => 'required',
				//'pm' => 'required',
				//'sd' => 'required'
			);
        
	       $validator = Validator::make($data, $rules);
	       
	       if ($validator->fails()){
                 // If validation falis redirect back to signup.
                return Redirect::to('admin/customers/edit/'.$customerid)->withErrors($validator);
                     
			}else{
				$check_title = Customer::where('customer', '=', Input::get('name'))->where('id','!=',Input::get('customerid'))->count();
				if($check_title != 0){
					Session::flash('error', 'Customer Name  has already been used. Please select another one!'); 
					return Redirect::to('admin/customers/edit/'.$customerid)->with('data',array('title' => 'Edit Customer'));
				}else{
					Customer::where('id', $customerid)->update(array(
						'customer'    =>  Input::get('name'),
						'email'    =>  Input::get('email'),
						'phone'    =>  Input::get('phone'),
						'address' =>   Input::get('address'),
						'pm_apm' =>   "pm_apm",
						//'project_manager' =>   Input::get('pm'),
						//'superintendent' =>   Input::get('sd')
					));
					
					
					$c_name=Input::get('c_name');
					$c_role=Input::get('c_role');
					$c_email=Input::get('c_email');
					$c_phone=Input::get('c_phone');
					$o_phone=Input::get('o_phone');
					$c_id=Input::get('c_id');
					
					if(!empty($c_name)){						
						//CustomerMember::where('customer_id','=',$customerid)->delete();
						foreach($c_name as $key=>$c){
							
							//check if member exists
							$is_exists=CustomerMember::where('id',$c_id[$key])
													->where('customer_id',$customerid)
													->count();
							
							//echo $is_exists;die;
							
							if($is_exists){
								
								$data=CustomerMember::where('id',$c_id[$key])
													->where('customer_id',$customerid)
													->first();
								$member_id=$data->id;
								
								CustomerMember::where('id', $member_id)->update(array(
									'name'    =>   $c,
									'role'    =>   $c_role[$key],
									'email'    =>  $c_email[$key],
									'phone'    =>  $c_phone[$key],
									'office_phone'    =>  $o_phone[$key]
								));
							}else{
								CustomerMember::create(array(
									'customer_id'    =>  $customerid,
									'name'    =>   $c,
									'role'    =>   $c_role[$key],
									'email'    =>  $c_email[$key],
									'phone'    =>  $c_phone[$key],
									'office_phone'    =>  $o_phone[$key]
								));
							}
						}
					}
					
				Session::flash('success', 'Customer  updated successfully'); 
				return Redirect::to('admin/customers/edit/'.$customerid)->with('data',array('title' => 'Edit Customer'));
           }
        }
	}
	
	public function delete($id=null){
			Customer::where('id','=',$id)->delete();
			Session::flash('success', 'Customer Deleted successfully'); 
			return Redirect::to('admin/customers/')->with('data',array('title' => 'CUSTOMER'));
	}
	
	public function autocomplete(){
        
        $searchTerm =Input::get('term');
        $users	= 	Customer::where('customer','LIKE','%'.$searchTerm.'%')->get();
        foreach($users as $u){
            $data[] = $u->customer;
        }
        //return json data
        echo json_encode($data);die;
    }
	
}
