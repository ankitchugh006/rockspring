<?php 
namespace App\Http\Controllers;

use Validator;
use Input;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Contracts\Mail\Mailer as Mail;

use App\Models\User;

class ApiAuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	public function __construct()
	{

	}	
	public function login(Request $request, Mail $mail, BcryptHasher $hash) { 
		//echo $request->input('push_id');die
		
				
		$validator = Validator::make(
				$request->all(), array(
				    'username' => 'required',
					'password' => 'required',
					'push_id' => 'required',
					'device_type' => 'required',
					'device_os' => 'required'
				)
        );

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' =>  \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			$field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
			
			$user = array(
				 $field => $request->input('username'),
				'password' => $request->input('password')
			);
			if (Auth::validate($user)) {
				if (Auth::attempt($user)){
					
					//updating some data on login
					$userdata = User::where($field, '=', $request->input('username'))->get();
					$userdata1 = $userdata->first();
					
					if($userdata1->status==0){
						 return response()->json([
									'success' => false,
									'code' => 401,
									'message' => \Config::get('constants.ACCOUNT_INACTIVE')
						]);
					}
					
					
					
					
					/*if($userdata1->role == 'role_superadmin'){
						
						return response()->json([
								'success' => false,
								'code' => 401,
								'message' => "You don't have privilidge to access the app"
								]);
					}*/
					
					$userdata1->push_id = $request->input('push_id');
					$userdata1->device_type = $request->input('device_type');
					$userdata1->device_os = $request->input('device_os');
					if(empty($userdata1->device_token)){
						$userdata1->device_token = str_random(40);
					}
					$userdata1->last_login = date('Y-m-d h:m:s');
					
					$data=array(
						'push_id'=>$request->input('push_id'),
						'device_type'=>$request->input('device_type'),
						'device_os'=>$request->input('device_os'),
					);
					if(empty($userdata1->device_token)){
						$data['device_token']=str_random(40);
					}
					
					User::where('id', $userdata1->id)->update($data);
					
					
					$userdata1->save();
					
				    return response([
                        'success' => true,
                        'code' => 200,
                        'message' => \Config::get('constants.CREDENTIALS_VERIFIED'),
                        'user' => $userdata1
                    ]);
				}
			} else{
				 return response()->json([
							'success' => false,
							'code' => 401,
							'message' => \Config::get('constants.INVALID_CREDENTIALS')
				]);
			}
        }
    }
    public function register(Request $request, Mail $mailer, UrlGenerator $URLGEN, BcryptHasher $hash) {
	     $validator = Validator::make($request->all(), array(    
						'name' => 'required',
						'email' => 'required|email|unique:users',
						'username' => 'required',
						'password' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,9})',
						'device_type' => 'required',
						'device_os' => 'required',
						'push_id' => 'required'
					),
					array(
							'password.regex' => \Config::get('constants.PASSWORD_FAILED')
					)
				);
        
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
			
            $check_email = User::where('email', '=', $request->input('email'))->count();
            $check_username = User::where('username', '=', $request->input('username'))->count();
            if ($check_email != 0) {
				 return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => array(\Config::get('constants.DUPLICATE_EMAIL')),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
			}elseif($check_username != 0){
				return response()->json([
                            'success' => false,
                            'code' => 400,
                            'errors' => array(\Config::get('constants.DUPLICATE_USERNAME')),
                            'message' => \Config::get('constants.VALIDATION_FAILED')
                ]);
			}else{
				$username=$request->input('username');
                $email=$request->input('email');
                /****************************/
                $have_spaces=preg_match('/\s/',$username);
                if($have_spaces){
					return response()->json([
								'success' => false,
								'code' => 400,
								'errors' => array(\Config::get('constants.REMOVE_SPACES')),
								'message' => \Config::get('constants.VALIDATION_FAILED')
					]);
				}
				
				$have_uppercase=preg_match('/[A-Z]/', $username);
				if($have_uppercase){
					return response()->json([
								'success' => false,
								'code' => 400,
								'errors' => array(\Config::get('constants.LOWERCASE')),
								'message' =>\Config::get('constants.VALIDATION_FAILED')
					]);
				}
				/***********************************/
				$token=str_random(40);
                $new = User::create(array(
                            'name' => $request->input('name'),
                            'username' => $username,
                            'email' => $email,
                            'password' => $hash->make($request->input('password')),
                            'device_type' => $request->input('device_type'),
                            'device_os' => $request->input('device_os'),
                            'device_token' => $token,
                            'push_id' => $request->input('push_id'),
                            'last_login' => date('Y-m-d h:m:s'),
                            'role' => 	'role_foreman',
                            'job_role' => 'job_foreman',
                            'status'=>1      
                ));
                
                
                $userdata = User::where('username', '=', $request->input('username'))->get();
				$userdata1 = $userdata->first();
                
                $mailer->send('emails.welcome', array(
                        'name' => $request->input('username')
                            ), function ($message) use($username, $email) {
                        $message->to($email, $username)->subject('Welcome to Rockspring');
                 });
                
                
            } 
        }
        if ($new) {

            return response()->json([
                        'success' => true,
                        'code' => 201,
                        'message' => \Config::get('constants.ACCOUNT_CREATED'),
                        'user' => $userdata1
                        
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'code' => 500,
                        'message' =>\Config::get('constants.SERVER_ERROR')
            ]);
        }
	}
	public function logout(Request $request) {
        $validator = Validator::make($request->all(), array('foreman_id' => 'required'));

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
            $Userdata = User::where('id', '=', $request->input('foreman_id'));

            if ($Userdata->count()) {

                $user = $Userdata->first();

                //$user->device_token = '';

                if ($user->save()) {
                    return response([
                        'success' => true,
                        'code' => 200,
                        'message' => \Config::get('constants.LOGOUT')
                    ]);
                } else {
                    return response()->json([
                                'success' => false,
                                'code' => 500,
                                'message' => \Config::get('constants.SERVER_ERROR')
                    ]);
                }
            } else {
                return response()->json([
                            'success' => false,
                            'code' => 401,
                            'message' => \Config::get('constants.USER_NOT_EXISTS')
                ]);
            }
        }
    }
    
    public function get_profile(Request $request,UrlGenerator $URLGEN){
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'device_token' => 'required'
				)
        );
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				$userdata = User::where('id', '=', $request->input('foreman_id'))->select('id as foreman_id','name','email','username')->first();
				
				return response([
					'success' => true,
					'code' => 200,
					'data' => $userdata->toArray()
				]);
				
			}else{
				return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
    
    public function update_profile(Request $request,UrlGenerator $URLGEN){
		
		$validator = Validator::make(
				$request->all(), array(
				    'foreman_id' => 'required',
				    'name' => 'required',
				    'email' => 'required|email',
				    'username' => 'required'
				)
        );
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' => \Config::get('constants.VALIDATION_FAILED')
            ]);
        }else{
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
				$check_email = User::where('email', '=', $request->input('email'))->where('id','!=',$request->input('foreman_id'))->count();
				$check_username = User::where('username', '=', $request->input('username'))->where('id','!=',$request->input('foreman_id'))->count();
				if ($check_email != 0) {
					 return response()->json([
								'success' => false,
								'code' => 400,
								'errors' => array(\Config::get('constants.DUPLICATE_EMAIL')),
								'message' => \Config::get('constants.VALIDATION_FAILED')
					]);
				}elseif($check_username != 0){
					return response()->json([
								'success' => false,
								'code' => 400,
								'errors' => array(\Config::get('constants.DUPLICATE_USERNAME')),
								'message' => \Config::get('constants.VALIDATION_FAILED')
					]);
				}else{
					
					$username=$request->input('username');

					/****************************/
					$have_spaces=preg_match('/\s/',$username);
					if($have_spaces){
						return response()->json([
									'success' => false,
									'code' => 400,
									'errors' => array(\Config::get('constants.REMOVE_SPACES')),
									'message' => 'Input Validation Failed'
						]);
					}
					
					$have_uppercase=preg_match('/[A-Z]/', $username);
					if($have_uppercase){
						return response()->json([
									'success' => false,
									'code' => 400,
									'errors' => array(\Config::get('constants.LOWERCASE')),
									'message' => \Config::get('constants.VALIDATION_FAILED')
						]);
					}
					
					$userdata = User::where('id', '=', $request->input('foreman_id'))->get();
					$userdata1 = $userdata->first();
					
					$userdata1->name = $request->input('name');
					$userdata1->email = $request->input('email');
					$userdata1->username = $request->input('username');
					$userdata1->save();
					
				    return response([
                        'success' => true,
                        'code' => 200,
                        'message' => 'Profile Updated!!'
                    ]);
				}
					
			}else{
				return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}
	}
    public function update_password_app(Request $request, BcryptHasher $hash,UrlGenerator $URLGEN){
		$validator = Validator::make($request->all(), array(
                    'foreman_id' => 'required',
                    'old_password' => 'required',
                    'new_password' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,9})',
                    'confirm_password' => 'required'
					),
					array(
							'new_password.regex' => \Config::get('constants.PASSWORD_FAILED')
					)
				);
        if ($validator->passes()) {
			$is_valid_user=User::Where('id',$request->input('foreman_id'))->where('device_token',$request->input('device_token'));
			if($is_valid_user->count()>0){
			    
			    if($request->input('new_password') !== $request->input('confirm_password')){
						return response()->json([
								'success' => false,
								'code' => 400,
								'message' =>  \Config::get('constants.PASSWORD_MISMATCH')
						]);
				}
				$user = User::where('id', '=', $request->input('foreman_id'))->first();
				//check old password 
				if ($hash->check(Input::get('old_password'), $user->password)) {
				
					
						//change password
						$user->password = $hash->make($request->input('confirm_password'));
						if ($user->save()) {
							return response([
								'success' => true,
								'code' => 200,
								'message' => \Config::get('constants.PASSWORD_UPDATED')
							]);
						}
					
				}else{
					return response()->json([
								'success' => false,
								'code' => 400,
								'message' =>  \Config::get('constants.INCORRECT_OLD_PASSWORD')
					]);
				}
			}else {
				return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => \Config::get('constants.INVALID_TOKEN')
				]);
			}
		}else{
			return response()->json([
								'success' => false,
								'code' => 400,
								'errors' => $validator->errors()->all(),
								'message' =>  \Config::get('constants.VALIDATION_FAILED')
					]);
		}
	}
	
    public function recoverpassword(Request $request, Mail $mail, BcryptHasher $hash,UrlGenerator $URLGEN){
		$email = $request->input('email');
        $validator = Validator::make($request->all(), array(
                    'email' => 'required|email'
        ));

        if ($validator->passes()) {
            $user = User::where('email', '=', $email)
                    ->where('password', '!=', '');

            if ($user->count()) {
                $code = str_random(60);
                $user = $user->first();
                $user->recoverycode = $code;
                $email = $user->email;
                $username = $user->username;

                if ($user->save()) {
                    $mail->send('emails.password', array(
                        'link' => url() . '/api/update_password?key=D4788EBA535DC57377BAD975DED90&code=' . $code,
                        'username' => $user->username
                            ), function ($message) use($username, $email) {
                        $message->to($email, $username)->subject('Password Recovery');
                    });
                }
                return response([
                    'success' => true,
                    'code' => 200,
                    'message' => 'Reset password link has been sent on the registered email ID',
                ]);
            } else {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => \Config::get('constants.EMAIL_NOT_FOUND')
                ]);
            }
        } else {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' =>  \Config::get('constants.VALIDATION_FAILED')
            ]);
        }
	}
	function update_password(Request $request, Mail $mail, BcryptHasher $hash,UrlGenerator $URLGEN){
		$user = User::where('recoverycode', '=', $request->input('code'));
        $user = $user->first();		
        return view('changepassword')->with('user', $user)->with('code', $request->input('code'));
	}

	public function changepassword(Request $request, BcryptHasher $hash) {
        $validator = Validator::make($request->all(), [
                    'password1' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,9})',
                    'code' => 'required'
        ],
        array(
							'password1.regex' => \Config::get('constants.PASSWORD_FAILED')
		));
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 400,
                        'errors' => $validator->errors()->all(),
                        'message' =>\Config::get('constants.VALIDATION_FAILED')
            ]);
        } else {
            $user = User::where('recoverycode', '=', $request->input('code'));

            if ($user->count()) {
                $user = $user->first();


                if ($user) {
                    $user->password = $hash->make($request->input('password2'));
                    if ($user->save()) {
                        return response([
                            'success' => true,
                            'code' => 200,
                            'message' =>  \Config::get('constants.PASSWORD_UPDATED')
                        ]);
                    } else {
                        return response()->json([
                                    'success' => false,
                                    'code' => 500,
                                    'message' => \Config::get('constants.SERVER_ERROR')
                        ]);
                    }
                } else {
                    return response()->json([
                                'success' => false,
                                'code' => 401,
                                'message' => \Config::get('constants.USER_NOT_EXISTS')
                    ]);
                }
            } else {
                return response()->json([
                            'success' => false,
                            'code' => 400,
                            'message' => 'Invalid User or Authentication Code'
                ]);
            }
        }
    }
}
