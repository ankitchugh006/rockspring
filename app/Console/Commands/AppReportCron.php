<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use PDF;


class AppReportCron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:appreport';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$subject = "Daily Report for ".date('m/d/Y',strtotime("-1 days"));		
		$superemail=getSuperEmail();
		//$superemail = 'ankit.chugh@42works.net';
		
		//send email to super
		$ten_days_ago = date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d'))));
		$projects	= 	Project::whereDate('updated_at','>',$ten_days_ago)->where('projects.is_completed','=',0)->get();
		
		$data = [
				   'projects' => array() 
		];
		$i=0;
		if(count($projects) > 0){
			foreach($projects as $up){
				$id=$up->id;
				$projectrequests		= 	ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
				$projectlogs	=			WorklogMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$surveys        =			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$tasks      	=			Task::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$files			=			File::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				if(count($projectlogs) > 0 || count($surveys) > 0)  {
					foreach($projectlogs as $log){
						$data['projects'][date('Y-m-d H:i:'.$i,strtotime($log->logged_at))] = $up;	
						$i++;									
					}										
				}else{ 
					$path='uploads/project_reports/'.base64_encode($up->id).'/'.$up->title.'.pdf';
					if(file_exists($path)){ 
						unlink($path);
					}
				}
			
			}
		}
		
		if(!empty($data['projects'])){ 
			foreach($data['projects'] as $key=>$p){
					$newdata['customer']=$superemail;
					$newdata['submitted_for']=date('Y-m-d',strtotime($key));
					$newdata['project']=$p;
					$pdf = PDF::loadView('emails.superReport2', $newdata);
					\File::makeDirectory('uploads/project_reports/'.base64_encode($p['id']).'/',$mode = 0777, true, true);
					$pdf->output();
					$pdf->save('uploads/project_reports/'.base64_encode($p['id']).'/'.$p['title'].'.pdf'); 
					//$pdf->save(storage_path('some-folder/some-subfolder/some-filename.pdf'));	
			}			
		}
	}
}
