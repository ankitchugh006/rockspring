<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use PDF;


class TimelogCron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'timelog:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$emails=array(
			'ankit.chugh@42works.net',
			'raman@42works.net',
			'rahul@42works.net'
		);
		$subject = 'Timelog reminder dosto';
		foreach($emails as $em){
			\Mail::send('emails.timelog', array(), function($message) use ($em,$subject)
			{
				$message->to($em, 'Super')->subject($subject);
			});		
			
		}
		

	}
}
