<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use PDF;


class ProjectRequest extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:request';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		/*$superemail=getSuperEmail();
		
		$date=date('Y-m-d');
		$todayrequests=ForemanRequestMain::whereDate('created_at','=',$date)->get();
		
		$data['todayrequests']=$todayrequests;
		
		\Mail::send('emails.requestReport', $data, function($message) use ($superemail)
		{
			$message->to($superemail, 'Manpower Requests')->subject('Manpower Requests!');
			//$message->attachData($pdf->output(), "report.pdf");
		});*/
		
		$ten_days_ago = date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d'))));
		$users = User::where('job_role','=','job_superintendent')->get();
		//print_r($users);
		if(count($users) > 0){ 
			foreach($users as $u){
				$userProjects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
                            ->where('foreman_id',$u->id)
                            ->where('type','=','super')
                            //->whereDate('projects.updated_at','>',$ten_days_ago)
                            ->where('projects.is_completed','=',0)
                            ->distinct('project_id')
							->select('projects.*')->get();
				
				
				//print_r($userProjects);
							
				$pdata['customer']=$u->name;
				$email=$u->email;
				$pdata['projects']=$userProjects;
							
				\Mail::send('emails.requestReport', $pdata, function($message) use ($email)
				{
					$message->to($email, 'Manpower Requests')->subject('Manpower Requests!');
					
				});		
				
			}
		}
		
		
		
	}
	

}
