<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use PDF;


class ForemanWorklogNotification extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:worklognotification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$superemail=getSuperEmail();
		$users=User::where('role','=','role_foreman')->get();
			
		if(!empty($users)){
			foreach($users as $u){
			
				//get user's project
				$user_projects=getUserPojectIdsCron($u->id);
					
				if(!empty($user_projects)){
					
					foreach($user_projects as $p){
						$is_worklog_submitted=WorklogMain::where('project_id', '=', $p)
											->where('foreman_id','=',$u->id)
											->count();
						
						$is_resource_requested=ForemanRequestMain::where('project_id', '=', $p)
											->where('foreman_id','=',$u->id)
											->count();
											
						if(($is_worklog_submitted==0) && ($is_resource_requested>0)){
							$push_id=get_push_id($u->id);
			
							#sending notification to foreman
							
							if(isset($push_id)){
								if($u->device_os=="android"){
									$message = array (
											'message' => "Work log need to be completed",
											'title'	=>    'Submit Worklog',
											'subtitle'	=>'Submit Worklog',
											'tickerText'	=> 'Submit Worklog',
											'vibrate'	=> 1,
											'sound'	=> 1,
											'largeIcon'	=> 'large_icon',
											'smallIcon'	=> 'small_icon'
									);
																	   
									send_push_notification(array($push_id),$message);
								}else{
									$iosmessage=array(
										'badge' => 1,
										'alert' => "Work log need to be completed",
										'sound' => 'default'
									);
									
									send_push_notification_ios($push_id,$iosmessage);
								}
							}
						}					
					}
				}
			}
		}
	}
	

}
