<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;

class ProjectInactive extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:inactive';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$currentdate=time();
		$weekago=date("Y-m-d", strtotime("-1 week"));
		$weekago = strtotime($weekago);

		$projects	= 	Project::all();
		if(Project::count() > 0){
				foreach($projects as $p){
					
				
					$updated_at_after_7=strtotime($p->updated_at);
					
					$updated_at_after_7 = strtotime("+10 day", $updated_at_after_7);
					
					
					$current_timestamp=time();
					
					if($current_timestamp > $updated_at_after_7){
						Project::where('id', $p->id)->update(array(
							'is_completed'    => 2
						));
					}		
				}
		}

	}

	

}
