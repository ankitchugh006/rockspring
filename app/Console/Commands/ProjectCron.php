<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use PDF;


class ProjectCron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$subject = "Daily Report for ".date('m/d/Y',strtotime("-1 days"));
		
		$superemail=getSuperEmail();
		//$superemail = 'ankit@yopmail.com';
		
		//send email to super
		$ten_days_ago = date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d'))));
		$projects	= 	Project::whereDate('updated_at','>',$ten_days_ago)
						->where('projects.is_completed','=',0)
						//->whereIn('id', array(14861, 14862, 14863,14849,14843))
						->get();
		
		$data = [
				   'projects' => array() 
		];
		
		$is_sent_mail=0; 
		$i=0;
		if(count($projects) > 0){
			foreach($projects as $up){
				$id=$up->id;
				$projectrequests		= 	ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
				$projectlogs	=			WorklogMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$surveys        =			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$tasks      	=			Task::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				$files			=			File::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
				if(count($projectlogs) > 0 || count($surveys) > 0)  {
					$is_sent_mail=1;
					foreach($projectlogs as $log){
						$data['projects'][date('Y-m-d H:i:'.$i,strtotime($log->logged_at))] = $up;	
						$i++;									
					}										
				}
			
			}
		}
		
		
		//print_r($data['projects']);die;
		
		if(!empty($data['projects'])){
			foreach($data['projects'] as $key=>$p){
				$newdata['customer']=$superemail;
				$newdata['submitted_for']=date('Y-m-d',strtotime($key));
				$newdata['project']=$p;
				if($is_sent_mail > 0){
					$pdf = PDF::loadView('emails.superReport2', $newdata);
				
					\Mail::send('emails.superReport2', $newdata, function($message) use ($superemail,$pdf,$subject) 
					{
						$message->to($superemail, 'Super')->subject($subject);
						$message->attachData($pdf->output(), "report.pdf");
					});	
					
				}
			}			
		} 
		$is_sent_mail=0;
		//send email to members
		$j=0;
		//echo  count($projects);die;
		if(count($projects) > 0){
					$data = [
							   'projectsmembers' => array()  
					];
					foreach($projects as $p){	
						
						$is_sent_mail=0;
						$id=$p->id;
						
						$projectlogs	=			WorklogMain::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						$surveys        =			Survey::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
					  
						if(count($projectlogs) > 0 || count($surveys) > 0) {	
							$is_sent_mail=1;
							foreach($projectlogs as $log){
								$hm = date('H',strtotime($log->logged_at));
								if($j > 59){
									$j=0;
									$hm = $hm+1;
								}
								$datm = date('Y-m-d '.$hm.':i:'.$j,strtotime($log->logged_at));
								$data['projectsmembers'][$datm] = $p;	  
							 	 //echo count($data['projectsmembers']).'<br/>';  
								$j++;												
							}								
						}

						/*if(!empty($p->member_customer)){
							$members=json_decode($p->member_customer);					

							if(!empty($members)){
								$datamember['projects'][0] = $p;
								foreach($members as $m){
									
									$memberdata=CustomerMember::where('id','=',$m->member_id)->first();
									
									if(!empty($memberdata->email) && !empty($m->report) && $m->report==1){
										
										
										
										$pdf = PDF::loadView('emails.superReport', $datamember);
										if($is_sent_mail > 0){
											if(!empty($data['projectsmembers'])){
												foreach($data['projectsmembers'] as $key=>$p){
													//$email=$memberdata->email;
													$email='ankit.chugh@42works.net';
													$name=$memberdata->name;
													$datamember['customer']=$name;
													$datamember['email']=$email;
													$datamember['project']=$p;
													$datamember['submitted_for']=date('Y-m-d',strtotime($key));
													$pdf2 = PDF::loadView('emails.superReport2', $datamember);
													\Mail::send('emails.superReport2', $datamember, function($message) use ($email,$pdf2,$subject)
													{
														$message->to($email, 'Super')->subject($subject);
														$message->attachData($pdf2->output(), "report.pdf");
													});		
												}
											}
										}
									}
								}
							}	
						}*/
						
				}




		
		}
		 
		//send email to project members new 
		//echo  count($data['projectsmembers']);die; 

		if(!empty($data['projectsmembers'])){
				foreach($data['projectsmembers'] as $key=>$p){

					$members=json_decode($p->member_customer);

					$datamember['projects'][0] = $p;

					if(!empty($members)){
						foreach($members as $m){
							
							$memberdata=CustomerMember::where('id','=',$m->member_id)->first();
							
							if(!empty($memberdata->email) && !empty($m->report) && $m->report==1){
								$email=$memberdata->email;
								//$email='ankit.chugh@42works.net';
								$name=$memberdata->name;
								$datamember['customer']=$name;
								$datamember['email']=$email;
								$datamember['project']=$p;
								$datamember['submitted_for']=date('Y-m-d',strtotime($key));
								$pdf2 = PDF::loadView('emails.superReport2', $datamember);
								\Mail::send('emails.superReport2', $datamember, function($message) use ($email,$pdf2,$subject)
								{
									$message->to($email, 'Super')->subject($subject);
									$message->attachData($pdf2->output(), "report.pdf");
								});		
							}
						}	


					}
					
				}
		}

		//Users to projects
		$k=0;
		$is_sent_mail=0;
		$users = User::get();

		if(count($users) > 0){ 
			foreach($users as $u){
				$pdata['projects'] = array();
				
				$userProjects = ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')
                            ->where('foreman_id',$u->id)
                            ->where('type','!=','foreman')
                            ->whereDate('projects.updated_at','>',$ten_days_ago)
                            ->where('projects.is_completed','=',0)
                            //->whereIn('projects.id', array(14861, 14862, 14863,14849,14843))
                            ->distinct('project_id')
							->select('projects.*')->get();
				
			 	
				
				
				$email=$u->email;
				//$email='ankit.chugh@42works.net';
				$is_sent_mail=0;
				
				if(count($userProjects) > 0){
					foreach($userProjects as $up){
						$id=$up->id;
						$projectrequests		= 	ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						$projectlogs	=			WorklogMain::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						$surveys        =			Survey::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						$tasks      	=			Task::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						$files			=		File::where('project_id','=',$id)->orderBy('id', 'DESC')->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
						if(count($projectlogs) > 0 || count($surveys) > 0) {	
							$is_sent_mail=1;
							foreach($projectlogs as $log){
								$h = date('H',strtotime($log->logged_at));
								if($k > 59){
									$k=0;
									$h = $h+1;
								}
								$dat = date('Y-m-d '.$h.':i:'.$k,strtotime($log->logged_at));
								$pdata['projects'][$dat] = $up;	
								$k++;																			
							}	
						}
					
					}						
				}
			
				
				
				
				if(!empty($pdata['projects'])){
					foreach($pdata['projects'] as $key=>$p){ 
						$email=$u->email;
						//$email='raman@42works.net'; 
						$name=$u->name;
						$pdata['customer']=$u->name;
						$pdata['email']=$email;
						$pdata['project']=$p;
						$pdata['submitted_for']=date('Y-m-d',strtotime($key));
						$pdf3 = PDF::loadView('emails.superReport2', $pdata);
						\Mail::send('emails.superReport2', $pdata, function($message) use ($email,$pdf3,$subject)
						{
							$message->to($email, 'Super')->subject($subject);
							$message->attachData($pdf3->output(), "report.pdf");
						}); 		
					}
				}
				
			}
		}
		

	}
}
