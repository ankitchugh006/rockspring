<?php
use App\Models\User;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Subcontractor;
use App\Models\Customer;
use App\Models\CustomerMember;
use App\Models\FileCategories;
use App\Models\File;
use App\Models\Surveys;
use App\Models\Survey;
use App\Models\ProjectSurvey;
use App\Models\ProjectSubcontractors;
use App\Models\WorklogMain;
use App\Models\Worklog;
use App\Models\Manpowerrecord;
use App\Models\SubcontractorRequest;
use App\Models\SubcontractorWorklog;

function project_has_resource($assignedresources=array(),$manpower_id=null){
	$is='';
	//echo $manpower_id;
	if(!empty($assignedresources)){
		foreach($assignedresources as $as){
			$mid=$as['manpower_id'];
			if($manpower_id==$mid){
				$is=$as['max_hours'];
			}
		}
	}
	return $is;
}	

function project_has_resource_get_resource_id($assignedresources=array(),$manpower_id=null){
	$is='';
	//echo $manpower_id;
	if(!empty($assignedresources)){
		foreach($assignedresources as $as){
			$mid=$as['manpower_id'];
			if($manpower_id==$mid){
				$is=$as['id'];
			}
		}
	}
	return $is;
}

function project_has_foreman($assignedforeman=array(),$foreman_id=null){
	$is=0;
	//echo $manpower_id;
	if(!empty($assignedforeman)){
		foreach($assignedforeman as $as){
			$mid=$as['foreman_id'];
			if($foreman_id==$mid){
				$is=1;
			}
		}
	}
	return $is;
}


function check_to_select($m){
	$storedresources=\Session::get('resources');
	$is=0;
	//echo $manpower_id;
	if(!empty($storedresources)){
		foreach($storedresources as $s){
			if($m==$s){
				$is=1;
			}
		}
	}
	return $is;
}
function check_to_set_value($m){
	$storedhours=\Session::get('hours');
	
	$is=0;
	//echo $manpower_id;
	if(!empty($storedhours)){
		foreach($storedhours as $key=>$s){
			if($m==$key){
				if(isset($s) && !empty($s)){
					$is=$s;
				}
			}
		}
	}
	
	return $is;
}
function check_to_set_foreman($uid){
	$is=0;
	$foremans=\Session::get('foremans');
	
	if(!empty($foremans)){
		foreach($foremans as $key=>$s){
			
				if($s==$uid){
					$is=1;
				}
			
		}
	}
	
	return $is;
	
}


function check_to_set_subcontractor($uid){

    $is=0;
	$foremans=\Session::get('subcontractors');
	
   
    
	if(!empty($foremans)){
		foreach($foremans as $key=>$s){
			
				if($s==$uid){
					$is=1;
				}
			
		}
	}
	
	return $is;
}

function project_has_subcontractor($project_id=0,$sub_id=0){
    $count=ProjectSubcontractors::where('project_id','=',$project_id)->where('subcontractor_id','=',$sub_id)->count();
    return $count;
}

function get_name($id=null){
	$n=User::Where('id',$id)->first();
	if(isset($n->name)){
		return $n->name;
	}elseif(isset($n->username)){
		return $n->username;
	}else{
		return;
	}
		
}



function get_number($id=null){
	$n=User::Where('id',$id)->first();
	if(isset($n->user_phone))
		return $n->user_phone;
	else
		return "N/A";
}

function get_push_id($id=null){
	$n=User::Where('id',$id)->first();
	if(isset($n->push_id))
		return $n->push_id;
	else
		return;
}
function get_project_title($id=null){
	$n=Project::Where('id',$id)->first();
	return $n->title;
}

function getQuestionTitle($id=null){
    
    $n=Surveys::Where('id',$id)->first();
	return $n->survey;
}

function getSurveyDate($id = 0){
	$s = ProjectSurvey::where('id',$id)->first();
	if(!empty($s)){
		return change_date_format($s->survey_date);
	}else{
		return '';
	}
}

function get_resource_title($id=null){
	$n=ProjectResources::Where('id',$id)->first();
	$title=Manpower::Where('id',$n->manpower_id)->first();
	if(!empty($title)){
		return !empty($title->title)?$title->title:"";
	}else{
		return "null";
	}
}

function get_resource_title_report($id=null){
	$n=ProjectResources::Where('id',$id)->first();
	$title=Manpower::Where('id',$n->manpower_id)->first();
	if(!empty($title)){
		return !empty($title->report_title)?$title->report_title:$title->title;
	}else{
		return 'null';
	}
}

function get_resource_code($id=null){
	$n=ProjectResources::Where('id',$id)->first();
	$code=Manpower::Where('id',$n->manpower_id)->first();
	return !empty($code->number)?$code->number:"";
}


function get_project_resource_id_from_manpower_id($project_id=0,$manpower_id=0){
	$n=ProjectResources::Where('manpower_id',$manpower_id)
					->Where('project_id',$project_id)
					->first();
	if(!empty($n->id))
		return $n->id;
	else
		return 10000000000000;
}

function get_cost_code($id=null){
	$n=ProjectResources::Where('id',$id)->first();
	$title=Manpower::Where('id',$n->manpower_id)->first();
	return $title->number;
}

function get_resource_title_direct($id=null){ 
	$title=Manpower::Where('id',$id)->first();
	if(!empty($title)){
		return $title->title;
	}else{
		return 'null';
	}
}

function get_subcontractor_title($id=null){
    
    $n=Subcontractor::Where('id',$id)->first();
    if(!empty($n->subcontractor) && $n->subcontractor != null)
		return $n->subcontractor;
	else
		return 'N/A';
}
function get_admin_id(){
	$adminid=User::Where('role','role_superadmin')->first();
	return $adminid->id;
}
function is_any_new_notification($user_id=null){
	$n=Notification::Where('highlight',0)->where('notification_to',$user_id)->get();
	return $n->count();
}


function is_any_new_notification_file($user_id=null,$project_id=null){
	$n=Notification::Where('highlight',0)
					->where('notification_to',$user_id)
					->where('project_id',$project_id)
					->where('notification_type','file_added')
					->get();
	return $n->count();
}
function is_any_new_notification_task($user_id=null,$project_id=null){
	$n=Notification::Where('highlight',0)
					->where('notification_to',$user_id)
					->where('project_id',$project_id)
					->where('notification_type','task_added')
					->get();
	return $n->count();
}

function get_notifications($user_id=null){
	$n=Notification::where('notification_to',$user_id)->orderBy('id', 'DESC')->take(5)->get();
	return $n->toArray();
}

function get_notifications_app($user_id=null,$project_id=null){
	$n=Notification::where('notification_to',$user_id)->where('project_id',$project_id)->orderBy('id', 'DESC')->take(5)->get();
	return $n->toArray();
}
function get_notification_text($type=null,$from=null,$id=null){
	$html='';
	
	if($type=='request'){
		$html .= '<a href="'.url().'/admin/projects/requests/'.$id.'">Foreman <b>'.get_name($from). '</b> Requested for new resources <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}elseif($type=='logging'){
		$html .= '<a href="'.url().'/admin/projects/logs/'.$id.'">Foreman <b>'.get_name($from). '</b> Submit Worklog <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}elseif($type=='survey'){
		$html .= '<a href="'.url().'/admin/projects/survey">Foreman <b>'.get_name($from). '</b> Submit a survey <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}
	return $html;
}

function is_my_project($project_id=0,$foreman_id=0){
		$is_my_project = ProjectForeman::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->count();
		return $is_my_project;
}


function is_resource_requested($project_id=0,$foreman_id=0){
	
	$status=false;
	$currentdate=date('Y-m-d');
	$is_resource_requested=ForemanRequestMain::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->whereDate('created_at','=',$currentdate)
											->count();
	if($is_resource_requested>0){
		$status=true;
	}
	return $status;
}

function is_request_approved(){
	$status=false;
	$currentdate=date('Y-m-d');
	$is_resource_requested=ForemanRequestMain::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->whereDate('created_at','=',$currentdate)
											->count();
	if($is_resource_requested>0){
		$status=true;
	}
	return $status;
}

function is_subcontractor_assigned($project_id=0,$foreman_id=0,$resource_id=0){
	$status=false;
	
	$date=date('Y-m-d');
	$is_subcontractor_assigned=SubcontractorRequest::leftJoin('manpower_requests', function($join) {
									$join->on('manpower_requests.id', '=', 'manpower_requests_subcontractors.request_id');
								})
							->where('manpower_requests_subcontractors.project_id', '=', $project_id)
											->where('manpower_requests_subcontractors.foreman_id','=',$foreman_id)
											->where('manpower_requests_subcontractors.resource_id','=',$resource_id)
											->whereDate('manpower_requests_subcontractors.processing_date','=',$date)
											->count();
	
	if($is_subcontractor_assigned>0){
		$status=true;
	}
	return $status;										
}

function get_all_processing_dates($project_id=0,$foreman_id=0){
	
	$dates=array();
	$currentdate=date('Y-m-d');
	$is_resource_requested=SubcontractorRequest::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											//->distinct('project_id')
											->get()
											;
	if($is_resource_requested->count() > 0){
		foreach($is_resource_requested as $d){
			
			$date = date_create($d->processing_date);
			$dates[] = date_format($date, 'Y-n-j');
		}
	}
	
	return $dates;
}

function is_resource_requested_for_date($project_id=0,$foreman_id=0,$date=null){
	
	$status=false;
	$currentdate=date('Y-m-d');
	
	$is_resource_requested=ForemanRequest::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->whereDate('processing_date','=',$date)
											->count();
	/*if($is_resource_requested>0){
		$status=true;
	}*/
	return $is_resource_requested;
}

function is_worklog_submitted($project_id=0,$foreman_id=0){
	$status=false;
	$currentdate=date('Y-m-d');
	$is_worklog_submitted=WorklogMain::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->whereDate('created_at','=',$currentdate)
											->count();
	if($is_worklog_submitted>0){
		$status=true;
	}
	return $status;
}

function is_survey_submitted($project_id=0,$foreman_id=0){
	$status=false;
	$currentdate=date('Y-m-d');
	$is_survey_submitted=Survey::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->whereDate('created_at','=',$currentdate)
											->count();
	if($is_survey_submitted>0){
		$status=true;
	}
	return $status;
}

function display_breedcrump(){ ?>
<ul class="breadcrumb"> 
		<i class="fa fa-home"></i> 
	  <?php $link = route('home');
		for($i = 1; $i <= count(Request::segments()); $i++) {
			echo '<li>'; 
				if($i < count(Request::segments()) & $i > 0) {
					$link .= "/" . Request::segment($i); ?> 
					<a href="<?= $link ?>"><?php  if(Request::segment($i)=='admin') { echo 'Home'; }else { echo Request::segment($i);} ?></a>
				<?php
				 } else { echo Request::segment($i); } 
			echo '</li>';
		} ?>
</ul>
<?php }

function change_date_format($date=''){
	$date = date_create($date);
	//return  date_format($date, 'F d, Y');
	return  date_format($date, 'M jS Y');
}
function change_date_format1($date=''){
	$date = date_create($date);
	//return  date_format($date, 'F d, Y');
	return  date_format($date, 'm/d/Y');
}
function getProjectTeam($projectid=0){
    $n=ProjectForeman::Where('project_id',$projectid)->orderby('id','ASC')->get();
	return $n;
}


function getProjectCustomerTeam($projectid=0){
	$n=Project::Where('id',$projectid)->first();
	
	$arr='';
	
	if(!empty($n->member_customer)){
		$arr=json_decode($n->member_customer);
		/*if(!empty($arr)){
			foreach($arr as $a){
				$data=CustomerMember::Where('id',$a->member_id)->first();
				if(!empty($data->name)){
					$team .= $data->name.', ';
				}
			}
		}*/
		return $arr;
		//return rtrim($team, ', ');
	}else{
		return '';
	}
}

function getProjectCustomerMembers($projectid=0){
	$n=Project::Where('id',$projectid)->first();
	
	
	if(!empty($n->member_customer)){
		return $arr=json_decode($n->member_customer);
		
	}else{
		return '';
	}
}

function getAllCustomerMembers($customer=0){
	$n=CustomerMember::Where('customer_id',$customer)->get();
	return $n;
}
function getAllCustomerMembersWc(){
	$n=CustomerMember::get();
	return $n;
}

function getMemberTitle($mid=0){
	$n=CustomerMember::Where('id',$mid)->first();
	
	if(!empty($n->role)){
		return $n->role;
	}else{
		return 'N/A';
	}
	
	
}
function getMemberName($mid=0){
	$n=CustomerMember::Where('id',$mid)->first();
	
	if(!empty($n->name)){
		return $n->name;
	}else{
		return 'N/A';
	}
	
	
}
function getMemberEmail($mid=0){
	$n=CustomerMember::Where('id',$mid)->first();
	if(!empty($n->email)){
		return $n->email;
	}else{
		return 'N/A';
	}
}
function getMemberPhone($mid=0){
	$n=CustomerMember::Where('id',$mid)->first();
	if(!empty($n->phone)){
		return $n->phone;
	}else{
		return 'N/A';
	}
}

function getSubcontractorName($id=0){
	$n=Subcontractor::Where('id',$id)->first();
	return $n->subcontractor;
	
}

function getCustomers(){
    $n=Customer::orderBy('customer', 'ASC')->get();
	return $n;
} 

function getFileCategories(){
    $n=FileCategories::get();
	return $n;
}

function getCatFileCount($cat_id=0){
    $n=File::whereRaw('FIND_IN_SET('.$cat_id.',category_ids)')->count();
	return $n;
}

function getFileCategoryTitle($id=0){
    $n=FileCategories::Where('id',$id)->first();
    if(!empty($n->category))
	   return $n->category;
    else
        return '';
} 

function getProjects(){
    $n=Project::get();
	return $n;
} 

function getProjectsHaveNoQuestions(){	
	$projects=array();
	$n=Project::get();
	if(!empty($n)){
		foreach($n as $p){
			$count=ProjectSurvey::where('project_id','=',$p->id)->count();
			if($count==0){
				$projects[]=$p;
			}
		}
	}
	return $projects;
	
}

function getQuestions(){
    $n=Surveys::get();
	return $n;
} 

function getCustomerName($id=0){
    $n=Customer::Where('id',$id)->first();
    if(!empty($n->customer)){
		return $n->customer;
	}else{
		return 'N/A';
	}
	
    
}
function getCustomerEmail($id=0){
    $n=Customer::Where('id',$id)->first();
	return $n->email;
    
}

function getProjectForemans($project_id=0){
    $n=ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
                            ->Where('assigned_projects_to_foreman.project_id',$project_id)
                            ->Where('assigned_projects_to_foreman.type','foreman')
							->select('assigned_projects_to_foreman.*')->first();
							
    if(!empty($n->foreman_id))
	   return get_name($n->foreman_id);
    else
        return 'N/A';
}

function getProjectSuperintendents($project_id=0){
    $n=ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
                            ->Where('assigned_projects_to_foreman.project_id',$project_id)
                            ->Where('assigned_projects_to_foreman.type','super')
							->select('assigned_projects_to_foreman.*')->first();
							
    if(!empty($n->foreman_id))
	   return get_name($n->foreman_id);
    else
        return 'N/A';
}

function getProjectEstimators($project_id=0){
	$n=ProjectForeman::join('users','users.id','=','assigned_projects_to_foreman.foreman_id')
                            ->Where('assigned_projects_to_foreman.project_id',$project_id)
                            ->Where('assigned_projects_to_foreman.type','estimator')
							->select('assigned_projects_to_foreman.*')->first();
							
    if(!empty($n->foreman_id))
	   return get_name($n->foreman_id);
    else
        return 'N/A';
}

function getSubcontractors(){
    $n=Subcontractor::get();
	return $n;
}

function getForemans(){
    
    $n=User::where('role','=','role_foreman')->get();
	return $n;
}

function getAdminSperUsers(){
    $n=User::where('role','!=','role_foreman')->where('role','!=','role_subcontractor')->orderBY('name','ASC')->get();
	return $n;
}

function getAdminForemanUsers(){
	
	$n=User::where('role','!=','role_superadmin')->orderBY('name','ASC')->get();
	return $n;
}

function getAllForemans(){
    $n=User::where('role','=','role_foreman')->orderBY('name','ASC')->get();
	return $n;
}

function getAllProjectManagers(){
	 $n=User::where('job_role','=','job_manager')->orderBY('name','ASC')->get();
	 return $n;
}
function getAllAssistantManagers(){
	 $n=User::where('job_role','=','job_assistant')->orderBY('name','ASC')->get();
	 return $n;
}


function getAllSuperintendents(){
	 $n=User::where('job_role','=','job_superintendent')->orderBY('name','ASC')->get();
	 return $n;
}
function getAllEstimators(){
	 $n=User::where('job_role','=','job_estimator')->orderBY('name','ASC')->get();
	 return $n;
}

function getUserRole($id=0){
	 $n=User::where('id','=',$id)->first();
	 return $n->role;
}

function getJobRole($id=0){
	 $n=User::where('id','=',$id)->first();
	 return $n->job_role;
}

function get_question_title($id=0){
		 $n=Surveys::where('id','=',$id)->first();
		 return $n->survey;
}

function getUserPojectIds($id=0){
	$user_role= getUserRole($id);
	$ids=array();
	
	if($user_role=='role_superadmin'){
		$projects=Project::get();
		
		if(count($projects)){
			foreach($projects as $p){
				$ids[]=$p->id;
			}
		}	
			
	
	}else{
		$projects=ProjectForeman::where('foreman_id', Auth::user()->id)->distinct('project_id')
					->select('project_id')->get();
		
		if(count($projects)){
			foreach($projects as $p){
				$ids[]=$p->project_id;
			}
		}		
	}
	
	return $ids;			
				
}

function getUserPojectIdsCron($id=0){ 
	$ids=array();
	$projects=ProjectForeman::join('projects','projects.id','=','assigned_projects_to_foreman.project_id')->where('foreman_id', $id)->distinct('project_id')
				->where('projects.is_completed','=',0)  
				->select('assigned_projects_to_foreman.project_id')->get();
	
	if(count($projects)){
		foreach($projects as $p){
			$ids[]=$p->project_id;
		}
	}			
	
	
	
	return $ids;			
				
}

function getPojectUserIds($id=0){
	$ids=array();
	$projects=ProjectForeman::where('project_id', $id)->select('foreman_id')->get();
	
	if(count($projects)){
		foreach($projects as $p){
			$ids[]=$p->foreman_id;
		}
	}			

	return $ids;			
				
}
function getSuperEmail(){
		 $n=User::where('role','=','role_superadmin')->first();
		 return $n->email;
}

function getUserEmail($id=0){
		 $n=User::where('id','=',$id)->first();
		 return $n->email;
}


function updateProjectDate($id=0){
	$date=new \DateTime();
	Project::where('id', $id)->update(array(
		'updated_at'    => $date,
		'is_completed'    => 0
	));
}

function getManualId($project_id=0){
	 $n=Project::where('id','=',$id)->first();
	 return $n->manual_id;
}

function getAllSurveyQuestions(){ 
	$n=Surveys::get();
	return $n;
}

function get_hours_logged_report($project_id,$manpower_id,$date_from = '',$date_to = ''){
	
	//echo $date_from.'---'.$date_to.'--'.$subcontractor;
	$n=ProjectResources::Where('manpower_id',$manpower_id)->Where('project_id',$project_id)->first();
	
		$worklog='';
		if(!empty($n->id)){
			if(!empty($subcontractor)){
				//echo $n->id;
				if(!empty($date_from) && empty($date_to)){
					$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
					$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					//->where('subcontractor_id', '=', $subcontractor)
					->get();
				}elseif(!empty($date_to) && empty($date_from)) {
					
						$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
					->whereDate('created_at', '<=', $date_to)
					//->where('subcontractor_id', '=', $subcontractor)
					->get();
				}elseif(!empty($date_to) && !empty($date_from)){
					
						$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
							->whereDate('created_at', '>=', $date_from)
							->whereDate('created_at', '<=', $date_to)
							//->where('subcontractor_id', '=', $subcontractor)
							->get();
				}else{
					$date_from = date("Y-m-d", strtotime("- 1 day"));
					$date_to = date("Y-m-d", strtotime("- 2 day"));
					$worklog=Worklog::where('project_id','=',$project_id)
					->Where('resource_id',$n->id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					//->where('subcontractor_id', '=', $subcontractor)
					->get();
				}
			}else{
				if(!empty($date_from) && empty($date_to)){
					$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
					$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					
					->get();
				}elseif(!empty($date_to) && empty($date_from)) {
					
						$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
					->whereDate('created_at', '<=', $date_to)
					
					->get();
				}elseif(!empty($date_to) && !empty($date_from)){
					
						$worklog=Worklog::where('project_id','=',$project_id)->Where('resource_id',$n->id)
							->whereDate('created_at', '>=', $date_from)
							->whereDate('created_at', '<=', $date_to)
							
							->get();
				}else{
					$date_from = date("Y-m-d", strtotime("- 1 day"));
					$date_to = date("Y-m-d", strtotime("- 2 day"));
					$worklog=Worklog::where('project_id','=',$project_id)
					->Where('resource_id',$n->id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					
					->get();
				}
			}
		}
	
	
	Session::forget('date_from');
	Session::forget('date_to');
	//Session::forget('subcontractor');
	
	return $worklog;
}


function get_row_span_report($project_id=0,$resource_id=0,$foreman_id=0,$date_from='',$date_to='',$subcontractor=''){
	if(!empty($subcontractor)){
		if(!empty($date_from) && empty($date_to)){
				$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '>=', $date_from)
				->whereDate('created_at', '<=', $date_to)
				->where('subcontractor_id', '=', $subcontractor)
				->count();
			}elseif(!empty($date_to) && empty($date_from)) {
				
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '<=', $date_to)
				->where('subcontractor_id', '=', $subcontractor)
				->count();
			}elseif(!empty($date_to) && !empty($date_from)){
				
					$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
						->Where('foreman_id',$foreman_id)
						->whereDate('created_at', '>=', $date_from)
						->whereDate('created_at', '<=', $date_to)
						->where('subcontractor_id', '=', $subcontractor)
						->count();
			}else{
				$date_from = date("Y-m-d", strtotime("- 1 day"));
				$date_to = date("Y-m-d", strtotime("- 2 day"));
				
				$n=SubcontractorWorklog::Where('resource_id',$resource_id)->Where('project_id',$project_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '>=', $date_from)
				->whereDate('created_at', '<=', $date_to)
				->where('subcontractor_id', '=', $subcontractor)
				->count();
			}
	}else{
		if(!empty($date_from) && empty($date_to)){
				$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '>=', $date_from)
				->whereDate('created_at', '<=', $date_to)
				
				->count();
			}elseif(!empty($date_to) && empty($date_from)) {
				
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '<=', $date_to)
				
				->count();
			}elseif(!empty($date_to) && !empty($date_from)){
				
					$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
						->Where('foreman_id',$foreman_id)
						->whereDate('created_at', '>=', $date_from)
						->whereDate('created_at', '<=', $date_to)
						
						->count();
			}else{
				$date_from = date("Y-m-d", strtotime("- 1 day"));
				$date_to = date("Y-m-d", strtotime("- 2 day"));
				
				$n=SubcontractorWorklog::Where('resource_id',$resource_id)->Where('project_id',$project_id)
				->Where('foreman_id',$foreman_id)
				->whereDate('created_at', '>=', $date_from)
				->whereDate('created_at', '<=', $date_to)
				
				->count();
			}
	}
	
	return $n;
}

function get_foreman_log_sum_report($project_id=0,$resource_id=0,$foreman_id=0,$date_from='',$date_to='',$subcontractor=''){
	if(!empty($subcontractor)){	
		if(!empty($date_from) && empty($date_to)){
			$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
			$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
			->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '>=', $date_from)
			->whereDate('created_at', '<=', $date_to)
			->where('subcontractor_id', '=', $subcontractor)
			->sum('hours_logged');
		}elseif(!empty($date_to) && empty($date_from)) {
			
			$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
			->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '<=', $date_to)
			->where('subcontractor_id', '=', $subcontractor)
			->sum('hours_logged');
		}elseif(!empty($date_to) && !empty($date_from)){
			
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
					->Where('foreman_id',$foreman_id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					->where('subcontractor_id', '=', $subcontractor)
					->sum('hours_logged');
		}else{
			$date_from = date("Y-m-d", strtotime("- 1 day"));
			$date_to = date("Y-m-d", strtotime("- 2 day"));
			$n=SubcontractorWorklog::Where('resource_id',$resource_id)
			->Where('project_id',$project_id)->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '>=', $date_from)
			->whereDate('created_at', '<=', $date_to)
			->where('subcontractor_id', '=', $subcontractor)
			->sum('hours_logged');
		}
	
	}else{
		if(!empty($date_from) && empty($date_to)){
			$date_to=date('Y-m-d', strtotime($date_from . ' +1 day'));
			$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
			->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '>=', $date_from)
			->whereDate('created_at', '<=', $date_to)
			
			->sum('hours_logged');
		}elseif(!empty($date_to) && empty($date_from)) {
			
			$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
			->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '<=', $date_to)
			
			->sum('hours_logged');
		}elseif(!empty($date_to) && !empty($date_from)){
			
				$n=SubcontractorWorklog::where('project_id','=',$project_id)->Where('resource_id',$resource_id)
					->Where('foreman_id',$foreman_id)
					->whereDate('created_at', '>=', $date_from)
					->whereDate('created_at', '<=', $date_to)
					
					->sum('hours_logged');
		}else{
			$date_from = date("Y-m-d", strtotime("- 1 day"));
			$date_to = date("Y-m-d", strtotime("- 2 day"));
			$n=SubcontractorWorklog::Where('resource_id',$resource_id)
			->Where('project_id',$project_id)->Where('foreman_id',$foreman_id)
			->whereDate('created_at', '>=', $date_from)
			->whereDate('created_at', '<=', $date_to)
			
			->sum('hours_logged');
		}
	}
	return $n;
}

function get_est_hours_report($project_id,$manpower_id){
	//echo $project_id.'--'.$manpower_id;die;
	$n=ProjectResources::Where('manpower_id',$manpower_id)->Where('project_id',$project_id)->first();
	if(count($n)){
		return $n->max_hours;
	}else{
		return '';
	}

}
function get_resource_id_report($project_id,$manpower_id){
	$n=ProjectResources::Where('manpower_id',$manpower_id)->Where('project_id',$project_id)->first();
	if(count($n)){
		return $n->id;
	}else{
		return '';
	}

}
function get_used_hours_report($project_id,$resource_id){
	$n=Worklog::Where('resource_id',$resource_id)->Where('project_id',$project_id)->sum('hours_logged');
	return $n;
}
function get_used_hours_report1($project_id,$resource_id){
	$total=0;
	$n=Worklog::Where('resource_id',$resource_id)->Where('project_id',$project_id)->get();
	
	if(count($n)>0){
		foreach($n as $a){
			$total += $a->hours_logged;
		}
	}
	return $total;
}
function get_used_hours_estimate($project_id,$resource_id){
	$total=0;
	$n=Worklog::Where('resource_id',$resource_id)->Where('project_id',$project_id)->get();
	
	if(count($n)>0){
		foreach($n as $a){
			$total += $a->hours_logged;
		}
	}
	return $total;
}

function get_manpower_report(){
	$n=Manpower::get();
	return $n;
}

function getWorklogImage($id=0){
	$n=WorklogMain::Where('id',$id)->first();
	return $n->photo;
	
}
 
function getWorklogImageReport($id=0){
	
	$worklog_id=Worklog::Where('id',$id)->first();	
	$n=WorklogMain::Where('id',$worklog_id->worklog_id)->first();	
	return $n->photo;
	
}
 
function getAssignRequestSub($p_id=0,$f_id=0,$req_id=0,$processing_date=''){
	$n=SubcontractorRequest::where('project_id',$p_id)
		->where('foreman_id',$f_id)
		->where('request_id',$req_id)
		->whereDate('processing_date', '=', $processing_date)
		->get();
	return $n;	
} 

function getAssignRequestSubDisplay($p_id=0,$f_id=0,$req_id=0){
	$n=SubcontractorRequest::where('project_id',$p_id)
		->where('foreman_id',$f_id)
		->where('request_id',$req_id)
		->groupBy('manpower_requests_subcontractors.subcontractor_id')
		->get();
	return $n;	
}

function getAssignRequestSubProject($p_id=0){
	$n=SubcontractorRequest::where('project_id',$p_id)->get();
	return $n;
	
} 
function getWorkLodDetail($nested_worklog_id=0){
	$n=SubcontractorWorklog::where('nested_worklog_id',$nested_worklog_id)->get();
	return $n->toArray();
	
	
}

function getSurveyMainImages($sid=0){
	$n=Survey::where('survey_id',$sid)->first();
	if(!empty($n->main_images))
		return $n->main_images;
	else
		return '';
}

function is_pm_project($allpmprojects,$pid=null){
	
	
	if(!empty($allpmprojects)){
		foreach($allpmprojects as $apm){
			if($apm->id==$pid->id){
				return  $pid;
			}
		}
	}
	
}

function getresourceContractors($project_id=0,$resource_id=0){
	$n=ProjectResources::where('project_id',$project_id)
						->where('id',$resource_id)	
						->first();
	if(!empty($n->subcontractors)){
		return explode(',',$n->subcontractors);
	}else{
		return array();	
	}					
		
	
}

function isManpowerClickedToday($pid=0){
	
	$return = 0;
	$user_id=Auth::user()->id;
	$manpowerrecord=Manpowerrecord::where('super_id', '=', $user_id)->first();
	
	$request=SubcontractorRequest::where('project_id',$pid)->get();
	
	if(count($request)){
		foreach($request as $r){
			
			if(!empty($manpowerrecord->created_at)){
				
				if(strtotime($r->created_at->format('Y-m-d h:i:s')) > strtotime($manpowerrecord->created_at->format('Y-m-d h:i:s')))
					$return = 1;
			}
		}
	}
	return $return;
	
}
function getContractorQuantity($subcontractor_id=0,$project_id=0,$resource_id=0,$foreman_id=0,$processing_date=''){

	$request=SubcontractorRequest::where('project_id',$project_id)
									->where('foreman_id',$foreman_id)
									->where('subcontractor_id',$subcontractor_id)
									->where('resource_id',$resource_id)
									->whereDate('processing_date', '=', $processing_date)
									->first();
	
	if(isset($request->quantity) && !empty($request->quantity)){
		return $request->quantity;
	}else{
		return '';
	}
}

function getReportDescriptionForResource($nested_worklog_id = 0){
	$desc = Worklog::where('id',$nested_worklog_id)->first();
	if(isset($desc->description) && !empty($desc->description)){
		return $desc->description;
	}else{
		return '';
	}
}

function is_latest_worklog_available($project_id=0){
		
		$projectlogs	=			WorklogMain::where('project_id','=',$project_id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
		$surveys        =			Survey::where('project_id','=',$project_id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
		if(count($projectlogs) > 0 || count($surveys) > 0)  {
			return 1;
		}else{
			return 0;
		}
}

function slugify($text){ 
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // lowercase
	  $text = strtolower($text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if (empty($text))
	  {
		return 'n-a';
	  }

	  return $text;
	}

function send_push_notification($registatoin_ids, $message) {    
        
         $url = 'https://android.googleapis.com/gcm/send';    
         $fields = array(    
         'registration_ids' => $registatoin_ids,    
         'data' => $message,    
         );    
         
         $headers = array(    
         'Authorization: key=AIzaSyAvZdsAXWNwLb3fjlU1xhmv1-Fx-FzULzg',    
         'Content-Type: application/json'    
         ); 
         /*$headers = array(    
         'Authorization: key=AIzaSyAvZdsAXWNwLb3fjlU1xhmv1-Fx-FzULzg',    
         'Content-Type: application/json'    
         );
         */     
         // Open connection    
         $ch = curl_init();    
         curl_setopt($ch, CURLOPT_URL, $url);    
         curl_setopt($ch, CURLOPT_POST, true);    
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);    
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   
         curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));    
         $result = curl_exec($ch);    
         if ($result === FALSE) {    
                 //die('Curl failed: ' . curl_error($ch));    
         }  
         echo '</pre>';  
        print_r($result);
         echo '<br/>';  
        print_r($registatoin_ids);
        curl_close($ch);    
        //echo $result;    
}     
function UR_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}
function send_push_notification_ios($token, $iosmessage='') {    
        
			
	// Put your device token here (without spaces):
	$deviceToken = $token;

	// Put your private key's passphrase here:
	$passphrase = '42works';


	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/formanfeed_old.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client(
		'ssl://gateway.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);

	echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = $iosmessage;

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));



	if (!$result)
		echo 'Message not delivered' . PHP_EOL;
	else
		echo 'Message successfully delivered' . PHP_EOL;


	// Close the connection to the server
	fclose($fp);
}     
