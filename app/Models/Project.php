<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'id',
        'manual_id',
        'title',
        'description',
        'city',
        'state',
        'zip',
        'customer',
        'customer_id',
        'address',
        'scope',
        'hps',
        'shift_type',
        'start_date',
        'added_by',
        'status',
        'is_completed',
        'member_customer',
        'survey_questions',
        'created_at'
    );
    
    protected $table = 'projects';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');	
	
	
}
