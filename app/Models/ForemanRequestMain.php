<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForemanRequestMain extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'project_id',
        'foreman_id',
        'subcontractor_id',
        'comment1',
        'comment2',
        'comment3'  
    );
    
    protected $table = 'manpower_requests_main';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');
	
	
}
