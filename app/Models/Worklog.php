<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worklog extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    
    protected $fillable = array(
        'worklog_id',
        'project_id',
        'resource_id',
        'subcontractor_id',
        'foreman_id',
        'quantity',
        'hours_logged',
        'logged_at',
        'description'
    );
    
    protected $table = 'worklogs';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('temp_pass', 'updated_at');

	
}
