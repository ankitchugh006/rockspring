<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectSurvey extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'project_id',
        'foreman_id',
        'survey_date',
        'question_id'
    );
    
    protected $table = 'project_surveys';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');
	
	
}
