<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubcontractorWorklog extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'nested_worklog_id',
        'project_id',
        'foreman_id',
        'subcontractor_id',
        'resource_id',
        'resource_code',
        'quantity',
        'hours_logged',
    );
    
    protected $table = 'worklogs_subcontractors';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');	
}
