<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerMember extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'customer_id',
        'name',
        'role',
        'phone',
        'office_phone',
        'email'
        
    );
    
    protected $table = 'customer_member';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');	
}
