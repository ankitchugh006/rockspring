<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileCategories extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'category',
        'description'
    );
    
    protected $table = 'file_categories';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');	
}
