<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForemanRequest extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $fillable = array(
        'request_id',
        'project_id',
        'foreman_id',
        'subcontractor_id',
        'resource_id',
        'processing_date',
        'quantity',
        'estimated_hours',
        'taken_hours'
    );
    
    protected $table = 'manpower_requests';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');
	
	
}
