<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manpowerrecord extends \Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    
    protected $fillable = array(
        'super_id'
    );
    
    protected $table = 'manpower_record';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('temp_pass', 'updated_at');

	
}
