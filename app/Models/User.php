<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \Eloquent implements AuthenticatableContract {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    
    protected $fillable = array(
        'name',
        'username',
        'email',
        'password',
        'user_phone',
        'user_photo',
        'user_desc',
        'access_token', 
        'device_token', 
        'device_type',
        'device_os',
        'push_id',
        'role',
        'job_role',
        'last_login',
        'status'

    );
    
    protected $table = 'users';
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'temp_pass', 'updated_at');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->access_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->access_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'access_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
	public function getFullName(){
		return $this->name;
	}
	public function get_user_role(){
		if($this->role=='role_foreman'){
			return 'Foreman';
		}elseif($this->role=='role_subcontractor'){
			return 'Subcontractor';
		}elseif($this->role=='role_administrator'){
			return 'Administrator';
		}
	}
    public function get_job_role(){
      
		if($this->job_role=='job_administrator'){
			return 'Administrator';
		}elseif($this->job_role=='job_manager'){
			return 'Project Manager';
		}elseif($this->job_role=='job_assistant'){
			return 'Assistant Project Manager';
		}elseif($this->job_role=='job_superintendent'){
			return 'Superintendent';
		}elseif($this->job_role=='job_foreman'){
			return 'Foreman';
		}elseif($this->job_role=='job_estimator'){
			return 'Estimator';
		}
	}
}
