<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        @section('title')
        <title>{{{$title}}}</title> 


        <!-- Bootstrap Core CSS -->
        <link href="<?php echo url(); ?>/css/admin/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo url(); ?>/css/admin/sb-admin.css" rel="stylesheet">
        <link href="<?php echo url(); ?>/css/main.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo url(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- choosen CSS -->
        <link href="<?php echo url(); ?>/css/admin/chosen.css" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo url(); ?>/css/admin/jquery.tablesorter.pager.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
         <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/css/admin/sweetalert.css">
        
        <style>
            body{
                background: #fff;
            }
            .navbar-header, .navbar-header a{
                    width: 100%;
                    text-align: center;
            }
            .navbar-brand>img {
                display: block;
                margin: 0 auto;
            }
            .pp-container{ 
                margin-top:38px;
            }
        </style>
    </head>

    <body>

        <div class="container">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo url() ?>/admin/projects">
                        <img src="{{ url('/images/logo-white.png') }}"  height="36" />
                    </a>
                </div>
              
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                   
                    <li class="dropdown">
                    @if (Auth::guest())
                    
                    @else
                  

                    @endif

                    </li>
                </ul>
              
            </nav>

            @yield('content')

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="<?php echo url(); ?>/js/admin/jquery.js"></script>
        <script src="<?php echo url(); ?>/js/admin/functions.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo url(); ?>/js/admin/bootstrap.min.js"></script>
        <script src="<?php echo url(); ?>/js/admin/chosen.jquery.js"></script>
        
        <script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.js"></script>
		<script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.widgets.js"></script>
		
		<!-- Tablesorter: optional -->
		<script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.pager.js"></script>
        <script src="<?php echo url(); ?>/js/admin/sweetalert.min.js"></script>
    
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
       
    </body>

</html>
