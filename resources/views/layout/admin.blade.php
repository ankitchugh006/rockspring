<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        @section('title')
        <title>{{{$title}}}</title>


        <!-- Bootstrap Core CSS -->
        <link href="<?php echo url(); ?>/css/admin/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo url(); ?>/css/admin/sb-admin.css" rel="stylesheet">
        <link href="<?php echo url(); ?>/css/main.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo url(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- choosen CSS -->
        <link href="<?php echo url(); ?>/css/admin/chosen.css" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo url(); ?>/css/admin/jquery.tablesorter.pager.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
         <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/css/admin/sweetalert.css">
        
        <script>
            var base_url = '<?php echo url() ?>';
        </script>
    </head>

    <body class="admin-logged-in">

        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo url() ?>/admin/projects">
                        <img src="{{ url('/images/logo-white.png') }}"  height="36" />
                    </a>
                </div>
                <?php if(Auth::user()->role=='role_administrator' && Auth::user()->job_role=='job_superintendent') { ?>
					
					 <!--div class="navbar-header" style="margin-left:0%">
						 <a style="float: right;font-weight: bold;margin-top: 11px;background: #fff;color: #337ab7;padding: 5px 10px;border-radius: 5px;" href="javascript:;" onClick="finalizemanpowerschedule()">Finalize Manpower Schedule</a>
					 </div>
					 <div class="navbar-header" style="margin-left: 1%;margin-top: 15px;">
						<i id="loading_text" style="color:#fff;font-weight:bold;"></i>
					</div-->
                <?php } ?>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <?php 
                    $is_any = is_any_new_notification(Auth::user()->id);
                    $notifications = get_notifications(Auth::user()->id);
                    if (Auth::user()->role == 'role_superadmin') {
                        ?>

                        <li class="dropdown notification_dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="fa fa-bell"></i><?php
                                /*if ($is_any > 0) {
                                    echo '<span class="highlight">' . $is_any . '</span>';
                                }
                                 */
                                ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu alert-dropdown">
                                <?php
                                if (count($notifications) > 0) {
                                    foreach ($notifications as $nt) {
                                        ?>
                                        <li>
                                            <?php echo get_notification_text($nt['notification_type'], $nt['notification_from'], $nt['project_id']); ?>
                                        </li>
                                        <li class="divider"></li>
                                    <?php } ?>

                                <?php } else { ?>
                                    <li>
                                        <a href="#">No Notification Found</a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                    <li class="dropdown">
                        @if (Auth::guest())
                    <li><a href="<?php echo url(); ?>">Login</a></li>
                    <li><a href="<?php echo url(); ?>/auth/register">Register</a></li>
                    @else
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo url() ?>/admin/myaccount"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo url() ?>/admin/myaccount/resetpassword"><i class="fa fa-fw fa-gear"></i> Reset Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo url(); ?>/admin/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>

                    @endif

                    </li>
                </ul>
                <!-- Sidebar -->
                @include('admin/sidebar')
            </nav>

            @yield('content')

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="<?php echo url(); ?>/js/admin/jquery.js"></script>
        <script src="<?php echo url(); ?>/js/admin/functions.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo url(); ?>/js/admin/bootstrap.min.js"></script>
        <script src="<?php echo url(); ?>/js/admin/chosen.jquery.js"></script>
        
        <script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.js"></script>
		<script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.widgets.js"></script>
		
		<!-- Tablesorter: optional -->
		<script src="<?php echo url(); ?>/js/admin/jquery.tablesorter.pager.js"></script>
        <script src="<?php echo url(); ?>/js/admin/sweetalert.min.js"></script>
    
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
        <script>
            $('.choosen_select').chosen();
            $('.choosen_project').chosen();
        </script>
        <script>
			
		$(function() {

		  // NOTE: $.tablesorter.theme.bootstrap is ALREADY INCLUDED in the jquery.tablesorter.widgets.js
		  // file; it is included here to show how you can modify the default classes
		  $.tablesorter.themes.bootstrap = {
			// these classes are added to the table. To see other table classes available,
			// look here: http://getbootstrap.com/css/#tables
			table        : 'table table-bordered table-striped',
			caption      : 'caption',
			// header class names
			header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
			sortNone     : '',
			sortAsc      : '',
			sortDesc     : '',
			active       : '', // applied when column is sorted
			hover        : '', // custom css required - a defined bootstrap style may not override other classes
			// icon class names
			icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
			iconSortNone : 'glyphicon glyphicon-resize-vertical', // class name added to icon when column is not sorted
			iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
			iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
			filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
			footerRow    : '',
			footerCells  : '',
			even         : '', // even row zebra striping
			odd          : ''  // odd row zebra striping
		  };	
		});
		
			
		$(document).ready(function(){
            $('#custom7').on('change', function(){
			   $('#custom7').val(this.checked ? 1 : 0);
			});
		
			$(".tablesort").tablesorter({
				// this will apply the bootstrap theme if "uitheme" widget is included
				// the widgetOptions.uitheme is no longer required to be set
				theme : "bootstrap",
               
				widthFixed: true,

				headerTemplate : '{content} {icon}',
				// widget code contained in the jquery.tablesorter.widgets.js file
				// use the zebra stripe widget if you plan on hiding any rows (filter widget)
				widgets : [ "uitheme",  "zebra" ],

				widgetOptions : {
				  // using the default zebra striping class name, so it actually isn't included in the theme variable above
				  // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
				  zebra : ["even", "odd"],

				  // reset filters button
				  filter_reset : ".reset",

				  // extra css class name (string or array) added to the filter element (input or select)
				  filter_cssFilter: "form-control",

				  // set the uitheme widget to use the bootstrap theme class names
				  // this is no longer required, if theme is set
				  // ,uitheme : "bootstrap"

				}
			  }) .tablesorterPager({
                 
					container: $(".ts-pager"),

					// target the pager page select dropdown - choose a page
					cssGoto  : ".pagenum",

					// remove rows from the table to speed up the sort of large tables.
					// setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
					removeRows: false,

					// output string - default is '{page}/{totalPages}';
					// possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
					output: '{page}/{totalPages}',
                
                    updateArrows: true,

                  
                    size: 25
                   
                

			  });
			
            
              $( ".autocomplete" ).autocomplete({
                  source: '<?php echo url() ?>/admin/users/autocomplete'
              });
               $( ".customer_autocomplete" ).autocomplete({
                  source: '<?php echo url() ?>/admin/customers/autocomplete'
               }); 
              $( ".file_autocomplete" ).autocomplete({
                  source: '<?php echo url() ?>/admin/files/autocomplete'
              }); 
              $( ".project_autocomplete" ).autocomplete({
                  source: '<?php echo url() ?>/admin/projects/autocomplete'
              }); 
				$( ".task_autocomplete" ).autocomplete({
                  source: '<?php echo url() ?>/admin/projects/tasks/task_autocomplete'
              }); 
              
			  $(document).on("focus", 'input.resourcesR', function () {
					
					$(this).autocomplete({
						source: '<?php echo url() ?>/admin/projects/resourceautocomplete'
					});

				});
				  
            $('#datetimepicker_add_project').datepicker({
				dateFormat: "mm/dd/yy"
			});
			
            $('#datetimepicker1').datepicker({
                dateFormat: "yy-mm-dd"
                });
            $('#datetimepicker2').datepicker({
                dateFormat: "yy-mm-dd"
                });
			$('#datetimepicker2_disabled_future').datepicker({
			dateFormat: "yy-mm-dd",
			maxDate: new Date,
			});
			$('#datetimepicker1_disabled_future').datepicker({
			 dateFormat: "yy-mm-dd",
			 maxDate: new Date,
			  onSelect: function(dateText, inst) {
					var selectedDate = $( this ).datepicker( "getDate" );
					$( "#datetimepicker2_disabled_future" ).datepicker( "option", "minDate", selectedDate );
				}
			});
            /*************add more functionality**************/
            var i=1;
            $("#addMore").click(function(){
				i++;
				var html='<tr id="trNo-'+i+'"><td><div class="form-group"><input class="form-control resourcesR" required  type="text" id="txtTags'+i+'" name="resources['+i+']" value="" placeholder="manpower"></div></td><td><input class="form-control" style="width:50px;" type="text" name="max_hours['+i+']" value=""></td><td><span onClick="remove_tr('+i+')" class="glyphicon glyphicon-remove" style="cursor:pointer"></span></td></tr>';
				$('#projectTable').append(html);
			});
            
		});
        function remove_tr(i){
			$("#trNo-"+i).remove();
		}
		$(document).ready(function(){
		    $(".report_picker").datepicker({
				"dateFormat": "M d, yy",
				"minDate": -7,
				"maxDate": new Date()
			})
			.attr("readonly", true);
			
			  $(".report_picker2").datepicker({  
				"dateFormat": "M d, yy",
				"minDate": -7,
				"maxDate": new Date()
			})
			.attr("readonly", true);
		  })
        </script>
        <style>
        .tablesorter-bootstrap .bootstrap-icon-unsorted {
			background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAOCAMAAADOvxanAAAAVFBMVEUAAABCQkJZWVkZGRnJyckgICAZGRkZGRn8/PweHh4dHR0aGhoaGhpUVFQbGxvQ0NDc3NxMTExSUlIbGxvr6+s4ODhKSkogICAtLS00NDQzMzMnJydSEPrQAAAAGHRSTlMA1ssZRLgdAQbDyisqsZo8QdXUq0r9xPepSRwiAAAAX0lEQVQI13XHSQKAIAwEwQAKxn13Ev7/T2Pu9qmarJKPXIicI4PH4hxaKNrhm2S8bJK5h4YzKHrzJNtK6yYT/TdXzpS5zuYg4MSQYF6i4IHExdw1UVRi05HPrrvT53a+qyMFC9t04gcAAAAASUVORK5CYII=");
			content:"-";
		}
        </style>
    </body>

</html>
