@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8 col-sm-6 col-xs-6 breadcrumb">   
                <?php echo display_breedcrump(); ?>
            </div>
            <div class="col-lg-4" style="text-align:right">
                <a href="<?php echo url() ?>/admin/surveys/addprojectsurvey" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New</button></a>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-manpower-types">
                                <thead>
                                    <tr>
                                       
                                        <th class="manpower-name">#</th>
                                        <th class="manpower-id">Project</th>
                                        <th class="manpower-id">Survey Question</th>
                                        <th class="manpower-actions">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($ProjectSurvey)) {
                                        foreach ($ProjectSurvey as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo ucfirst($u->id) ?></td>
                                                <td><?php echo get_project_title($u->project_id) ?></td>
                                                <td><?php echo getQuestionTitle($u->question_id) ?></td>
                                                
                                                <td> 
                                                    <a onclick="return confirm('Are you sure you want to delete ?')" href="<?php echo url() ?>/admin/surveys/deleteprojectsurvey/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    ?>
                                </tbody>
                            </table>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
