@extends('layout.admin')

@section('content')

<?php
$projects=getProjectsHaveNoQuestions();

$questions=getQuestions();
?>
 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/surveys/savesurvey">
							<div class="form-group">
								<label>Select Project</label>
								<select class="form-control" name="project_id">
									<?php   
										if(!empty($projects)){
											foreach($projects as $p){ ?>
												<option value="<?php echo $p->id ?>"><?php echo $p->title ?></option>
											<?php }
										}
									?>
								</select>
							</div>
							
							<div class="form-group">
								<label>Select Question</label>
								<div class="table-responsive">
										<table class="table select-project-table">
											<tbody>
												<?php if(!empty($questions)) {
												foreach($questions as $q){ ?>										 
												<tr>
														<td>
															<div class="form-group">
																<div class="checkbox">
																	<label style="font-weight:500">
																		<input type="checkbox" value="<?php echo $q->id ?>" name="question[]"><?php echo $q->survey ?>
																	</label>
																</div>
															</div>
														</td>
														
												</tr>	
												<?php } } ?>
											</tbody>
										</table>
								</div>
							</div>
							
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
