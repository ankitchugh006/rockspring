@extends('app')

@section('content')
<div class="container-fluid">
    
        <div class="panel panel-default login-admin">
            <div class="panel-heading">Login<i class="fa fa-sign-in pull-right"></i></div>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="<?php echo url(); ?>/admin/login">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-12 control-label">Username</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="username" value="<?php if(isset($_SESSION['remember_me'])) { echo $_SESSION['remember_me']; } ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12 control-label">Password</label>
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password" value="<?php if(isset($_SESSION['remember_me_p'])) { echo $_SESSION['remember_me_p']; } ?>">
                        </div>
                    </div>
                   
					<div class="form-group">
						<div class="col-md-12">
							<input name="remember" type="checkbox"  value="1" <?php if(isset($_SESSION['remember_me']) && !empty($_SESSION['remember_me'])) { echo 'checked="checked"'; } else { echo ''; } ?>/> Remember me
						</div>
					</div>
                    <!--div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                            <label>
                                                    <input type="checkbox" name="remember"> Remember Me
                                            </label>
                                    </div>
                            </div>
                    </div-->

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                                Login
                            </button>

                            <a href="<?php echo url(); ?>/admin/forgotpassword">Forgot Your Password?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                           

                            <a href="<?php echo url(); ?>/privacy-policy">Privacy Policy</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
</div>
@endsection
