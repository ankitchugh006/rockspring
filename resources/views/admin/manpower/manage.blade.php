@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8 col-sm-6 col-xs-6 breadcrumb">   
                <?php echo display_breedcrump(); ?>
            </div>
            <div class="col-lg-4" style="text-align:right">
                <a href="<?php echo url() ?>/admin/manpowers/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New Type</button></a>
            </div>
        </div>
        <div class="space"></div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-manpower-types">
                                <thead>
                                    <tr>

                                        <th class="manpower-name">Cost Code</th>
                                        <th class="manpower-id">Task</th>
                                        <th class="manpower-actions">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($manpowers)) {
                                        foreach ($manpowers as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo ucfirst($u->number) ?></td>
                                                <td><?php echo ucfirst($u->title) ?></td>

                                                <td>
                                                    <a href="<?php echo url() ?>/admin/manpowers/edit/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>		

                                                    <a onclick="return confirm('Are you sure you want to delete this type?')" href="<?php echo url() ?>/admin/manpowers/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            {!! $manpowers->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
