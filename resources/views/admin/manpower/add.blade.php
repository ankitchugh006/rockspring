@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/manpowers/save">

                            <div class="form-group">
                                <label>Task</label>
                                <input class="form-control" name="title" placeholder="Task" value="{{ old('title') }}">
                            </div>
							<div class="form-group">
                                <label>Title for Report</label>
                                <input class="form-control" name="report_title" placeholder="Task" value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label>Number</label>
                                <input class="form-control" name="number" placeholder="Number" value="{{ old('number') }}">
                            </div>
							
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
