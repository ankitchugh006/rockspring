@extends('layout.admin')

@section('content')
 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/manpowers/update">

                            <div class="form-group">
                                <label>Task</label>
                                <input class="form-control" name="title" placeholder="task" value="<?php echo !empty($manpower->title)?$manpower->title:"" ?>">
                            </div>
							<div class="form-group">
                                <label>Title for Report</label>
                                <input class="form-control" name="report_title" placeholder="Task" value="<?php echo !empty($manpower->report_title)?$manpower->report_title:"" ?>">
                            </div>
                            <div class="form-group">
                                <label>Number</label>
                                <input class="form-control" name="number" placeholder="number" value="<?php echo !empty($manpower->number)?$manpower->number:"" ?>">
                            </div>
							<input type="hidden" value="<?php echo !empty($manpower->id)?$manpower->id:"" ?>" name="manpowerid" />
                            
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
