@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>
              <div class="col-lg-4" style="text-align:right">
                    <a href="<?php echo url() ?>/admin/files/categories/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New Category</button></a>
            </div>
            
        </div>
        <!-- /.row -->
       
       
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Number of Files</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($categories)) {
                                        foreach ($categories as $u) {
                                           
                                            ?>
                                            <tr>
                                                <td><?php echo $u->id ?></td>
                                                <td><?php echo ucfirst($u->category) ?></td>
                                                <td><?php echo ($u->description) ?></td>
                                                <td><?php echo getCatFileCount($u->id) ?></td>
                                                <td>
                                                    <a onclick="return confirm('Are you sure you want to delete this category?')" href="<?php echo url() ?>/admin/files/categories/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
									<tr>
									  <th colspan="7" class="ts-pager form-horizontal">
										<button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
										<button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
										<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
										<button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
										<button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
										
										<select class="pagenum input-mini" title="Select page number"></select>
									  </th>
									</tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
