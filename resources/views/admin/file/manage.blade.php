@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->

        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-6">   
                <form id="sort_form" method="GET" action="">


                    <!--div class="col-lg-3">
                            
                                    <div class="form-group">
                                            <select id="sort_select" class="form-control sort_select" name="order_by">
                                                    <option value="created_at" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'created_at') {
                        echo 'selected';
                    }
                    ?>>-Sort By Date-</option>
                                                    <option value="name" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'name') {
                        echo 'selected';
                    }
                    ?>>-Sort By Name-</option>
                                            </select>
                                    </div>
                            
                    </div-->
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group"  > 					
                                <input  class="form-control s_filter file_autocomplete" name="name" placeholder="Enter Name or title" value="<?php
                                if (isset($_GET['name'])) {
                                    echo $_GET['name'];
                                }
                                ?>">					
                            </div> 

                        </div>	
                        <div class="col-xs-2">
                            <input  type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-lg-6" style="text-align:right">
                <a href="<?php echo url() ?>/admin/files/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add File</button></a>
                <a href="<?php echo url() ?>/admin/files/categories" ><button class="btn btn-ls btn-primary" type="button">All Categories</button></a>
            </div>


        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort">
                                <thead>
                                    <tr>
                                        <!--th>#</th-->
                                        <th>Project</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Uploaded by</th>
                                        <th>Categories</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($files)) {
                                        foreach ($files as $u) {
                                            $i++;
                                           
											$ext='';
											if(!empty($u->file)){
												$info     = get_headers($u->file, true); 
												$ext=explode('.',$u->file);
												//echo '<pre>';
												//print_r($info);
												
											 }
											 ?>
                                           
                                            <tr>
                                                <!--td><?php echo $i; ?></td-->
                                                <td><a href="<?php echo url() ?>/admin/projects/view_project/<?php echo $u->project_id; ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo ucfirst($u->title) ?></td>
                                                <td>
													<?php  if(!empty($u->file) && UR_exists($u->file)){ 
													if($info['Content-Type']=="image/png"  || $info['Content-Type']=="image/jpeg"){ ?>
														<a style="cursor:pointer" data-toggle="modal" data-target="#viewfileModal<?php echo $i; ?>"  ><span style="color:#337ab7">View File</span></a>
                                                  
															 <!-- Modal -->
														<div id="viewfileModal<?php echo $i; ?>" class="modal fade" role="dialog">
														  <div class="modal-dialog">

															<!-- Image Model content-->
															<div class="modal-content">
															  <div class="modal-header">
																<button type="button" class="close" data-dismiss="modal">&times;</button>
																<h4 class="modal-title">File</h4>
															  </div>
															  <div class="modal-body">
																	<img src="<?php  echo $u->file; ?>" />
																		
																	
															  </div>
															  <div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															  </div>
															</div>

														  </div>
														</div>
													<?php }elseif(UR_exists($u->file)){
													?>	
													<a target="_blank" href="<?php echo ucfirst($u->file) ?>">View File</a>
												 <?php } } ?>
                                                </td>
                                                
                                                <td><?php echo get_name($u->added_by) ?></td>

                                                <td>



                                                    <?php
                                                    $a = '';
                                                    if (!empty($u->category_ids)) {
                                                        $cat = explode(',', $u->category_ids);

                                                        foreach ($cat as $id) {
                                                            $a .= getFileCategoryTitle($id) . ',';
                                                        }
                                                        echo rtrim($a, ',');
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
                                                    <a onclick="return confirm('Are you sure you want to delete this file?')" href="<?php echo url() ?>/admin/files/delete_single/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="ts-pager form-horizontal">
                                            <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                            <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                            <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                            <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                            <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>

                                            <select class="pagenum input-mini" title="Select page number"></select>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
