@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/files/save" enctype="multipart/form-data">
							<div class="form-group">
                                <label>Choose File</label>
                                <input type="file"  name="file" placeholder="file" value="{{ old('name') }}">
                            </div>
                            
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="title" placeholder="Title" value="{{ old('title') }}">
                            </div>
                            
                          <div class="form-group select-foreman">
                              <?php $projects=getProjects(); ?>
                                <label>Project</label>
                                <select  class="form-control choosen_project" name="project">
									 <option value="">-Select Project-</option>
                                    <?php
                                    if (!empty($projects)) {
                                        foreach ($projects as $u) {

                                            ?>
                                            <option value="<?php echo $u->id ?>"><?php echo $u->title; ?></option>

                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            
                             <div class="form-group select-foreman">
                              <?php $categories=getFileCategories(); ?>
                                <label>Select Category</label>
                                <select multiple class="form-control choosen_select" name="categories[]">
                                    <?php
                                    if (!empty($categories)) {
                                        foreach ($categories as $u) {
                                            ?>
                                            <option value="<?php echo $u->id ?>"><?php echo $u->category; ?></option>

                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            
                            
                           
                           
							
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
