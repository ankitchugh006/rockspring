@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/projects/tasks/save" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>Task</label>
                                <input type="text" class="form-control" name="task" placeholder="Title" value="{{ old('task') }}">
                            </div>
                             <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" placeholder="Description">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Task Thumbnail</label>
                                <input type="file"  name="task_image" placeholder="title" value="{{ old('task_image') }}">
                            </div>
                            
                          <div class="form-group select-foreman">
                              <?php $projects=getProjects(); ?>
                                <label>Project</label>
                                <select  class="form-control choosen_project" name="project">
									 <option value="">-Select Project-</option>
                                    <?php
                                    if (!empty($projects)) {
                                        foreach ($projects as $u) {

                                            ?>
                                            <option value="<?php echo $u->id ?>"><?php echo $u->title; ?></option>

                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            
                           
                             
                          <div class="form-group select-foreman">
                              <?php $for=getForemans(); ?>
                                <label>Foreman</label>
                                <select  class="form-control choosen_project" name="foreman">
                                    <option value="">-Select Foreman-</option>
                                    <?php
                                    if (!empty($for)) {
                                        foreach ($for as $u) {

                                            ?>
                                            <option value="<?php echo $u->id ?>"><?php echo $u->username; ?></option>

                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            
							
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
