@extends('layout.admin')

@section('content')
<style>
.chosen-container.chosen-container-multi{
	width:100% !important;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
                <?php echo display_breedcrump(); ?>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
				<div class="row">
                <form role="form" method="POST" action="<?php echo url(); ?>/admin/projects/save">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label>Job #</label>
                        <input class="form-control" name="project_id" placeholder="<?php echo '1234' ?>" value="{{ old('project_id') }}">
                    </div>
                    <div class="form-group">
                        <label>Job Name</label>
                        <input class="form-control" name="title" placeholder="Title" value="{{ old('title') }}">
                    </div>
                    <div class="form-group">
                        <label>Job Address</label>
                        <textarea  class="form-control" name="description" placeholder="Address">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>City</label>
                        <input class="form-control" name="city" placeholder="<?php echo 'City' ?>" value="{{ old('city') }}">
                    </div>

                    <div class="form-group">
                        <label>State</label>
                        <input class="form-control" name="state" placeholder="<?php echo 'State' ?>" value="{{ old('state') }}">
                    </div>

                    <div class="form-group">
                        <label>Zip</label>
                        <input class="form-control" name="zip" placeholder="<?php echo 'Zip' ?>" value="{{ old('zip') }}">
                    </div>


                    <h3 class="text-info">Select Customer For Project</h3>
                    <hr style="border:1px solid;">
                    <?php
                    $customers = getCustomers();

                    $customerid = \Session::get('customer');
                    ?>
                    <div class="form-group">
                        <label>Customer</label>
                        <select class="form-control choosen_select" name="customer" id="project_customer">
                            <option  value="">--Select Customer--</option>
                            <?php
                            if (count($customers)) {
                                foreach ($customers as $c) {
                                    $selected = '';

                                    if ($customerid == $c->id) {
                                        $selected = 'selected';
                                    }

                                    echo '<option value="' . $c->id . '" ' . $selected . '>' . $c->customer . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group" id="project_customer_members1" style="float:left;width:100%">
                    
						<?php 
						$jh=0;
						$allmembers=getAllCustomerMembersWc();
						
						$members= \Session::get('member_customer');
						$members_check= \Session::get('member_customer_check');
						
					
						if(!empty($members)){ ?>
								<table id="member_row_inner"  class="table table-hover table-striped member_table_'+jh+'">
								<tr>
								<th>Report?</th>
								<th>Member</th>
								<th>Title</th>
								<th>Email</th>
								<th>phone</th>
								<th>
								<div class="add-member" id="add-<?php echo $jh ?>">
								<a href="javascript:;" onclick="member_add('<?php echo $jh ?>')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
								</th>
								</tr>
								
								<?php 
								foreach($members as $key=>$m){ 
									$jh++;
									?>
									<tr id="member_row_<?php echo $jh ?>" class="member_row">
									<td>
									<input type="checkbox" name="member_customer_check[<?php echo $jh ?>]" value='<?php echo $jh ?>'<?php if($members_check) { if(in_array($key,$members_check)){ echo 'checked'; } } ?> >
									</td>

									<td>
									<select id="member-<?php echo $jh ?>" class="form-control choosen_select" name="member_customer[<?php echo $jh ?>]" onChange="getMemberData(this.value,<?php echo $jh ?>)">
										<option value="">-Members-</option>
										<?php  if(!empty($allmembers)){ 
											foreach($allmembers as $am){ ?>
												<option value="<?php echo $am->id; ?>"  
												
												<?php if($am->id==$m) { echo 'selected'; } ?>
												><?php echo $am->name; ?></option>
											<?php }
											?>
											
										<?php } ?>
										
									</select>
									</td>


									<td>
										<span id="title-<?php echo $jh ?>"><?php echo getMemberTitle($m) ?></span>
									</td>


									<td>
										<span id="email-<?php echo $jh ?>"><?php echo getMemberEmail($m) ?></span>
									</td>

									<td>
										<span id="phone-<?php echo $jh ?>"><?php echo getMemberPhone($m) ?></span>
									</td>

									<td>
									<a href="javascript:;" onclick="member_remove('<?php echo $jh ?>')"><span class="glyphicon glyphicon-minus" style="color:red"></span></a>
									</td>
									</tr>
									
							<?php }
							?>
							<input type="hidden" id="jh" value="<?php echo $jh ?>">
							 <?php } else {  ?>
								 <input type="hidden" id="jh" value="<?php echo 0; ?>">
								 <?php } ?>
							</table>
                    </div>
                    
                    <!--div class="form-group">
                        <label>Customer</label>
                        <input class="form-control" name="customer" placeholder="Customer" value="{{ old('customer') }}">
                    </div>
                    
                    <div class="form-group">
                        <label>Address</label>
                        <textarea  class="form-control" name="address" placeholder="Address">{{ old('address') }}</textarea>
                    </div-->
                    <h3 class="text-info">Scope</h3>
                    <hr style="border:1px solid;">
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" value="demo" name="scope[]">Demo                            
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="drywall" name="scope[]">Drywall                            
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="paint" name="scope[]">Paint                            
                        </label>
                    </div>


					</div>
					<div class="col-lg-12">
                    <h3 class="text-info">Budget</h3>
                    <hr style="border:1px solid;">

                    <div class="table">
                        <table class="table" id="projectTable">
                            <tr>

                                <td><b>Manpower Types</b></td>
                                <td><b>Available Hours</b></td>
                                <!--td><b>Subcontractors</b></td-->
                            </tr>
                            <!--tr>
                                    <td>
                                            <div class="form-group">
                                                                    <input id="txtTags0" required class="form-control resourcesR"  type="text" name="resources[1]" value="" placeholder="manpower">
                                            </div>
                                    </td>
                                    <td>
                                            <input style="width:50px;"  class="form-control" type="text" name="max_hours[1]" value="">
                                    </td>
                                    <td>
                                            <a href="javascript:;" id="addMore" class="btn btn-xs btn-primary">Add More</a>
                                    </td>
                            </tr-->

                            <?php
                            //print_r(\Session::get('resources'));
                            if (!empty($manpowers)) {
                                foreach ($manpowers as $m) {
                                    $cts = check_to_select($m->id);
                                    $ctv = check_to_set_value($m->id);
                                    ?><tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="manpower_check" onClick="validateText(<?php echo $m->id ?>)"  id="check-<?php echo $m->id ?>" type="checkbox" name="resources[<?php echo $m->id; ?>]" value="<?php echo $m->id; ?>" <?php
                                                        if ($cts == 1) {
                                                            echo 'checked';
                                                        }
                                                        ?>><?php echo $m->title; ?>(#<?php echo $m->number; ?>)
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span >Available Hours</span><input <?php  if ($ctv == 0 || $ctv == '') { echo 'readonly';} ?> id="input-<?php echo $m->id ?>" style="width:40px;" type="text" name="max_hours[<?php echo $m->id; ?>]" value="<?php
                                            if ($ctv > 0) {
                                                echo $ctv;
                                            }
                                            ?>">
                                        </td>
                                        
                                        <!--td>
											<select  data-placeholder = "-Select Contractors-" class="form-control choosen_select" multiple name="contractors[]" id="contractor_select<?php echo $m->id ?>" onChange="setContractorHidden(this,<?php echo $m->id ?>)" >
											<?php 
												$contractors=getSubcontractors();
												if(count($contractors)){
													foreach($contractors as $c){ ?>
														<option value="<?php echo $c->id; ?>"><?php echo $c->subcontractor; ?></option>
													<?php }
												}
											 ?>
											 </select>
                                        </td>
										<input type="hidden" id="contractor_hidden<?php echo $m->id ?>" name="contractor_hidden[<?php echo $m->id ?>]" value="" / --> 
                                    </tr>	
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
					</div>
					<div class="col-lg-8">
                    <h3 class="text-info">Rock Spring Project Team</h3>
                    <hr style="border:1px solid;">
                    <div class="form-group select-foreman">
                        <label>PM</label>
                        <select  data-placeholder = "-Select PM-" class="form-control choosen_select" multiple name="pm[]" >
                          
                            <?php
                            $adminusers = getAllProjectManagers();
                            if (!empty($adminusers)) {
                                foreach ($adminusers as $u) {
                                    $pm = \Session::get('pm');
                                    ?>
                                    <option value="<?php echo $u->id ?>" <?php
                                    if (!empty($pm)) {
                                        if (in_array($u->id,$pm)) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $u->name; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <br/>
                        <label>APM</label>
                        <select  data-placeholder = "-Select APM-"  class="form-control choosen_select" name="apm[]" multiple>
                            
                            <?php
                            $adminusers = getAllAssistantManagers();
                            if (!empty($adminusers)) {
                                foreach ($adminusers as $u) {
                                    $apm = \Session::get('apm');
                                    ?>
                                    <option value="<?php echo $u->id ?>" <?php
                                    if (!empty($apm)) {
                                        if (in_array($u->id,$apm)) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $u->name; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <br/>
                        <label>Superintendent</label>
                        <select  data-placeholder = "-Select Super-"  class="form-control choosen_select" name="super[]" multiple >
                           
                            <?php
                            $adminusers = getAllSuperintendents();
                            if (!empty($adminusers)) {
                                foreach ($adminusers as $u) {
                                    $super = \Session::get('super');
                                    ?>
                                    <option value="<?php echo $u->id ?>" <?php
                                    if (!empty($super)) {
									   if (in_array($u->id,$super)) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $u->name; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <br/>
                        <label>Estimator</label>
                        <select  data-placeholder = "-Select Estimator-"  class="form-control choosen_select" name="estimator[]" multiple >
                            
                            <?php
                            $adminusers = getAllEstimators();
                            if (!empty($adminusers)) {
                                foreach ($adminusers as $u) {
                                    $est = \Session::get('estimator');
                                    ?>
                                    <option value="<?php echo $u->id ?>" <?php
                                    if (!empty($est)) {
                                        if (in_array($u->id,$est)) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $u->name; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <br/>
                        <label>Foreman</label>
                        <select  data-placeholder = "-Select Foreman-"  class="form-control choosen_select" name="foremans" >
                           
                            <?php
                            $users = getAdminForemanUsers();
                            if (!empty($users)) {
                                $foremans = \Session::get('foremans');
                                foreach ($users as $u) {
                                    ?>
                                    <option value="<?php echo $u->id ?>" <?php
                                    if (!empty($foremans)) {
                                        if ($foremans == $u->id) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $u->name; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
					
					<h3 class="text-info">Select Survey Questions</h3>
                    <hr style="border:1px solid;">
					
					<!--select  class="form-control choosen_select" name="surveys[]" multiple-->
						<!--option value="">--Select Question--</option-->
						<div class="checkbox">
							<label>
								<input type="checkbox"  id="check-all-survey" ><b>Select All</b>
							</label>
						</div>
						<?php
						$questions=getAllSurveyQuestions();
						if (count($questions) > 0) {
							$surveys = \Session::get('surveys');
							foreach ($questions as $u) {
								?>
								<div class="checkbox">
									<label>
										<input type="checkbox" class="check-all-child"
										
										<?php if (!empty($surveys)) {
											if (in_array($u->id,$surveys)) {
												echo 'checked';
											}
										} ?> value="<?php echo $u->id ?>" name="surveys[]" id="check-<?php echo $u->id ?>" ><?php echo $u->survey; ?>
									</label>
								</div>
								<!--option value="<?php echo $u->id ?>" <?php
								if (!empty($surveys)) {
									if (in_array($u->id,$surveys)) {
										echo 'selected';
									}
								}
								?>><?php echo $u->survey; ?></option-->

								<?php
							}
						}
						?>
					<!--/select-->
					
                    <h3 class="text-info">Project Details</h3>
                    <hr style="border:1px solid;">

                    <div class="form-group">
                        <label>Hours Per Shift</label>
                        <input class="form-control" name="hps" placeholder="Hours Per Shift" value="{{ old('hps') }}">
                    </div>
                    <div class="form-group">
                        <label>Project Shift</label>
                        <select  class="form-control" name="shift_type">
                            <option value="day">Day</option>
                            <option value="night">Night</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Start Date</label>
                        <input  id="datetimepicker_add_project" type="text" class="form-control" name="start_date" placeholder="Start Date" value="{{ old('start_date') }}">
                    </div>

                    <!--h3 class="text-info">Select Subcontractor for project</h3>
                    <hr style="border:1px solid;">
                    <div class="table-responsive">
                        <table class="table select-project-table">
                    <?php
                    $subcontractors = getSubcontractors();
                    if (!empty($subcontractors)) {

                        foreach ($subcontractors as $s) {
                            $ctf = check_to_set_subcontractor($s->id);
                            $checked = '';
                            if ($ctf == 1) {
                                $checked = 'checked';
                            }
                            ?>
                                       <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" <?php echo $checked ?> name="subcontractors[<?php echo $s->id; ?>]" value="<?php echo $s->id; ?>"><?php echo $s->subcontractor; ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                    </tr>
                                  
                        <?php }
                    }
                    ?>
                        </table>
                    </div-->


                    <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
				</div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
