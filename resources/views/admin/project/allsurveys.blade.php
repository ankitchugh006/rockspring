@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Projects Survey</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-project-survey">

                                <thead>
                                    <tr>
                                        
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foreman</th>
                                        <th class="project-date">Date</th>
                                        <th class="project-date">Images</th>
                                        <th class="project-actions">Actions</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($surveys)) {
                                        foreach ($surveys as $u) {
											
                                            ?>
                                            <tr>
                                               
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>

                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
													<a href="javascript:;"  data-toggle="modal" data-target="#imgModal<?php echo $u->id ?>">View Images</a>
													<div id="imgModal<?php echo $u->id ; ?>" class="modal fade" role="dialog">
													  <div class="modal-dialog">

														<!-- Image Model content-->
														<div class="modal-content">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Images</h4>
														  </div>
														  <div class="modal-body">
															    <?php
															    $images=getSurveyMainImages($u->id);
															       if($images != NULL){
																	   
																		$arr=explode(',',$images);
																		
																		foreach($arr as $a){
																			echo '<img src="'.$a.'" /><br/>';
																		}
																	}else{
																		echo 'No images found';
																	}	
																	 ?>
																
														  </div>
														  <div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														  </div>
														</div>

													  </div>
													</div>
                                                </td>
                                                <td><a href="<?php echo url() ?>/admin/projects/survey/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View  Detail</button></a>
           
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
                {!! $surveys->render() !!}
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
