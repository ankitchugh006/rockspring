@extends('layout.admin')

@section('content')

<style>
.quantity_none{
	display:none;
	width:50%;
	border:1px solid #ccc;
	
}
button.quantity_none{
	width:18% !important;
}
.chosen-container{
	width:100% !important;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    <?php echo get_project_title($project->id) ?>
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">

                <div class="project-estimate">
                    <ul class="project-estimate-list">
                       <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></li>
                        <li><strong>Total Project Requests: </strong> <?php echo $data['requests']; ?></li>
                        <li><strong>Total hours Logged: </strong> <?php echo $data['hourslogged']; ?></li>
                        <li><strong>Total hours Estimated: </strong> <?php echo $data['total_estimated_hours']; ?></li>
                         <?php if(Auth::user()->role=='role_administrator' && Auth::user()->job_role=='job_superintendent' && (isManpowerClickedToday($project->id) > 0)) { ?>
							<li><strong>Notification: </strong><a onclick="updateProject(<?php echo $project->id; ?>)" class="btn btn-xs btn-primary" href="javascript:;">Send Update</a><i id="loading_text_update" style="font-size:12px;color:#000;font-weight:bold;margin-left:20px;color:#337ab7"></i></li>
						 <?php } ?>
                    </ul>
                </div>
            </div>

            <br/><br/>
            <div class="col-lg-12">
                <div class="panel panel-primary fullwidth">
                    <div class="panel-heading">
                        <h3 class="panel-title">Requested Resources </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
							 
                            <table class="table table-bordered table-hover table-striped table-individual-request">

                                <thead>
                                    <tr>
                                       
                                        <th class="project-name">Project</th>
                                        <th class="project-requested-by">Requested By</th>
                                        <th class="project-resources">Resources</th>
                                        <th class="project-workers">Requested Workers</th>
                                        <th class="project-workers">Assigned Workers</th>
                                        <th class="project-workers">Subcontractor</th>
                                        <th class="project-date">Requested On</th>
                                        <th class="project-date">Processing For</th>
                                        <th class="project-actions">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($requests)) {
                                        foreach ($requests as $u) {
                                            $i++;
                                            ?>
                                            <tr <?php
                                            if ($i == 1) {
                                                echo 'style=""';
                                            }
                                            ?>>
                                                
												<td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo get_resource_title($u->resource_id) ?></td>
                                               
                                                
                                                <td>
														<span class="display_quantity" id="display_quantity<?php echo $i ?>"><?php echo ($u->quantity) ?></span>
														<input type="text" class="quantity_none text_quantity" id="text_quantity<?php echo $i ?>" value="<?php echo ($u->quantity) ?>">
														<button class="btn btn-xs quantity_none quantity_go btn-primary" id="quantity_go<?php echo $i ?>">Go</button>
														<input type="hidden" id="request_id<?php echo $i ?>" value="<?php  echo $u->id; ?>" />
														<input type="hidden" id="foreman_id<?php echo $i ?>" value="<?php  echo $u->foreman_id; ?>" />
												</td>
												 <td>
													 <?php 
															$subs=getAssignRequestSub($u->project_id,$u->foreman_id,$u->id,$u->processing_date);
															$ttl="";
															if(!empty($subs)){	
																foreach($subs as $c){
																	$ttl += $c->quantity;	
																}
																
															}
															echo $ttl;
														?>
												</td>
                                                <td>
												<?php 
													$subs=getAssignRequestSubDisplay($u->project_id,$u->foreman_id,$u->id);
													$s="";
													if(!empty($subs)){	
														foreach($subs as $c){
															$s .= get_subcontractor_title($c->subcontractor_id).', ';
															
														}
														echo rtrim($s, ', ');
													}
													
                                                ?>
                                                </td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td><?php echo change_date_format($u->processing_date) ?></td>
                                                <td>
													<a href="javascript:;" class="btn btn-xs btn-primary" onClick="override_manpower('<?php echo $u->id ?>','<?php echo $i ?>')">Override</a>
													<a href="javascript:;" data-toggle="modal" data-target="#myModal<?php  echo $u->id; ?>"  class="btn btn-xs btn-info">Assign Subs</a>
												</td>
                                            </tr>
                                            <!-- Modal -->
                                            <tr>
												<td colspan="8" style="display:none;">
													<select  style="display:none" id="select_to_copy_new<?php echo $u->id ?>" name="subcontractor_id[]"   class="form-control trigger_update" required >
															<option value="">-Select Contractor-</option>
															<?php 
															 $contractors=getSubcontractors(); 
															if(!empty($contractors)){ 
																	foreach($contractors as $c){?>
																		<option value="<?php echo $c->id; ?>"> <?php echo $c->subcontractor; ?></option>
																	<?php }
															}
															?>
														</select>
													 
												</td>
                                            </tr>
                                           <tr>
											   <td colspan="8">
											<div id="myModal<?php  echo $u->id; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

												<!-- Modal content-->
												<div class="modal-content">
												  <div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Assign Subcontractor</h4>
												  </div>
												  <div class="modal-body">
													<?php
													
													 $subs1=getAssignRequestSub($u->project_id,$u->foreman_id,$u->id);
													 $resourceContractors=getresourceContractors($u->project_id,$u->resource_id);
													 ?>
													 
													 
													   <form  id="<?php echo $u->id ?>" name="<?php echo $u->id ?>" action="<?php echo url(); ?>/admin/projects/requests/assigncontractor" method="POST">
															<div id="sab_to_appand_<?php echo $u->id ?>">
																<?php if(count($resourceContractors)){ 
																	
																	$subcount=0;
																	foreach($resourceContractors as $r){ 
																		$subcount++;
																		?>
																		<div id="sab_to_appand_inner<?php  echo $subcount ?>">
																			<input type="hidden" name="subcontractor_id[]" value="<?php echo $r; ?>">	
																			<div class="form-group col-md-7">	
																				<?php echo get_subcontractor_title($r); ?>
																			</div>
																			<?php //echo $u->processing_date; ?>	
																			<div class="form-group col-md-4">
																				<input placeholder="manpower" id="subcontractor_manpower_<?php echo $u->id; ?>"   <?php if($subcount==1) { echo 'required'; } ?> class="form-control subcontractor_manpower" type="text" name="subcontractor_quantity[]" value="<?php echo getContractorQuantity($r,$u->project_id,$u->resource_id,$u->foreman_id,$u->processing_date) ?>" />
																			</div>
																			
								
																		</div>
																	<?php } ?>	
																<?php }else{ echo '<p>No Contractors Assigned</p>';} ?>	
															</div>	
															
															<input type="hidden" name="request_id" value="<?php echo $u->id; ?>" />
															<input type="hidden" name="main_request_id" value="<?php echo $u->request_id; ?>" />
															<input type="hidden" name="foreman_id" value="<?php echo $u->foreman_id; ?>" />
															<input type="hidden" name="project_id" value="<?php echo $u->project_id; ?>" />
															<input type="hidden" name="resource_id" value="<?php echo $u->resource_id; ?>" />
															<input type="hidden" name="processing_date" value="<?php echo $u->processing_date; ?>" />
															
															<a class="btn btn-primary" href="javascript:;" onClick="check_subcontractor_manpower_save('<?php echo $u->id ?>','<?php echo $u->quantity ?>')">Save</a>
															<input type="submit" id="register-form-submit<?php echo $u->id; ?>" style="display: none;" />
															<!--button class="btn btn-primary" type="submit">Save</button-->
															
															
														
														
													 </form>		
												  </div>
												  <div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												  </div>
												</div>
												
											  </div>
											</div></td>
											</tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
