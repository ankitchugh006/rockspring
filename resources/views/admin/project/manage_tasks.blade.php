@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8">   
                <form id="sort_form" method="GET" action="">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group"  > 					
                                <input  class="form-control s_filter task_autocomplete" name="name" placeholder="task" value="<?php
                                if (isset($_GET['name'])) {
                                    echo $_GET['name'];
                                }
                                ?>">					
                            </div> 

                        </div>	
                        <div class="col-xs-2">
                            <input  type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </div>
                </form>
            </div>
            <div style="text-align:right" class="col-lg-4">
                <a href="<?php echo url() ?>/admin/projects/tasks/add"><button type="button" class="btn btn-ls btn-primary"><i class="fa fa-plus"></i> Add New Task</button></a>
            </div>




        </div>

        <div class="row">
            <div class="col-lg-12">
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-projects tablesort">
                                <thead>
                                    <tr>
										 <th class="project-name">Project</th>
                                        <th class="project-count">Photo Thumbnail</th>
                                        <th class="project-name">Task</th>
                                        <th class="project-name">Description</th>
                                       
                                        <th class="project-customer">Added By</th>
                                        <th class="project-date">Date Added</th>
                                        <th class="project-date">Status</th>
                                        <th class="project-date">Take Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($tasks)) {
										$i=0;
                                        foreach ($tasks as $u) {
											$i++;
                                            ?>
                                            <tr>
												 <td><a href="<?php echo url() ?>/admin/projects/view_project/<?php echo $u->project_id; ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td>
                                                    <?php
                                                    if (isset($u->task_image) && !empty($u->task_image)) {
                                                        $a = (explode('/', $u->task_image));
                                                        echo '<a style="cursor:pointer" data-toggle="modal" data-target="#taskModal'.$i.'"  ><span style="color:#337ab7">View Image</span></a>';
                                                    }
                                                    ?>
                                                     <!-- Modal -->
												<div id="taskModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													<!-- Image Model content-->
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
															<img src="<?php  echo $u->task_image; ?>" />
																
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
													
                                                </td>	
                                                <td><?php echo $u->task ?></td>
                                                <td><?php echo $u->description ?></td>
                                               
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
                                                    <?php if ($u->status == 1) { ?>
                                                        <span  class="status-active">Resolved</span>

                                                    <?php } else { ?>
                                                        <span  class="status-active">Open</span>
                                                    <?php } ?>
                                                </td>
                                                <td>
													
                                                    <?php if ($u->status == 1) { ?>
                                                        <a onclick="change_task_status('<?php echo $u->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs btn-warning">Mark as Open</button></a>
                                                    <?php } else { ?>
                                                        <a onclick="change_task_status('<?php echo $u->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs btn-info">Mark as Resolved</button></a>
                                                    <?php } ?>
													<a onclick="return confirm('Are you sure you want to delete this task?')" href="<?php echo url(); ?>/admin/projects/tasks/delete/<?php echo $u->id; ?>" ><button class="btn btn-xs btn-danger" type="button">Delete</button></a>


                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8" class="ts-pager form-horizontal">
                                            <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                            <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                            <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                            <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                            <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>

                                            <select class="pagenum input-mini" title="Select page number"></select>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
