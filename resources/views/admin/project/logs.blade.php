@extends('layout.admin')

@section('content')

<style>

.hours_none{
	display:none;
	width:50%;
	border:1px solid #ccc;
}
</style>
<div id="page-wrapper">
    <?php
    //echo '<pre>';
    //print_r($logs);
    ?>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    <?php echo get_project_title($project->id) ?>
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">

                <div class="project-estimate">
                    <ul class="project-estimate-list">
                        <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></li>
                        <li><strong>Total Project Requests: </strong> <?php echo $data['requests']; ?></li>
                        <li><strong>Total hours Logged: </strong> <?php echo $data['hourslogged']; ?></li>
                        <li><strong>Total hours Estimated: </strong> <?php echo $data['total_estimated_hours']; ?></li>

                    </ul>
                </div>
            </div>
            <br/><br/>
            <div class="col-lg-12">
				@if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
                <div class="panel panel-primary fullwidth">
                    <div class="panel-heading">
                        <h3 class="panel-title">Log Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-individual-worklogs">

                                <thead>
                                    <tr>
                                        
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foreman</th>
                                      
                                        <th class="project-resources">Resources</th>
                                        <th class="project-resources">Quantity</th>
                                        <th class="project-resources">Hours Logged</th>
										  <!--th class="project-foreman">Image</th-->
                                        
                                        <th class="project-description">Description</th>
                                        <th class="project-date">Log Date</th>
                                        <!--th class="project-actions">Action</th-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($logs)) {
                                        foreach ($logs as $u) {
                                            $i++;
                                            ?>
                                            <tr <?php
                                            if ($i == 1) {
                                                echo 'style=""';
                                            }
                                            ?>>
                                                
                                               <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo get_resource_title($u->resource_id) ?></td>
                                                <td><?php echo ($u->quantity) ?></td>
                                                <td><?php echo ($u->hours_logged) ?></td>
                                                <!--td>
													<?php 
													if(!empty(getWorklogImage($u->worklog_id))){
														echo '<a style="cursor:pointer" data-toggle="modal" data-target="#imgModal'.$i.'"><span style="color:#337ab7">View Image</span></a>';
													}
													?>
													 
												<div id="imgModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
															<img src="<?php  echo getWorklogImage($u->worklog_id) ?>" />
																
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
													
                                                </td-->
                                                
                                                <td><?php echo $u->description ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <!--td><a data-toggle="modal" data-target="#myModal<?php echo $i; ?>" class="btn btn-xs btn-primary">View More Detail</a-->
                                              <!--a href="javascript:;" class="btn btn-xs btn-primary" onClick="override_hours('<?php echo $u->id ?>','<?php echo $i ?>')">Override Hours</a-->
                                                <!-- Modal -->
												<!--div id="myModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
														 
																<table class="table table-bordered table-hover table-striped table-individual-worklogs">
																	<tr>
																		<th class="project-name">Project</th>
																		<th class="project-foreman">Foreman</th>
																		<th class="project-resources">Resource</th>
																		<th class="project-resources">Subcontractor</th>
																		<th class="project-date">Quantity</th>
																		<th class="project-actions">Hours Logged</th>
																	</tr>	
																		
																	<?php
																	 $detail=getWorkLodDetail($u->id);
																	
																	 if(count($detail)){
																		 foreach($detail as $d){ ?>
																			<tr>
																				<td><?php echo get_project_title($d['project_id']) ?></td>
																				<td><?php echo get_name($d['foreman_id']) ?></td>
																				<td><?php echo get_resource_title($d['resource_id']) ?></td>
																				<td><?php echo get_subcontractor_title($d['subcontractor_id']) ?></td>
																				<td><?php echo $d['quantity'] ?></td>
																				<td><?php echo $d['hours_logged'] ?></td>
																			</tr>
																		
																		 <?php }
																	 }
																	 ?>
																 </table>
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div-->
												</td>
                                            </tr>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
        <!-- /.row -->
		
		
		
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
