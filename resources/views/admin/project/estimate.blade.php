@extends('layout.admin')

@section('content')
<?php
?>

<style>
.huge {
    font-size: 14px;
    font-weight: bold;
}
.project-estimate .panel{
	margin-bottom:0;
}
.col-xs-121 > div:last-child {
    min-height: 70px;
}
.project-estimate .col-xs-121 div span:nth-of-type(2){
	padding:0;
}
.member_table tr td{
	padding:5px;
}
.upper{
	min-height: 266px !important;
}
#complete_drop .dropdown-menu a:hover{
	color:#fff !important
}
#complete_drop .dropdown-menu{
	right:0;
	left:auto;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                   <a href="<?php echo url() ?>/admin/projects/view/<?php echo ($project->manual_id) ?>"><?php echo ($project->manual_id) ?> - <?php echo ($project->title) ?></a>
                    
                    <div class="panel-body" style="float:right;padding:0">
							
							<a href="<?php echo url() ?>/admin/projects/edit/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
							<a onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo url() ?>/admin/projects/delete/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
							
							<div id="complete_drop" class="dropdown" style="display: inline;cursor: pointer;">
							 <button style="cursor:pointer;padding: 1px 10px;" class="dropdown-toggle btn btn-xs btn-success" type="button" data-toggle="dropdown" aria-expanded="false">Change Status
							  <span class="caret"></span></button>
							  <ul class="dropdown-menu">
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;">Active <?php if($project->is_completed == 0)   {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '2')" href="javascript:;">Inactive <?php if($project->is_completed == 2) {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;">Complete <?php if($project->is_completed == 1) {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
							  </ul>
							</div>
							<?php if ($project->is_completed == 0) { ?>
								<!--a onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs  btn-success">Mark as Complete</button></a-->									
							<?php }else{ ?>
								<!--a onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs  btn-info">Mark as Incomplete</button></a-->	
							<?php } ?>
					</div>
                    
                </h1>
                <?php echo display_breedcrump(); ?>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <div class="">

                <div class="project-estimate">

<!--                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-121 ">
                                        <div class="huge">Job #</div>
                                        <div><?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Project Team</div>
                                                            <div class="upper">
																	<table class="table table-bordered member_table" cellpadding="5">
                                                                    <?php
                                                                    $team=getProjectTeam($project->id);
                                                                    
                                                                    if (!empty($team)) {
                                                                            foreach ($team as $u) {
                                                                                   echo '<tr><td style="text-transform:capitalize">'.get_name($u->foreman_id).'</td><td style="text-transform:uppercase">'.$u->type.'</td></tr>';
                                                                            }
                                                                    }
                                                                    ?>
                                                                    </table>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 " style="text-transform:capitalize">
                                                            <div class="huge">Customer Project Team <span style="float:right"><i>Customer - </i><a style="color:#fff;float:none;font-size:16px;line-height:0" href="<?php echo url() ?>/admin/customers/edit/<?php echo  $project->customer_id; ?>"><?php echo getCustomerName($project->customer_id) ?></a></span></div>
                                                            <div class="upper">
																<table class="table table-bordered member_table" cellpadding="5">
                                                                    <?php
                                                                    $team=getProjectCustomerTeam($project->id);
                                                                    if(!empty($team)){
																		foreach($team as $t){
																			echo '<tr><td>'.getMemberName($t->member_id).'</td><td>'.getMemberTitle($t->member_id).'</td><td>'.getMemberEmail($t->member_id).'</td><td>'.getMemberPhone($t->member_id);'</td></tr>';
																		}
																	}else{
																		echo 'N/A';
																	}
                                                                    ?>
                                                                 </table>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Job Address</div>
                                                            <div><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>

                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                        <div class="huge"><span>Total hours Logged</span><span>Total hours Estimated 
                                                        <a href="<?php  echo url(); ?>/admin/projects/estimate/<?php echo $project->manual_id ?>">
															<button class="btn btn-xs btn-danger" type="button" style="font-size: 9px;font-weight: bold;padding: 1px 2px;">View Budget</button>
														</a>
                                                        </span></div>
                                                        <div><span><?php echo $hourslogged; ?></span><span><?php echo $total_estimated_hours; ?></span></div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    

                    <!--div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Customer</div>
                                                            <div>
																<?php if($project->customer_id){ ?>
																<a style="float:left" href="<?php echo url() ?>/admin/customers/edit/<?php echo  $project->customer_id; ?>"><?php echo !empty($project->customer_id) ? getCustomerName($project->customer_id) : 'N/A' ?></a>
																<?php } ?>
															</div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div-->
                    

<!--                    <a href="" style="color:#333">
                        <div class="col-md-6">
                                <div class="panel">
                                        <div class="panel-heading">
                                                <div class="row">

                                                        <div class="col-xs-121 ">
                                                                <div class="huge">Total hours Estimated</div>
                                                                <div><?php echo $total_estimated_hours; ?></div>
                                                        </div>
                                                </div>
                                        </div>

                                </div>
                        </div>
                    </a>-->
                </div>
            </div>


			<!--div class="col-lg-12">
				 <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Actions</h3>
                    </div>
                    
                    <div class="panel-body">
							
							<a href="<?php echo url() ?>/admin/projects/edit/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
							<a onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo url() ?>/admin/projects/delete/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
							<?php if ($project->is_completed == 0) { ?>
								<a onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs  btn-success">Mark as Complete</button></a>									
							<?php }else{ ?>
								<a onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs  btn-info">Mark as Incomplete</button></a>	
							<?php } ?>
					</div>
                 </div>
			</div-->
				 
        </div>
        <!-- /.row -->
        
        <div class="row">
			<div class="col-lg-12">
			<hr style="border:2px solid #ccc">	
			<h3 style="color:#337ab7;">Budget</h3>
              <form method="POST" action="<?php echo url() ?>/admin/projects/update_estimate">
                    <div class="table-responsive">
                        <table class="table" id="projectTable">
							<tr>
								
								<td><b>Manpower Types</b></td>
								<td align="center"><b>Budgeted Hours</b></td>
								<td align="center"><b>Logged Hours</b></td>
								<td align="center"><b>Remaining Hours</b></td>
								<!--td style="width:36%;"><b>Statistics</b></td-->
								
							</tr>
						

                            <?php
                            //CALCULATE TOTAL
                            if (!empty($manpowers)) {
                              
                                $total=0;
                                foreach ($manpowers as $m) {
                                   
                                    $has_manpower = project_has_resource($assignedresources, $m->id);
                                     if (($has_manpower >= 0)) {
									   $total += $has_manpower;
									  
									}
								}
							}
							
							//-----------------------------------
                            if (!empty($manpowers)) {
                                $i = 0;
                                
                                foreach ($manpowers as $m) {
                                    $i++;
                                    $has_manpower = project_has_resource($assignedresources, $m->id);
                                    //echo $m->id;
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label <?php
                                                        if (($has_manpower >= '0')) {
                                                            echo 'class="check_disable" ';
                                                        }?> >
                                                        <!--input  id="check-<?php echo $m->id ?>" onClick="validateText(<?php echo $m->id ?>)" type="checkbox" name="resources[]" value="<?php echo $m->id; ?>" <?php
                                                        if (($has_manpower >= '0')) {
                                                            echo 'checked readonly';
                                                        }
                                                        ?> --><?php echo $m->title; ?>

                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td align="center">
											
                                            <!--input id="input-<?php echo $m->id ?>" style="width:45px;margin-left:10%;" <?php
                                            if (($has_manpower == '' && $has_manpower == 0)) {
                                                echo 'readonly';
                                            }
                                            ?> type="text" name="max_hours[]" value="<?php
                                                   if (($has_manpower >= 0)) {
													  
                                                       echo $has_manpower;
                                                   }
                                                   ?>"--> 
                                             <?php
											   if (($has_manpower >= 0)) {
												  
												   echo $has_manpower;
											   }
											   ?>      
                                                   
                                        </td>
                                        
                                        <td align="center">
												<?php 
												 $resource_id=get_resource_id_report($project->id,$m->id);
												  echo  get_used_hours_estimate($project->id,$resource_id); ?>
                                        </td>
                                        <td align="center">
												<?php 
													if($has_manpower > get_used_hours_estimate($project->id,$resource_id))
														echo $has_manpower - get_used_hours_estimate($project->id,$resource_id); 
													elseif( get_used_hours_estimate($project->id,$resource_id) > $has_manpower){
															$over=get_used_hours_estimate($project->id,$resource_id)-$has_manpower;
															echo "<span style='color:red'>[overtime : ".$over."]</span>";
													}elseif( get_used_hours_estimate($project->id,$resource_id) == $has_manpower)
															echo "0";
													?>
                                        </td>
										<!--td>
											<?php 
											//CALCULATE %AGE
											$age=0;
											 if (($has_manpower > 0)) {
												  $age=($has_manpower/$total)*100;
												  
											 }
											 ?>
											 <div style="width:100%;height:20px;background:#ccc">
												<div style="width:<?php echo $age ?>%;height:20px;background:repeating-linear-gradient(45deg,#606dbc,#606dbc 5px,#465298 5px, #465298 10px)"></div>
											</div>
										</td-->
                                        <?php
                                    }
                                }
                                ?>

                        </table>
                    </div>

					<input type="hidden" name="projectid" value="<?php echo $project->id ?>">
					<input type="hidden" name="manual_id" value="<?php echo $project->manual_id ?>">
					<!--button type="submit" class="btn btn-primary">Update Budget</button-->
			</form>  
		</div>	  
    </div>
    <!-- /.container-fluid -->
	
	
	
</div>
<!-- /#page-wrapper -->
@stop

