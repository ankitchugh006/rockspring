@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8 col-sm-6 col-xs-6">   
                <form id="sort_form" method="GET" action="">
                    <!--div class="col-lg-3">

                        <div class="form-group">
                            <select  class="form-control sort_select" name="order_by">
                                <option value="created_at" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'created_at') {
                        echo 'selected';
                    }
                    ?>>-Sort By Date-</option>
                                <option value="title" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'title') {
                        echo 'selected';
                    }
                    ?>>-Sort By Title-</option>
                            </select>
                        </div>

                    </div-->

                    <div class="col-lg-4">

                        <div class="form-group">
                            <select  class="form-control sort_select" name="complete">
								
								 <option value="0" <?php
                                if (isset($_GET['complete']) && $_GET['complete'] == '0') {
                                    echo 'selected';
                                }
                                ?>>-Active-</option>

								
								<option value="" <?php
                                if (isset($_GET['complete']) && $_GET['complete'] == '') {
                                    echo 'selected';
                                }
                                ?>>-All-</option>

								
                               
                                <option value="2" <?php
                                if (isset($_GET['complete']) && $_GET['complete'] == '2') {
                                    echo 'selected';
                                }
                                ?>>-Inactive-</option>     

                                <option value="1" <?php
                                if (isset($_GET['complete']) && $_GET['complete'] == '1') {
                                    echo 'selected';
                                }
                                ?>>-Closed-</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-4">
                        <div class="form-group"  > 					
                            <input  class="form-control s_filter project_autocomplete" name="title" placeholder="Enter Project Title" value="<?php
                            if (isset($_GET['title'])) {
                                echo $_GET['title'];
                            }
                            ?>">					
                        </div> 

                    </div>	
                    <div class="col-xs-2">
                        <input  type="submit" class="btn btn-primary" value="Search">
                    </div>

                </form>
            </div>

            <div class="col-lg-4" style="text-align:right">
                <a href="<?php echo url() ?>/admin/projects/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New Project</button></a>
            </div>
        </div>



        <div class="row">
            <div class="col-lg-12">
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-projects tablesort">
                                <thead>
                                    <tr>
                                        <th class="project-count">Job #</th>
                                        <th class="project-name">Job Name</th>
                                        <!--th class="project-name">Foreman</th-->
                                        <th class="project-customer">Customer</th>
                                        <th class="project-customer">Super</th>
                                        <th class="project-customer">Foreman</th>
                                        <th class="project-customer">Estimator</th>
                                        <!--th class="project-date">Date Added</th-->

                                        <!--th class="project-date">Date Added</th-->
                                        <!--th class="project-completed" >Completed</th-->
                                        <th class="project-actions">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($projects)) {
                                        foreach ($projects as $u) {
                                            ?>
                                            <tr>
                                                <td><a href="<?php echo url() ?>/admin/projects/view/<?php echo ($u->manual_id) ?>"><?php echo $u->manual_id ?></a></td>
                                                <td style="text-transform:uppercase"><a href="<?php echo url() ?>/admin/projects/view/<?php echo $u->manual_id ?>"><?php echo $u->title ?></a></td>

                                                <td>
												
																<?php if($u->customer_id){ ?>
																	<a href="<?php echo url() ?>/admin/customers/edit/<?php echo  $u->customer_id; ?>"><?php echo !empty($u->customer_id) ? getCustomerName($u->customer_id) : 'N/A' ?></a>
																<?php }else{ ?>
																	N/A
																	<?php } ?>
															
													
													
												</td>
                                                <td>
                                                    <?php
                                                    echo $foremans = getProjectSuperintendents($u->id);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $foremans = getProjectForemans($u->id);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $foremans = getProjectEstimators($u->id);
                                                    ?>
                                                </td>

        <!--td><?php echo change_date_format($u->created_at) ?></td-->
                                                <!--td>
                                                <?php if ($u->is_completed == 1) { ?>
                                                                    <i onclick="change_complete_status('<?php echo $u->id ?>', '0')" class="fa fa-check u_active"></i>
                                                <?php } else { ?>
                                                                    <i onclick="change_complete_status('<?php echo $u->id ?>', '1')" class="fa fa-close u_inactive"></i>
                                                <?php } ?>
                                                </td-->
                                                <!--td>
                                                    <a href="<?php echo url() ?>/admin/projects/view/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-primary">View</button></a>	
                                                    <a href="<?php echo url() ?>/admin/projects/edit/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
                                                    <a onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo url() ?>/admin/projects/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                <?php if ($u->is_completed == 0) { ?>
                                                                    <a onclick="change_complete_status('<?php echo $u->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs  btn-success">Mark as Complete</button></a>									
                                                <?php } ?>
                                                </td-->
                                                <td>
													<?php  if(is_latest_worklog_available($u->id)){ ?>
															<a class="btn btn-primary btn-sm" href="<?php echo url() ?>/admin/report/latestreportdownload/<?php echo $u->id ?>">Latest Report</a>
													<?php } ?>	
													
													<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal<?php echo $u->id ?>">Upto Last 7 Days</a>
													
													
													
													<!--a style="color:#000" href="<?php echo url() ?>/admin/report/daterangereportdownload/<?php echo $u->id ?>">Last 7 Days</a-->
													
													
													<!--div class="dropdown">
													  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Download Report
													  <span class="caret"></span></button>
													  <ul class="dropdown-menu">
													    <?php
															$path='uploads/project_reports/'.base64_encode($u->id).'/'.$u['title'].'.pdf'; 
															if(file_exists($path)){ ?>

																<li><a style="color:#000" href="<?php echo url().'/'.$path ?>" download="<?php echo $u['title']; ?>"><button class="btn btn-xs btn-primary" type="button">Download Report</button></a></li>
															<?php 
															}
														?>
														<li><a style="color:#000" href="<?php echo url() ?>/admin/report/latestreportdownload/<?php echo $u->id ?>">Latest Report</a></li>
														<li><a style="color:#000" href="<?php echo url() ?>/admin/report/daterangereportdownload/<?php echo $u->id ?>">Last 7 Day</a></li>
													  </ul>
													</div-->
													<!-- Modal -->
                                           
												<div class="modal fade" id="myModal<?php echo $u->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												  <div class="modal-dialog" role="document">
													<div class="modal-content">
													  <div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabel"><b>Select Range</b></h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														  
														</button>
													  </div>
													  <div class="modal-body">
														 <form name="<?php echo $u->id ?>" id="<?php echo $u->id ?>" method="get" action="<?php echo url() ?>/admin/report/daterangereportdownload/<?php echo $u->id ?>" /> 
															<input placeholder="From" name="from_date" type="text" class="report_picker" required="required" value="<?php echo date('M d, Y',strtotime ('-7 days')) ?>" /> 
															<input placeholder="To" name="to_date" type="text"  class="report_picker2" required="required" value="<?php echo date('M d, Y')  ?>" />  
															<input value="submit" class="btn btn-primary" style="color:#fff" type="submit" />
														 </form>	
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														
													  </div>
													</div>
												  </div>
												</div>
														
												</td>	
													
                                            </tr>
                                            									
												
                                            <?php
                                        }
                                    }else{
										echo  '<tr><td colspan="6" align="center">No Projects Found</td></tr>';
									}
                                    ?>
                                </tbody>
                                <?php   if (count($projects)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="ts-pager form-horizontal">
                                            <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                            <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                            <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                            <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                            <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>

                                            <select class="pagenum input-mini" title="Select page number"></select>
                                        </th>
                                    </tr>
                                </tfoot>
                                <?php } ?>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
