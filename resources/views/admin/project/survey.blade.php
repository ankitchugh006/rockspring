@extends('layout.admin')

@section('content')
<style>
.table-hover > tbody > tr:hover {
    background: none repeat scroll 0 0 #fff !important;
}
</style>
<div id="page-wrapper">
<?php 
//echo '<pre>';
//print_r($survey->toArray());
//echo '</pre>';
 ?>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    <?php echo $project->title; ?>
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <h3>Survey Detail</h3>

                <div class="">
                    <ul class="project-estimate-list">
                        <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?></li>
                        
                        <hr style="color:#ccc;border: 1px solid;float: left;width: 100%;margin:0">
                     </ul>
                     
                     
                     <div class="row">

						<div class="col-lg-12">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h3 class="panel-title">Survey Detail</h3>
								</div>

								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-bordered table-hover table-striped table-project-survey">

											<thead>
												<tr>
													
													<th class="project-name">Survey Question</th>
													<th class="project-foreman">Submitted</th>
													<th class="project-date">Image</th>
													<th class="project-actions">Description</th>

												</tr>
											</thead>
											<tbody>
												<?php 
												    $i=0;
												    
													if(!empty($survey)){
													foreach($survey as $s){  $i++; ?>
													
														<tr>
														   
															<td><?php echo get_question_title($s->question_id) ?> </strong></td>
															<td>
															<?php if (($s->status == '1')) {
																	echo 'YES';
																} else if ($s->status == '0') {
																	echo 'NO';
																}else{
																	echo 'N/A';
																} 
															?>
															</td>

															<td style="text-align:center">
																<?php															
																	if (isset($s->image) && !empty($s->image) && (strpos($s->image,"default_survey") === false)) {
																		echo ' <a style="cursor:pointer" data-toggle="modal" data-target="#myModal'.$i.'">
																		<img style="width:60px;height:60px;" src="'.$s->image.'" alt="'.$s->description.'">
																		</a>';
																	} 
																?>
															</td>
															<td>
															<?php 
															
															if (isset($s->description) && !empty($s->description)) {
																echo $s->description;
															} ?>
																	
															</td>
														</tr>
														<!-- Modal -->
														<div class="modal fade" id="myModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														  <div class="modal-dialog" role="document">
															<div class="modal-content">
															  <div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																<h4 class="modal-title" id="myModalLabel">Image</h4>
															  </div>
															  <div class="modal-body">
																	<img  src="<?php echo $s->image ?>">
															  </div>
															  <div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																
															  </div>
															</div>
														  </div>
														</div>
										
																								
													<?php } } ?>
													</tbody>	   
												</table>

									</div>
								</div>
								

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
