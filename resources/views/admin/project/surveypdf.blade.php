
<div id="page-wrapper">
<?php 
//echo '<pre>';
//print_r($survey->toArray());
//echo '</pre>';
 ?>
    <div class="container-fluid">

   

        <div class="row">
            <div class="col-lg-12">
                <h3>Survey Detail</h3>

                <div class="">
                    <ul class="project-estimate-list" style="list-style-type:none">
						<li><strong>Job Name: </strong> <?php echo!empty($project->title) ? $project->title : ''; ?></li>
                        <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?></li>
                        
                        <hr style="color:#ccc;border: 1px solid;float: left;width: 100%;margin:0">
                     </ul>
                     
                     
                     <div class="row">

						<div class="col-lg-12">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h3 class="panel-title">Survey Detail</h3>
								</div>

								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-bordered table-hover table-striped table-project-survey">

											<thead>
												<tr>
													
													<th class="project-name">Survey Question</th>
													<th class="project-foreman">Submitted</th>
													<th class="project-date">Image</th>
													<th class="project-actions">Description</th>

												</tr>
											</thead>
											<tbody>
												<?php 
												    $i=0;
												    
													if(!empty($survey)){
													foreach($survey as $s){  $i++; ?>
													
														<tr>
														   
															<td><?php echo get_question_title($s->question_id) ?> </strong></td>
															<td>
															<?php if (($s->status == '1')) {
																	echo 'YES';
																} else if ($s->status == '0') {
																	echo 'NO';
																}else{
																	echo 'N/A';
																} 
															?>
															</td>

															<td style="text-align:center">
																<?php															
																	if (isset($s->image) && !empty($s->image) && (strpos($s->image,"default_survey") === false)) {
																		echo ' <a style="cursor:pointer" data-toggle="modal" data-target="#myModal'.$i.'">
																		<img style="width:60px;height:60px;" src="'.$s->image.'" alt="'.$s->description.'">
																		</a>';
																	} 
																?>
															</td>
															<td>
															<?php 
															
															if (isset($s->description) && !empty($s->description)) {
																echo $s->description;
															} ?>
																	
															</td>
														</tr>
														
										
																								
													<?php } } ?>
													</tbody>	   
												</table>

									</div>
								</div>
								

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>

