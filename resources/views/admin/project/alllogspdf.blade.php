<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="project-estimate">
                    <ul class="project-estimate-list" style="list-style-type:none">
						<li><strong>Job Name: </strong> <?php echo!empty($project->title) ? $project->title : ''; ?></li>
                        <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></li>
                        <li><strong>Total Project Requests: </strong> <?php echo $data['requests']; ?></li>
                        <li><strong>Total hours Logged: </strong> <?php echo $data['hourslogged']; ?></li>
                        <li><strong>Total hours Estimated: </strong> <?php echo $data['total_estimated_hours']; ?></li>
                    </ul>
                </div>
            </div>
            <br/><br/>
            <div class="col-lg-12">
                <div class="panel panel-primary fullwidth">
                    <div class="panel-heading">
                        <h3 class="panel-title">Log Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-individual-worklogs">

                                <thead>
                                    <tr>
                                        
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foreman</th>
                                      
                                        <th class="project-resources">Resources</th>
                                        <th class="project-resources">Quantity</th>
                                        <th class="project-resources">Hours Logged</th>
										  <!--th class="project-foreman">Image</th-->
                                        
                                        <th class="project-description">Description</th>
                                        <th class="project-date">Log Date</th>
                                        <!--th class="project-actions">Action</th-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($logs)) {
                                        foreach ($logs as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                               <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo get_resource_title($u->resource_id) ?></td>
                                                <td><?php echo ($u->quantity) ?></td>
                                                <td><?php echo ($u->hours_logged) ?></td>
                                                
                                                
                                                <td><?php echo $u->description ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                
												</td>
                                            </tr>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
	</div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
