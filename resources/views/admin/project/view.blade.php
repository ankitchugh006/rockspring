@extends('layout.admin')

@section('content')
<?php
?>

<style>
.huge {
    font-size: 14px;
    font-weight: bold;
}
.project-estimate .panel{
	margin-bottom:0;
}
.col-xs-121 > div:last-child {
    min-height: 70px;
}
.project-estimate .col-xs-121 div span:nth-of-type(2){
	padding:0;
}
.member_table tr td{
	padding:5px;
}
.upper{
	min-height: 266px !important;
}
#complete_drop .dropdown-menu a:hover{
	color:#fff !important
}
#complete_drop .dropdown-menu{
	right:0;
	left:auto;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <a href="<?php echo url() ?>/admin/projects"><?php echo ($project->manual_id) ?> - <?php echo ($project->title) ?></a>
                    
                    <div class="panel-body" style="float:right;padding:0">
							
							<a href="<?php echo url() ?>/admin/projects/edit/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
							<a onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo url() ?>/admin/projects/delete/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
							
							<!--label style="font-size:14px">Change Status:
								<select name="change_project_status" id="change_project_status" onChange="change_complete_status('<?php echo $project->id ?>', this.value)">
									<option value="0" <?php if($project->is_completed == 0) {  echo 'selected';} ?>>Active</option>
									<option value="2" <?php if($project->is_completed == 2) {  echo 'selected';} ?>>Inactive</option>
									<option value="1" <?php if($project->is_completed == 1) {  echo 'selected';} ?>>Complete</option>
								</select>
							</label-->
						
							<div id="complete_drop" class="dropdown" style="display: inline;cursor: pointer;">
							 <button style="cursor:pointer;padding: 1px 10px;" class="dropdown-toggle btn btn-xs btn-success" type="button" data-toggle="dropdown" aria-expanded="false">Change Status
							  <span class="caret"></span></button>
							  <ul class="dropdown-menu">
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;">Active <?php if($project->is_completed == 0)   {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '2')" href="javascript:;">Inactive <?php if($project->is_completed == 2) {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
								<li><a style="color:#333" onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;">Complete <?php if($project->is_completed == 1) {  echo '<i class="fa fa-check pull-right"></i>';} ?></a>	</li>
							  </ul>
							</div>
							<?php if ($project->is_completed == 0) { ?>
								<!--a onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs  btn-success">Mark as Complete</button></a-->									
							<?php }else{ ?>
								<!--a onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs  btn-info">Mark as Incomplete</button></a-->	
							<?php } ?>
					</div>
                    
                </h1>
                <?php echo display_breedcrump(); ?>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <div class="">

                <div class="project-estimate">

<!--                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-121 ">
                                        <div class="huge">Job #</div>
                                        <div><?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Project Team</div>
                                                            <div class="upper">
																	<table class="table table-bordered member_table" cellpadding="5">
                                                                    <?php
                                                                    $team=getProjectTeam($project->id);
                                                                    
                                                                    if (!empty($team)) {
                                                                            foreach ($team as $u) {
                                                                                   echo '<tr><td style="text-transform:capitalize">'.get_name($u->foreman_id).'</td><td style="text-transform:uppercase">'.$u->type.'</td></tr>';
                                                                            }
                                                                    }
                                                                    ?>
                                                                    </table>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 " style="text-transform:capitalize">
                                                            <div class="huge">Customer Project Team <span style="float:right"><i>Customer - </i><a style="color:#fff;float:none;font-size:16px;line-height:0" href="<?php echo url() ?>/admin/customers/edit/<?php echo  $project->customer_id; ?>"><?php echo getCustomerName($project->customer_id) ?></a></span></div>
                                                            <div class="upper">
																<table class="table table-bordered member_table" cellpadding="5">
                                                                    <?php
                                                                    $team=getProjectCustomerTeam($project->id);
                                                                    if(!empty($team)){
																		foreach($team as $t){
																			echo '<tr><td>'.getMemberName($t->member_id).'</td><td>'.getMemberTitle($t->member_id).'</td><td>'.getMemberEmail($t->member_id).'</td><td>'.getMemberPhone($t->member_id);'</td></tr>';
																		}
																	}else{
																		echo 'N/A';
																	}
                                                                    ?>
                                                                 </table>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Job Address</div>
                                                            <div><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>

                    
                    <div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                        <div class="huge"><span>Total hours Logged</span><span>Total hours Estimated 
                                                        <a href="<?php  echo url(); ?>/admin/projects/estimate/<?php echo $project->manual_id ?>">
															<button class="btn btn-xs btn-danger" type="button" style="font-size: 9px;font-weight: bold;padding: 1px 2px;">View Budget</button>
														</a>
                                                        </span></div>
                                                        <div><span><?php echo $hourslogged; ?></span><span><?php echo $total_estimated_hours; ?></span></div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div>
                    

                    <!--div class="col-md-6">
                            <div class="panel">
                                    <div class="panel-heading">
                                            <div class="row">

                                                    <div class="col-xs-121 ">
                                                            <div class="huge">Customer</div>
                                                            <div>
																<?php if($project->customer_id){ ?>
																<a style="float:left" href="<?php echo url() ?>/admin/customers/edit/<?php echo  $project->customer_id; ?>"><?php echo !empty($project->customer_id) ? getCustomerName($project->customer_id) : 'N/A' ?></a>
																<?php } ?>
															</div>
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                    </div-->
                    

<!--                    <a href="" style="color:#333">
                        <div class="col-md-6">
                                <div class="panel">
                                        <div class="panel-heading">
                                                <div class="row">

                                                        <div class="col-xs-121 ">
                                                                <div class="huge">Total hours Estimated</div>
                                                                <div><?php echo $total_estimated_hours; ?></div>
                                                        </div>
                                                </div>
                                        </div>

                                </div>
                        </div>
                    </a>-->
                </div>
            </div>


			<!--div class="col-lg-12">
				 <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Actions</h3>
                    </div>
                    
                    <div class="panel-body">
							
							<a href="<?php echo url() ?>/admin/projects/edit/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
							<a onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo url() ?>/admin/projects/delete/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
							<?php if ($project->is_completed == 0) { ?>
								<a onclick="change_complete_status('<?php echo $project->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs  btn-success">Mark as Complete</button></a>									
							<?php }else{ ?>
								<a onclick="change_complete_status('<?php echo $project->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs  btn-info">Mark as Incomplete</button></a>	
							<?php } ?>
					</div>
                 </div>
			</div-->
				 
          
            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Worklogs
                        <a href="<?php echo url() ?>/admin/projects/projectlogs/<?php echo $project->manual_id; ?>"  style="float:right"><button type="button" class="btn btn-xs btn-danger">View All<i class="fa fa-chevron-circle-right"></i></button></a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">

                                <thead>
                                    <tr>
                                        
                                        
                                        <th width="14%">Foreman</th>
                                        <th>Project Description</th>
                                        <th>Comments</th>
                                        <th>Image</th>
                                        <th>Log Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($projectlogs) > 0) {
                                        $i = 0;
                                        foreach ($projectlogs as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                               
                                                
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo $u->description ?></td>
                                                <td><?php echo $u->additional_comments ?></td>
                                                <td>
                                                    <?php
                                                    if (isset($u->photo) && !empty($u->photo)) {
                                                        $a = (explode('/', $u->photo));
                                                        echo '<a style="cursor:pointer" data-toggle="modal" data-target="#imgModal'.$i.'"  ><span style="color:#337ab7">View Image</span></a>';
                                                    }
                                                    ?>
                                                     <!-- Modal -->
												<div id="imgModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													<!-- Image Model content-->
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
															<img src="<?php  echo $u->photo; ?>" />
																
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
													
                                                </td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td><a href="<?php echo url() ?>/admin/projects/logs/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View Log Detail</button></a></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="8" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Surveys
                        <a href="<?php echo url() ?>/admin/projects/projectsurveys/<?php echo $project->manual_id; ?>"  style="float:right"><button type="button" class="btn btn-xs btn-danger">View All<i class="fa fa-chevron-circle-right"></i></button></a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">

                                <thead>
                                    <tr>
                                      
                                       
                                        <th width="14%">Foreman</th>
                                        <th>Date</th>
                                        <th>Images</th>
                                        <th>Actions</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($surveys) > 0) {
                                        foreach ($surveys as $u) {
                                            ?>
                                            <tr>
                                               
                                                
                                                <td><?php echo get_name($u->foreman_id) ?></td>

                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
													<a href="javascript:;"  data-toggle="modal" data-target="#imgModal<?php echo $u->id ?>">View Images</a>
													<div id="imgModal<?php echo $u->id ; ?>" class="modal fade" role="dialog">
													  <div class="modal-dialog">

														<!-- Image Model content-->
														<div class="modal-content">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Images</h4>
														  </div>
														  <div class="modal-body">
															    <?php
															    $images=getSurveyMainImages($u->id);
															       if($images != NULL){
																	   
																		$arr=explode(',',$images);
																		
																		foreach($arr as $a){
																			echo '<img src="'.$a.'" /><br/>';
																		}
																	}else{
																		echo 'No images found';
																	}	
																	 ?>
																
														  </div>
														  <div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														  </div>
														</div>

													  </div>
													</div>
                                                </td>
                                                <td><a href="<?php echo url() ?>/admin/projects/survey/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View  Detail</button></a></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Survey Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
          
          <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
						Tasks
						<a href="javascript:;" data-toggle="modal" data-target="#taskModal" style="float:right"><button type="button" class="btn btn-xs btn-danger">Add Task<i class="fa fa-chevron-circle-right"></i></button></a>
						 <a href="<?php echo url() ?>/admin/projects/project_tasks/<?php echo $project->manual_id; ?>"  style="float:right;margin-right: 14px;"><button type="button" class="btn btn-xs btn-danger">View All<i class="fa fa-chevron-circle-right"></i></button></a>
						</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">

                                <thead>
                                    <tr>
                                        <th class="project-count" width="14%">Photo Thumbnail</th>
                                        <th class="project-name">Task</th>
                                        <th class="project-name">Description</th>
                                        
                                        <th class="project-customer">Added By</th>
                                        <th class="project-date">Date Added</th>
                                        <th class="project-date">Status</th>
                                        <th class="project-date">Take Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=0;
                                    if (count($tasks) > 0) {
                                        foreach ($tasks as $u) {
											$i++;
                                            ?>
                                            <tr>
												<td>
                                                    <?php
                                                    if (isset($u->task_image) && !empty($u->task_image)) {
                                                        $a = (explode('/', $u->task_image));
                                                        echo '<a style="cursor:pointer" data-toggle="modal" data-target="#taskModal'.$i.'"  ><span style="color:#337ab7">View Image</span></a>';
                                                    }
                                                    ?>
                                                     <!-- Modal -->
												<div id="taskModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													<!-- Image Model content-->
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
															<img src="<?php  echo $u->task_image; ?>" />
																
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
													
                                                </td>	
                                                
                                                <td><?php echo $u->task ?></td>
                                                <td><?php echo $u->description ?></td>
                                                
												<td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
                                                <?php if ($u->status == 1) { ?>
                                                                <span  class="status-active">Resolved</span>
                                                                
                                                <?php } else { ?>
                                                               <span  class="status-active">Open</span>
                                                <?php } ?>
                                                </td>
                                                <td>
                                                    
                                                    <?php if ($u->status == 1) { ?>
                                                                <a onclick="change_task_status('<?php echo $u->id ?>', '0')" href="javascript:;"><button type="button" class="btn btn-xs btn-warning">Mark as Open</button></a>
                                                    <?php } else { ?>
                                                                    <a onclick="change_task_status('<?php echo $u->id ?>', '1')" href="javascript:;"><button type="button" class="btn btn-xs btn-info">Mark as Resolved</button></a>
                                                    <?php } ?>
                                                    
                                                   <a onclick="return confirm('Are you sure you want to delete this task?')" href="<?php echo url(); ?>/admin/projects/tasks/delete/<?php echo $u->id; ?>" ><button class="btn btn-xs btn-danger" type="button">Delete</button></a>
                                                   
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Tasks Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
          
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                       
                        <h3 class="panel-title"></i> 
                        Files
						<a href="javascript:;" data-toggle="modal" data-target="#myModal" style="float:right"><button type="button" class="btn btn-xs btn-danger">Add File<i class="fa fa-chevron-circle-right"></i></button></a>
						<a href="<?php echo url() ?>/admin/projectfiles/<?php echo $project->manual_id; ?>"  style="float:right;margin-right: 14px;"><button type="button" class="btn btn-xs btn-danger">View All<i class="fa fa-chevron-circle-right"></i></button></a>
						</h3>
                        
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                       
                                       
                                        <th>File</th>
										<th width="14%">Decription</th>
                                        <th>Uploaded by</th>
                                        <th>Categories</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (count($files)) {
                                        foreach ($files as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                               
                                               
                                                <td>
													<?php
													$ext='';
													if(!empty($u->file)){
														$info     = get_headers($u->file, true); 
														$ext=explode('.',$u->file);
														//echo '<pre>';
														//print_r($info);
														
													 }
													 ?>
												<?php  if(!empty($u->file) && UR_exists($u->file)){ 
													if($info['Content-Type']=="image/png"  || $info['Content-Type']=="image/jpeg"){ ?>
														<a style="cursor:pointer" data-toggle="modal" data-target="#viewfileModal<?php echo $i; ?>"  ><span style="color:#337ab7">View File</span></a>
                                                  
															 <!-- Modal -->
														<div id="viewfileModal<?php echo $i; ?>" class="modal fade" role="dialog">
														  <div class="modal-dialog">

															<!-- Image Model content-->
															<div class="modal-content">
															  <div class="modal-header">
																<button type="button" class="close" data-dismiss="modal">&times;</button>
																<h4 class="modal-title">File</h4>
															  </div>
															  <div class="modal-body">
																	<img src="<?php  echo $u->file; ?>" />
																		
																	
															  </div>
															  <div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															  </div>
															</div>

														  </div>
														</div>
													<?php }else{
													?>	
													<a  target="_blank" href="<?php echo ucfirst($u->file) ?>">View File</a>
												 <?php } } ?>
												 </td>
                                                 <td><?php echo ucfirst($u->title) ?></td>
                                                 <td><?php echo get_name($u->added_by) ?></td>
                                              
                                                <td>
                                                    
                                                    
                                                    
                                                    <?php 
                                                        $a='';
                                                        if(!empty($u->category_ids)){
                                                            $cat=explode(',',$u->category_ids);
                                                            
                                                            foreach($cat as $id){
                                                                $a .= getFileCategoryTitle($id).',';
                                                            }
                                                            echo rtrim($a,',');
                                                        }else{
															$a='N/A';
														}
                                                    ?>
                                                </td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
                                                    <a onclick="return confirm('Are you sure you want to delete this file?')" href="<?php echo url() ?>/admin/files/delete/<?php echo $u->id ?>/<?php echo $project->manual_id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td>
                                            </tr>
                                        <?php }
                                    }else{
                                        echo '<tr><td colspan="8" align="center">No Files Found</td></tr>';
                                    }
                                    ?>
                                </tbody>
                               
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
       
            
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
	
	
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Add File</h4>
		  </div>
		  <div class="modal-body">
			<div class="">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/files/save" enctype="multipart/form-data">
							<div class="form-group">
                                <label>Choose File</label>
                                <input type="file" class="" name="file" placeholder="title" value="{{ old('name') }}" required>
							</div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="title" placeholder="Name" value="{{ old('title') }}" required>
                            </div>
                          
							
						     <input type="hidden" name="project" value="<?php echo $project->id; ?>">
						     <input type="hidden" name="redirect" value="1">
                             <div class="form-group select-foreman">
                              <?php $categories=getFileCategories(); ?>
                                 <label>Select Category</label>
                                <select multiple class="form-control choosen_select" name="categories[]" required>
                                    <?php
                                    if (!empty($categories)) {
                                        foreach ($categories as $u) {
                                            ?>
                                            <option value="<?php echo $u->id ?>"><?php echo $u->category; ?></option>

                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            
                            
                           
                           
							
                            <input type="hidden"  name="manual_id" value="<?php echo $project->manual_id ?>">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	
	
	<!-----------------TASK MODEL--------------------------->
	
	
	<!-- Modal -->
	<div id="taskModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Add Task</h4>
		  </div>
		  <div class="modal-body">
			<form enctype="multipart/form-data" action="<?php echo url(); ?>/admin/projects/tasks/save" method="POST" role="form">

				<div class="form-group">
					<label>Task</label>
					<input type="text" value="" placeholder="Task" name="task" class="form-control">
				</div>
				<div class="form-group">
					<label>Task Thumbnail</label>
					<input type="file" value="" placeholder="title" name="task_image" class="">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea placeholder="Description" name="description" class="form-control"></textarea>
				</div>
				<input type="hidden" name="project" value="<?php echo $project->id ?>" />
				<input type="hidden"  name="manual_id" value="<?php echo $project->manual_id ?>">
			    <input type="hidden" name="redirect" value="1">
				
				<input type="hidden" name="foreman" value="<?php echo Auth::user()->id; ?>">
				
	
				
				<button class="btn btn-primary" type="submit">Save</button>
			</form>
                   
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	
</div>
<!-- /#page-wrapper -->
@stop
