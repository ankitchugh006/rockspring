@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Projects Requests
                        <a style="float:right" href="<?php echo url(); ?>/admin/projects/view_all_requests"><button class="btn btn-xs btn-danger" type="button">View All<i class="fa fa-chevron-circle-right"></i></button></a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort" >
                                <thead>
                                    <tr>
                                        
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foremen</th>
                                       
                                        <th class="project-details">Details/Name Request</th>
                                        <th class="project-req-material">Requested Material</th>
                                        <th class="project-comment">Additional Comments</th>
                                        <th class="project-req-on">Request On</th>
                                        <th class="project-actions">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($requests)) {	
                                        foreach ($requests as $u) {
                                            ?>
                                            <tr>
                                               
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                 
                                                <td><?php echo ($u->comment1) ?></td>
                                                <td><?php echo ($u->comment2) ?></td>
                                                <td><?php echo ($u->comment3) ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td><a href="<?php echo url() ?>/admin/projects/requests/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View Request Detail</button></a></td>
                                            </tr>
                                        <?php }
                                    }else{
										echo '<tr><td colspan="7" align="center" style="text-transform: inherit;">No requests found for today</td></tr>';
									}
                                    ?>
                                </tbody>
                                <?php if (count($requests)) {	 ?>
                                <tfoot>
									<tr>
									  <th colspan="7" class="ts-pager form-horizontal">
										<button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
										<button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
										<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
										<button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
										<button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
										
										<select class="pagenum input-mini" title="Select page number"></select>
									  </th>
									</tr>
                                </tfoot>
                                <?php } ?>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
