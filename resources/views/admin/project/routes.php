<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'admin\HomeController@index'
]);

Route::group(['prefix' => 'api'], function () {
    Route::post('login', [
        'as' => 'login',
       // 'uses' => 'api\ApiAuthController@login'
        'uses' => 'ApiAuthController@login',
        'middleware' => 'ApiAuth'
    ]);
    Route::post('register', [
        'as' => 'register',
        'uses' => 'ApiAuthController@register',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('getprofile', [
        'as' => 'getprofile',
        'uses' => 'ApiAuthController@get_profile',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('updateprofile', [
        'as' => 'updateprofile',
        'uses' => 'ApiAuthController@update_profile',
        'middleware' => 'ApiAuth'
    ]); 
    
	Route::post('logout', [
        'as' => 'logout',
        'uses' => 'ApiAuthController@logout',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('recoverpassword', [
        'as' => 'recoverpassword',
        'uses' => 'ApiAuthController@recoverpassword',
        'middleware' => 'ApiAuth'
    ]); 
    Route::get('update_password', [
        'as' => 'update_password',
        'uses' => 'ApiAuthController@update_password',
        'middleware' => 'ApiAuth'
    ]); 	
	Route::post('changepassword', [
        'as' => 'changepassword',
        'uses' => 'ApiAuthController@changepassword',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('change_password_app', [
        'as' => 'update_password_app',
        'uses' => 'ApiAuthController@update_password_app',
        'middleware' => 'ApiAuth'
    ]); 
    
    
    /**********projects api*************/
    Route::post('myprojects', [
        'as' => 'myprojects',
        'uses' => 'ApiProjectController@myprojects',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('requestresources', [
        'as' => 'requestresources',
        'uses' => 'ApiProjectController@request_resources',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('worklog', [
        'as' => 'worklog',
        'uses' => 'ApiProjectController@worklog',
        'middleware' => 'ApiAuth'
    ]); 
    
    Route::post('submitworklog', [
        'as' => 'submitworklog',
        'uses' => 'ApiProjectController@submit_worklog',
        'middleware' => 'ApiAuth'
    ]); 
    Route::post('submitdailysurvey', [
        'as' => 'submitdailysurvey',
        'uses' => 'ApiProjectController@submit_daily_survey',
        'middleware' => 'ApiAuth'
    ]);
    
    		
});

// route to show the login form
Route::group(array('namespace'=>'admin'), function()
{
    Route::get('/', 'LoginController@index');
    Route::get('/login', 'LoginController@index');
	Route::post('/admin/login', 'LoginController@login');
	
	Route::get('/admin/logout', 'LoginController@logout');
	Route::get('/admin', 'HomeController@index');
	Route::get('/admin/users', 'UserController@index');
	Route::get('/admin/users/edit', 'UserController@index');
	Route::get('/admin/users/edit/{id}', 'UserController@edit');
	Route::post('/admin/users/update', 'UserController@update');
	
	Route::get('/admin/users/add', 'UserController@add');
	Route::post('/admin/users/save', 'UserController@save');
	Route::post('/admin/users/change_status', 'UserController@change_status');
	
	Route::get('/admin/users/delete/{id}', 'UserController@delete');
	
	Route::get('/admin/myaccount/', 'AccountController@index');
	Route::post('/admin/myaccount/update', 'AccountController@update');
	Route::get('/admin/myaccount/resetpassword', 'AccountController@resetpassword');
	Route::post('/admin/myaccount/reset', 'AccountController@reset');
	
	Route::get('/admin/projects/', 'ProjectController@index');
	Route::get('/admin/projects/add', 'ProjectController@add');
	Route::post('/admin/projects/save', 'ProjectController@save');
	
	Route::get('/admin/projects/edit/{id}', 'ProjectController@edit');
	Route::get('/admin/projects/delete/{id}', 'ProjectController@delete');
	Route::get('/admin/projects/view/{id}', 'ProjectController@view');
	Route::get('/admin/projects/view', 'ProjectController@index');
	
	Route::post('/admin/projects/update', 'ProjectController@update');
	Route::post('/admin/projects/remove_resources', 'ProjectController@remove_resources');
	
	Route::post('/admin/notification/unhighlight', 'NotificationController@unhighlight');
	
	Route::get('/admin/projects/requests/{id}', 'ProjectController@projectrequests');
	Route::get('/admin/projects/requests', 'ProjectController@allrequests');
	
	Route::get('/admin/projects/logs/{id}', 'ProjectController@logs');
	Route::get('/admin/projects/logs', 'ProjectController@all_logs');
	
	Route::get('/admin/projects/survey/{id}', 'ProjectController@survey');
	Route::get('/admin/projects/survey', 'ProjectController@all_survey');
	
	Route::post('/admin/projects/change_complete_status', 'ProjectController@change_complete_status');
	
	Route::get('/admin/manpowers', 'ManpowerController@index');
	Route::get('/admin/manpowers/add', 'ManpowerController@add');
	Route::post('/admin/manpowers/save', 'ManpowerController@save');
	Route::get('/admin/manpowers/edit/{id}', 'ManpowerController@edit');
	Route::get('/admin/manpowers/delete/{id}', 'ManpowerController@delete');
	Route::post('/admin/manpowers/update', 'ManpowerController@update');
	
	Route::get('/admin/forgotpassword', 'ForgotPasswordController@index');
	Route::post('/admin/forgotpassword/send', 'ForgotPasswordController@send');
	
});




Route::get('home', 'HomeController@index');


