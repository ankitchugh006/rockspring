@extends('layout.admin')

@section('content')
<?php
?>
<style>
.readonly_check{
	pointer-events:none !important;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
                <?php echo display_breedcrump(); ?>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
				<div class="row">
                <form role="form" method="POST" action="<?php echo url(); ?>/admin/projects/update">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label>Job Name</label>
                        <input class="form-control" name="title" placeholder="title" value="<?php if(old('title')) { echo (old('title')); } else { echo  $project->title; } ?>">
                    </div> 
                    <div class="form-group">
                        <label>Job Address</label>
                        <textarea  class="form-control" name="description" placeholder="description"><?php echo !empty (old('description')) ? old('description') : $project->description ; ?></textarea>
                    </div>
                    
                     <div class="form-group">
                        <label>City</label>
                        <input class="form-control" name="city" placeholder="<?php echo 'City'?>" value="<?php echo !empty (old('city')) ? old('city') : $project->city ; ?>">
                    </div>
                    
                     <div class="form-group">
                        <label>State</label>
                        <input class="form-control" name="state" placeholder="<?php echo 'State'?>" value="<?php echo !empty (old('state')) ? old('state') : $project->state ; ?>">
                    </div>
                    
                     <div class="form-group">
                        <label>Zip</label>
                        <input class="form-control" name="zip" placeholder="<?php echo 'Zip'?>" value="<?php echo !empty (old('zip')) ? old('zip') : $project->zip ; ?>">
                    </div>
                    
                    
                    <h3 style="color:#337ab7;">Select Customer For Project</h3>
                    <hr style="border:1px solid;">
                    
                    <?php 
						$customers=getCustomers();
					
						$customer_id = \Session::get('customer');
                     ?>
                    <div class="form-group">
                        <label>Customer</label>
                          <select class="form-control choosen_select" name="customer" id="project_customer">
                            <option  value="">--Select Customer--</option>
                            <?php if(count($customers)){
                                foreach($customers as $c){
                                    $a='';
                                    if($project->customer_id==$c->id || $customer_id == $c->id){
                                        
                                        $a='selected';
                                    }
                                    echo '<option value="'.$c->id.'" '.$a.'>'.$c->customer.'</option>';
                                }

                            } ?>
                        </select>
                    </div>
					<div class="form-group" id="project_customer_members1" style="float:left;width:100%">
                    
						<?php 
						$jh=0;
						
						$members=getProjectCustomerMembers($project->id);
						$allmembers=getAllCustomerMembers($project->customer_id);
						
						$allmemberswc=getAllCustomerMembersWc();
						$members_session= \Session::get('member_customer');
						$members_check_session= \Session::get('member_customer_check');
						
						if(!empty($members_session)){ ?>
							<table id="member_row_inner"  class="table table-hover table-striped member_table_'+jh+'">
								<tr>
								<th>Report?</th>
								<th>Member</th>
								<th>Title</th>
								<th>Email</th>
								<th>phone</th>
								<th>
								<div class="add-member" id="add-<?php echo $jh ?>">
								<a href="javascript:;" onclick="member_add('<?php echo $jh ?>')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
								</th>
								</tr>
								
								<?php 
								foreach($members_session as $key=>$m){ 
									$jh++;
									?>
									<tr id="member_row_<?php echo $jh ?>" class="member_row">
									<td>
									<input type="checkbox" name="member_customer_check[<?php echo $jh ?>]" value='<?php echo $jh ?>'<?php if(in_array($key,$members_check_session)){ echo 'checked'; } ?> >
									</td>

									<td>
									<select id="member-<?php echo $jh ?>" class="form-control choosen_select" name="member_customer[<?php echo $jh ?>]" onChange="getMemberData(this.value,<?php echo $jh ?>)">
										<option value="">-Members-</option>
										<?php  if(!empty($allmemberswc)){ 
											foreach($allmemberswc as $am){ ?>
												<option value="<?php echo $am->id; ?>"  
												
												<?php if($am->id==$m) { echo 'selected'; } ?>
												><?php echo $am->name; ?></option>
											<?php }
											?>
											
										<?php } ?>
										
									</select>
									</td>


									<td>
										<span id="title-<?php echo $jh ?>"><?php echo getMemberTitle($m) ?></span>
									</td>


									<td>
										<span id="email-<?php echo $jh ?>"><?php echo getMemberEmail($m) ?></span>
									</td>

									<td>
										<span id="phone-<?php echo $jh ?>"><?php echo getMemberPhone($m) ?></span>
									</td>

									<td>
									<a href="javascript:;" onclick="member_remove('<?php echo $jh ?>')"><span class="glyphicon glyphicon-minus" style="color:red"></span></a>
									</td>
									</tr>
									
							<?php }
							?>
							
						
						<?php }else {
							if(!empty($members)){ ?>
								<table id="member_row_inner"  class="table table-hover table-striped member_table_'+jh+'">
								<tr>
								<th>Report?</th>
								<th>Member</th>
								<th>Title</th>
								<th>Email</th>
								<th>phone</th>
								<th>
								<div class="add-member" id="add-<?php echo $jh ?>">
								<a href="javascript:;" onclick="member_add('<?php echo $jh ?>')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
								</th>
								</tr>
								
								<?php 
								foreach($members as $m){ 
									$jh++;
									?>
									<tr id="member_row_<?php echo $jh ?>" class="member_row">
									<td>
									<input type="checkbox" name="member_customer_check[]" value='<?php echo $jh ?>' <?php if(!empty($m->report) && $m->report==1){ echo 'checked'; } ?>>
									</td>

									<td>
									<select id="member-<?php echo $jh ?>" class="form-control choosen_select" name="member_customer[<?php echo $jh ?>]" onChange="getMemberData(this.value,<?php echo $jh ?>)">
										<option value="">-Members-</option>
										<?php  if(!empty($allmembers)){ 
											foreach($allmembers as $am){ ?>
												<option value="<?php echo $am->id; ?>"  
												
												<?php if($am->id==$m->member_id) { echo 'selected'; } ?>
												><?php echo $am->name; ?></option>
											<?php }
											?>
											
										<?php } ?>
										
									</select>
									</td>


									<td>
										<span id="title-<?php echo $jh ?>"><?php echo getMemberTitle($m->member_id) ?></span>
									</td>


									<td>
										<span id="email-<?php echo $jh ?>"><?php echo getMemberEmail($m->member_id) ?></span>
									</td>

									<td>
										<span id="phone-<?php echo $jh ?>"><?php echo getMemberPhone($m->member_id) ?></span>
									</td>

									<td>
									<a href="javascript:;" onclick="member_remove('<?php echo $jh ?>')"><span class="glyphicon glyphicon-minus" style="color:red"></span></a>
									</td>
									</tr>
							<?php } } } ?>
							</table>
							<input type="hidden" id="jh" value="<?php echo $jh; ?>">
                    </div>
                    <!--div class="form-group">
                        <label>Customer</label>
                        <input class="form-control" name="customer" placeholder="Customer" value="<?php echo!empty($project->customer) ? $project->customer : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea  class="form-control" name="address" placeholder="Address"><?php echo!empty($project->address) ? $project->address : ''; ?></textarea>
                    </div-->
                    
                    <?php 
                    $scope=explode(',',$project->scope);
                    $scope_session = \Session::get('scope');
                    if(!empty($scope_session)){
						$scope=$scope_session;
					}
                    
                    ?>
                    
                    <h3 style="color:#337ab7;">Scope</h3>
                    <hr style="border:1px solid;">
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="demo" <?php  if(in_array('demo',$scope)){ echo 'checked'; }?> name="scope[]">Demo                            
                            </label>
                        </div>
                         <div class="checkbox">
                            <label>
                                <input type="checkbox" value="drywall" <?php  if(in_array('drywall',$scope)){ echo 'checked'; }?> name="scope[]">Drywall                            
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="paint"  <?php  if(in_array('paint',$scope)){ echo 'checked'; }?> name="scope[]">Paint                            
                            </label>
                        </div>
                    </div>
                     
                    
                    </div>
                    <div class="col-lg-12">
                    <h3 style="color:#337ab7;">Budget</h3>
                    <hr style="border:1px solid;">
                    
                    
                    <div class="table">
                        <table class="table" id="projectTable">
							<tr>
								
								<td><b>Manpower Types</b></td>
								<td><b>Available Hours</b></td>
								<!--td><b>Subcontractors</b></td-->
							</tr>
							<?php 
							
							
							/*
							 if (!empty($assignedresources)) {
                                $i = 0;
                                foreach ($assignedresources as $m) {
									$i++;
									 ?>
							<tr>
								<td>
									<div class="form-group">
												<input required class="form-control"  type="text" name="resources[<?php echo $i ?>]" value="<?php echo $m['manpower_id'] ?>" placeholder="manpower" readonly>
									</div>
								</td>
								<td>
									<input style="width:50px;"  class="form-control" type="text" name="max_hours[<?php echo $i ?>]" value="<?php echo $m['max_hours'] ?>" readonly>
								</td>
								
							</tr>
							
							<?php } } */?>

                            <?php
                            if (!empty($manpowers)) {
                                $i = 0;
                                foreach ($manpowers as $m) {
                                    $i++;
                                    $has_manpower = project_has_resource($assignedresources, $m->id);
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label 
														<?php 
														if (($has_manpower >= '0')) {
                                                            echo 'class="readonly_check"';
                                                        } ?>>
                                                        <input class="manpower_check" id="check-<?php echo $m->id ?>" onClick="validateText(<?php echo $m->id ?>)"  type="checkbox" name="resources[<?php echo $m->id ?>]" value="<?php echo $m->id; ?>" <?php
                                                        if (($has_manpower >= '0')) {
                                                            echo 'checked readonly class="readonly_check"';
                                                        }
                                                        ?> ><?php echo $m->title; ?>

                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
											<span>Available Hours</span>
                                            <input  id="input-<?php echo $m->id ?>" style="width:40px;margin-left:10%;" <?php
                                            if (($has_manpower == '' && $has_manpower == 0)) {
                                                echo 'readonly';
                                            }
                                            ?> type="text" name="max_hours[<?php echo $m->id ?>]" value="<?php
                                                   if (($has_manpower >= 0)) {
                                                       echo $has_manpower;
                                                   }
                                                   ?>">
                                        </td>
										<!--td>
											<select  data-placeholder = "-Select Contractors-" class="form-control choosen_select" multiple name="contractors[<?php echo $m->id ?>]" id="contractor_select<?php echo $m->id ?>" onChange="setContractorHidden(this,<?php echo $m->id ?>)" >
											<?php 
												$resoucce_idd=get_project_resource_id_from_manpower_id($project->id,$m->id);
												$contractors=getSubcontractors();
												$assignresourcecontractors=getresourceContractors($project->id,$resoucce_idd);
												if(count($contractors)){
													foreach($contractors as $c){ ?>
														<option value="<?php echo $c->id; ?>" <?php if(in_array($c->id,$assignresourcecontractors)) { echo 'selected'; } ?>><?php echo $c->subcontractor; ?></option>
													<?php }
												}
											 ?>
											 </select>
                                        </td>
										<input type="hidden" id="contractor_hidden<?php echo $m->id ?>" name="contractor_hidden[<?php echo $m->id ?>]" value="<?php echo implode(',',$assignresourcecontractors); ?>" /--> 
                                        <?php
                                    }
                                }
                                ?>

                        </table>
                    </div>
                    
                    <?php
                    $projectteam=getProjectTeam($project->id); 
                   // $adminusers=getAdminSperUsers();
                    $foremans=getAllForemans();
                   
                    ?>
                    </div>
                    <div class="col-lg-8">
                    <h3 style="color:#337ab7;">Rock Spring Project Team</h3>
                    <hr style="border:1px solid;">
                    <div class="form-group select-foreman">
                         <label>PM</label>
                         <select  class="form-control choosen_select" multiple name="pm[]" >
                           <?php 
                            $pm = \Session::get('pm');
                            
                            if(!empty($pm)){
								if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($pm)){
											foreach($pm as $pt){
												
													if ($pt == $u->id) {
														echo 'selected';
													}
												
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}else{
								$adminusers = getAllProjectManagers();
								if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($projectteam)){
											foreach($projectteam as $pt){
												if($pt->type=='pm'){
													if ($pt->foreman_id == $u->id) {
														echo 'selected';
													}
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}
                            ?>
                        </select>
                        <br/>
                        
                        <label>APM</label>
                        <select  class="form-control choosen_select" multiple  name="apm[]" >
                            <?php
                            $apm = \Session::get('apm');
                            
                            if(!empty($pm)){
								 if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($apm)){
											foreach($apm as $pt){
												
													if ($pt == $u->id) {
														echo 'selected';
													
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}else{
								$adminusers = getAllAssistantManagers();
								 if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($projectteam)){
											foreach($projectteam as $pt){
												if($pt->type=='apm'){
													if ($pt->foreman_id == $u->id) {
														echo 'selected';
													}
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}
                            ?>
                        </select>
                        <br/>
                        <label>Super</label>
                        <select  class="form-control choosen_select" multiple  name="super[]" >
                            <?php
                            
                             $super = \Session::get('super');
                             if(!empty($super)){
								  if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
											?>
											<option value="<?php echo $u->id ?>" <?php
											if(!empty($super)){
												foreach($super as $pt){
													
														if ($pt == $u->id) {
															echo 'selected';
														
													}
												}
												
											}
											?>><?php echo $u->name; ?></option>

											<?php
										}
									}
							 }else{
								 $adminusers = getAllSuperintendents();
								 if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($projectteam)){
											foreach($projectteam as $pt){
												if($pt->type=='super'){
													if ($pt->foreman_id == $u->id) {
														echo 'selected';
													}
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}
                            ?>
                        </select>
                       <br/>
                        <label>Estimator</label>
                        
						<select  class="form-control choosen_select" multiple name="estimator[]" >
                            <?php
                            
                              $estimator = \Session::get('estimator');
                              if(!empty($estimator)){
								   if (!empty($adminusers)) {
										foreach ($adminusers as $u) {
													 
											?>
											<option value="<?php echo $u->id ?>" <?php
											if(!empty($estimator)){
												foreach($estimator as $pt){
													
														if ($pt == $u->id) {
															echo 'selected';
														}
													
												}
												
											}
											?>><?php echo $u->name; ?></option>

											<?php
										}
									}
							  }else{
								$adminusers = getAllEstimators();
								 if (!empty($adminusers)) {
									foreach ($adminusers as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($projectteam)){
											foreach($projectteam as $pt){
												if($pt->type=='estimator'){
													if ($pt->foreman_id == $u->id) {
														echo 'selected';
													}
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}
                            ?>
                        </select>
                       
                        <br/>
                          <label>Foreman</label>
                          <select  class="form-control choosen_select" name="foremans" >
                           <?php
                           
                            $foremanssession = \Session::get('foremans');
                            
                            if(!empty($foremanssession)){
								if (!empty($foremans)) {
									foreach ($foremans as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										
													if ($foremanssession == $u->id) {
														echo 'selected';
													}
											
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}else{
								$foremans = getAdminForemanUsers();
								 if (!empty($foremans)) {
									foreach ($foremans as $u) {
												 
										?>
										<option value="<?php echo $u->id ?>" <?php
										if(!empty($projectteam)){
											foreach($projectteam as $pt){
												if($pt->type=='foreman'){
													if ($pt->foreman_id == $u->id) {
														echo 'selected';
													}
												}
											}
											
										}
										?>><?php echo $u->name; ?></option>

										<?php
									}
								}
							}
                            ?>
                        </select>
                        
                        
                    </div>
                    <h3 class="text-info">Select Survey Questions</h3>
                    <hr style="border:1px solid;">
					<?php 
					$survey_questions=explode(',',$project->survey_questions);
					//print_r($survey_questions);
					 ?>
					 
					
					
					<!--select  class="form-control choosen_select" name="surveys[]" multiple-->
						<!--option value="">--Select Question--</option-->
						<div class="checkbox">
							<label>
								<input type="checkbox"  id="check-all-survey"><b>Select All</b>
							</label>
						</div>
						<?php
						$questions=getAllSurveyQuestions();
						
						if (count($questions) > 0) {
							$surveys = \Session::get('surveys');
							foreach ($questions as $u) {
								?>
								<div class="checkbox">
									<label>
										<input type="checkbox" class="check-all-child"
										
										<?php if (!empty($survey_questions)) {
											if (in_array($u->id,$survey_questions)) {
												echo 'checked';
											}
										} ?> value="<?php echo $u->id ?>" name="surveys[]" id="check-<?php echo $u->id ?>" ><?php echo $u->survey; ?>
									</label>
								</div>
								<!--option value="<?php echo $u->id ?>" <?php
								if (!empty($surveys)) {
									if (in_array($u->id,$surveys)) {
										echo 'selected';
									}
								}
								?>><?php echo $u->survey; ?></option-->

								<?php
							}
						}
						?>
					<!--/select-->
					
                    <h3 style="color:#337ab7;">Project Details</h3>
                    <hr style="border:1px solid;">
                    
                    <div class="form-group">
                        <label>Hours Per Shift</label>
                        <input class="form-control" name="hps" placeholder="Hours Per Shift" value="<?php echo !empty (old('hps')) ? old('hps') : $project->hps ; ?>">
                    </div>
                    <div class="form-group">
						<?php $shift_type=\Session::get('shift_type'); ?>
                        <label>Project Shift</label>
                        <?php if(!empty($shift_type)){ ?>
							   <select  class="form-control" name="shift_type">
                           <option value="day" <?php if($shift_type=='day' || $shift_type==''){ echo 'selected';} ?>>Day</option>
                           <option value="night" <?php if($shift_type=='night'){ echo 'selected';} ?>>Night</option>
                        </select>
						<?php }else { ?>
                        <select  class="form-control" name="shift_type">
                           <option value="day" <?php if($project->shift_type=='day' || $project->shift_type==''){ echo 'selected';} ?>>Day</option>
                           <option value="night" <?php if($project->shift_type=='night'){ echo 'selected';} ?>>Night</option>
                        </select>
						<?php } ?>
                    </div>
                    
                     <div class="form-group">
                        <label>Start Date</label>
                        <input  id="datetimepicker_add_project" type="text" class="form-control" name="start_date" placeholder="Start Date" value="<?php echo !empty (old('start_date')) ? old('start_date') : $project->start_date ; ?>">
                    </div>
                   
                    
                    <!--h3 style="color:#337ab7;">Select Subcontractor for project</h3>
                    <hr style="border:1px solid;">
                    <div class="table-responsive">
                        <table class="table select-project-table">
                        <?php 
                        $subcontractors=getSubcontractors();
                        if(!empty($subcontractors)){

                            foreach($subcontractors as $s){ 
                            $has_subcontractor = project_has_subcontractor($project->id, $s->id);
                                    $checked='';
                                    if ($has_subcontractor > 0) {
                                         $checked='checked disabled readonly';
                                    }   
                                
                            ?>
                               <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" <?php echo $checked ?> name="subcontractors[<?php echo $s->id; ?>]" value="<?php echo $s->id; ?>"><?php echo $s->subcontractor; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                </tr>
                          
                        <?php }   }
                        ?>
                        </table>
                    </div-->
                    
                    <input type="hidden" name="projectid" value="<?php echo $project->id ?>">
                    <input type="hidden" name="manual_id" value="<?php echo $project->manual_id ?>">
                    <button type="submit" class="btn btn-primary">Update Project</button>
                    </div>
                </form>
				</div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
