@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Worklogs
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project Logs</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-project-worklog">
                                <thead>
                                    <tr>
                                       
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foreman</th>
                                        
                                        <th class="project-description">Description</th>
                                        <th class="project-comment">Comments</th>
                                        <th class="project-image">Image</th>
                                        <th class="project-log-date">Worklog For</th>
                                        <th class="project-log-date">Worklog On</th>
                                        <th class="project-actions">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($logs)) {
                                        $i = 0;
                                        foreach ($logs as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                               
                                                <td><?php echo $u->description ?></td>
                                                <td><?php echo $u->additional_comments ?></td>
                                                <td>
                                                    <?php
                                                    if (isset($u->photo) && !empty($u->photo)) {
                                                        $a = (explode('/', $u->photo));
                                                        echo '<a style="cursor:pointer" data-toggle="modal" data-target="#imgModal'.$i.'"  ><span style="color:#337ab7">View Image</span></a>';
                                                    }
                                                    ?>
                                                     <!-- Modal -->
												<div id="imgModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													<!-- Image Model content-->
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Worklog Detail</h4>
													  </div>
													  <div class="modal-body">
															<img src="<?php  echo $u->photo; ?>" />
																
															
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
													
                                                </td>
                                                <td><?php echo !empty($u->logged_at)?change_date_format($u->logged_at):"N/A" ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <td>
													<a href="<?php echo url() ?>/admin/projects/logs/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View Log Detail</button></a>
					
												</td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            {!! $logs->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
