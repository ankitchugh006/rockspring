<?php
use App\Models\User;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;

function project_has_resource($assignedresources=array(),$manpower_id=null){
	$is='';
	//echo $manpower_id;
	if(!empty($assignedresources)){
		foreach($assignedresources as $as){
			$mid=$as['manpower_id'];
			if($manpower_id==$mid){
				$is=$as['max_hours'];
			}
		}
	}
	return $is;
}	
function project_has_foreman($assignedforeman=array(),$foreman_id=null){
	$is=0;
	//echo $manpower_id;
	if(!empty($assignedforeman)){
		foreach($assignedforeman as $as){
			$mid=$as['foreman_id'];
			if($foreman_id==$mid){
				$is=1;
			}
		}
	}
	return $is;
}

function check_to_select($m){
	$storedresources=\Session::get('resources');
	$is=0;
	//echo $manpower_id;
	if(!empty($storedresources)){
		foreach($storedresources as $s){
			if($m==$s){
				$is=1;
			}
		}
	}
	return $is;
}
function check_to_set_value($m){
	$storedhours=\Session::get('hours');
	echo '<pre>';
	print_r($storedhours);
	
	
	$is=0;
	//echo $manpower_id;
	if(!empty($storedhours)){
		foreach($storedhours as $key=>$s){
			if($m==$key){
				if(isset($s) && !empty($s)){
					$is=$s;
				}
			}
		}
	}
	echo $is;
	return $is;
}
function get_name($id=null){
	$n=User::Where('id',$id)->first();
	if(isset($n->name))
		return $n->name;
	else
		return;
}
function get_push_id($id=null){
	$n=User::Where('id',$id)->first();
	if(isset($n->push_id))
		return $n->push_id;
	else
		return;
}
function get_project_title($id=null){
	$n=Project::Where('id',$id)->first();
	return $n->title;
}

function get_resource_title($id=null){
	$n=ProjectResources::Where('id',$id)->first();
	$title=Manpower::Where('id',$n->manpower_id)->first();
	return $title->title;
}
function get_admin_id(){
	$adminid=User::Where('role','role_admin')->first();
	return $adminid->id;
}
function is_any_new_notification($user_id=null){
	$n=Notification::Where('highlight',0)->where('notification_to',$user_id)->get();
	return $n->count();
}
function get_notifications($user_id=null){
	$n=Notification::where('notification_to',$user_id)->orderBy('id', 'DESC')->take(5)->get();
	return $n->toArray();
}
function get_notification_text($type=null,$from=null,$id=null){
	$html='';
	
	if($type=='request'){
		$html .= '<a href="'.url().'/admin/projects/requests/'.$id.'">Foreman <b>'.get_name($from). '</b> Requested for new resources <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}elseif($type=='logging'){
		$html .= '<a href="'.url().'/admin/projects/logs/'.$id.'">Foreman <b>'.get_name($from). '</b> Submit Worklog <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}elseif($type=='survey'){
		$html .= '<a href="'.url().'/admin/projects/survey">Foreman <b>'.get_name($from). '</b> Submit a survey <span class="label label-primary" style="float:right;padding:6px;">View Detail</span></a>';
	}
	return $html;
}

function is_my_project($project_id=0,$foreman_id=0){
		$is_my_project = ProjectForeman::where('project_id', '=', $project_id)
											->where('foreman_id','=',$foreman_id)
											->count();
		return $is_my_project;
}

function display_breedcrump(){ ?>
<ul class="breadcrumb"> 
		<i class="fa fa-home"></i> 
	  <?php $link = route('home');
		for($i = 1; $i <= count(Request::segments()); $i++) {
			echo '<li>'; 
				if($i < count(Request::segments()) & $i > 0) {
					$link .= "/" . Request::segment($i); ?> 
					<a href="<?= $link ?>"><?php  if(Request::segment($i)=='admin') { echo 'Home'; }else { echo Request::segment($i);} ?></a>
				<?php
				 } else { echo Request::segment($i); } 
			echo '</li>';
		} ?>
</ul>
<?php }


function send_push_notification($registatoin_ids, $message) {    
        
         $url = 'https://android.googleapis.com/gcm/send';    
         $fields = array(    
         'registration_ids' => $registatoin_ids,    
         'data' => $message,    
         );    
         $headers = array(    
         'Authorization: key=AIzaSyB6OCDL0Qo6IaLl9-Pyfj4EFMypi9dEWYI',    
         'Content-Type: application/json'    
         );    
         // Open connection    
         $ch = curl_init();    
         curl_setopt($ch, CURLOPT_URL, $url);    
         curl_setopt($ch, CURLOPT_POST, true);    
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);    
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   
         curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));    
         $result = curl_exec($ch);    
         if ($result === FALSE) {    
                 //die('Curl failed: ' . curl_error($ch));    
         }    
        //print_r($result);die;
        curl_close($ch);    
        //echo $result;    
}     

