@extends('layout.admin')

@section('content')

<style>

.hours_none{
	display:none;
	width:18%;
	border:1px solid #ccc;
}
</style>
<div id="page-wrapper">
    <?php
    //echo '<pre>';
    //print_r($logs);
    ?>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    <?php echo get_project_title($project->id) ?>
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">

                <div class="project-estimate">
                    <ul class="project-estimate-list">
                        <li><strong>Job #: </strong> <?php echo!empty($project->manual_id) ? $project->manual_id : ''; ?></li>
                        <li><strong>Job Address:: </strong> <?php echo!empty($project->description) ? $project->description : ''; ?></li>
						
                        <li><strong>Total Project Requests: </strong> <?php echo $data['requests']; ?></li>
                        <li><strong>Total hours Logged: </strong> <?php echo $data['hourslogged']; ?></li>
                        <li><strong>Total hours Estimated: </strong> <?php echo $data['total_estimated_hours']; ?></li>

                    </ul>
                </div>
            </div>
            <br/><br/>
            <div class="col-lg-12">
				@if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
                <div class="panel panel-primary fullwidth">
                    <div class="panel-heading">
                        <h3 class="panel-title">Log Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-individual-worklogs">

                                <thead>
                                    <tr>
                                        
                                        <th class="project-name">Project</th>
                                        <th class="project-foreman">Foreman</th>
                                      
                                        <th class="project-resources">Resources</th>
										  <th class="project-foreman">Image</th>
                                        <th class="project-workers">Workers</th>
                                        <th class="project-hours">Hours Logged</th>
                                        <th class="project-log">Total Logged</th>
                                        <th class="project-description">Description</th>
                                        <th class="project-date">Logged On</th>
                                        <th class="project-actions">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($logs)) {
                                        foreach ($logs as $u) {
                                            $i++;
                                            ?>
                                            <tr <?php
                                            if ($i == 1) {
                                                echo 'style=""';
                                            }
                                            ?>>
                                                
                                               <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->project_id ?>"><?php echo get_project_title($u->project_id) ?></a></td>
                                                <td><?php echo get_name($u->foreman_id) ?></td>
                                                <td><?php echo get_resource_title($u->resource_id) ?></td>
                                                <td>
													<?php 
													 echo '<a target="_blank" href="' . getWorklogImage($u->worklog_id) . '"><span style="color:#337ab7">View Image</span></a>';
													?>
                                                </td>
                                                <td><?php echo ($u->quantity) ?></td>
                                                <td>
														<span class="display_hours" id="display_hours<?php echo $i ?>"><?php echo ($u->hours_logged) ?></span>
														<input type="text" class="hours_none text_hours" id="text_hours<?php echo $i ?>" value="<?php echo ($u->hours_logged) ?>">
														<button class="btn btn-xs hours_none hours_go btn-primary" id="hours_go<?php echo $i ?>">Go</button>
														<input type="hidden" id="log_id<?php echo $i ?>" value="<?php  echo $u->id; ?>" />
														<input type="hidden" id="foreman_id<?php echo $i ?>" value="<?php  echo $u->foreman_id; ?>" />
												</td>

                                                <td><?php
                                                    if ($u->quantity > 0) {
                                                        echo ($u->quantity * $u->hours_logged);
                                                    } else {
                                                        echo $u->hours_logged;
                                                    }
                                                    ?></td>
                                                <td><?php echo $u->description ?></td>
                                                <td><?php echo change_date_format($u->created_at) ?></td>
                                                <!--td><a data-toggle="modal" data-target="#myModal<?php echo $i; ?>" class="btn btn-xs btn-primary">Override Hours</a></td-->
                                                <td><a href="javascript:;" class="btn btn-xs btn-primary" onClick="override_hours('<?php echo $u->id ?>','<?php echo $i ?>')">Override Hours</a></td>
                                                <!-- Modal -->
												<div id="myModal<?php echo $i; ?>" class="modal fade" role="dialog">
												  <div class="modal-dialog">

													<!-- Modal content-->
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Override Hours</h4>
													  </div>
													  <div class="modal-body">
															<!------------------override hours--------------------------->
															
															<form action="<?php echo url() ?>/admin/projects/logs/override" method="POST" role="form">
																<div class="form-group">
																	<label>Hours Logged for <?php echo get_resource_title($u->resource_id) ?></label>
																	<input class="form-control" value="<?php echo $u->hours_logged ?>" name="hours_logged" required>
																</div>
																
																<input type="hidden" name="log_id" value="<?php  echo $u->id; ?>" />
																<input type="hidden" name="main_log_id" value="<?php  echo $u->worklog_id; ?>" />
																<input type="hidden" name="foreman_id" value="<?php  echo $u->foreman_id; ?>" />
																
																<div class="form-group">
																	<button class="btn btn-primary" type="submit">Override</button>
																</div>
															</form>
																
															<!------------------------------------------------------------>
													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>

												  </div>
												</div>
                                            </tr>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
        <!-- /.row -->
		
		
		
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
