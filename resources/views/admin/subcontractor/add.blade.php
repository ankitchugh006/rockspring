@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-12">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/subcontractors/save">

                            <div class="form-group">
                                <label>Subcontractor</label>
                                <input class="form-control" name="subcontractor" placeholder="Subcontractor" value="{{ old('subcontractor') }}">
                            </div>
                            <div class="form-group">
                                <label>Owner</label>
                                <input class="form-control" name="owner" placeholder="Owner" value="{{ old('owner') }}">
                            </div>
                            <input type="hidden" name="email" value="dd@dd.com" />
                           <!--div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
                            </div-->
							<div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}">
                            </div>
                           
                            <h3 style="color:#337ab7;">Members</h3>
							<hr style="border:1px solid;" >

							<div id="customer_members" style="margin-bottom:20px;">
								<table id="member_row_inner" class="table table-hover table-striped">
									<tr>
										<th>Member</th>
										<th>Title</th>
										<th>Email</th>
										<th>phone</th>
										<th style="width:36px;">
										<div class="add-member" id="add-3">
											<a href="javascript:void(0)" id="add_contractor">
												<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
											</a>

										</div>
										</th>
										</tr>
										<?php
										
										$c_name=\Session::get('c_name');
										$c_role=\Session::get('c_role');
										$c_email=\Session::get('c_email');
										$c_phone=\Session::get('c_phone');
										$i = 0;
										
										if (!empty($c_name)) {
										   
											foreach ($c_name as $key=>$c) {
												$i++;
												?>
												<tr id="customer_count-<?php echo $i ?>" class="customer_row">
													<td><input required="" class="form-control" name="c_name[]" placeholder="name" value="<?php echo $c; ?>"></td>
													<td>
														<select name="c_role[]" class="form-control" required>';
															<option value="PM" <?php
															if ($c_role[$key] == 'PM') {
																echo 'selected';
															}
															?>>PM</option>
															<option value="APM" <?php
															if ($c_role[$key] == 'APM') {
																echo 'selected';
															}
															?>>APM</option>
															<option value="SUPER" <?php
															if ($c_role[$key] == 'SUPER') {
																echo 'selected';
															}
															?>>Superintendent</option>
														</select>
													</td>
													<td><input required="" class="form-control" name="c_email[]" placeholder="email" value="<?php echo $c_email[$key] ?>"></td>
													<td><input required="" class="form-control" name="c_phone[]" placeholder="phone" value="<?php echo $c_phone[$key] ?>"></td>
													<td><a onclick="customer_remove(<?php echo $i ?>)" href="javascript:;"><span aria-hidden="true" class="glyphicon glyphicon-minus"></span></a></td>
												</tr>
												<?php
											}
										}else{ ?>
											<tr id="customer_count-0" class="customer_row">
												<td><input required="" class="form-control" name="c_name[]" placeholder="name" value=""></td>
												<td>
													<select name="c_role[]" class="form-control" required>
														<option value="PM">PM</option>
														<option value="APM">APM</option>
														<option value="SUPER">Superintendent</option>
													</select>
												</td>
												<td><input required="" class="form-control" name="c_email[]" placeholder="email" value=""></td>
												<td><input required="" class="form-control" name="c_phone[]" placeholder="phone" value=""></td>
												<td></td>
											</tr>
											
										<?php }
										?>
									</table>
								</div>
                           
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
