@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>
            <div class="col-lg-8 col-sm-6 col-xs-6 breadcrumb">   
                <?php echo display_breedcrump(); ?>
            </div>
            <div class="col-lg-4" style="text-align:right">
                <a href="<?php echo url() ?>/admin/subcontractors/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New Contractor</button></a>
            </div>
        </div>
        <!-- /.row -->
        <div class="space"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort" id="table-subcontracter">
                                <thead>
                                    <tr>
                                        <!--th class="subcontractor-id">#</th-->
                                        <th class="subcontractor-name">Subcontractor</th>
                                        <th class="subcontractor-name">Owner</th>
                                        <!--th class="subcontractor-name">Email</th-->
                                        <th class="subcontractor-name">Phone</th>
                                        <!--th class="subcontractor-actions">Actions</th-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($subcontractors)) {
                                        foreach ($subcontractors as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <!--td><?php echo $i; ?></td-->
                                                <td><a href="<?php echo url() ?>/admin/subcontractors/edit/<?php echo $u->id; ?>"><?php echo ucfirst($u->subcontractor) ?></a></td>
                                                <td><?php echo ucfirst($u->owner) ?></td>
                                                <!--td style="text-transform:lowercase"><?php echo ucfirst($u->email) ?></td-->
                                                <td><?php echo ucfirst($u->phone) ?></td>
                                                <!--td>	
                                                     <a href="<?php echo url() ?>/admin/subcontractors/edit/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>	
                                                     <a onclick="return confirm('Are you sure you want to delete this subcontractor?')" href="<?php echo url() ?>/admin/subcontractors/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td-->
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            {!! $subcontractors->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
