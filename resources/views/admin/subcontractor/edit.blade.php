@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-12">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/subcontractors/update">

                            <div class="form-group">
                                <label>Subcontractor</label>
                                <input class="form-control" name="subcontractor" placeholder="Subcontractor" value="<?php echo !empty($subcontractor->subcontractor)?$subcontractor->subcontractor:""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Owner</label>
                                <input class="form-control" name="owner" placeholder="Owner" value="<?php echo !empty($subcontractor->owner)?$subcontractor->owner:""; ?>">
                            </div>
                            
                            <!--input type="hidden" name="email" value="<?php echo !empty($subcontractor->email)?$subcontractor->email:""; ?>" /-->
                            
                           <div class="form-group" STYLE="display:none;">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Email" value="<?php echo !empty($subcontractor->email)?$subcontractor->email:""; ?>">
                            </div>
							<div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" name="phone" placeholder="Phone" value="<?php echo !empty($subcontractor->phone)?$subcontractor->phone:""; ?>">
                            </div>
                            
                             <h3 style="color:#337ab7;">Members</h3>
							<hr style="border:1px solid;" >

							<div id="customer_members" style="margin-bottom:20px;">
								<table id="member_row_inner" class="table table-hover table-striped">
									<tr>
										<th>Member</th>
										<th>Title</th>
										<th>Email</th>
										<th>phone</th>
										<th style="width:36px;">
									<div class="add-member" id="add-3">
										<a href="javascript:void(0)" id="add_contractor">
											<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
										</a>

									</div>
									</th>
									</tr>
									<?php
									if (!empty($subcontractor_members)) {
										$i = 0;
										foreach ($subcontractor_members as $c) {
											$i++;
											?>
											<tr id="customer_count-<?php echo $i ?>" class="customer_row">
												<input type="hidden" name="c_id[]" value="<?php echo $c->id ?>">
												<td><input required="" class="form-control" name="c_name[]" placeholder="name" value="<?php echo $c->name ?>"></td>
												<td>
													<select name="c_role[]" class="form-control" required>';
														<option value="PM" <?php
														if ($c->role == 'PM') {
															echo 'selected';
														}
														?>>PM</option>
														<option value="APM" <?php
														if ($c->role == 'APM') {
															echo 'selected';
														}
														?>>APM</option>
														<option value="SUPER" <?php
														if ($c->role == 'SUPER') {
															echo 'selected';
														}
														?>>Superintendent</option>
													</select>
												</td>
												<td><input required="" class="form-control" name="c_email[]" placeholder="email" value="<?php echo $c->email ?>"></td>
												<td><input required="" class="form-control" name="c_phone[]" placeholder="phone" value="<?php echo $c->phone ?>"></td>
												<td><!--a onclick="customer_remove(<?php echo $i ?>)" href="javascript:;"><span aria-hidden="true" class="glyphicon glyphicon-minus"></span></a--></td>
											</tr>
											<?php
										}
									}
									?>
								</table>

							</div>
                            
                            
                            <input type="hidden" name="subcontractorid" value="<?php echo !empty($subcontractor->id)?$subcontractor->id:""; ?>">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a onclick="return confirm('Are you sure you want to delete this subcontractor?')" href="<?php echo url() ?>/admin/subcontractors/delete/<?php echo $subcontractor->id ?>"><button type="button" class="btn  btn-danger">Delete</button></a>	
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
