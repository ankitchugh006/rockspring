@extends('layout.admin')

@section('content')
<script>
function check(input) {
if (input.value != document.getElementById('password1').value) {
input.setCustomValidity('Password not matching.');
} else {
// input is valid -- reset the error message
input.setCustomValidity('');
}
}
</script>
<div id="page-wrapper">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					 @if(Session::has('error'))
                                <div class="alert alert-danger">
                                  <ul><li>{{ Session::get('error') }}</li></ul>
                                </div>
                     @endif
					 @if(Session::has('success'))
                                <div class="alert alert-info">
                                  <ul><li>{{ Session::get('success') }}</li></ul>
                                </div>
                     @endif
					<form class="form-horizontal" role="form" method="POST" action="<?php  echo url(); ?>/admin/myaccount/reset">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password" id="password1">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" oninput="check(this)" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Reset Password
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
