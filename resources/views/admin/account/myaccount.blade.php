@extends('layout.admin')

@section('content')

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{{$title}}}
                        </h1>
						<?php  echo display_breedcrump(); ?>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						 @if(Session::has('error'))
									<div class="alert alert-danger">
									  <ul><li>{{ Session::get('error') }}</li></ul>
									</div>
						 @endif
						 @if(Session::has('success'))
									<div class="alert alert-info">
									  <ul><li>{{ Session::get('success') }}</li></ul>
									</div>
						 @endif
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="POST" action="<?php echo url(); ?>/admin/myaccount/update">

                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" name="name" placeholder="full name" value="<?php echo !empty($user->name)?$user->name:''; ?>">
                            </div>

                            <div class="form-group">
                                <label>Username</label>
                                <input class="form-control" name="username" placeholder="username" value="<?php echo !empty($user->username)?$user->username:''; ?>">
                            </div>
							<div class="form-group">
                                <label>User Phone</label>
                                <input class="form-control" name="userphone" placeholder="User Phone" value="<?php echo !empty($user->user_phone)?$user->user_phone:''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="email" value="<?php echo !empty($user->email)?$user->email:''; ?>">
                            </div>
                             <div class="form-group">
                                <label>Bio</label>
                                <textarea  class="form-control" name="userdesc"><?php echo !empty($user->user_desc)?$user->user_desc:''; ?></textarea>
                            </div>
                            <input type="hidden" name="id" value="<?php echo !empty($user->id)?$user->id:''; ?>">
                            <button type="submit" class="btn btn-default">Update</button>
                        </form>

                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
@stop
