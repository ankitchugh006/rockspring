@extends('layout.admin')

@section('content')

<?php

$projects=\Session::get('projects');
$manpower=\Session::get('manpower');
$date_from=\Session::get('date_from');
$date_to=\Session::get('date_to');

?>
<style>
.tbl-report th{
    width: 170px;
}
.tbl-report th:nth-child(1) {
    width: 130px;
}
.tbl-report td[colspan*="3"] table td {
    width: 150px;
}
.form-horizontal .form-group .chosen-container {
    width: 100% !important;
}
.modal-body .chosen-container {
    width: 100% !important;
}
.chosen-container {
    width: 100% !important;
}
.table-project-worklog td{
	 width: 190px;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                     Date Range Report
                </h1>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-12">   
                <form class="form-horizontal"  role="form" method="GET" action="<?php echo url(); ?>/admin/report/generate">
					<div class="col-lg-12">	   
						<div class="form-group pull-right">
							<div class="col-sm-1 col-xs-12 text-right">
								<label style="margin-top:6px;">Date</label>
							</div>
							<div class="col-sm-11 col-xs-12">
								<div class="col-sm-4 col-xs-12" style="padding:0">
									<input id="datetimepicker1_disabled_future" class="form-control" name="date_from"  placeholder="Date From" value="<?php  echo @Input::get('date_from')?>">
								</div>
								<div class="col-sm-4 col-xs-12" style="padding:0;margin-left:10px;">
									<input  id="datetimepicker2_disabled_future" class="form-control" name="date_to" placeholder="Date To" value="<?php  echo @Input::get('date_to')?>">
								</div>
								 <div  class="col-sm-3 col-xs-12">
									<button type="submit" class="btn btn-primary">Generate Report</button>
								 </div>
							</div>
						</div>
					</div>
					
				</form>
            </div>
            
        </div>
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project Logs</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered  table-striped table-project-worklog tbl-report">
                                <thead>
                                    <tr>
                                        <th class="project-name tbl-report-th">Job</th>
                                        <th class="project-name tbl-report-th">Manpower Type</th>
                                        <th class="project-name tbl-report-th">Foreman</th>
                                        <th class="project-name tbl-report-th">Hours</th>
                                      
               
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                 
                                    if (count($projects)) {
										
                                        $i = 0;
                                        foreach ($projects as $u) {
											
											
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->id ?>"><?php echo get_project_title($u->id) ?></a></td>
                                                <td colspan="5">
													<?php 
													if(!empty($manpower)){
														
														echo '<table class="table table-bordered  table-striped table-project-worklog">';
														$total=0;
														
														foreach($manpower as $m){
															$log=get_hours_logged_report($u->id,$m,$date_from,$date_to);
															if(!empty($log)){
																
																$cou=0;
																foreach($log as $l){
																	
																	$total += $l->hours_logged;
																	$cou++;
																	$rowspan=get_row_span_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																	$sum	=get_foreman_log_sum_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																	 ?>
																
																	<tr>
																		<td><?php echo get_resource_title_direct($m) ?></td>
																		<td><?php echo get_name($l->foreman_id) ?></td>
										
																		<td><?php echo $l->hours_logged ?></td>
																		
																		
																	</tr>
																<?php } ?>
																
															<?php }
														}
														if($total>0){	?>
															<tr style="background:#2e6da4;color:#fff">
																	<td class="tbl-report-td" colspan=2><b>Total</b></td>
																	<td class="tbl-report-td"><b><?php echo $total ?></b></td>
																	
															</tr>
														<?php }
														echo '</table>';
													}else{
															//get project manpower
															$manpower1=get_manpower_report();
															
															echo '<table class="table table-bordered  table-striped table-project-worklog">';
															$total=0;
															foreach($manpower1 as $m){
																$log=get_hours_logged_report($u->id,$m->id,$date_from,$date_to);
																
																if(!empty($log)){	
																	$cou=0;
																	foreach($log as $l){ 
																		$total += $l->hours_logged;
																		$cou++;
																		$rowspan=get_row_span_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																		$sum	=get_foreman_log_sum_report($u->id,$l->resource_id,$l->foreman_id,$date_from,$date_to);
																		?>
																	
																		<tr>
																			<td><?php echo get_resource_title_direct($m->id) ?></td>
																			<td><?php echo get_name($l->foreman_id) ?></td>
																			<td><?php echo $l->hours_logged ?></td>
																			
																			
																			
																		</tr>
																	<?php } ?>
																<?php }
															} 
																
																
															
															if($total>0){	?>
															<tr style="background:#2e6da4;color:#fff">
																	<td class="tbl-report-td" colspan=2><b>Total</b></td>
																	<td class="tbl-report-td"><b><?php echo $total ?></b></td>
																	
															</tr>
															<?php }
															 echo '</table>';
															
														}
													 ?>
                                                </td>
                                             
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div class="row">
					<div class="col-lg-12" style="padding:20px 15px">
							<div class="col-lg-4">
								<a class="pull-left btn btn-primary" download="<?php echo url() ?>/uploads/adminreport.pdf" href="<?php echo url() ?>/uploads/adminreport.pdf">Click here to download Report</a>
							</div>
							<form method="POST" id="report_send">
								<div class="col-lg-8 text-right">
									<!--div class="col-lg-6">
										<input id="report_email" type="email" class="form-control" required>
									</div-->
									<div class="col-lg-12">
										<a href="javascript:;'" data-toggle="modal" data-target="#myModal" class=" btn btn-primary" >Send via Email</a>
									</div>
								</div>
							</form>
					</div>
		</div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Select Email Recipient</h4>
	  </div>
	  <form method="POST" action="<?php echo url() ?>/admin/report/sendadminreport">
		  <div class="modal-body">
			<?php $adminusers=getAdminSperUsers(); ?>
			
			 <select  class="form-control choosen_select" name="email[]" multiple> 
			
			   <?php
				if (!empty($adminusers)) { 
					foreach ($adminusers as $u) {
							?>
							<option value="<?php echo $u->email; ?>"  ><?php echo $u->email; ?></option>
							<?php
						
					}
				}
				?>
			 </select>
		
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-primary" style="background:#337ab7">Send Email</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
	  </form>
	</div>

  </div>
</div>

@stop
