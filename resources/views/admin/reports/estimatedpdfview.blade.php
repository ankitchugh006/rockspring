
<?php

$projects=\Session::get('projects');
$manpower=\Session::get('manpower');
?>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Report
                </h1>
            </div>
            
        </div>
       
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project Logs</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table border="1" cellpadding="5" cellspacing="0" width="500" style="text-align:left;">
                                <thead>
                                    <tr>
                                        <th style="width:100px;" class="project-name">Job</th>
                                        <th style="width:200px;" class="project-name">Manpower Type</th>
                                        <th style="width:100px;" class="project-name">Est Hours</th>
                                        <th style="width:100px;" class="project-name">Act Hours</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                 
                                    if (!empty($projects)) {
                                        $i = 0;
                                        foreach ($projects as $u) {
											
											
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                                <td style="width:100px;"><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->id ?>"><?php echo get_project_title($u->id) ?></a></td>
                                                <td colspan="3">
													<?php 
													if(!empty($manpower)){
														echo '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
														foreach($manpower as $m){
															 $est_hours  =get_est_hours_report($u->id,$m);
															 if($est_hours){
															 $resource_id=get_resource_id_report($u->id,$m);
															 $used_hours=get_used_hours_report($u->id,$resource_id);
															 ?>
															<tr>
																		<td style="width:200px;"><?php echo get_resource_title_direct($m) ?></td>
																		<td style="width:100px;"><?php echo $est_hours ?></td>
																		<td style="width:100px;"><?php echo $used_hours; ?></td>
																	</tr>
														<?php } }
														echo '</table>';
													}else{
														$manpower1=get_manpower_report();
														echo '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
															foreach($manpower1 as $m){
																$est_hours  =get_est_hours_report($u->id,$m->id);
																 if($est_hours){
																 $resource_id=get_resource_id_report($u->id,$m->id);
																 $used_hours=get_used_hours_report($u->id,$resource_id);
																 ?>
																 <tr>
																	<td style="width:200px;"><?php echo get_resource_title_direct($m->id) ?></td>
																	<td style="width:100px;"><?php echo $est_hours ?></td>
																	<td style="width:100px;"><?php echo $used_hours; ?></td>
																</tr>
														<?php } }
														echo '</table>';
													}
													 ?>
                                                </td>
                                               
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
        

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

