@extends('layout.admin')

@section('content')

<?php

$projects=\Session::get('projects');
$manpower=\Session::get('manpower');
?>
<style>
.tbl-report th{
    width: 290px;
}
.tbl-report th:nth-child(1) {
    width: 130px;
}
.tbl-report td[colspan*="3"] table td {
    width: 290px;
}
.form-horizontal .form-group .chosen-container {
    width: 100% !important;
}
.modal-body .chosen-container {
    width: 100% !important;
}
.chosen-container {
    width: 100% !important;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Estimated Vs Actual Report
                </h1>
            </div>
            
        </div>
       
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project Logs</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered  table-striped table-project-worklog tbl-report">
                                <thead>
                                    <tr>
                                        <th class="project-name">Job</th>
                                        <th class="project-name">Manpower Type</th>
                                        <th class="project-name">Est Hours</th>
                                        <th class="project-name">Act Hours</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                 
                                    if (!empty($projects)) {
                                        $i = 0;
                                        foreach ($projects as $u) {
											
											
                                            $i++;
                                            ?>
                                            <tr>
                                                
                                                <td><a href="<?php echo url(); ?>/admin/projects/view_project/<?php echo $u->id ?>"><?php echo get_project_title($u->id) ?></a></td>
                                                <td colspan="3">
													<?php 
													if(!empty($manpower)){
														echo '<table class="table table-bordered  table-striped table-project-worklog">';
														foreach($manpower as $m){
															 $est_hours  =get_est_hours_report($u->id,$m);
															 if($est_hours){
															 $resource_id=get_resource_id_report($u->id,$m);
															 $used_hours=get_used_hours_report($u->id,$resource_id);
															 ?>
															<tr>
																<td><?php echo get_resource_title_direct($m) ?></td>
																<td><?php echo $est_hours ?></td>
																<td><?php echo $used_hours; ?></td>
															</tr>
														<?php } }
														echo '</table>';
													}else{
														$manpower1=get_manpower_report();
														echo '<table class="table table-bordered  table-striped table-project-worklog">';
															foreach($manpower1 as $m){
																$est_hours  =get_est_hours_report($u->id,$m->id);
																 if($est_hours){
																 $resource_id=get_resource_id_report($u->id,$m->id);
																 $used_hours=get_used_hours_report($u->id,$resource_id);
																 ?>
																 <tr>
																	<td><?php echo get_resource_title_direct($m->id) ?></td>
																	<td><?php echo $est_hours ?></td>
																	<td><?php echo $used_hours; ?></td>
																</tr>
														<?php } }
														echo '</table>';
													}
													 ?>
                                                </td>
                                               
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="7" align="center">No Logs Found</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
  
		<div class="row">
					<div class="col-lg-12" style="padding:20px 15px">
							<div class="col-lg-4">
								<a class="pull-left btn btn-primary" download="<?php echo url() ?>/uploads/estimatedadminreport.pdf" href="<?php echo url() ?>/uploads/estimatedadminreport.pdf">Click here to download Report</a>
							</div>
							<form method="POST" id="report_send">
								<div class="col-lg-8 text-right">
									<!--div class="col-lg-6">
										<input id="report_email" type="email" class="form-control" required>
									</div-->
									<div class="col-lg-12">
										<a href="javascript:;'" data-toggle="modal" data-target="#myModal" class=" btn btn-primary" >Send via Email</a>
									</div>
								</div>
							</form>
					</div>
		</div>
    </div>
    <!-- /.container-fluid -->
	
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Select Email Recipient</h4>
	  </div>
	  <form method="POST" action="<?php echo url() ?>/admin/report/estimatesendadminreport">
		  <div class="modal-body">
			<?php $adminusers=getAdminSperUsers(); ?>
			
			 <select  class="form-control choosen_select" name="email[]" multiple> 
			
			   <?php
				if (!empty($adminusers)) { 
					foreach ($adminusers as $u) {
							?>
							<option value="<?php echo $u->email; ?>"  ><?php echo $u->email; ?></option>
							<?php
						
					}
				}
				?>
			 </select>
		
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-primary" style="background:#337ab7">Send Email</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
	  </form>
	</div>

  </div>
</div>
</div>
<!-- /#page-wrapper -->
@stop
