@extends('layout.admin')

@section('content')
<style>
.chosen-container-multi .chosen-choices{
	 background: -moz-linear-gradient(center top , #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%) repeat scroll 0 0 padding-box rgba(0, 0, 0, 0);
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Estimated Vs Actual Report
                </h1>
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
            
        </div>
       
        <!-- /.row -->

        <div class="row">
			 <form class="form-horizontal"  role="form" method="POST" action="<?php echo url(); ?>/admin/report/estimatedgenerate">
				<div class="col-lg-8">	   
						
							<!--div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Date From</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									<input id="datetimepicker1" class="form-control" name="date_from"  placeholder="Date From" value="{{ old('date_from') }}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Date To</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									<input id="datetimepicker2" class="form-control" name="date_to" placeholder="Date To" value="{{ old('date_to') }}">
								</div>
							</div-->
							<div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Job</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									<?php $projects=getUserPojectIds(Auth::user()->id); ?>
									
									<select  data-placeholder = "-All-"  class="form-control choosen_project" name="job[]" multiple>
										
										<?php
										$old=old('job');
										if (!empty($projects)) {
											foreach ($projects as $u) {

												?>
												<option value="<?php echo $u ?>" <?php  if($u==$old){ echo 'selected';}?>><?php echo get_project_title($u); ?></option>

												<?php
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Manpower</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									 <select  data-placeholder = "-All-" class="form-control choosen_project" name="manpower[]" multiple> 
									
									   <?php
										$old=old('manpower');
										if (!empty($manpowers)) { 
											foreach ($manpowers as $m) {
													?>
													<option value="<?php echo $m->id; ?>" <?php  if($m->id==$old){ echo 'selected';}?>><?php echo $m->title; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                           <div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Foreman</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									<select data-placeholder = "-All-" class="form-control choosen_project" name="foreman[]" multiple> 
									 
									   <?php
										$old=old('foreman');
										if (!empty($users)) { 
											foreach ($users as $u) {
													?>
													<option value="<?php echo $u->id; ?>" <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->name; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Suprintendent</label>
								</div>
								 <?php   $adminusers = getAdminSperUsers(); ?>
								<div class="col-sm-10 col-xs-12">
									 <select data-placeholder = "-All-"  class="form-control choosen_project" name="supri[]" multiple> 
									 
									   <?php
										$old=old('supri');
										if (!empty($adminusers)) { 
											foreach ($adminusers as $u) {
													?>
													<option value="<?php echo $u->id; ?>"  <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->name; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>PM</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									 <select  data-placeholder = "-All-" class="form-control choosen_project" name="pm[]" multiple> 
										 
									   <?php
										$old=old('pm');
										if (!empty($adminusers)) { 
											foreach ($adminusers as $u) {
													?>
													<option value="<?php echo $u->id; ?>" <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->name; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>APM</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									 <select data-placeholder = "-All-" class="form-control choosen_project" name="apm[]" multiple>
										 
									   <?php
										$old=old('apm');
										if (!empty($adminusers)) { 
											foreach ($adminusers as $u) {
													?>
													<option value="<?php echo $u->id; ?>" <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->name; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                           <div class="form-group">
								<div class="col-sm-2 col-xs-12">
									<label>Estimator</label>
								</div>
								<div class="col-sm-10 col-xs-12">
									 <select  data-placeholder = "-All-" class="form-control choosen_project" name="estimator[]" multiple> 
										
									   <?php
										$old=old('estimator');
										if (!empty($adminusers)) { 
											foreach ($adminusers as $u) {
													?>
													<option value="<?php echo $u->id; ?>" <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->name; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								</div>
							</div>
                            
                            <div class="form-group">
								 
									<div class="col-sm-2 col-xs-12">
										<label>Customer</label>
									</div>
									<div class="col-sm-10 col-xs-12">
									 <select  class="form-control choosen_project" name="customer">
										 <option value="">-All-</option>
									   <?php
									   $customers=getCustomers();
										$old=old('customer');
										if (!empty($customers)) { 
											foreach ($customers as $u) {
													?>
													<option value="<?php echo $u->id; ?>" <?php  if($u->id==$old){ echo 'selected';}?>><?php echo $u->customer; ?></option>

													<?php
												
											}
										}
										?>
									 </select>
								  </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 form-group">
							  <button type="submit" class="btn btn-primary">Generate Report</button>
							 </div>
							 
							 
						
				</div>
             </form>
        </div>
        <!-- /.row -->
		
		
		
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
