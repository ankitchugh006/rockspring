@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Subcontractors
                </h1>
            </div>
            <div class="col-lg-8">   
                <?php echo display_breedcrump(); ?>
            </div>
            <!--div class="col-lg-4" style="text-align:right">
                                                <a href="<?php echo url() ?>/admin/users/add" ><button class="btn btn-ls btn-primary" type="button">Add Subcontractor</button></a>
                                </div-->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name</th>
                                        <th>username</th>
                                        <th>email</th>
                                        <th>role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($users)) {
                                        foreach ($users as $u) {
                                            ?>
                                            <tr>
                                                <td><?php echo $u->id ?></td>
                                                <td><?php echo $u->name ?></td>
                                                <td><?php echo $u->username ?></td>
                                                <td><?php echo $u->email ?></td>
                                                <td><?php echo $u->get_user_role() ?></td>
                                                <td>
                                                    <?php if ($u->status == 1) { ?>
                                                        <span class="status-active">Active</span>
                                                       <!--i onclick="change_status('<?php echo $u->id ?>','0')" class="fa fa-check u_active"></i-->
                                                    <?php } else { ?>
                                                       <!--i onclick="change_status('<?php echo $u->id ?>','1')" class="fa fa-close u_inactive"></i-->
                                                        <span class="status-block">Blocked</span>
                                                    <?php } ?>
                                                <td>
                                                    <a href="<?php echo url() ?>/admin/users/edit/<?php echo $u->username ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
                                                    <a onclick="return confirm('Are you sure you want to delete this user?')" href="<?php echo url() ?>/admin/users/delete_sub/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                    <?php if ($u->status == 1) { ?>
                                                        <a onclick="change_status('<?php echo $u->id ?>', '0')"  href="javascript:;"><button type="button" class="btn btn-xs btn-warning">Block</button></a>									
                                                    <?php } else { ?>
                                                        <a onclick="change_status('<?php echo $u->id ?>', '1')"  href="javascript:;"><button type="button" class="btn btn-xs btn btn-info">Unblock</button></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
									<tr>
									  <th colspan="7" class="ts-pager form-horizontal">
										<button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
										<button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
										<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
										<button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
										<button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
										
										<select class="pagenum input-mini" title="Select page number"></select>
									  </th>
									</tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
