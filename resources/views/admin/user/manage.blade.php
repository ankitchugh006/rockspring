@extends('layout.admin')

@section('content')

<?php $user_role= getUserRole(Auth::user()->id); ?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    Users
                </h1>
            </div>
			<div class="col-lg-12">   
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				@if(Session::has('error'))
				<div class="alert alert-danger">
					<ul><li>{{ Session::get('error') }}</li></ul>
				</div>
				@endif
				@if(Session::has('success'))
				<div class="alert alert-info">
					<ul><li>{{ Session::get('success') }}</li></ul>
				</div>
				@endif
			</div>
            <div class="col-lg-8">   
                <form id="sort_form" method="GET" action="">

	
                    <!--div class="col-lg-3">
                            
                                    <div class="form-group">
                                            <select id="sort_select" class="form-control sort_select" name="order_by">
                                                    <option value="created_at" <?php if (isset($_GET['order_by']) && $_GET['order_by'] == 'created_at') {
                    echo 'selected';
                } ?>>-Sort By Date-</option>
                                                    <option value="name" <?php if (isset($_GET['order_by']) && $_GET['order_by'] == 'name') {
                    echo 'selected';
                } ?>>-Sort By Name-</option>
                                            </select>
                                    </div>
                            
                    </div-->
                    <div class="col-lg-4">
                        <div class="form-group"  > 					
                            <input  class="form-control s_filter autocomplete" name="name" placeholder="Enter Name" value="<?php if (isset($_GET['name'])) {
                    echo $_GET['name'];
                } ?>">					
                        </div> 

                    </div>	
                    <div class="col-xs-2">
                        <input  type="submit" class="btn btn-primary" value="Search">
                    </div>

                </form>
            </div>
            <div class="col-lg-4">   
                <div style="text-align:right">
					<?php if(getUserRole(Auth::user()->id) == 'role_superadmin') { ?>
						<a href="<?php echo url() ?>/admin/users/add"><button type="button" class="btn btn-ls btn-primary"><i class="fa fa-plus"></i> Add New User</button></a>
					<?php } ?>
                </div>
            </div>
        </div>
       
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort" >
                                <thead>
                                    <tr>
                                        <!--th>#</th-->
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>User Role</th> 
                                        <th>Job Role</th>        
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
if (!empty($users)) {
    foreach ($users as $u) {
        ?>
                                            <tr>
                                                <!--td><?php echo $u->id ?></td-->
                                                <td><?php echo $u->name ?></td>
                                                <td style="text-transform: unset"><?php echo $u->email ?></td>
                                                <td><?php echo!empty($u->user_phone) ? $u->user_phone : 'N/A' ?></td>
                                                <td><?php echo $u->get_user_role() ?></td>
                                                <td><?php echo $u->get_job_role() ?></td>

                                                <td>
                                                    <?php if ($u->status == 1) { ?>
                                                        <span class="status-active">Active</span>
                                                            <!--i onclick="change_status('<?php echo $u->id ?>','0')" class="fa fa-check u_active"></i-->
                                                    <?php } else { ?>
                                                            <!--i onclick="change_status('<?php echo $u->id ?>','1')" class="fa fa-close u_inactive"></i-->
                                                        <span class="status-block">Blocked</span>
                                                    <?php } ?>
                                                <td>
                                                    <a href="<?php echo url() ?>/admin/users/edit/<?php echo $u->username ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>
                                                   <?php  if($user_role=='role_superadmin'){ ?>
														<a onclick="return confirm('Are you sure you want to delete this user?')" href="<?php echo url() ?>/admin/users/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
													<?php } ?>
                                            <?php if ($u->status == 1) { ?>
                                                        <a onclick="change_status('<?php echo $u->id ?>', '0')"  href="javascript:;"><button type="button" class="btn btn-xs btn-warning">Block</button></a>									
                                            <?php } else { ?>
                                                        <a onclick="change_status('<?php echo $u->id ?>', '1')"  href="javascript:;"><button type="button" class="btn btn-xs btn btn-info">Unblock</button></a>
                                            <?php } ?>
                                                </td>
                                            </tr>
        <?php
    }
}
?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="ts-pager form-horizontal">
                                            <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                            <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                            <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                            <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                            <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>

                                            <!--select class="pagenum input-mini" title="Select page number"></select-->
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>

<?php // echo $users->appends(Input::except('page'))->render()  ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
