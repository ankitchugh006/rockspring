@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Add User
                </h1>
                <?php echo display_breedcrump(); ?>

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-xs-12 col-sm-9">

                <form role="form" method="POST" action="<?php echo url(); ?>/admin/users/save">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" name="name" placeholder="Full name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Username</label>
                                <input class="form-control" name="username" placeholder="Username" value="{{ old('username') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
								<i>Once Submitted,Email will non editable</i>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Password</label>
                                <input  type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>User Phone</label>
                                <input class="form-control" name="userphone" placeholder="User Phone" value="{{ old('userphone') }}">
                            </div>
                        </div>
                        <?php if(Auth::check() && Auth::user()->role=='role_superadmin') { ?>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>User Role</label>
                                <select class="form-control" name="role" id="user_role">
                                    <option value="role_administrator">Administrators</option>
                                    <option value="role_foreman">Foreman</option>                 
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group" id="job_role_container">
                                <label>Job Role</label>
                                <select class="form-control" name="job_role" id="job_role">
                                    <option value="job_administrator">Administrator</option>
                                    <option value="job_manager">Project Manager</option>
									<option value="job_assistant">Assistant Project Manger</option>
                                    <option value="job_superintendent">Superintendent</option>
                                    <option value="job_estimator">Estimator</option>
                                    <!--option value="job_foreman">Foreman</option-->
                                </select>
                            </div>
                        </div>
                        
                        <?php }else{ ?>
							<div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>User Role</label>
                                <select class="form-control" name="role">
                                    <option value="role_foreman">Foreman</option>          
                                </select>
                            </div>
                        </div>
							
						 <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Job Role</label>
                                <select class="form-control" name="job_role">
                                    <option value="job_foreman">Foreman</option>
                                </select>
                            </div>
                        </div>	
							
							<?php } ?>
                       
                    </div>
                    <!--div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Bio</label>
                                <textarea  class="form-control" name="userdesc">{{ old('userdesc') }}</textarea>
                            </div>
                        </div>
                    </div-->
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
