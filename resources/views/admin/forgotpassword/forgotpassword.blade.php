@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Forgot Password<i class="fa fa-compass pull-right"></i></div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <ul><li>{{ Session::get('error') }}</li></ul>
                    </div>
                    @endif
                    @if(Session::has('success'))
                    <div class="alert alert-info">
                        <ul><li>{{ Session::get('success') }}</li></ul>
                    </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo url(); ?>/admin/forgotpassword/send">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-12 control-label">Enter Your Email</label>
                            <div class="col-md-12">
                                <input type="email" placeholder="Enter Your Email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                                    Submit
                                </button>
                                <a href="<?php echo url(); ?>">Back to Login </a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
