 <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
	<?php if(Auth::check() && Auth::user()->role=='role_superadmin' || Auth::check() && Auth::user()->role=='role_administrator'){ ?>
	<ul class="nav navbar-nav side-nav" >
		<li  <?php if(isset($active) && ($active=='DASHBOARD')) { echo 'class="active"';} ?>>
			<a href="<?php echo url() ?>/admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
		</li>
		<li  <?php if(isset($active) && ($active=='FOREMEN' || $active=='USER' || $active=='SUBCONTRACTOR' || $active=='ADD_USER')) { echo 'class="active"';} ?>>
			<a href="javascript:;"  data-target="#users" data-toggle="collapse"><i class="fa fa-user"></i> All users <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="users" class="collapse <?php if(isset($active) && ($active=='FOREMEN' || $active=='USER' || $active=='SUBCONTRACTOR' || $active=='ADD_USER')) { echo 'in';} ?>">
				<li <?php if(isset($active) && ($active=='FOREMEN')) { echo 'class="active"';} ?>>
					<a  href="<?php  echo url() ?>/admin/users">Users</a>
				</li>
				<!--li <?php if(isset($active) && ($active=='SUBCONTRACTOR')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/subcontractor">Subcontractors</a>
				</li-->
				<li <?php if(isset($active) && ($active=='ADD_USER')) { echo 'class="active"';} ?>>
					<a href="<?php echo url() ?>/admin/users/add">Add New User</a>
				</li>
			</ul>
			
		</li>
		<li <?php if(isset($active) && ($active=='PROJECT' || $active=='ADD_PROJECT' || $active=='REQUEST' || $active=='WORKLOG' || $active=='SURVEY')) { echo 'class="active"';} ?>>
			<a href="javascript:;" data-target="#demo" data-toggle="collapse"><i class="fa fa-tasks"></i> All Projects <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="demo" class="collapse <?php if(isset($active) && ($active=='PROJECT' || $active=='ADD_PROJECT' || $active=='REQUEST' || $active=='WORKLOG' || $active=='SURVEY')) { echo 'in';} ?>">
				<li  <?php if(isset($active) && ($active=='PROJECT')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects">Projects</a>
				</li>
				<li <?php if(isset($active) && ($active=='ADD_PROJECT')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/add">Add New Project</a>
				</li>
				<li <?php if(isset($active) && ($active=='REQUEST')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/requests">Manpower Requests</a>
				</li>
				<li <?php if(isset($active) && ($active=='WORKLOG')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/logs">Work Log Submissions</a>
				</li>
				<li <?php if(isset($active) && ($active=='SURVEY')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/survey">Daily Survey Submissions</a>
				</li>
                <li <?php if(isset($active) && ($active=='TASK')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/tasks">Task List</a>
				</li>
			</ul>
		</li>
		<li <?php if(isset($active) && ($active=='MANPOWER')) { echo 'class="active"';} ?>>
			<a href="<?php  echo url() ?>/admin/manpowers"><i class="fa fa-sitemap"></i> Manpower Types</a>
		</li>
        <li <?php if(isset($active) && ($active=='SUBCONTACTOR' || $active=='ADD_SUBCONTACTOR')) { echo 'class="active"';} ?>>
            <a href="javascript:;" data-target="#Subcontractors" data-toggle="collapse"><i class="fa fa-tasks"></i>All Subcontractors <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="Subcontractors" class="collapse <?php if(isset($active) && ($active=='SUBCONTACTOR' || $active=='ADD_SUBCONTACTOR')) { echo 'in';} ?>">
                <li <?php if(isset($active) && ($active=='SUBCONTACTOR')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/subcontractors"><i class="fa fa-sitemap"></i>Subcontractors</a>
                </li>
                <li <?php if(isset($active) && ($active=='ADD_SUBCONTACTOR')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/subcontractors/add"><i class="fa fa-sitemap"></i> Add New Subcontractor</a>
                </li>
            </ul>
        </li>
		<li <?php if(isset($active) && ($active=='CUSTOMER' || $active=='ADD_CUSTOMER')) { echo 'class="active"';} ?>>
              <a href="javascript:;"  data-target="#customers" data-toggle="collapse"><i class="fa fa-tasks"></i>All Customers <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="customers" class="collapse <?php if(isset($active) && ($active=='CUSTOMER' || $active=='ADD_CUSTOMER')) { echo 'in';} ?>">
                <li <?php if(isset($active) && ($active=='CUSTOMER')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/customers"><i class="fa fa-sitemap"></i> Customers</a>
                </li>
                <li <?php if(isset($active) && ($active=='ADD_CUSTOMER')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/customers/add"><i class="fa fa-sitemap"></i> Add New Customers
</a>
                </li>
            </ul>
			
		</li>
        
        
        
         <!--li <?php if(isset($active) && ($active=='SURV' || $active=='SURV' || $active=='SURV')) { echo 'class="active"';} ?>>
              <a href="javascript:;"  data-target="#srrv"><i class="fa fa-tasks"></i> Surveys <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="srrv" class="collapse in">
                <li <?php if(isset($active) && ($active=='SURV')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/surveys"><i class="fa fa-sitemap"></i> Survey Questions</a>
                </li>
                  <li <?php if(isset($active) && ($active=='SURV')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/surveys/projectsurveys"><i class="fa fa-sitemap"></i> Project Surveys</a>
                </li>
             </ul>
        </li-->
             
        <li <?php if(isset($active) && ($active=='FILES' || $active=='ADD_FILES' || $active=='CATEGORIES')) { echo 'class="active"';} ?>>
              <a href="javascript:;"  data-target="#files" data-toggle="collapse"><i class="fa fa-tasks"></i>All Files <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="files" class="collapse <?php if(isset($active) && ($active=='FILES' || $active=='ADD_FILES' || $active=='CATEGORIES')) { echo 'in';} ?>">
                <li <?php if(isset($active) && ($active=='FILES')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/files"><i class="fa fa-sitemap"></i> Files
</a>
                </li>
                <li <?php if(isset($active) && ($active=='ADD_FILES')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/files/add"><i class="fa fa-sitemap"></i> Add New File
</a>
                </li>
                <li <?php if(isset($active) && ($active=='CATEGORIES')) { echo 'class="active"';} ?>>
                    <a href="<?php  echo url() ?>/admin/files/categories"><i class="fa fa-sitemap"></i> Categories
</a>
                </li>
            </ul>
			
		</li>
		
	</ul>
	<?php } ?>
</div>
<!-- /.navbar-collapse -->
