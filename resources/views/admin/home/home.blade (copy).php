@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <!--ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </li>
                </ol-->
            </div>
        </div>
        <!-- /.row -->

		<?php if(Auth::user()->role!='role_superadmin' && Auth::user()->role!='role_administrator'){ ?>
			 <div class="row">
				You have not sufficient permissions to access the admin features
			 </div>
		<?php  }else{ ?>
        <!--div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $projects; ?></div>
                                <div>Projects</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo url() ?>/admin/projects">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-user fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $users ?></div>
                                <div>Foremen</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo url() ?>/admin/users">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-clock-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $hourslogged; ?></div>
                                <div>Hours Logged</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo url() ?>/admin/projects/logs">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-chevron-circle-right fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $requests; ?></div>
                                <div>Man Power Requests</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo url() ?>/admin/projects/requests">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div-->
        <!-- /.row -->
        <div class="row">
			
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> 
                        Project Requests recieved today ( For <b><?php echo change_date_format(date("d-m-Y", time()+86400));  ?></b> )
						<a style="float:right" href="<?php echo url() ?>/admin/projects/requests"><button class="btn btn-xs btn-danger" type="button">View All <i class="fa fa-chevron-circle-right"></i></button></a>
						</h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
								
                                <thead>
										<tr>
											
											<th>Project</th>
											<th>Foreman</th>
											<th>Details/Name Request</th>
											<th>Requested Material</th>
											<th>Additional Comments</th>
											<th>Requested On</th>
											<th>Action</th>
										</tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($todaysrequests)>0) {
                                        $i = 0;
                                        foreach ($todaysrequests as $u) {
                                            $i++;
                                            ?>
                                            <tr>
											
											<td><?php echo get_project_title($u->project_id) ?></td>
											<td><?php echo get_name($u->foreman_id) ?></td>
											<td><?php echo ($u->comment1) ?></td>
											<td><?php echo ($u->comment2) ?></td>
											<td><?php echo ($u->comment3) ?></td>
											<td><?php echo change_date_format($u->created_at) ?></td>
											<td><a href="<?php echo url() ?>/admin/projects/requests/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View Request Detail</button></a></td>
										</tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="8" align="center">No Requests for today</td>
                                        </tr>
                                    <?php } ?>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                </div>
               <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Worklogs
                        <a style="float:right" href="<?php echo url() ?>/admin/projects/logs"><button class="btn btn-xs btn-danger" type="button">View All <i class="fa fa-chevron-circle-right"></i></button></a>
                        </h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
								
                                <thead>
										
											<th>Project</th>
											<th>Foreman</th>
											
											<th>Description</th>
											<th>Comments</th>
											<th>Image</th>
											<th>Log Date</th>
											<th>Action</th>
                                </thead>
                                <tbody>
										<?php  
										if(!empty($worklogs)){
										$i=0;
										foreach($worklogs as $u){ 
										$i++;
										?>
										<tr>
											
											<td><?php echo get_project_title($u->project_id) ?></td>
											<td><?php echo get_name($u->foreman_id) ?></td>
										
											<td><?php echo $u->description ?></td>
											<td><?php echo $u->additional_comments ?></td>
											<td>
												<p><?php if(isset($u->photo) && !empty($u->photo)) {  $a=(explode('/',$u->photo)); echo '<a target="_blank" href="'.$u->photo.'"><span style="color:#337ab7">View Image</span></a>'; } ?></p>
											</td>
											<td><?php echo change_date_format($u->created_at) ?></td>
											<td><a href="<?php echo url() ?>/admin/projects/logs/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View Log Detail</button></a></td>
										</tr>
										<?php } }else{?>
											<tr><td colspan="8" align="center">No Logs Found</td></tr>
										<?php } ?>
									</tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
                           <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Daily Survey
                        <a style="float:right" href="<?php echo url() ?>/admin/projects/survey"><button class="btn btn-xs btn-danger" type="button">View All <i class="fa fa-chevron-circle-right"></i></button></a>
                        </h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
								
                                <thead>
										<tr>
											
											<th>Project</th>
											<th>Foreman</th>
											<th>Date</th>
											<th>Actions</th>
											
										</tr>
                                </thead>
                              <tbody>
										<?php  
										if(!empty($surveys)){
										foreach($surveys as $u){ ?>
										<tr>
											
											<td><?php echo get_project_title($u->project_id) ?></td>
											<td><?php echo get_name($u->foreman_id) ?></td>
											
											<td><?php echo change_date_format($u->created_at) ?></td>
											<td><a href="<?php echo url() ?>/admin/projects/survey/<?php echo $u->id; ?>"><button class="btn btn-xs btn-primary" type="button">View  Detail</button></a></td>
										</tr>
										<?php } }?>
									</tbody>
                        </div>

                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
