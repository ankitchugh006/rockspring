 <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
	<?php if(Auth::check() && Auth::user()->role=='role_superadmin' || Auth::check() && Auth::user()->role=='role_administrator'){ ?>
	<ul class="nav navbar-nav side-nav" >
		<!--li  <?php if(isset($active) && ($active=='DASHBOARD')) { echo 'class="active"';} ?>>
			<a href="<?php echo url() ?>/admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
		</li-->
		<li  <?php if(isset($active) && ($active=='FOREMEN' || $active=='USER' || $active=='SUBCONTRACTOR' || $active=='ADD_USER')) { echo 'class="active"';} ?>>
			<a href="<?php  echo url() ?>/admin/users"><i class="fa fa-user"></i> All users</a>
			
		</li>
		<li <?php if(isset($active) && ($active=='PROJECT' || $active=='ADD_PROJECT' || $active=='REQUEST' || $active=='WORKLOG' || $active=='SURVEY' || $active=='FILES' || $active=='ADD_FILES' || $active=='TASK')) { echo 'class="active"';} ?>>
			<a href="javascript:;" data-target="#demo" data-toggle="collapse"><i class="fa fa-tasks"></i> All Projects <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="demo" class="collapse <?php if(isset($active) && ($active=='PROJECT' || $active=='ADD_PROJECT' || $active=='REQUEST' || $active=='WORKLOG' || $active=='SURVEY' || $active=='FILES' || $active=='ADD_FILES' || $active=='TASK' )) { echo 'in';} ?>">
				<li  <?php if(isset($active) && ($active=='PROJECT')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects">Projects</a>
				</li>
				<li <?php if(isset($active) && ($active=='ADD_PROJECT')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/add">Add New Project</a>
				</li>
				
				<li <?php if(isset($active) && ($active=='WORKLOG')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/logs">Work Log Submissions</a>
				</li>
				<li <?php if(isset($active) && ($active=='SURVEY')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/survey">Daily Survey Submissions</a>
				</li>
                <li <?php if(isset($active) && ($active=='TASK')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/projects/tasks">Task List</a>
				</li>
				 <li <?php if(isset($active) && ($active=='FILES' || $active=='ADD_FILES' || $active=='CATEGORIES')) { echo 'class="active"';} ?>>
					<a href="<?php  echo url() ?>/admin/files" >All Files </a>
				</li>
			</ul>
		</li>
		<li <?php if(isset($active) && ($active=='MANPOWER')) { echo 'class="active"';} ?>>
			<a href="<?php  echo url() ?>/admin/manpowers"><i class="fa fa-sitemap"></i> Manpower Types</a>
		</li>
        <!--li <?php if(isset($active) && ($active=='SUBCONTACTOR' || $active=='ADD_SUBCONTACTOR')) { echo 'class="active"';} ?>>
            <a href="<?php  echo url() ?>/admin/subcontractors" ><i class="fa fa-tasks"></i>All Subcontractors </a>
            
        </li-->
		<li <?php if(isset($active) && ($active=='CUSTOMER' || $active=='ADD_CUSTOMER')) { echo 'class="active"';} ?>>
              <a href="<?php  echo url() ?>/admin/customers"  ><i class="fa fa-tasks"></i>All Customers </a>
            
			
		</li>
         <li <?php if(isset($active) && ($active=='SURV' || $active=='SURV' || $active=='SURV')) { echo 'class="active"';} ?>>
              <a href="<?php  echo url() ?>/admin/surveys"  ><i class="fa fa-tasks"></i> Survey Questions </a>
        </li>
        
        <li <?php if(isset($active) && ($active=='REPORT' || $active=='EREPORT' || $active=='REPORT' || $active=='REPORT' || $active=='REPORT' || $active=='REPORT' || $active=='REPORT')) { echo 'class="active"';} ?>>
			<a href="javascript:;" data-target="#reports" data-toggle="collapse"><i class="fa fa-tasks"></i> Admin Reports <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="reports" class="collapse <?php if(isset($active) && ($active=='REPORT' || $active=='REPORT' || $active=='EREPORT' || $active=='REPORT' || $active=='REPORT' || $active=='REPORT' || $active=='REPORT')) { echo 'in';} ?>">
				  <li <?php if(isset($active) && ($active=='REPORT' || $active=='REPORT' || $active=='REPORT')) { echo 'class="active"';} ?>>
					  <a href="<?php  echo url() ?>/admin/reports"  ><i class="fa fa-tasks"></i> Date Range Reports </a>
				</li>
				 <li <?php if(isset($active) && ($active=='EREPORT' || $active=='EREPORT' || $active=='EREPORT')) { echo 'class="active"';} ?>>
					  <a href="<?php  echo url() ?>/admin/estimatedreports"  ><i class="fa fa-tasks"></i> Estimated Vs Actual Report </a>
				</li>   
			</ul>
        </li>
        <!--li <?php if(isset($active) && ($active=='FILES' || $active=='ADD_FILES' || $active=='CATEGORIES')) { echo 'class="active"';} ?>>
			<a href="<?php  echo url() ?>/admin/files" ><i class="fa fa-tasks"></i>All Files </a>
		</li-->
		
	</ul>
	<?php } ?>
</div>
<!-- /.navbar-collapse -->
