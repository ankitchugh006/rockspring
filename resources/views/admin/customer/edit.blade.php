@extends('layout.admin')

@section('content')
<style>

.chosen-container{
	width:100% !important;
}
</style>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
                <?php echo display_breedcrump(); ?>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    <ul><li>{{ Session::get('error') }}</li></ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    <ul><li>{{ Session::get('success') }}</li></ul>
                </div>
                @endif
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
				
                <form role="form" method="POST" action="<?php echo url(); ?>/admin/customers/update">

                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" placeholder="title" value="<?php echo!empty($customer->customer) ? $customer->customer : "" ?>">
                    </div>



                    <!--div class="form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" placeholder="email" value="<?php echo!empty($customer->email) ? $customer->email : "" ?>">
                    </div-->

                    <div class="form-group">
                        <label>Phone number</label>
                        <input class="form-control" name="phone" placeholder="phone" value="<?php echo!empty($customer->phone) ? $customer->phone : "" ?>">
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" name="address" placeholder="address"><?php echo!empty($customer->address) ? $customer->address : "" ?></textarea>
                    </div>

                    <!--div class="form-group">
                        <label>PM</label>
                                                        <select  class="form-control choosen_select" name="pm">
                                                         <option value="">--Select PM--</option>
                    <?php
                    $managers = getAllProjectManagers();
                    if (!empty($managers)) {
                        foreach ($managers as $u) {
                            $pm = $customer->project_manager;
                            ?>
                                                                                                                                                                                        <option value="<?php echo $u->id ?>" <?php
                            if (!empty($pm)) {
                                if ($pm == $u->id) {
                                    echo 'selected';
                                }
                            }
                            ?>><?php echo $u->username; ?></option>

                            <?php
                        }
                    }
                    ?>
                                                        </select>
                                                </div>
                                                
                                                 <div class="form-group">
                        <label>Superintendent</label>
                                                
                                                        <select  class="form-control choosen_select" name="sd">
                                                         <option value="">--Select Superintendent--</option>
                    <?php
                    $superintendent = getAllSuperintendents();
                    if (!empty($superintendent)) {
                        foreach ($superintendent as $u) {
                            $sd = $customer->superintendent;
                            ?>
                                                                                                                                                                                        <option value="<?php echo $u->id ?>" <?php
                            if (!empty($sd)) {
                                if ($sd == $u->id) {
                                    echo 'selected';
                                }
                            }
                            ?>><?php echo $u->username; ?></option>

                            <?php
                        }
                    }
                    ?>
                                                        </select>
                                                </div-->
                    <!--div class="form-group">
<label>PM/APM</label>
<input class="form-control" name="pm_apm"  value="<?php echo!empty($customer->pm_apm) ? $customer->pm_apm : "" ?>">
</div-->
                    <input type="hidden" name="pm_apm"  value="pm_apm">	

                    <h3 style="color:#337ab7;">Members                    
                    </h3>
                    <hr style="border:1px solid;" >

                    <div id="customer_members" style="margin-bottom:20px;">
                        <table id="member_row_inner" class="table table-hover table-striped">
                            <tr>
                                <th>Member</th>
                                <th>Title</th>
                                <th>Email</th>
                                <th>cell phone</th>
                                <th>office phone</th>
                                <th style="width:36px;">
                            <div class="add-member" id="add-3">
                                <a href="javascript:void(0)" id="add_customer">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </a>

                            </div>
                            </th>
                            </tr>
                            <?php
                            if (!empty($customer_members)) {
                                $i = 0;
                                foreach ($customer_members as $c) {
                                    $i++;
                                    ?>
                                    <tr id="customer_count-<?php echo $i ?>" class="customer_row">
                                        <input type="hidden" name="c_id[]" value="<?php echo $c->id ?>">
                                        <td><input required="" class="form-control" name="c_name[]" placeholder="name" value="<?php echo $c->name ?>"></td>
                                        <td>
                                            <select name="c_role[]" class="form-control" required>';
                                                <option value="PM" <?php
                                                if ($c->role == 'PM') {
                                                    echo 'selected';
                                                }
                                                ?>>PM</option>
                                                <option value="APM" <?php
                                                if ($c->role == 'APM') {
                                                    echo 'selected';
                                                }
                                                ?>>APM</option>
                                                <option value="SUPER" <?php
                                                if ($c->role == 'SUPER') {
                                                    echo 'selected';
                                                }
                                                ?>>Superintendent</option>
                                            </select>
                                        </td>
                                        <td><input autocomplete="off" required="" class="form-control" name="c_email[]" placeholder="email" value="<?php echo $c->email ?>"></td>
                                        <td><input autocomplete="off" required="" class="form-control" name="c_phone[]" placeholder="phone" value="<?php echo $c->phone ?>"></td>
                                        <td><input autocomplete="off" required="" class="form-control" name="o_phone[]" placeholder="phone" value="<?php echo $c->office_phone ?>"></td>
                                        <td><!--a onclick="customer_remove(<?php echo $i ?>)" href="javascript:;"><span aria-hidden="true" class="glyphicon glyphicon-minus"></span></a--></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>

                    </div>
					<input type="hidden" name="email" value="<?php echo!empty($customer->email) ? $customer->email : "" ?>">
                    <input type="hidden" name="customerid" value="<?php echo!empty($customer->id) ? $customer->id : "" ?>">
                    <input type="hidden" id="customer_count" value="<?php echo $i; ?>">
                    <button type="submit" class="btn btn-primary">Update</button>
                    
                    <a onclick="return confirm('Are you sure you want to delete this customer?')" href="<?php echo url() ?>/admin/customers/delete/<?php echo!empty($customer->id) ? $customer->id : "" ?>"><button type="button" class="btn btn-danger">Delete</button></a>	
                <br/>
                    
                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
