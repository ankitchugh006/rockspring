@extends('layout.admin')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->

        <div class="row">
            <div class="col-lg-12">   
                <h1 class="page-header">
                    {{{$title}}}
                </h1>
            </div>

            <div class="col-lg-8 col-sm-6 col-xs-6">    
                <form id="sort_form" method="GET" action="">


                    <!--div class="col-lg-3">
                            
                                    <div class="form-group">
                                            <select id="sort_select" class="form-control sort_select" name="order_by">
                                                    <option value="created_at" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'created_at') {
                        echo 'selected';
                    }
                    ?>>-Sort By Date-</option>
                                                    <option value="name" <?php
                    if (isset($_GET['order_by']) && $_GET['order_by'] == 'name') {
                        echo 'selected';
                    }
                    ?>>-Sort By Name-</option>
                                            </select>
                                    </div>
                            
                    </div-->
                    <div class="row">
                        <div class="col-lg-6 col-xs-8">
                            <div class="form-group"  > 					
                                <input  class="form-control s_filter customer_autocomplete" name="name" placeholder="Enter Name" value="<?php
                                if (isset($_GET['name'])) {
                                    echo $_GET['name'];
                                }
                                ?>">					
                            </div> 

                        </div>	
                        <div class="col-xs-2">
                            <input  type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-lg-4" style="text-align:right">
                <a href="<?php echo url() ?>/admin/customers/add" ><button class="btn btn-ls btn-primary" type="button"><i class="fa fa-plus"></i> Add New Customer</button></a>
            </div>


        </div>
        <!-- /.row -->


        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{{$title}}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesort">
                                <thead>
                                    <tr>
                                        <!--th>#</th-->
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <!--th>Email</th-->
                                        <th>Phone</th>

                                        <!--th>Action</th-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (!empty($customers)) {
                                        foreach ($customers as $u) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <!--td><?php echo $i; ?></td-->
                                                <td><a href="<?php echo url() ?>/admin/customers/edit/<?php echo $u->id; ?>"><?php echo ucfirst($u->customer) ?></a></td>
                                                <td><?php echo ucfirst($u->address) ?></td>
                                                <!--td><?php echo ucfirst($u->email) ?></td-->
                                                <td><?php echo ucfirst($u->phone) ?></td>

                                                <!--td>
                                                    <a  href="<?php echo url() ?>/admin/customers/edit/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-primary">Edit</button></a>	

                                                    <a onclick="return confirm('Are you sure you want to delete this customer?')" href="<?php echo url() ?>/admin/customers/delete/<?php echo $u->id ?>"><button type="button" class="btn btn-xs btn-danger">Delete</button></a>									
                                                </td-->
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="ts-pager form-horizontal">
                                            <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                            <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                            <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                            <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                            <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>

                                            <select class="pagenum input-mini" title="Select page number"></select>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
@stop
