<!DOCTYPE html>
<html>
    <head>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">


<link href="<?php echo url(); ?>/css/main.css" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="http://bootsnipp.com/dist/scripts.min.js"></script>
<script src="http://d1n0x3qji82z53.cloudfront.net/src-min-noconflict/ace.js"></script>
<style>
 #non-logged-in .panel-heading{
	 background-color: #0d3558;
	border-color: #dddddd;
	color: #fff;
	font-weight: bold;
 }
 #non-logged-in .btn-primary {
	  background-color: #337ab7;
	  border-color: #2e6da4;
	  color: #ffffff;
}
#non-logged-in .btn-primary:hover{
	background-color: #286090;
	border-color: #204d74;
	color: #ffffff;
}
</style>

	<script>
	$(document).ready(function(){
		$("input[type=password]").keyup(function(){
			var ucase = new RegExp("[A-Z]+");
			var lcase = new RegExp("[a-z]+");
			var num = new RegExp("[0-9]+");

			if($("#password1").val() == $("#password2").val()){
				$("#pwmatch").removeClass("glyphicon-remove");
				$("#pwmatch").addClass("glyphicon-ok");
				$("#pwmatch").css("color","#00A41E");
			}else{
				$("#pwmatch").removeClass("glyphicon-ok");
				$("#pwmatch").addClass("glyphicon-remove");
				$("#pwmatch").css("color","#FF0004");
			}
		});
	});
	</script>
	
	<script>
	function check(input) {
		if (input.value != document.getElementById('password1').value) {
			input.setCustomValidity('Password not matching.');
		} else {
			// input is valid -- reset the error message
			input.setCustomValidity('');
		}
	}
	</script>
	</head> 

<body>


<nav class="navbar navbar-default center">
	<div class="container">
		<div class="navbar-header col-md-4 col-md-offset-4" style=" float: none;margin: 0 auto !important;width: 400px;">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ url() }}">
				<img src="{{ url('/images/logo-white.png') }}"  height="36" />
			</a>
		</div>

 
	</div>
</nav>	

<div class="fullwidth" id="non-logged-in">

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				 @if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					 @if(Session::has('error'))
								<div class="alert alert-danger">
								  <ul><li>{{ Session::get('error') }}</li></ul>
								</div>
					 @endif
					 @if(Session::has('success'))
								<div class="alert alert-info">
								  <ul><li>{{ Session::get('success') }}</li></ul>
								</div>
					 @endif
				<div class="panel panel-default">
					<div class="panel-heading">Change Password<i class="fa fa-compass pull-right"></i></div>
					<div class="panel-body">
						<form method="post" id="passwordForm" action="<?php echo url() ?>/changepassword" class="form-horizontal">
							<div class="form-group">
								<div class="col-md-12">
									<input type="password" class="form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off" required="">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="password" class="form-control" oninput="check(this)" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off" required="">
								</div>
							</div>
							
							<div class="form-group">
									<div class="col-md-12">
										<span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Matched
									</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<button style="margin-right: 15px;" class="btn btn-primary" type="submit">
										Submit
									</button>
									<a href="<?php echo url() ?>">Back to Login </a>

								</div>
							</div>
							<input type="hidden" name="key" value="D4788EBA535DC57377BAD975DED90" />
							<input type="hidden" name="code" value="<?php echo $code; ?>" />
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
	
<!--div class="row">
	<div class="col-sm-12">
		<center><h1>Change Password</h1></center>
	</div>
</div>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<p class="text-center">Use the form below to change your password.</p>
		 @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		 @if(Session::has('error'))
					<div class="alert alert-danger">
					  <ul><li>{{ Session::get('error') }}</li></ul>
					</div>
		 @endif
		 @if(Session::has('success'))
					<div class="alert alert-info">
					  <ul><li>{{ Session::get('success') }}</li></ul>
					</div>
		 @endif
		<form method="post" id="passwordForm" action="<?php echo url() ?>/changepassword" class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off" required="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<input type="password" class="input-lg form-control" oninput="check(this)" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off" required="">
				</div>
			</div>
			
			<div class="form-group">
					<div class="col-md-12">
						<span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Matched
					</div>
			</div>
			
			<input type="hidden" name="key" value="D4788EBA535DC57377BAD975DED90" />
			<input type="hidden" name="code" value="<?php echo $code; ?>" />
			<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
		</form>
	</div>
</div--><!--/row-->

</body></html>
