<?php
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorWorklog;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Manpower Requests Report</title>
    <style type="text/css">
		
    </style>
  </head>
	<body>

		<?php 

		$mi=0;
		
		if(count($todayrequests) > 0){
		foreach($todayrequests as $req){
			$mi++;
			$projectrequests	= 	ForemanRequest::where('request_id','=',$req->id)->get();
			
		
			?>

	<table cellpadding="0" cellspacing="0"  style="width: 700px;margin-bottom:40px;border:1px solid #c6c6c6;">
		
		<tr style="width:700px;">
			<td style="width:700px;background:#00214C; color:#fff;padding:7px 0;font-family: arial,sans-serif;">
				<table style="width:700px;">
					<tr style="width:700px;">
						<td style="padding:7px;vertical-align: top;">
							<img src="<?php  echo url() ?>/images/rs-logo1.png" alt="" />
						</td>

						<td style="text-align:right; padding:0 7px;">
							<h2 style="margin:7px 0; font-size:30px;line-height:0.8em;">
								<?php echo get_project_title($req->project_id); ?>
							</h2>
							
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	
		<tr style="width:700px;">
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width: 700px; margin: 0;font-family: arial,sans-serif;">
					
				  <tr style="width:700px;line-height: 1.4em; color:#fff; background:#000;">
					<td style="padding: 10px 8px;text-align: left;">
						<b>Date </b><?php echo date('m/d/y') ?>
					</td>
					<td style="padding: 6px;text-align: center;">
						<b>Job# </b><?php echo $req->project_id; ?>
					</td>
				
				  </tr>
				</table>
			</td>
		</tr>
		
			<tr style="width:700px;">
				<td style="padding-top:10px;text-align:center;  width:700px; float:left; color:#00214C;font-size:20px; font-weight:bold;text-transform: capitalize;">
						Detail
					<hr style="width:700px; float:left;">

				</td>
			</tr>
			<tr style="width:700px;">
			
				<td>
					<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 20px 0 0;font-family: arial,sans-serif;">
						
						
						<thead style="border:1px solid #000;">
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;background:#EBEBEB;text-transform: capitalize;">
							    
							    <th style="padding: 10px 8px;;text-align:left; border-color: #c6c6c6 transparent #c6c6c6 #c6c6c6;border-style: solid none solid solid;border-width: 2px 2px 2px 2px;width:10%">
							    	Requested By
							    </th>
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Resources
							    </th>
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Workers
							    </th>
							     <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Requested On
							    </th>
							     <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Processing For
							    </th>
							     <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Action
							    </th>
							</tr>
						</thead>
						<tbody>
					  
					   <?php
					   $workers=0;
						if (count($projectrequests) > 0) {
							foreach ($projectrequests as $u) {
								
								?>
								
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
							    <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	<?php echo get_name($u->foreman_id) ?>
							    </td>
								
								 <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	<?php echo get_resource_title($u->resource_id) ?>
							    </td>
							     <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	<?php echo ($u->quantity) ?>
							    </td>
							     <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	 <?php echo change_date_format($u->created_at) ?>
							    </td>
							    <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	 <?php echo change_date_format($u->processing_date) ?>
							    </td>
							     <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	 <a href="http://foremanfeed.rockspringcontracting.com/admin/projects/requests/<?php echo $req->id ?>">View Detail</a>
							    </td>
							</tr>

							
								<?php
							}
						?>
							
						<?php } else {
							?>
							<tr><td style="padding: 10px 8px;border: 1px solid #c6c6c6;" colspan="6" align="center">No requests found for this project</td></tr>
						<?php } ?>
						</tbody>
					</table>
				
				</td>
			
			</tr>
		
		
		</table>
		
		<!------------------------break------------------------->
			<?php if(count($todayrequests) != $mi) { ?>
				<div class="page-break" style="page-break-after: always;"></div>
			<?php } 
			
			} ?>
		
		<?php }else{
				echo '<p align="center">No Project Requests</p>';
			} ?>

	



		

		

	</body>
</html>
