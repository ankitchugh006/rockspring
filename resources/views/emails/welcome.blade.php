<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h1>Hi, {{ $name }}!</h1>
		<p>We'd like to personally welcome you to the Rockspring. Thank you for registering!</p>
	</body>
</html>
