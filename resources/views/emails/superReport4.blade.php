<!DOCTYPE html>
<html>
<head>
	 <title>Report</title>
</head>
<?php
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorWorklog;

		
$mi=0;


$one_days_ago = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
$mi++;

$id=$project->id;

$projectrequests	= 			ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
$projectlogs		=			Worklog::where('project_id','=',$id)
								->whereDate('logged_at','=',$submitted_for)
								->selectRaw('*')
								//->groupBy('resource_id')
								//->orderBy('resource_code', 'ASC')
								->get();
	
$surveys        	=			Survey::join('project_surveys','project_surveys.id','=','daily_survey.survey_id')
										->where('daily_survey.project_id','=',$id)
										->whereDate('project_surveys.survey_date','=',$submitted_for)
										->orderBy('daily_survey.id', 'ASC')->get();

$tasks      		=			Task::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
$files				=			File::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();	

$prepared_by = ProjectForeman::where('project_id',$project->id)->where('type','=','foreman')->first();

$prepared_by_name=get_name($prepared_by->foreman_id);
$prepared_by_number=get_number($prepared_by->foreman_id);


if(count($projectlogs) > 0 || count($surveys) > 0) { ?>


	<body style="margin: 0; padding: 0;">

		<!-- Table -->
			<table cellpadding="10" style="font-family: 'Arial'; margin: 0 auto; float: none; border-collapse: collapse;border: 1px solid #BEC4CA;" width="670px"> 

				<!-- Haeder Section -->
					<tr style="border: 2px solid #fff;">
						<td colspan="3" style="border-collapse: collapse;text-align:center"><h2 style="margin: 0; padding: 0;">Daily Report</h2></td>
						
					</tr>
					<tr style="background: #00214C; color: #fff; border: 1px solid #00214C;">
						
						<td style="width: 205px; border-collapse: collapse">
							<img style="width: 110%; margin-top: 0px;" src="<?php  echo url() ?>/images/rs-logo1.png""></td><!-- Logo-->
						<td style="border-collapse: collapse;"></td>
						<td style="text-align: right; width: 215px; border-collapse: collapse;">
							<h1 style="margin: 0; padding:0; "><?php echo $project->title ?></h1>
							<p style="margin: 5px 0; border-collapse: collapse;"><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></p>
							
						</td>	
					</tr>
					<tr style="background: #00214C; color: #fff; border: 1px solid #00214C;">
						<td>
							<p style="margin: 5px 0; border-collapse: collapse;"><?php echo 'Foreman: '.$prepared_by_name ?></p>	
						</td>					
						<td>
							<p style="margin: 5px 0; text-align: center; border-collapse: collapse;"><?php echo 'Submitted on: '.date('m/d/Y',strtotime("-1 days")); ?></p>	
						</td>
						<td>
							<p style="margin: 5px 0; text-align: right; border-collapse: collapse;">Submitted for: <?php echo !empty($submitted_for)?date('m/d/Y',strtotime($submitted_for)):"N/A" ?></p>	
						</td>
					</tr>
				<!-- End of Haeder Section -->

				<!-- Work Log Section -->
					<tr>
						<td  style="border-bottom: 2px solid #00214C;"></td>
						<td style="color:#00214C; border-bottom: 2px solid #00214C;text-align: center;">
							<h2 style="margin:0; padding: 0;">Work Log</h2>
						</td>
						<td style=" border-bottom: 2px solid #00214C;   ;"></td>
					</tr>
					<tr>
						<th style="text-align: left; background: #EBEBEB; border-bottom: 2px solid #BEC4CA;">
							Manpower Type
						</th>
						<th style="background: #EBEBEB; border-bottom: 2px solid #BEC4CA;">Description</th>
						<th style="background: #EBEBEB; border-bottom: 2px solid #BEC4CA;">Workers</th>
					</tr>
					
					 <?php
						$workers=0;
						if (count($projectlogs) > 0) {
							foreach ($projectlogs as $u) { 
								$workers += $u->quantity ;
								?>
							<tr style="text-align: center;">
								<td style="text-align: left; border-bottom: 2px solid #BEC4CA;"><?php echo get_resource_title_report($u->resource_id); ?></td>
								<td style="border-bottom: 2px solid #BEC4CA;">{{ $u->description }}</td>
								<td style="border-bottom: 2px solid #BEC4CA;"><?php echo $u->quantity ?></td>
							</tr>
								<?php
							}
						?>
							<tr style="text-align: center;">
								<td style="text-align: left; border-bottom: 2px solid #BEC4CA;" colspan="2"><b>Total number of workers</b></td>
								<td style="border-bottom: 2px solid #BEC4CA;">
									<?php echo $workers; ?>
								</td>
							 </tr>
						<?php } else {
							?>
							<tr style="page-break-inside:avoid; page-break-after:auto;"><td style="padding: 10px 8px;border: 1px solid #c6c6c6;" colspan="4" align="center">No logs found for this project</td></tr>
						<?php } ?>
					
					
				<!-- End of Work Log Section -->

				<!-- Daily Survey Section -->
					<tr>
						<td  style="border-bottom: 2px solid #00214C; "></td>
						<td style="color:  #00214C;  border-bottom: 2px solid #00214C; text-align: center;">
							<h2 style="margin:0; padding: 0;">Daily Survey</h2>
						</td>
						<td style=" border-bottom: 2px solid #00214C;"></td>
					</tr>
					<tr>
						<th style="text-align: left; background: #EBEBEB; border-bottom: 2px solid #BEC4CA;   ">
							Survey Question
						</th>
						<th style="background: #EBEBEB; border-bottom: 2px solid #BEC4CA;">Yes/no</th>
						<th style="background: #EBEBEB; border-bottom: 2px solid #BEC4CA;">Description</th>
					</tr>
					
					 <?php
					    
						if (count($surveys) > 0) {
							foreach ($surveys as $u) {
								
								?>
							<tr style="text-align: center;">
							   <td style="text-align: left; border-bottom: 2px solid #BEC4CA;"><?php echo getQuestionTitle($u->question_id) ?></td>
							   <td style="border-bottom: 2px solid #BEC4CA;">
									<?php if (($u->status == '1')) {
											echo 'YES';
										} else if ($u->status == '0') {
											echo 'NO';
										}else{
											echo 'NO';
										} 
									?>
								</td>
							    <td style="border-bottom: 2px solid #BEC4CA;"><?php echo ($u->description) ?></td>
							</tr>
								<?php
							}
						?>
							
						<?php } else {
							
							$questions=getAllSurveyQuestions();
							if (count($questions) > 0) {
								foreach ($questions as $u) { ?>
								<tr style="text-align: center;">
									<td style="text-align: left; border-bottom: 2px solid #BEC4CA;"><?php echo $u->survey; ?></td>
									<td style="border-bottom: 2px solid #BEC4CA;">NO</td>
									<td style="border-bottom: 2px solid #BEC4CA;"><?php echo ($u->description) ?></td>
								</tr>
						
							<?php }
							}
						}
							?>
									
				<!-- End of Daily Survey Section -->

				<!-- Photos Section -->
					<tr>
						<td  style="border-bottom: 2px solid #00214C;  "></td>
						<td style="color:  #00214C;  border-bottom: 2px solid #00214C; text-align: center;">
							<h2 style="margin:0; padding: 0;">Photos</h2>
						</td>
						<td style=" border-bottom: 2px solid #00214C;"></td>
					</tr>
					<tr style="text-align: center;">
						<td style=" border-bottom: 2px solid #BEC4CA;" colspan="3">
						 <?php
							$i=0;
							if (count($surveys) > 0) {
								if(!empty($surveys[0]->main_images)){
									$main_images = explode(',',$surveys[0]->main_images);
									if(!empty($main_images)){
										foreach ($main_images as $u) {?>												
														<a target="_blank" href="<?php echo $u ?>"><img style="width: 120px; height: 120px; margin-bottom: 10px;" src="<?php echo $u; ?>" /></a>												

											<?php
										}
									}
								} 
							}
							?>	
						</td>
					</tr>	

				<!-- Photos Section -->
			</table>
		<!-- Table -->

	</body>
	<?php } ?>
</html>


