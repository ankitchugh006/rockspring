<?php
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorWorklog;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report</title>
    
	</head>
	<body>


	<table cellpadding="0" cellspacing="0"  style="width: 700px;border:1px solid #c6c6c6;">
		
			<tr style="width:700px;">
				<td style="width:700px;background:#00214C; color:#fff;padding:7px 0;font-family: arial,sans-serif;">
					<table style="width:700px;">
						<tr style="width:700px;">
							<td style="padding:7px;vertical-align: top;">
								<img src="<?php  echo url() ?>/images/rs-logo1.png" alt="" />
							</td>

							<td style="text-align:right; padding:0 7px;">
								<h2 style="margin:7px 0; font-size:30px;line-height:0.8em;">
									<?php echo $project->title ?>
								</h2>
								<h3 style="margin:7px 0;font-size:16px;font-weight:normal;color:#fff">
									<a style="color:#fff"><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></a>
								</h3>
							</td>
						</tr>
					</table>	
				</td>
			</tr>
		
			<tr style="width:700px;">
				<td>
					<table border="0" cellpadding="0" cellspacing="0" style="width: 700px; margin: 0;font-family: arial,sans-serif;">
						
					  <tr style="width:700px;line-height: 1.4em; color:#fff; background:#000;">
						<td style="padding: 10px 8px;text-align: left;">
							<b>Date </b><?php echo date('m/d/y') ?>
						</td>
						<td style="padding: 6px;text-align: center;">
							<b>Job# </b><?php echo $project->manual_id; ?>
						</td>
					
						
					  </tr>
					</table>
				</td>
			</tr>
		<tr style="width:700px;">
			<td style="padding-top:20px;text-align:center;  width:700px; float:left; color:#00214C;font-size:20px; font-weight:bold;text-transform: capitalize;">
					MANPOWER REQUEST SUMMARY
				<hr style="width:700px; float:left;">
			</td>
		</tr>
	</table>
		

	<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 20px 0 0;font-family: arial,sans-serif;">
		
				
				 <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
					Resource
				</th>
				 <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
					Processing Date
				</th>
				 <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
					Quantity
				</th>
			</tr>
		
	   <?php if (count($resources) > 0) {
			foreach ($resources as $u) {
				
				?>
				
			<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
		
				
				 <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
					 <?php echo get_resource_title($u['resource_id']) ?>
				</td>
				<td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
					 <?php echo change_date_format($u['processing_date']) ?>
				</td>
				<td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
					 <?php echo $u['quantity'] ?>
				</td>
			</tr>
			
			<?php } } ?>
	</table>
	
	
</body>
</html>
