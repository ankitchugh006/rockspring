<?php
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorWorklog;
use App\Models\SubcontractorRequest;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report</title>
    <style type="text/css">
		
    </style>
  </head>
	<body>

		<?php 

		$mi=0;
		if(!empty($projects)){

		foreach($projects as $project){

		/*$subs1=SubcontractorRequest::where('project_id',$project->id)
		->groupBy('manpower_requests_subcontractors.subcontractor_id')
		->select(\DB::raw('tbl_manpower_requests_subcontractors.subcontractor_id,tbl_manpower_requests_subcontractors.resource_id,SUM(tbl_manpower_requests_subcontractors.quantity) as quantity'))
		->get();*/
		$subs1=SubcontractorRequest::where('project_id',$project->id)
				->whereDate('created_at','=',date('Y-m-d')) 	
				->get();
		
		$prepared_by_name ="N/A"; 	
		$prepared_by_number ="N/A"; 	
		$prepared_by = ProjectForeman::where('project_id',$project->id)->where('type','=','foreman')->first();
		if(count($prepared_by)>0){
			$prepared_by_name=get_name($prepared_by->foreman_id);
			$prepared_by_number=get_number($prepared_by->foreman_id);
		}
		if(count($subs1)>0){
		?>
		
	<table cellpadding="0" cellspacing="0"  style="width: 700px;margin-bottom:40px;border:1px solid #c6c6c6;">
		
		<tr style="width:700px;">
			<td style="width:700px;background:#00214C; color:#fff;padding:7px 0;font-family: arial,sans-serif;">
				<table style="width:700px;">
					<tr style="width:700px;">
						<td style="padding:7px;vertical-align: top;">
							<img src="<?php  echo url() ?>/images/rs-logo1.png" alt="" />
						</td>

						<td style="text-align:right; padding:0 7px;">
							<h2 style="margin:7px 0; font-size:30px;line-height:0.8em;">
								<?php echo $project->title ?>
							</h2>
							<h3 style="margin:7px 0;font-size:16px;font-weight:normal;color:#fff">
								<a style="color:#fff"><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></a>
							</h3>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	
		<tr style="width:700px;">
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width: 700px; margin: 0;font-family: arial,sans-serif;">
					
				  <tr style="width:700px;line-height: 1.4em; color:#fff; background:#000;">
					<td style="padding: 10px 8px;text-align: left;">
						<b>Date: </b><?php echo date('m/d/y') ?>
					</td>
					<td style="padding: 6px;text-align: center;">
						<b>Job#: </b><?php echo $project->manual_id; ?>
					</td>
					<td style="padding: 6px;text-align: right;">
						<b>Foreman: </b><?php echo $prepared_by_name; ?><br/>
						<b>Phone: </b> <?php echo $prepared_by_number; ?><br/>
						
					</td>
				  </tr>
				</table>
			</td>
		</tr>


		
		
			
			<tr style="width:700px;">
			
				<td>
					<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 20px 0 0;font-family: arial,sans-serif;">
						
						
						<thead style="border:1px solid #000;">
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;background:#EBEBEB;text-transform: capitalize;">
							    <th style="padding: 10px 8px;;text-align:left; border-color: #c6c6c6 transparent #c6c6c6 #c6c6c6;border-style: solid none solid solid;border-width: 2px 2px 2px 2px;width:10%">
							    	Trade
							    </th>
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;width:70%">
							    	Sub
							    </th>
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;width:20%">
							    	Workers
							    </th>
							   
							    
							</tr>
						</thead>
						<tbody>
							<?php if(count($subs1)){	
									$subcount=0;
									foreach($subs1 as $s){
										$subcount++; ?>
								<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
									<td style="padding: 10px 8px;;text-align:left;border-bottom: 2px solid #c6c6c6;">
										<?php  echo get_cost_code($s->resource_id) ?>-<?php  echo get_resource_title($s->resource_id) ?>
									</td>
									 
									<td style="padding: 10px 8px;border-bottom: 2px solid #c6c6c6;">
										<?php  echo get_subcontractor_title($s->subcontractor_id) ?>
									</td>
									<td style="padding: 10px 8px;border-bottom: 2px solid #c6c6c6;">
										<?php  echo $s->quantity ?>
									</td>
							    
							    
							</tr>
						<?php }
						} else{ echo '<tr><td style="padding: 10px 8px;border-bottom: 2px solid #c6c6c6;" colspan="3" align="center">No Records Found</td></tr>';}?>
						</tbody>
					</table>
				
				</td>
			
			</tr>
		</table>
		
		<!------------------------break------------------------->
			<?php if(count($projects) != $mi) { ?>
				<div class="page-break" style="page-break-after: always;"></div>
			<?php } 
			
			} ?>
		
		<?php } 
		
		}?>

	



		

		

	</body>
</html>
