<?php
use App\Models\User;
use App\Models\Project;
use App\Models\Manpower;
use App\Models\ProjectResources;
use App\Models\ProjectForeman;
use App\Models\ProjectSubcontractors;
use App\Models\ForemanRequest;
use App\Models\ForemanRequestMain;
use App\Models\Worklog;
use App\Models\WorklogMain;
use App\Models\Survey;
use App\Models\Surveys;
use App\Models\ProjectSurvey;
use App\Models\Task;
use App\Models\File;
use App\Models\CustomerMember;
use App\Models\SubcontractorWorklog;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report</title>
    <style type="text/css">
		
    </style>
  </head>
	<body>
	<h2>Daily Report</h2>
		<?php 

		$mi=0;
		if(count($projects) > 0){

		foreach($projects as $project){

		$one_days_ago = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
		$mi++;

		$id=$project->id;

		$projectrequests	= 			ForemanRequestMain::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->get();
		$projectlogs		=			Worklog::where('project_id','=',$id)
										->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')
										->selectRaw('*')
										//->groupBy('resource_id')
										//->orderBy('resource_code', 'ASC')
										->get();
										
		$surveys        	=			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'ASC')->get();
		$surveys        	=			Survey::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'ASC')->get();
		$tasks      		=			Task::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();
		$files				=			File::where('project_id','=',$id)->whereRaw('Date(created_at) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))')->orderBy('id', 'DESC')->get();	
		
		$prepared_by = ProjectForeman::where('project_id',$project->id)->where('type','=','foreman')->first();
        
		$prepared_by_name=get_name($prepared_by->foreman_id);
		$prepared_by_number=get_number($prepared_by->foreman_id);
		
		
		if(count($projectlogs) > 0 || count($surveys) > 0) {

		?>

	<table cellpadding="0" cellspacing="0"  style="width: 700px;margin-bottom:40px;border:1px solid #c6c6c6;table-layout: fixed;">
		
		<tr style="width:700px;">
			<td style="width:700px;background:#00214C; color:#fff;padding:7px 0;font-family: arial,sans-serif;">
				<table style="width:700px;">
					<tr style="width:700px;">
						<td style="padding:7px;vertical-align: top;">
							<img src="<?php  echo url() ?>/images/rs-logo1.png" alt="" />
						</td>

						<td style="text-align:right; padding:0 7px;">
							<h2 style="margin:7px 0; font-size:30px;line-height:0.8em;color:#fff">
								<?php echo $project->title ?>
							</h2>
							<h3 style="margin:7px 0;font-size:16px;font-weight:normal;color:#fff">
								<a style="color:#fff"><?php echo!empty($project->description) ? $project->description : ''; ?>, <?php echo!empty($project->city) ? $project->city : ''; ?>, <?php echo!empty($project->state) ? $project->state : ''; ?> - <?php echo!empty($project->zip) ? $project->zip : ''; ?></a><br/>
							
							</h3>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	    <tr style="width:700px;">
			<td style="width:700px;background:#00214C; color:#fff;padding:7px 0;font-family: arial,sans-serif;">
				<table style="width:700px;">
					<tr style="width:700px;">
						<td style="text-align:left;padding:0 7px;">
							<?php echo 'Foreman: '.$prepared_by_name ?>
						</td>

						<td style="text-align:center; padding:0 7px;">
							<span style="display:block"><?php echo 'Submitted on: '.date('m/d/Y',strtotime("-1 days")); ?></span>
						</td>
						<td style="text-align:right; padding:0 7px;">
							<span style="display:block">Submitted for: <?php echo !empty($submitted_for)?date('m/d/Y',strtotime($submitted_for)):"N/A" ?></span>
						</td>
						
					</tr>
				</table>	
			</td>
		</tr>
	
		

		

		
			<tr style="width:700px;">
				<td style="padding-top:10px;text-align:center;  width:700px; float:left; color:#00214C;font-size:20px; font-weight:bold;text-transform: capitalize;">
						Work Log
					<hr style="width:700px; float:left;">
				</td>
			</tr>
			<tr style="width:700px;">
			
				<td style=" width: 100%; ">
					<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 20px 0 0;font-family: arial,sans-serif;table-layout:fixed">
						
						
						<thead style="border:1px solid #000;width: 100%;">
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;background:#EBEBEB;text-transform: capitalize;width: 100%;">
							    <th style="padding: 10px 8px;;text-align:left; border-color: #c6c6c6 transparent #c6c6c6 #c6c6c6;border-style: solid none solid solid;border-width: 2px 2px 2px 2px;">
							    	Manpower Type
							    </th>
							   
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;text-align:center">
							    	Description
							    </th>
							   
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Workers
							    </th>
							   
							</tr>
						</thead>
						<tbody>
					  
					   <?php
					    $workers=0;
						if (count($projectlogs) > 0) {
							foreach ($projectlogs as $u) { 
								$workers += $u->quantity ;
								?>
							<tr>
							    <td style="text-align:left;border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: left; float: left;display: block"><?php echo get_resource_title_report($u->resource_id); ?></span></td>
							    <td style="border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: center; display: block;">{{ $u->description }}</span></td>
							    <td style="border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: center; float: left;margin-left: 45%;"><?php echo $u->quantity ?></span></td>
							</tr>
								<?php
							}
						?>
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
							    <td style="padding: 10px 8px;;text-align:left;border-bottom: 2px solid #c6c6c6;" colspan="2"><b>Total number of workers</b></td>
							    <td style="padding: 10px 8px;;text-align:center;border-bottom: 2px solid #c6c6c6;">
							    	<?php echo $workers; ?>
							    </td>
							 </tr>
						<?php } else {
							?>
							<tr><td style="padding: 10px 8px;border: 1px solid #c6c6c6;" colspan="4" align="center">No logs found for this project</td></tr>
						<?php } ?>
						</tbody>
					</table>
				</td>
			</tr>
			
			<tr style="width:700px;">
				<td style="padding-top:10px;text-align:center;  width:700px; float:left; color:#00214C;font-size:20px; font-weight:bold;text-transform: capitalize;">
						Daily Survey
					<hr style="width:700px; float:left;">
				</td>
			</tr>
			<tr style="width:700px;">
			
				<td style=" width: 100%; ">
					<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 20px 0 0;font-family: arial,sans-serif;table-layout:fixed">
						
						
						<thead style="border:1px solid #000;width: 100%;">
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;background:#EBEBEB;text-transform: capitalize;width: 100%;">
							    <th style="padding: 10px 8px;;text-align:left; border-color: #c6c6c6 transparent #c6c6c6 #c6c6c6;border-style: solid none solid solid;border-width: 2px 2px 2px 2px;">
							    	Survey Question
							    </th>
							   
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;text-align:center">
							    	YES/NO
							    </th>
							   
							    <th style="padding: 10px 8px;border-color: #c6c6c6 transparent #c6c6c6 transparent;border-style: solid none solid none;border-width: 2px 2px 2px 2px;">
							    	Description
							    </th>
							   
							</tr>
						</thead>
						<tbody>
					  
					   <?php
					    
						if (count($surveys) > 0) {
							foreach ($surveys as $u) {
								
								?>
							<tr>
							    <td style="text-align:left;border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: left; float: left;display: block"><?php echo getQuestionTitle($u->question_id) ?></span></td>
							    <td style="border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: center; display: block;">
									<?php if (($u->status == '1')) {
											echo 'YES';
										} else if ($u->status == '0') {
											echo 'NO';
										}else{
											echo 'NO';
										} 
									?></span></td>
							    <td style="border-bottom: 2px solid #c6c6c6;"><span style="padding:10px 8px; text-align: center; display: block;margin-left: 7%;"><?php echo ($u->description) ?></span></td>
							</tr>
								<?php
							}
						?>
							
						<?php } else {
							
							$questions=getAllSurveyQuestions();
							if (count($questions) > 0) {
								foreach ($questions as $u) { ?>
								<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
									 <td style="text-align:left;border-bottom: 2px solid #c6c6c6;">
										 <span style="padding:10px 8px; text-align: left; float: left;display: block">
											<?php echo $u->survey; ?>
										</span>
									</td>
									<td style="border-bottom: 2px solid #c6c6c6;">
										<span style="padding:10px 8px; text-align: center; display: block;">
											NO
										</span>
									</td>
									<td style="border-bottom: 2px solid #c6c6c6;">
										<span style="padding:10px 8px; text-align: center;margin-left: 7%;display:block">
											<?php echo ($u->description) ?>
										</span>
									</td>
								</tr>
						
							<?php }
							}
						}
							?>
						</tbody>
					</table>
				</td>
			</tr>
			
			
		<?php //if (count($files) > 0) { ?>
			<tr style="width:700px;">
				<td style="padding-top:10px;text-align:center;  width:700px; float:left; color:#00214C;font-size:20px; font-weight:bold;text-transform: capitalize;">
						Photos
					<hr style="width:700px; float:left;">
				</td>
			</tr>
			<tr style="width:700px;">		
				<td>
					<table cellpadding="0" cellspacing="0" style="width: 700px;margin: 10px 0 0;font-family: arial,sans-serif;">
						<tbody>	
							<tr style="text-align: center; font-family: arial,sans-serif; line-height: 1.4em;">
							 <?php
								$i=0;
								if (count($surveys) > 0) {
									if(!empty($surveys[0]->main_images)){
										$main_images = explode(',',$surveys[0]->main_images);
										if(!empty($main_images)){
											foreach ($main_images as $u) {?>
												
													<td style="padding: 10px 8px;text-align:center">	
															<a target="_blank" href="<?php echo $u ?>"><img style="max-width:200px;" width="100" height="100" src="<?php echo $u; ?>" /></a>												
													</td>
												
												<?php
											}
										}
									} 
								}
								?>	
							</tr>				 
						</tbody>
					</table>			
				</td>
			
			</tr>
		<?php // } ?>
		</table>		
		<!------------------------break------------------------->
			<?php if(count($projects) != $mi) { ?>
				<div class="page-break" style="page-break-after: always;"></div>
			<?php } 
			
			} ?>
		<?php }
		} ?>
	</body>
</html>
